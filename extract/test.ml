open Itreeai
open Printf
open String

let rec repeat n s =
  if n = 0 then "" else s ^ repeat (n - 1) s

let show_binop: intvBinOp -> string = function
  | IntvAdd -> "+"
  | IntvSub -> "-"

let rec show_expr: expr -> string = function
  | Var v -> v
  | Lit l -> string_of_int l
  | UnOp e -> sprintf "--%s" (show_expr e)
  | BinOp (op, l, r) ->
      sprintf "(%s) %s (%s)" (show_expr l) (show_binop op) (show_expr r)

let rec show_stmt (s: stmt) (i: int): string =
  let ind = repeat (2*i) " " in
  match s with
  | Skip -> ind ^ "skip;\n"
  | Assign (x, e) -> ind ^ sprintf "%s := %s;\n" x (show_expr e)
  | Assert e -> ind ^ sprintf "assert(%s);\n" (show_expr e)
  | Seq (a, b) -> show_stmt a i ^ show_stmt b i
  | If (c, t, e) -> ind ^ "if " ^ show_expr c ^ " then\n" ^ show_stmt t (i+1) ^
                    ind ^ "else\n" ^ show_stmt e (i+1)
  | While (c, b) -> ind ^ "while " ^ show_expr c ^ "\n" ^ show_stmt b (i+1)

let show_zinf: zinf -> string = function
  | Fin z -> string_of_int z
  | Inf -> "inf"
let show_zinf_opp: zinf -> string = function
  | Fin z -> string_of_int (-z)
  | Inf -> "-inf"

let show_intv (i: intv): string =
  if i.low = Fin 0 && i.high = Fin (-1) then "bot" else
  sprintf "[%s:%s]" (show_zinf_opp i.low) (show_zinf i.high)
let show_intv_option: intv option -> string = function
  | None -> "None"
  | Some v -> "Some " ^ show_intv v

let show_amem: intv IMPMemory.MapLattice.coq_MapL -> string = function
  | IMPMemory.MapLattice.Bot -> "bot"
  | IMPMemory.MapLattice.TopExcept m ->
      let pair (name, v) = name ^ "=" ^ show_intv v in
      "TopExcept " ^ concat ", " (List.map pair m)

let show_units: _ -> string = function
  | Unit_UU02e3__bot -> "bot"
  | _ -> "ttˣ"

let print_program (s: stmt) =
  let str = show_stmt s 0 in
  List.iter (printf "| %s\n") (split_on_char '\n' (trim str))

let print_concrete_vars (vars: (string * _) list) rvars =
  List.iter (fun ((str1, i1), (str2, i2)) ->
    printf "  '%s' %d | '%s' %d\n" str1 i1 str2 i2
  ) (List.combine vars rvars)

let print_abstract_vars (vars: (string * _) list) =
  List.iter (fun (str1, c1) -> printf " %s=%s" str1 (show_intv c1)) vars

let print_concrete_test stmt expected_vars result_vars status =
  print_endline "=== Concrete test ===";
  print_endline "Program:";
  print_program stmt;
  print_endline "Variables (expected | result):";
  print_concrete_vars expected_vars result_vars;
  print_endline (if status then "\x1b[32;1mSuccess\x1b[0m"
                           else "\x1b[31;1mFailed\x1b[0m")

let print_abstract_test stmt expected_vars final_state status =
  let (mem, (err, ret)) = final_state in
  print_endline "=== Abstract test ===";
  print_endline "Program:";
  print_program stmt;
  printf "Expected:\n ";
  print_abstract_vars expected_vars;
  print_endline "\nFinal state:";
  printf "  ret = %s\n" (show_units ret);
  printf "  err = %s\n" (show_units err);
  printf "  mem = %s\n" (show_amem mem);
  print_endline (if status then "\x1b[32;1mSuccess\x1b[0m"
                           else "\x1b[31;1mFailed\x1b[0m")

let run_tests result test =
  match test with
  | TestConcrete (stmt, vars) ->
      let (rvars, b) = ccheck stmt vars in
      print_concrete_test stmt vars rvars b;
      b && result
  | TestAbstract (stmt, vars) ->
      let (final_state, b) = acheck stmt vars in
      print_abstract_test stmt vars final_state b;
      b && result

let _ =
  let b = List.fold_left run_tests true tests in
  exit (if b then 0 else 1)
