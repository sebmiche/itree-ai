 Set Warnings "-extraction-opaque-accessed".

(** * Tests for concrete and abstraction interpretation of IMP *)

(* Extraction settings *)
Require Coq.extraction.Extraction.
Extraction Language OCaml.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlNatInt.
Require Import ExtrOcamlNativeString.
Require Import ExtrOcamlZInt.

From Coq Require Import Lists.List Strings.String ZArith.
From ITree Require Import ITree.
From ITreeAI Require Import
  AflowMonad NumericalDomain MemoryDomain Lattice LatticeIntv LatticeOption
  examples.ImpArithFail.

Import ListNotations.
Import ImpNotations.
Local Open Scope expr_scope.
Local Open Scope list_scope.
Local Open Scope string_scope.

(* Implemented in OCaml - may not finish *)
Parameter exec: forall {E R}, itree E R -> R.
Extract Inlined Constant exec => "
  let rec _exec tree =
    match observe tree with
    | RetF v -> v
    | _ -> _exec (burn 1 tree) in
  _exec".

Definition concrete_exec (s: stmt): string -> Z :=
  let (env, tt) := exec (imp_eval s (IMPMemory.Map.empty _)) in
  fun var =>
    match mem_query env var with
    | Some n => n
    | None => Z.of_nat 0
    end.

Definition abstract_exec (s: stmt): IMPMemory.MapLattice.MapL Intv * (_ * _) :=
  exec (unfold (imp_evalˣ s IMPMemory.MapLattice.Bot)).

Definition ccheck prog vals: (_ * bool) :=
  let env := concrete_exec prog in
  (List.map (fun '(var, x) => (var, env var)) vals,
   List.forallb (fun '(var, x) => num_eq (env var) x) vals).
Definition acheck prog vals: (_ * bool) :=
  let env := abstract_exec prog in
  (env,
   List.forallb (fun '(var, x) =>
    match mem_query (fst env) var with
    | Some y => num_eq y x
    | _ => false
    end) vals).

Definition prog1 :=
  "x" ← 0;;; "y" ← "x"+1.

Definition prog2 :=
  "z" ← 2;;;
  IF "x" THEN "y" ← 2 ELSE "y" ← "z" FI.

Definition prog3 :=
  "x" ← 2;;;
  "y" ← 0;;;
  WHILE "x" DO ("y" ← 1;;; "x" ← "x"-1);;;
  "z" ← 5;;;
  ASSERT "y";;;
  "z" ← 6.

Definition prog4 :=
  "x" ← 0;;;
  "x" ← 2;;;
  "y" ← 3;;;
  "x" ← 4.

Inductive TestType :=
  | TestConcrete (prog: stmt) (vars: list (string * Z))
  | TestAbstract (prog: stmt) (vars: list (string * Intv)).

From ITreeAI Require Import XL_Intv.

Definition tests := [
  TestConcrete prog1 [("x",0); ("y",1); ("z",0)];
  TestAbstract prog2 [("y",Intervals.const 2);
                      ("z",Intervals.const 2)];
  TestConcrete prog3 [("x",0); ("y",1); ("z",6)];
  TestAbstract prog3 [("x",{| Intervals.low := Inf; Intervals.high := 2 |});
                      ("y",{| Intervals.low := 0; Intervals.high := 1 |});
                      ("z",{| Intervals.low := -5; Intervals.high := 6 |})];
  TestAbstract prog4 [("x",Intervals.const 4);
                      ("y",Intervals.const 3)]
].

(* Warning: Mind your working directory! *)
Extraction "itreeai" ccheck acheck burn tests.
