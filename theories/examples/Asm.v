(** * The Asm language  *)

(** We now consider as a target a simple control-flow-graph language, so-called
    _Asm_.  Computations are represented as a collection of basic blocks linked
    by jumps.  *)

(* begin hide *)
From Coq Require Import
     Strings.String
     Program.Basics
     ZArith.ZArith
     Morphisms
     Setoid
     RelationClasses
     DecidableType.

Require Import ExtLib.Structures.Monad.
From Equations Require Import Equations.

From ITree Require Import
     ITree
     ITreeFacts
     ITreeMonad
     Eq.Eqit
     Basics.CategorySub
     Events.State
     Events.StateFacts.

From ITreeAI.Lattices Require Import
  Lattice LatticeConst LatticeVec LatticeVoid.
From ITreeAI Require Import
  AflowMonad AbstractMonad FiniteMemory
  NumericalDomain Events EventHierarchy Soundness Util SurfaceDSL.
From ITreeAI.Combinators Require Import Seq TailMRec.

Import Monads.
Import ListNotations.
Import MonadNotation.
Open Scope monad_scope.
Open Scope bool_scope.
(* end hide *)

(** ** Syntax *)

(** We define a countable set of memory addresses, represented as [string]s: *)
Definition addr : Set := string.

(** We define a set of register names (for this simple example, we identify them
with [nat]). *)
Definition reg : Set := nat.

(** For simplicity, _Asm_ manipulates [nat]s as values too. *)
Definition value : Set := nat.

Notation label := nat (only parsing).

(** We consider constants and variables as operands. *)
Variant operand : Set :=
| Oimm (_ : value)
| Oreg (_ : reg).

(** The instruction set covers moves and arithmetic operations, as well as load
    and stores to the heap.  *)
Variant instr : Set :=
| Imov   (dest : reg) (src : operand)
| Iadd   (dest : reg) (src : reg) (o : operand)
| Isub   (dest : reg) (src : reg) (o : operand)
| Imul   (dest : reg) (src : reg) (o : operand)
| IEq    (dest : reg) (src : reg) (o : operand)
| ILe    (dest : reg) (src : reg) (o : operand)
| IAnd   (dest : reg) (src : reg) (o : operand)
| INot   (dest : reg) (o : operand)
| Iload  (dest : reg) (addr : addr)
| Istore (addr : addr) (val : operand).

(** We consider both direct and conditional jumps *)
Variant branch : Type :=
| Bjmp (_ : label)                (* jump to label *)
| Bbrz (_ : reg) (yes no : label) (* conditional jump *)
| Bhalt (* take a register as argument? *)
.

(** A basic [block] is a sequence of straightline instructions followed by a
    branch that either halts the execution, or transfers control to another
    [block]. *)
Inductive block : Type :=
| bbi (_ : instr) (_ : block)
| bbb (_ : branch).

Definition asm : Type := list block.

(** The dynamic result of blocks is summarized in [blockres]. It's similar to
    [branch] but it's an internal interpreter type not a piece of syntax. For
    simplicity we use the same type for the concrete and abstract sides. *)
Inductive blockres {value: Type}: Type :=
| BRjmp (_: label)
| BRbrz (_: value) (zero nonzero: label)
| BRhalt
| BRtop.
Arguments blockres: clear implicits.

Definition blockres_succ {T} (b: blockres T): list label :=
  match b with
  | BRjmp l => [l]
  | BRbrz _ l₁ l₂ => [l₁; l₂]
  | BRhalt => []
  | BRtop => []
  end.

Fact eqb_rw: forall x y, (x =? y) = true <-> x = y.
Proof.
 intros *. assert (H := Nat.eqb_spec x y). split.
  - intros E. rewrite E in H. now inversion H.
  - intros ->. apply Nat.eqb_refl.
Qed.

Fact Fin_eq_refl {N} (i: Fin.t N) {T} (x y: T):
  (if EquivDec.equiv_dec i i then x else y) = x.
Proof.
  unfold EquivDec.equiv_dec, EqDec_Fin. now case (Fin.eq_dec i i).
Qed.

#[global, refine]
Instance LatticeBase_blockres {T: Typeˣ}: LatticeBase (blockres T) := {
  lble br1 br2 :=
    match br1, br2 with
    | BRjmp l1, BRjmp l2 => l1 =? l2
    | BRbrz v1 z1 n1, BRbrz v2 z2 n2 => (v1 ⊆? v2) && (z1 =? z2) && (n1 =? n2)
    | BRhalt, BRhalt => true
    | _, BRtop => true
    | _, _ => false
    end;
  bot := BRhalt;
  top := BRtop;
  join br1 br2 :=
    match br1, br2 with
    | BRjmp l1, BRjmp l2 =>
        if l1 =? l2 then br2 else BRtop
    | BRbrz v1 z1 n1, BRbrz v2 z2 n2 =>
        if (z1 =? z2) && (n1 =? n2) then BRbrz (v1 ⊔ v2) z2 n2 else BRtop
    | BRhalt, BRhalt => BRhalt
    | _, _ => BRtop
    end;
  meet br1 br2 :=
    match br1, br2 with
    | BRjmp l1, BRjmp l2 =>
        if l1 =? l2 then br2 else BRhalt
    | BRbrz v1 z1 n1, BRbrz v2 z2 n2 =>
        if (z1 =? z2) && (n1 =? n2) then BRbrz (v1 ⊓ v2) z2 n2 else BRhalt
    | BRtop, br2 => br2
    | br1, BRtop => br1
    | _, _ => BRhalt
    end;
}.
Proof.
  - intros []; cbn; now rewrite ? Nat.eqb_refl, ? lle_refl.
  - intros [] [] []; cbn; intuition;
      rewrite ? Bool.andb_true_iff, ! eqb_rw in *; subst; try easy.
    destruct H as [[? <-] <-], H0 as [[? <-] <-]. intuition.
    eapply lle_trans; eauto.
  - intros [] []; cbn; auto.
    * case_eq (n =? n0); auto.
    * case_eq (zero =? zero0); case_eq (nonzero =? nonzero0); cbn; intuition.
      now rewrite H, H0, join_l.
  - intros [] []; auto.
    * case (n =? n0); auto; apply Nat.eqb_refl.
    * case (zero =? zero0); case (nonzero =? nonzero0); cbn; intuition.
      now rewrite join_r, ! Nat.eqb_refl.
Defined.

#[global, refine]
Instance Lattice_blockres {T: Typeˣ}: Lattice (blockres T) := {
  widen br1 br2 :=
    match br1, br2 with
    | BRjmp l1, BRjmp l2 =>
        if l1 =? l2 then br2 else BRtop
    | BRbrz v1 z1 n1, BRbrz v2 z2 n2 =>
        if (z1 =? z2) && (n1 =? n2) then BRbrz (widen v1 v2) z2 n2 else BRtop
    | BRhalt, BRhalt => BRhalt
    | _, _ => BRtop
    end;
  measure_N := S (measure_N T);
  measure br :=
    match br with
    | BRtop => (0, Natvec.natvec_zero _)
    | BRbrz v _ _ => (1, measure v)
    | _ => (1, Natvec.natvec_zero _)
    end;
}.
Proof.
  - intros [] []; cbn; auto.
    * case_eq (n =? n0); auto.
    * case_eq (zero =? zero0); case_eq (nonzero =? nonzero0); cbn; intuition.
      now rewrite H, H0, widen_l.
  - intros [] []; cbn; auto.
    * case (n =? n0); cbn; auto. apply Nat.eqb_refl.
    * case (zero =? zero0); case (nonzero =? nonzero0); cbn; intuition.
      now rewrite widen_r, ! Nat.eqb_refl.
  - intros []; cbn; auto. intuition apply Natvec.natvec_le_Reflexive.
  - intros [] [] [=]; intuition; try now (constructor 1; auto).
    constructor 2; split; [easy|]. apply measure_sound_le.
    rewrite ! Bool.andb_true_iff in H0; apply H0.
  - intros [] z [[] [Heq H]]; rewrite Heq in *; cbn in *; intuition.
    * case_eq (n =? n0); auto. rewrite Nat.eqb_sym in H. contradiction.
    * destruct (zero =? zero0) eqn:Hzero, (nonzero =? nonzero0) eqn:Hnonzero;
        cbn in *; intuition.
      right; split; [easy|]. apply measure_sound_lt.
      exists t0; split; [easy|].
      rewrite Nat.eqb_sym in Hzero, Hnonzero.
      intros Ht; rewrite Ht, Hzero, Hnonzero in H. now apply H.
Defined.

#[global, refine]
Instance GaloisConnection_blockres_blockres {T: Type} {Tˣ: Typeˣ}
    {GC_Tˣ_T: GaloisConnection Tˣ T}:
    GaloisConnection (blockres Tˣ) (blockres T) := {
  galois_in br brˣ :=
    match br, brˣ with
    | BRjmp l₁, BRjmp l₂ => l₁ = l₂
    | BRbrz t z₁ n₁, BRbrz tˣ z₂ n₂ => galois_in t tˣ /\ z₁ = z₂ /\ n₁ = n₂
    | BRhalt, BRhalt => True
    | _, BRtop => True
    | _, _ => False
    end;
}.
Proof.
  - intros [] [] [] [=]; auto.
    * rewrite eqb_rw in H0; now subst.
    * rewrite ! Bool.andb_true_iff in H0. rewrite ? eqb_rw in *.
      intuition (subst; try easy). eapply galois_lble; eauto.
  - intros [] [] []; cbn; auto; try contradiction.
    * intros -> <-. now rewrite Nat.eqb_refl.
    * intros (? & -> & ->) (? & <- & <-). rewrite ! Nat.eqb_refl; cbn.
      intuition; now apply galois_meet.
  - now intros [].
Defined.

(* ========================================================================== *)
(** ** Semantics *)

Variant binop := Add | Sub | Mul | Eq | Le | And.
Variant unop := Not.

(* Concrete numerical domain *)
#[local] Instance ASM_NumDomain: NumDomain value unop binop := {
  num_nat := id;
  num_eq := Nat.eqb;
  num_istrue v := negb (v =? 0)%nat;
  num_isfalse v := (v =? 0)%nat;

  num_unary op v :=
    match op with
    | Not => if Nat.eqb v 0 then 1 else 0
    end;

  num_binary op v1 v2 :=
    match op with
    | Add => v1 + v2
    | Sub => v1 - v2
    | Mul => v1 * v2
    | Eq  => if Nat.eqb v1 v2 then 1 else 0
    | Le  => if Nat.leb v1 v2 then 1 else 0
    | And => match v1, v2 with | 0,_ | _,0 => 0 | _,_ => 1 end
    end
}.

Module ASMRegIntf <: MemoryIntf.
  Module AddrDecidable := Util.NatDecidable.
  Definition Value := value.
  Definition uninitialized := 0.
  Definition Valueˣ: Typeˣ := Const Value.
  #[global] Instance GC_Value: Valueˣ <#> Value := _.
End ASMRegIntf.

Module ASMMemIntf <: MemoryIntf.
  Module AddrDecidable := Util.StringDecidable.
  Definition Value := value.
  Definition uninitialized := 0.
  Definition Valueˣ: Typeˣ := Const Value.
  #[global] Instance GC_Value: Valueˣ <#> Value := _.
End ASMMemIntf.

Module ASMRegs := FiniteMemory ASMRegIntf.
Module ASMMemory := FiniteMemory ASMMemIntf.

Existing Instance ASMRegs.memE_EventRelation.
Existing Instance ASMMemory.memE_EventRelation.

Notation valueˣ := (Const value: Typeˣ).
Definition E  := @memE reg value unit
              +' @memE addr value unit.
Definition Eˣ := @memE reg valueˣ unitˣ
              +' @memE addr valueˣ unitˣ.

Definition blockres_to_tailmrecᵒ_ret {N T} `{NumDomain T}
    (br: blockres T): Fin.t N * unit + unit :=
  match br with
  | BRjmp label =>
      match lt_dec label N with
      | left H => inl (Fin.of_nat_lt H, tt)
      | right _ => inr tt
      end
  | BRbrz value lyes lno =>
      match lt_dec (if num_istrue value then lyes else lno) N with
      | left H => inl (Fin.of_nat_lt H, tt)
      | right _ => inr tt
      end
  (* BRTop isnused in the concrete case *)
  | BRhalt | BRtop =>
      inr tt
  end.

Definition blockres_to_TailBranch {N}
    (br: blockres (Const value)): TailBranch N * unitˣ * unitˣ :=
  match br with
  | BRjmp lab =>
      match lt_dec lab N with
      | left H => (TailBranch_mk_branch [Fin.of_nat_lt H], ttˣ, ttˣ)
      | right _ => (TailBranch_mk_ret, ttˣ, ttˣ)
      end
  | BRbrz value lab_yes lab_no =>
      (* value is currently ignored *)
      match lt_dec lab_yes N, lt_dec lab_no N with
      | left Hy, left Hn =>
        (TailBranch_mk_branch [Fin.of_nat_lt Hy; Fin.of_nat_lt Hn], ttˣ, ttˣ)
      | _, _ => (top, ttˣ, ttˣ)
      end
  (* BRTop is unused in practice *)
  | BRhalt => (TailBranch_mk_ret, ttˣ, ttˣ)
  | BRtop => (top, ttˣ, ttˣ)
  end.

Import surface.
Open Scope surface.

Definition frontend_operand {b} (o: operand): SurfaceAST E Eˣ value valueˣ b :=
  match o with
    | Oimm v => ret (num_nat v) and (num_nat v)
    | Oreg r => do (ReadAddr r) and (ReadAddr r)
  end.

Definition getreg {b} (r : reg) : SurfaceAST E Eˣ value valueˣ b :=
  do ReadAddr r and ReadAddr r.
Definition setreg {b} (r : reg) v : SurfaceAST E Eˣ unit unitˣ b :=
  do WriteAddr r and WriteAddr r on v.
Definition setreg_op {b} (o : unop) (r : reg) (v : csum value valueˣ b)
 : SurfaceAST E Eˣ unit unitˣ b :=
  setreg r (bimap_csum v (num_unary o) (num_unary o)).
Definition setreg_bop {b} (o : binop) (r : reg) (lv rv : csum value valueˣ b)
 : SurfaceAST E Eˣ unit unitˣ b :=
  setreg r (bimap2_csum lv rv (num_binary o) (num_binary o)).
Definition load {b} (a : addr) : SurfaceAST E Eˣ value valueˣ b :=
  do ReadAddr a and ReadAddr a.
Definition store {b} (a : addr) v : SurfaceAST E Eˣ unit unitˣ b :=
  do WriteAddr a and WriteAddr a on v.  

Definition frontend_instr {b} (i: instr): SurfaceAST E Eˣ unit unitˣ b :=
  match i with
  | Imov d s =>
    v <- frontend_operand s;;
    setreg d v
  | Iadd d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop Add d lv rv
  | Isub d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop Sub d lv rv
  | Imul d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop Mul d lv rv
  | IEq d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop Eq d lv rv
  | ILe d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop Le d lv rv
  | INot d r =>
    lv <- frontend_operand r;;
    setreg_op Not d lv
  | IAnd d l r =>
    lv <- getreg l;;
    rv <- frontend_operand r;;
    setreg_bop And d lv rv
  | Iload d addr =>
    val <- load addr;;
    setreg d val
  | Istore addr v =>
    val <- frontend_operand v;;
    store addr val
  end.

Fixpoint frontend_block {b} {N} (bk : block) :
    SurfaceAST E Eˣ (Fin.t N * unit + unit) (TailBranch N * unitˣ * unitˣ) b :=
  match bk with
  | bbi i b' => frontend_instr i;; frontend_block b'
  | bbb br =>
      match br with
      | Bjmp l => ret (blockres_to_tailmrecᵒ_ret (BRjmp l))
                  and (blockres_to_TailBranch (BRjmp l))
      | Bbrz r zero nonzero =>
          v <- getreg r;;
          ret (fun v => blockres_to_tailmrecᵒ_ret (BRbrz v zero nonzero))
          and (fun v => blockres_to_TailBranch (BRbrz v zero nonzero)) on v
      | Bhalt => ret (blockres_to_tailmrecᵒ_ret BRhalt)
                 and (blockres_to_TailBranch BRhalt)
      end
  end.

Equations frontend_asm {b} (s: asm): SurfaceAST E Eˣ unit unitˣ b :=
  frontend_asm [] := ret tt and ttˣ;
  frontend_asm (i::asm) :=
    AST_CFG (length asm)
            (vec_map (fun b _ => frontend_block b) (vec_of_list (i::asm)))
            Fin.F1 (mksum tt ttˣ).

Arguments getreg: simpl never.
Arguments setreg: simpl never.
Arguments load: simpl never.
Arguments store: simpl never.

Declare Scope instr_scope.
Declare Scope asm_scope.
Delimit Scope instr_scope with instr.
Delimit Scope asm_scope with asm.
Bind Scope instr_scope with instr.
Bind Scope asm_scope with asm.

Notation "⟦ e ⟧"  := (frontend_instr (b := false) e) (format "⟦ e ⟧"):
  instr_scope.
Notation "⟦ c ⟧"  := (frontend_asm   (b := false) c) (format "⟦ c ⟧"):
  asm_scope.
Notation "⟦ e ⟧#" := (frontend_instr (b := true) e) (format "⟦ e ⟧#"):
  instr_scope.
Notation "⟦ c ⟧#" := (frontend_asm   (b := true) c) (format "⟦ c ⟧#"):
  asm_scope.

Open Scope asm_scope.

(* ========================================================================== *)

Definition con_interp {R} (t: itree E R) :=
  hoist
    (interp_state ASMMemory.handle_mem)
    (interp_state (case_stateC ASMRegs.handle_mem) t).

Definition abs_interp {R} (p: aflow Eˣ R) :=
  hoist
    (aflow_interp_state ASMMemory.handle_amem)
    (aflow_interp_state (case_stateA ASMRegs.handle_amem) p).

Definition concrete_eval (s: asm):
  itree _ (ASMMemory.Memory * (ASMRegs.Memory * unit)) :=
  con_interp (ast2itree ⟦s⟧) (ASMRegs.Map.empty _) (ASMMemory.Map.empty _).

Definition abstract_eval (s: asm):
  aflow _ (ASMMemory.Memoryˣ * (ASMRegs.Memoryˣ * unitˣ)) :=
  abs_interp (ast2aflow ⟦s⟧#) top top.

(* --- *)

Opaque num_unary num_binary.

Proposition operand_sound (o: operand):
  SoundAST (frontend_operand o) (frontend_operand o).
Proof.
  destruct o.
  - now apply SoundAST_Ret.
  - now unshelve eapply SoundAST_Event.
Qed.

Proposition setreg_op_sound (o: unop) (r: reg) v vˣ:
  galois_in v vˣ ->
  SoundAST (setreg_op o r v) (setreg_op o r vˣ).
Proof.
  intros. unshelve eapply SoundAST_Event; eauto.
  constructor; auto. simp bimap_csum. now apply num_unary_sound.
Qed.

Proposition setreg_bop_sound (o: binop) (r: reg) vl vr vlˣ vrˣ:
  galois_in vl vlˣ ->
  galois_in vr vrˣ ->
  SoundAST (setreg_bop o r vl vr) (setreg_bop o r vlˣ vrˣ).
Proof.
  intros. unshelve eapply SoundAST_Event; eauto.
  constructor; auto. simp bimap2_csum. now apply num_binary_sound.
Qed.

Proposition instr_sound (i: instr):
  SoundAST ⟦i⟧%instr ⟦i⟧#%instr.
Proof.
  induction i; cbn.
  all: repeat (apply SoundAST_Seq; eauto; intros).
  all: try now unshelve eapply SoundAST_Event.
  all: apply operand_sound || apply setreg_op_sound || apply setreg_bop_sound.
  all: easy.
Qed.

Lemma blockres_sound {N} (r: blockres value) (rˣ: blockres valueˣ):
  galois_in r rˣ ->
  galois_in (blockres_to_tailmrecᵒ_ret (N := N) r)
            (blockres_to_TailBranch rˣ).
Proof.
  revert r rˣ. intros v1 v2 Hin.
  destruct v1 as [l1|t1 z1 n1| |] eqn:Hv1, v2 as [l2|t2 z2 n2| |] eqn:Hv2;
    cbn in *; auto; intuition.
  * subst; case (lt_dec l2 N); [|easy].
    intros l; split; [|easy]. cbn. now rewrite Fin_eq_refl.
  * case lt_dec; auto.
  * subst. case negb, (lt_dec z2 N), (lt_dec n2 N); intuition;
      try now case lt_dec.
    - cbn. rewrite Fin_eq_refl. easy.
    - cbn. rewrite Fin_eq_refl. cbn. now rewrite Bool.orb_true_r.
  * now case lt_dec.
Qed.

Proposition block_sound {N} (b: block):
  SoundAST (frontend_block (N := N) b) (frontend_block b).
Proof.
  induction b.
  - cbn. apply SoundAST_Seq. apply instr_sound. intros. apply IHb.
  - destruct b.
    * apply SoundAST_Ret. simp mksum. now apply blockres_sound.
    * apply SoundAST_Seq. now unshelve eapply SoundAST_Event.
      intros. apply SoundAST_Ret. simp bimap_csum.
      now apply blockres_sound.
    * now apply SoundAST_Ret.
Qed.

Proposition asm_sound (l: asm):
  SoundAST (frontend_asm l) (frontend_asm l).
Proof.
  destruct l; simp frontend_asm.
  - now apply SoundAST_Ret.
  - apply SoundAST_CFG. easy. intros i * Hr.
    rewrite ! vec_nth_map. apply block_sound.
Qed.

Proposition interp_asm_sound (l: asm):
  sound (concrete_eval l) (abstract_eval l).
Proof.
  intros Hr Hm.
  apply sound_unfold.
  apply interp_sound_stateT.
  { easy. }
  { apply ASMMemory.handle_amem_sound. }
  apply interp_sound_stateT.
  { easy. }
  { apply handler_sound_stateT_case, ASMRegs.handle_amem_sound. }
  apply sound_ast2itree_ast2aflow.
  apply asm_sound.
Qed.

(* We only depend on [Eqdep.Eq_rect_eq.eq_rect_eq] for stronger inversion principles
   when inverting equalities between dependent pairs such as [Vis] nodes.
 *)
Print Assumptions interp_asm_sound.
