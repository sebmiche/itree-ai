(* ** IMP source language

   This file showcases the library by demonstrating the construction and
   certification of an abstract interpreter for IMP in the layered monadic
   style. It's essentially Section 3/Figure 5 of the paper. *)

From Coq Require Import Arith.PeanoNat String DecidableType Program ZArith.
From Equations Require Import Equations.

From ITree Require Import ITree Interp Monad Events.State Events.FailFacts.
Import Basics.Monads.

From ITreeAI Require Import
  Lattice LatticeIntv XL_Intv LatticeOption Util EventHierarchy FailA
  NumericalDomain Events AflowMonad AbstractMonad Soundness
  FiniteMemory SurfaceDSL.

Import MonadNotation.

Local Open Scope monad_scope.
Local Open Scope Z_scope.

(* Arithmetic events from the definition of the IMP language. For the sake of
   avoiding repetition we define our set of operators from the interval
   domain's but it could be a new variant. *)
Variant arithE {V : Type} : Type -> Type :=
| do_unop  (op : IntvUnOp ) (x : V)   : arithE V
| do_binop (op : IntvBinOp) (l r : V) : arithE V.

(* Arithmetic event handler. This handles into the identity monad (implicit in
   `~> M`, by contrast with e.g. `~> failT M`) and requires the value type to
   be an numerical domain with the appropriate set of unary/binary operators.
   The `NumDomain` class bridges the lattice/domain library with the language
   (for the abstract program) by specifying which lattice can be used for which
   abstract operations. *)
Definition handle_arith (M: Type -> Type) `{Monad M} {V}
  `{NumDomain V IntvUnOp IntvBinOp} :
  @arithE V ~> M :=
  fun _ e => match e with
          | do_unop  op x   => ret (num_unary op x)
          | do_binop op l r => ret (num_binary op l r)
          end.

(* Due to how simple arithmetic events are, we don't need a concrete and an
   abstract definition; the same definition works for both, simply by setting
   `V` to either the concrete domain (integers) or the abstract domain (here,
   intervals). *)

(* =========================================================================== *)
(** ** IMP Syntax *)

Definition var : Set := string.
Definition value : Type := Z.

Inductive expr : Type :=
| Var   (_ : var)
| Lit   (_ : nat)
| UnOp  (_ : IntvUnOp) (_ : expr)
| BinOp (_ : IntvBinOp) (_ _ : expr).

Inductive stmt : Type :=
| Assert (b : expr)
| Skip                           (* ; *)
| Assign (x : var) (e : expr)    (* x = e *)
| Seq    (a b : stmt)            (* a ; b *)
| If     (i : expr) (t e : stmt) (* if (i) then { t } else { e } *)
| While  (t : expr) (b : stmt).  (* while (t) { b } *)

(* ========================================================================== *)
(** ** IMP Notations *)

Module ImpNotations.
  Declare Scope expr_scope.
  Delimit Scope expr_scope with expr.
  Bind Scope expr_scope with expr.

  Definition Var_coerce: string -> expr := Var.
  Definition Lit_coerce: nat -> expr := Lit.
  Coercion Var_coerce: string >-> expr.
  Coercion Lit_coerce: nat >-> expr.

  Notation "'--' e" := (UnOp IntvMinus e) (at level 120) : expr_scope.
  Infix "+" := (BinOp IntvAdd) : expr_scope.
  Infix "-" := (BinOp IntvSub) : expr_scope.

  Notation "x '←' e" := (Assign x e) (at level 60).
  Notation "'ASSERT' e" := (Assert e) (at level 60).
  Notation "a ';;;' b" := (Seq a b) (at level 110, right associativity).
  Notation "'IF' i 'THEN' t 'ELSE' e 'FI'" := (If i t e) (at level 100).
  Notation "'WHILE' t 'DO' b" := (While t b) (at level 100).
End ImpNotations.
Import ImpNotations.


(* ========================================================================== *)
(** ** IMP semantics *)

(* Instantiation of the finite memory layer. The memory in IMP is a simple
   name -> value store ("finite memory"), for which the library already
   provides an implementation. We instantiate it by specifying the type of
   names, values, and the default value of variables (0).

   This gives us the types of concrete and abstract events (memE), the
   associated "Galois connection" (memE_EventRelation), as well as the
   concrete and abstract handlers and their proof of soundness. See
   `Domains.FiniteMemory` for these definitions. *)

Module IMPMemoryIntf <: MemoryIntf.
  Module AddrDecidable := Util.StringDecidable.
  Definition Value := value.
  Definition uninitialized := 0.
  Definition Valueˣ: Typeˣ := Intv.
  #[global] Instance GC_Value: Valueˣ <#> Value := _.
End IMPMemoryIntf.

Module IMPMemory := FiniteMemory IMPMemoryIntf.
#[global] Existing Instance IMPMemory.memE_EventRelation.

(* Concrete semantics *)

(* Concrete and abstract version of assertion events. Notice the return type is
   different because it's easier to only use lattice types in the abstract
   program. *)
Variant assertEc {V : Type} : Type -> Type :=
  | assertc (v : V) : assertEc unit.
Variant assertEa {V : Type} : Type -> Type :=
  | asserta (v : V) : assertEa unitˣ.

(* Concrete handler: this is a typical monadic assert. *)
Definition handle_assert_c (M: Type -> Type) `{Monad M} {V UnOp BinOp}
  `{NumDomain V UnOp BinOp} : @assertEc V ~> failT M :=
  fun _ '(assertc v) =>
    if num_isfalse v
    then ret None
    else ret tt.

(* Abstract handler: this version attempts to decide the condition by using
   the numerical domain's `num_isfalse` and `num_istrue` partial-decision
   functions *)
Definition handle_assert_a (M: Type -> Type) `{Monad M} {V} `{Lattice V}
  {UnOp BinOp} `{NumDomain V UnOp BinOp}:
    @assertEa V ~> failA M :=
  fun _ '(asserta v) =>
    if num_isfalse v
    then ret optionˣ_none
    else if num_istrue v
         then ret (optionˣ_some ttˣ)
         else ret (top: optionˣ _).

(* Galois connection for assert events: this one is trivial, assert events
   match if the conditions match (`galois_in v a`). The output lattice for the
   abstract event is the usual lattice on `unitˣ` with its associated Galois
   connection (already provided by `Lattices.Lattice`). *)
#[local, refine]
  Instance assertE_EventRelation {V A} `{Lattice A} {G: GaloisConnection A V}:
  @assertEc V =# @assertEa A :=
  {
    evl_in _ _ e1 e2 :=
      match e1, e2 with
      | assertc v, asserta a => galois_in v a
      end;
    evl_lattice _ e2 :=
      match e2 with
      | asserta _ => Lattice_unitˣ
      end;
    evl_galois _ _ e1 e2 _ :=
      match e1, e2 with
      | assertc _, asserta _ => GaloisConnection_unitˣ_unit
      end;
  }.
Proof. intros U1 U2 [] [] H1 H2; intuition. Defined.

(* TODO: Make it so that this EvE instance is available immediately *)
#[global] Existing Instance IMPMemory.memE_EventRelation.

(* Galois connection for arithmetic events: kinda verbose, but again only the
   `evl_in` part is interesting: a concrete and an abstract arith events match
   if they have the same arity, operator, and matching operands. *)
#[local, refine]
  Instance arithE_EventRelation {V A} {LA: Lattice A} {G: GaloisConnection A V} :
  @arithE V =# @arithE A :=
  {
    evl_in _ _ e1 e2 :=
      match e1, e2 with
      | do_unop op1 v1    , do_unop op2 v2     => op1 = op2 /\ galois_in v1 v2
      | do_binop op1 l1 r1, do_binop op2 l2 r2 => op1 = op2 /\ galois_in l1 l2 /\ galois_in r1 r2
      | _, _ => False
      end;

    evl_lattice _ e2 :=
      match e2 with
      | do_unop op2 v2 => LA
      | do_binop op2 l2 r2 => LA
      end;

    evl_galois _ _ e1 e2 H :=
      match e1, e2, H with
      | do_unop op1 v1, do_unop op2 v2, H => G
      | do_binop op1 l1 r1, do_binop op2 l2 r2, H => G
      | _, _, H => match H with end
      end
  }.
Proof. intros U1 U2 [] [] H1 H2; intuition. Defined.

(* Expression front-end to a combinator AST *)

Import surface.
Open Scope surface.

(* Shorthand names for the set of concrete/abstract events (E/Eˣ) and the
   denotation monads (M/Mˣ). *)
Definition E := @assertEc value +' @memE string value unit +' @arithE value.
Definition M := failT (stateT IMPMemory.Memory (itree void1)).
Definition Eˣ := @assertEa IMPMemory.Valueˣ
                 +' @memE string IMPMemory.Valueˣ unitˣ
                 +' @arithE IMPMemory.Valueˣ.
Definition Mˣ := failA (stateT IMPMemory.Memoryˣ (aflow void1)).

Reserved Notation "⟦ e ⟧_E".
Reserved Notation "⟦ s ⟧".

(* Denotation of an IMP expression as a SurfaceAST. This mostly emits pairs of
   matching events or return-statements. Notice how we implicitly quantify over
   a boolean b, which is how we create "both" the concrete and abstract
   interpreters at the same time (SurfaceAST constructors implicitly take this
   b as parameter as well). *)
Fixpoint denote_expr {b} (e: expr): SurfaceAST E Eˣ value IMPMemory.Valueˣ b :=
  match e with
  | Var v =>
      do ReadAddr v and ReadAddr v
  | Lit n =>
      ret num_nat n and num_nat n
  | UnOp op e =>
      x <- ⟦ e ⟧_E;;
      do do_unop op and do_unop op on x
  | BinOp op e₁ e₂ =>
      x₁ <- ⟦ e₁ ⟧_E;;
      x₂ <- ⟦ e₂ ⟧_E;;
      do do_binop op and do_binop op on x₁ and x₂
  end
where "⟦ e ⟧_E" := (denote_expr e).
Notation "⟦ e ⟧_E" := (denote_expr e).

(* Denotation of an IMP statement as a SurfaceAST. This time we use control
   flow combinators, which is a bit more exciting. *)
Fixpoint denote_stmt {b} (s: stmt): SurfaceAST E Eˣ unit unitˣ b :=
  match s with
  | Skip =>
      ret tt and ttˣ
  | Assign x e =>
      v <- ⟦e⟧_E;;
      do (WriteAddr x) and (WriteAddr x) on v
  | Seq a b =>
      ⟦a⟧;; ⟦b⟧
  | If e s₁ s₂ =>
      v <- ⟦e⟧_E;;
      if v then ⟦s₁⟧ else ⟦s₂⟧
  | While b c =>
      while_u ⟦b⟧_E do ⟦c⟧
  | Assert b =>
      v <- ⟦b⟧_E;;
      do assertc and asserta on v
  end
where "⟦ s ⟧" := (denote_stmt s).
Notation "⟦ s ⟧" := (denote_stmt s).

(* Layered cake of event handlers for the concrete side. `hoist` is a standard
   monadic combinator which allows us to chain the event handling rounds
   despite monad transformers modifying the datatypes. *)
Definition imp_interp {R} (t: itree E R): M R :=
  hoist
    (fun _ u => hoist
               (interp (handle_arith _))
               (interp_state (case_stateC IMPMemory.handle_mem) u))
    (interp_fail (case_failC (handle_assert_c _)) t).

Definition imp_eval (s: stmt) σ : itree _ _ :=
  imp_interp (ast2itree ⟦s⟧) σ.

(* Layered cake of event handlers for the abstract side. *)
Definition imp_interpˣ {R} (t: aflow Eˣ R): Mˣ R :=
  hoist
    (fun _ u =>
       hoist
         (aflow_interp (handle_arith _))
         (aflow_interp_state (case_stateA IMPMemory.handle_amem) u))
    (aflow_interp_fail (case_failA (handle_assert_a _)) t).

Definition imp_evalˣ (s: stmt) σ : aflow _ _ :=
  imp_interpˣ (ast2aflow ⟦s⟧) σ.


(* ========================================================================== *)
(** ** Soundness proof
    We just prove the soundness of leaves and events; the AST will transport
    that to a full syntactic correspondance of control flow structures. Then,
    we unfold the layered interpretation and prove each handler sound to obtain
    that the final abstract program is a sound approximation of the concrete
    one. *)

(* Proof that concrete/abstract expressions get matching ASTs. This is trivial
   since we get both of them from the same function (`denote_expr`). Only at
   leaves (rets/events) do we need to prove the correspondance. *)
Proposition frontend_expr_sound (e: expr):
  SoundAST ⟦e⟧_E ⟦e⟧_E.
Proof.
  induction e.
  - now unshelve eapply SoundAST_Event.
  - apply SoundAST_Ret. cbn [mksum]. apply num_nat_sound.
  - cbn.
    apply SoundAST_Seq. { apply IHe. } intros.
    now unshelve eapply SoundAST_Event.
  - cbn.
    apply SoundAST_Seq. { apply IHe1. } intros.
    apply SoundAST_Seq. { apply IHe2. } intros.
    now unshelve eapply SoundAST_Event.
Qed.

(* Same for statements. *)
Proposition frontend_stmt_sound (s: stmt):
  SoundAST ⟦s⟧ ⟦s⟧.
Proof.
  induction s.
  - cbn.
    apply SoundAST_Seq. { apply frontend_expr_sound. } intros.
    now unshelve eapply SoundAST_Event.
  - now apply SoundAST_Ret.
  - apply SoundAST_Seq. { apply frontend_expr_sound. } intros.
    now unshelve eapply SoundAST_Event.
  - now eapply SoundAST_Seq.
  - cbn. apply SoundAST_Seq. { apply frontend_expr_sound. } intros.
    now apply SoundAST_If.
  - cbn. apply SoundAST_While; try easy.
    * intros; apply frontend_expr_sound.
    * intros. apply SoundAST_Seq; auto. intros; apply frontend_expr_sound.
Qed.

(* Proof that the concrete/abstract handlers are sound. Remember that we use
   the same handler for both and just substitute a different numerical type.
   As a result, this comes down to the concrete/abstract numerical domains
   being sound, a property that Coq pulls implicitly from the typeclass store.
   The actual proof in this case comes from the interval domain and you can
   find its entry point in `Domains.NumericalDomain.AbsNumDomain_Intv`. *)
Lemma handle_arith_sound :
  handler_sound_idT (handle_arith (itree void1)) (handle_arith (aflow void1)).
Proof.
  red; intros *.
  destruct e, eˣ; try now intuition auto.
  - apply sound'_ret. destruct H as [-> H]. now apply num_unary_sound.
  - apply sound'_ret. destruct H as [-> H]. now apply num_binary_sound.
Qed.

(* Same proof the handler soundness for the failure handlers. This one comes
   down to the numerical domain's partial-decision functions `num_istrue` and
   `num_isfalse` being correct. There is a bit of API layering to undo. *)
Lemma handle_assert_sound :
  handler_sound_failT
    (handle_assert_c (itree void1))
    (handle_assert_a (aflow void1)).
Proof.
  red; intros. destruct e as [v], eˣ as [vˣ]; cbn -[num_isfalse].
  case (num_isfalse v) eqn:Hfalse1.
  - assert (v = 0) as -> by (destruct v; now try inversion Hfalse1).
    cbn in H; apply Intervals.inb_iff_true in H. rewrite H; cbn.
    case Intervals.eqb; now apply sound'_ret.
  - assert (@num_isfalse Intv _ _ _ vˣ = false) as ->.
    { case (num_isfalse vˣ) eqn:Hfalse2; try easy.
      now rewrite (num_isfalse_sound _ _ Hfalse2 H) in Hfalse1. }
    case Intervals.inb; now apply sound'_ret.
Qed.

(* Proof that running both interpreters on an expression yields matching
   results. Now we're getting into classical abstract interpretation theorems.
   This is the part where we go through Figure 6 from bottom to top and show
   how return-value-soundness (`sound`) can be obtained from matching-control-
   flow-soundness (`sound'`) which itself is preserved by event handling (when
   handlers are sound, which they are), and was originally true because the
   SurfaceAST were matching. *)
Proposition interp_expr_sound (e: expr) s1 s2:
  galois_in s1 s2 ->
  sound (imp_interp (ast2itree ⟦e⟧_E) s1) (imp_interpˣ (ast2aflow ⟦e⟧_E) s2).
Proof.
  intros Hinit.
  apply sound_unfold.
  apply interp_sound_idT.
  { apply handle_arith_sound. }
  apply interp_sound_stateT.
  { apply Hinit. }
  { apply handler_sound_stateT_case, IMPMemory.handle_amem_sound. }
  apply interp_sound_failT.
  { apply handler_sound_failT_case, handle_assert_sound. }
  apply sound_ast2itree_ast2aflow.
  apply frontend_expr_sound.
Qed.

(* Same for statements. Word-for-word the same proof. *)
Proposition interp_stmt_sound (s: stmt) s1 s2:
  galois_in s1 s2 ->
  sound (imp_interp (ast2itree ⟦s⟧) s1) (imp_interpˣ (ast2aflow ⟦s⟧) s2).
Proof.
  intros Hinit.
  apply sound_unfold.
  apply interp_sound_idT.
  { apply handle_arith_sound. }
  apply interp_sound_stateT.
  { apply Hinit. }
  { apply handler_sound_stateT_case, IMPMemory.handle_amem_sound. }
  apply interp_sound_failT.
  { apply handler_sound_failT_case, handle_assert_sound. }
  apply sound_ast2itree_ast2aflow.
  apply frontend_stmt_sound.
Qed.

(* We only depend on [Eqdep.Eq_rect_eq.eq_rect_eq] for stronger inversion
   principles when inverting equalities between dependent pairs such as [Vis]
   nodes. *)
Print Assumptions interp_stmt_sound.
