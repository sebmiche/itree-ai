(** * IMP source language *)

From Coq    Require Import Arith.PeanoNat Strings.String DecidableType.
From ExtLib Require Import Structures.Monad.
From ITree  Require Import ITree Events.State Props.Finite Eqit.
From Equations Require Import Equations.
From ITreeAI Require Import
  Lattice LatticeConst FiniteMemory SurfaceDSL
  NumericalDomain Events AflowMonad AbstractMonad Soundness.

Import Basics.Monads.
Import MonadNotation.
Local Open Scope monad_scope.


(* ========================================================================== *)
(** ** IMP Syntax *)

Definition var : Set := string.
Definition value : Type := nat.

Variant binop := Add | Sub | Mul.

Inductive expr : Type :=
  | Var   (_ : var)
  | Lit   (_ : value)
  | BinOp (_ : binop) (_ _ : expr).

Inductive stmt : Type :=
  | Skip                           (* ; *)
  | Assign (x : var) (e : expr)    (* x = e *)
  | Seq    (a b : stmt)            (* a ; b *)
  | If     (i : expr) (t e : stmt) (* if (i) then { t } else { e } *)
  | While  (t : expr) (b : stmt).  (* while (t) { b } *)

Module ImpNotations.
  Declare Scope expr_scope.
  Declare Scope stmt_scope.
  Delimit Scope expr_scope with expr.
  Delimit Scope stmt_scope with stmt.

  Definition Var_coerce: string -> expr := Var.
  Definition Lit_coerce: nat -> expr := Lit.
  Coercion Var_coerce: string >-> expr.
  Coercion Lit_coerce: nat >-> expr.

  Bind Scope expr_scope with expr.
  Bind Scope stmt_scope with stmt.

  Infix "+" := (BinOp Add) : expr_scope.
  Infix "-" := (BinOp Sub) : expr_scope.
  Infix "*" := (BinOp Mul) : expr_scope.

  Notation "x '←' e" := (Assign x e)
    (at level 60, e at level 50).
  Notation "a ';;;' b" := (Seq a b)
    (at level 100, right associativity).
  Notation "'IF' i 'THEN' t 'ELSE' e 'FI'" := (If i t e)
    (at level 100, right associativity).
  Notation "'WHILE' t 'DO' b" := (While t b)
    (at level 100, right associativity).
End ImpNotations.
Import ImpNotations.


(* ========================================================================== *)
(** ** IMP semantics *)


(* Concrete numerical domain *)
#[export] Instance IMP_NumDomain: NumDomain value void binop := {
  num_nat := id;
  num_eq := Nat.eqb;
  num_istrue v := negb (v =? 0)%nat;
  num_isfalse v := (v =? 0)%nat;

  num_unary op v := match op with end;

  num_binary op v1 v2 :=
    match op with
    | Add => v1 + v2
    | Sub => v1 - v2
    | Mul => v1 * v2
    end
}.

(* Finite memory *)

Module IMPMemoryIntf <: MemoryIntf.
  Module AddrDecidable := Util.StringDecidable.
  Definition Value := value.
  Definition uninitialized := 0.
  Definition Valueˣ: Typeˣ := Const Value.
  #[global] Instance GC_Value: Valueˣ <#> Value := _.
End IMPMemoryIntf.

Module IMPMemory := FiniteMemory IMPMemoryIntf.
#[global] Existing Instance IMPMemory.memE_EventRelation.

(* Event sets and final monads *)

Definition E := @memE var value unit.
Definition M := stateT IMPMemory.Memory (itree void1).

Definition Eˣ := @memE var IMPMemory.Valueˣ unitˣ.
Definition Mˣ := stateT IMPMemory.Memoryˣ (aflow void1).

(* Expression front-end to a combinator AST *)

Import surface.
Open Scope surface.

Program Fixpoint frontend_expr {b} (e: expr): SurfaceAST E Eˣ value IMPMemory.Valueˣ b :=
  match e with
  | Var v => do ReadAddr v and ReadAddr v
  | Lit n => ret num_nat n and num_nat n
  | BinOp op e₁ e₂ =>
      x₁ <- frontend_expr e₁;;
      x₂ <- frontend_expr e₂;;
      ret num_binary op and num_binary op on x₁ and x₂
  end.

Program Fixpoint frontend_stmt {b} (s: stmt): SurfaceAST E Eˣ unit unitˣ b :=
  match s with
  | Skip =>
      ret tt and ttˣ
  | Assign x e =>
      v <- frontend_expr e;;
      do WriteAddr x and WriteAddr x on v
  | Seq a b =>
      frontend_stmt a;;
      frontend_stmt b
  | If c t e =>
      v <- frontend_expr c;;
      if v then frontend_stmt t else frontend_stmt e
  | While b c =>
      while_u frontend_expr b do frontend_stmt c
  end.

Definition imp_interp {R} (t: itree E R): M R :=
  interp_state IMPMemory.handle_mem t.

Definition imp_eval (s: stmt): itree _ (IMPMemory.Memory * unit) :=
  imp_interp (ast2itree (frontend_stmt s)) (IMPMemory.Map.empty _).

Definition imp_interpˣ {R} (t: aflow Eˣ R): Mˣ R :=
  fun s => aflow_interp_state IMPMemory.handle_amem t s.

Definition imp_evalˣ (s: stmt): aflow _ (_ * unitˣ) :=
  imp_interpˣ (ast2aflow (frontend_stmt s)) IMPMemory.MapLattice.Bot.

Notation "⟦ e ⟧"  := (ast2itree (frontend_expr e)) (format "⟦ e ⟧"): expr_scope.
Notation "⟦ c ⟧"  := (ast2itree (frontend_stmt c)) (format "⟦ c ⟧"): stmt_scope.
Notation "⟦ e ⟧#" := (ast2aflow (frontend_expr e)) (format "⟦ e ⟧#"): expr_scope.
Notation "⟦ c ⟧#" := (ast2aflow (frontend_stmt c)) (format "⟦ c ⟧#"): stmt_scope.
Open Scope stmt_scope.


(* ========================================================================== *)
(** ** Soundness proof
    We just prove the soundness of leaves and events; the AST will transport
    that to a full syntactic correspondance of control flow structures. Then,
    we unfold the layered interpretation and prove each handler sound to obtain
    that the final abstract program is a sound approximation of the concrete
    one. *)

Proposition frontend_expr_sound (e: expr):
  SoundAST (frontend_expr (b := false) e) (frontend_expr (b := true) e).
Proof.
  induction e.
  - now unshelve eapply SoundAST_Event.
  - now apply SoundAST_Ret.
  - Opaque num_binary. cbn.
    apply SoundAST_Seq. { apply IHe1. } intros.
    apply SoundAST_Seq. { apply IHe2. } intros.
    apply SoundAST_Ret. simp bimap2_csum.
    now apply num_binary_sound.
Qed.

Proposition frontend_stmt_sound (s: stmt):
  SoundAST (frontend_stmt s) (frontend_stmt s).
Proof.
  induction s.
  - now apply SoundAST_Ret.
  - apply SoundAST_Seq. { apply frontend_expr_sound. } intros.
    now unshelve eapply SoundAST_Event.
  - now eapply SoundAST_Seq.
  - cbn. apply SoundAST_Seq. { apply frontend_expr_sound. } intros.
    now apply SoundAST_If.
  - cbn. apply SoundAST_While; try easy.
    * intros; apply frontend_expr_sound.
    * intros. apply SoundAST_Seq; auto. intros; apply frontend_expr_sound.
Qed.

Proposition interp_expr_sound (e: expr) s1 s2:
  galois_in s1 s2 ->
  sound (imp_interp ⟦e⟧%expr s1) (imp_interpˣ ⟦e⟧#%expr s2).
Proof.
  intros H.
  apply sound_unfold.
  apply interp_sound_stateT.
  { apply H. }
  { apply IMPMemory.handle_amem_sound. }
  apply sound_ast2itree_ast2aflow.
  apply frontend_expr_sound.
Qed.

Proposition interp_stmt_sound (s: stmt) s1 s2:
  galois_in s1 s2 ->
  sound (imp_interp ⟦s⟧%stmt s1) (imp_interpˣ ⟦s⟧#%stmt s2).
Proof.
  intros H.
  apply sound_unfold.
  apply interp_sound_stateT.
  { apply H. }
  { apply IMPMemory.handle_amem_sound. }
  apply sound_ast2itree_ast2aflow.
  apply frontend_stmt_sound.
Qed.
