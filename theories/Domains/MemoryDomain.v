(** * Memory domains over lattices.

    Memory domains are lattices that support memory queries and assignments.

    TODO: While this is not yet implemented, they should also support numerical
    expression evaluation (both forwards and backwards) when built over a
    numerical domain. (How to safely mix types in memory if some value types
    are not numerical?) *)

From Coq Require Import Classes.EquivDec FMaps.
From ITreeAI Require Import Util.

Class MemDomain M (Addr Value: Type) := {
  mem_store: M -> Addr -> Value -> M;
  mem_query: M -> Addr -> option Value;

  mem_query_default m a default :=
    match mem_query m a with
    | Some v => v
    | None => default
    end;
}.

(* There is no AbsMemDomain because the functionality is the same in both
   worlds; a difference only needs to be made when adding in numerical domain
   interfaces to support relational domains. *)

(* Normal MemDomain on a map. *)
Module MapMemDomain (Map: WS).
  #[global] Instance MD {T}: MemDomain (Map.t T) Map.key T := {
    mem_store m addr val := Map.add addr val m;
    mem_query m addr := Map.find addr m;
  }.
End MapMemDomain.
