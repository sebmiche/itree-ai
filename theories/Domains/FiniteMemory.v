(** * Finite memory

    This module provides abstract memory in the form of an (Addr -> Value) map
    built from a MemDomain, along with a lifted lattice, event handler, and
    termination proof. *)

From Coq Require Import Lists.List Classes.EquivDec DecidableType.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad.
From Paco Require Import paco.
From ITree Require Import ITree Props.Finite.
From ITreeAI Require Import
  Lattice Soundness MemoryDomain Events Util AflowMonad AbstractMonad.
Import Basics.Monads.

Variant memE {Addr Value Unit: Type}: Type -> Type :=
  | ReadAddr  (a: Addr): memE Value
  | WriteAddr (a: Addr) (v: Value): memE Unit.

Definition handle_memE (M: Type -> Type) `{Monad M}
    S {Addr Value Unit} `{MemDomain S Addr Value} uninitialized the_tt:
    @memE Addr Value Unit ~> stateT S M :=
  fun _ e m =>
    match e with
    | ReadAddr a    => ret (m, mem_query_default m a uninitialized)
    | WriteAddr a v => ret (mem_store m a v, the_tt)
    end.

Module Type MemoryIntf.
  Declare Module AddrDecidable: DecidableType.
  Parameter Value: Type.
  Parameter uninitialized: Value.

  (* We require a widening-capable lattice over the abstract value type. When
     writing just a concrete interpreter, the Const lattice can be supplied as
     a default. *)
  Parameter Valueˣ: Typeˣ.
  #[global] Declare Instance GC_Value: Valueˣ <#> Value.
End MemoryIntf.

Module FiniteMemory (I: MemoryIntf).
  Include I.
  Definition Addr := AddrDecidable.t.

  (* The finite map used to instantiate the store *)
  Module Map := FMapWeakList.Make AddrDecidable.
  Definition Memory := Map.t Value.
  Module Memory_MemDomain := MapMemDomain Map.

  Definition handle_mem := handle_memE (itree void1) Memory uninitialized tt.

  Lemma handle_mem_all_finite {R} e s: all_finite (@handle_mem R e s).
  Proof. destruct e; cbn; auto with itree. Qed.

  Module MapLattice := MapLattice Map.

  (* The memory lattice is lifted to the store type *)
  Definition Memoryˣ := MapLattice.MapL Valueˣ.
  #[local] Instance Lattice_Memoryˣ: Lattice Memoryˣ := _.
  #[local] Instance GC_Memory: Memoryˣ <#> Memory := _.
  #[local] Instance Memoryˣ_MemDomain: MemDomain Memoryˣ Addr Valueˣ := _.

  Definition handle_amem := handle_memE (aflow void1) Memoryˣ top ttˣ.

  Lemma handle_amem_all_finite {R} e s:
    all_finite (unfold (handle_amem R e s)).
  Proof. destruct e; cbn; simp unfold; cbn; auto with itree. Qed.

  (* Soundness of memory events *)
  #[local, refine]
  Instance memE_EventRelation:
      @memE Addr Value unit =# @memE Addr Valueˣ unitˣ := {
    evl_in _ _ e1 e2 :=
      match e1, e2 with
      | ReadAddr a1, ReadAddr a2 => a1 = a2
      | WriteAddr a1 v1, WriteAddr a2 v2 => a1 = a2 /\ galois_in v1 v2
      | _, _ => False
      end;
    evl_lattice _ e2 :=
      match e2 with
      | ReadAddr _ => Valueˣ
      | WriteAddr _ _ => Lattice_unitˣ
      end;
    evl_galois _ _ e1 e2 H :=
      match e1, e2, H with
      | ReadAddr a1, ReadAddr a2, H => GC_Value
      | WriteAddr a1 v1, WriteAddr a2 v2, H => GaloisConnection_unitˣ_unit
      | _, _, H => match H with end
      end;
  }.
  Proof. intros U1 U2 [] [] H1 H2; intuition. Defined.

  Lemma handle_amem_sound:
    handler_sound_stateT handle_mem handle_amem.
  Proof.
    Opaque evl_RAns.
    intros T1 T2 e1 e2 Hin s1 s2.
    destruct e1 as [a1|a1 v1]; destruct e2 as [a2|a2 v2]; intuition.
    - cbn in Hin; subst a2; cbn. apply sound'_ret; split; auto.
      unfold mem_query_default; cbn.
      now apply MapLattice.map_find_sound.
    - cbn in Hin; destruct Hin as [<- Hv]; cbn.
      apply sound'_ret. split; try reflexivity.
      now apply MapLattice.map_add_sound.
  Qed.
End FiniteMemory.
