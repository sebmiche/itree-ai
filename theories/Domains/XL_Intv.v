(** Arithmetic of integer intervals
    From github.com/xavierleroy/cdf-mech-sem/blob/master/AbstrInterp2.v *)

From Coq Require Import ZArith Lia.

Open Scope bool.
Open Scope Z.

(* Z with infinity; from https://github.com/xavierleroy/cdf-mech-sem/ *)
Inductive zinf : Type := Fin (h: Z) | Inf.

Coercion Fin : Z >-> zinf.

Module Zinf.
  Definition In (n: Z) (N: zinf) : Prop :=
    match N with Fin h => n <= h | Inf => True end.

  Lemma In_mono: forall n1 n2 N, n1 <= n2 -> In n2 N -> In n1 N.
  Proof.
    unfold In; destruct N; intros. lia. auto.
  Qed.

  Definition inb (n: Z) (N: zinf): bool :=
    match N with Fin h => Z.leb n h | Inf => true end.

  Lemma inb_iff_true n N: inb n N = true <-> In n N.
  Proof.
    destruct N; cbn. symmetry. apply Bool.reflect_iff, Z.leb_spec0. easy.
  Qed.

  Lemma inb_iff_false n N: inb n N = false <-> ~ In n N.
  Proof.
    pose proof (H := inb_iff_true n N).
    destruct (inb n N); intuition discriminate.
  Qed.

  Definition le (N1 N2: zinf) : Prop :=
    forall n, In n N1 -> In n N2.

  Lemma le_Fin: forall n1 N2, le (Fin n1) N2 <-> In n1 N2.
  Proof.
    unfold le; cbn; intros; split; intros.
  - apply H. lia.
  - destruct N2; cbn in *; auto. lia.
  Qed.

  Lemma le_is_Inf: forall N h, (forall n, h <= n -> In n N) -> N = Inf.
  Proof.
    destruct N; cbn; intros; auto.
    specialize (H (Z.max h0 (h + 1))). lia.
  Qed.

  Lemma le_Inf: forall N, le Inf N <-> N = Inf.
  Proof.
    unfold le; intros; split; intros.
  - apply le_is_Inf with 0. intros; apply H; exact I.
  - subst N; exact I.
  Qed. 

  Lemma le_antisymm: forall N1 N2, le N1 N2 -> le N2 N1 -> N1 = N2.
  Proof.
    unfold le, In. intros [] []; auto; intros H1 H2.
  - f_equal. apply Z.le_antisymm; [now apply H1 | now apply H2].
  - pose proof (H2 (h+1) I). lia.
  - pose proof (H1 (h+1) I). lia.
  Qed.

  Lemma le_trans: forall N1 N2 N3, le N1 N2 -> le N2 N3 -> le N1 N3.
  Proof.
    unfold le, In; intros [] [] []; auto.
  Qed.

  Definition ble (N1 N2: zinf) : bool :=
    match N1, N2 with _, Inf => true | Inf, _ => false | Fin h1, Fin h2 => h1 <=? h2 end.

  Lemma ble_1: forall N1 N2, ble N1 N2 = true -> le N1 N2.
  Proof.
    unfold ble, le, In; intros.
    destruct N1, N2; auto.
    apply Z.leb_le in H. lia.
    discriminate.
  Qed.

  Lemma ble_2: forall N1 N2, le N1 N2 -> ble N1 N2 = true.
  Proof.
    unfold ble; intros. destruct N1.
  - apply le_Fin in H. destruct N2; auto. apply Z.leb_le; auto.
  - apply le_Inf in H. rewrite H. auto.
  Qed.

  Lemma ble_trans: forall N1 N2 N3,
    ble N1 N2 = true -> ble N2 N3 = true -> ble N1 N3 = true.
  Proof.
    intros * H12 H23. apply ble_1 in H12; apply ble_1 in H23; apply ble_2.
    eapply le_trans; eauto.
  Qed.

  Definition max (N1 N2: zinf) : zinf :=
    match N1, N2 with Inf, _ => Inf | _, Inf => Inf | Fin h1, Fin h2 => Fin (Z.max h1 h2) end.

  Lemma max_1: forall n N1 N2, In n N1 -> In n (max N1 N2).
  Proof.
    unfold In, max; intros. destruct N1; auto. destruct N2; auto. lia.
  Qed.

  Lemma max_2: forall n N1 N2, In n N2 -> In n (max N1 N2).
  Proof.
    unfold In, max; intros. destruct N1; auto. destruct N2; auto. lia.
  Qed.

  Definition min (N1 N2: zinf) : zinf :=
    match N1, N2 with Inf, _ => N2 | _, Inf => N1 | Fin h1, Fin h2 => Fin (Z.min h1 h2) end.

  Lemma min_1: forall n N1 N2, In n N1 -> In n N2 -> In n (min N1 N2).
  Proof.
    unfold In, min; intros. destruct N1; auto. destruct N2; auto. lia.
  Qed.

  Definition add (N1 N2: zinf) : zinf :=
    match N1, N2 with Inf, _ => Inf | _, Inf => Inf | Fin h1, Fin h2 => Fin (h1 + h2) end.

  Lemma add_1: forall n1 n2 N1 N2, In n1 N1 -> In n2 N2 -> In (n1 + n2) (add N1 N2).
  Proof.
    unfold In, add; intros. destruct N1; auto. destruct N2; auto. lia.
  Qed.

  Definition pred (N: zinf) : zinf :=
    match N with Inf => Inf | Fin n => Fin (n - 1) end.

  Lemma pred_1: forall n N, In n N -> In (n - 1) (pred N).
  Proof.
    unfold pred, In; intros; destruct N; auto. lia.
  Qed.

(** We define widening between two possibly infinite integers as follows:
    if the integer increases strictly, we jump to infinity, otherwise
    we keep the first integer. *)

  Definition widen (N1 N2: zinf) : zinf :=
     if ble N2 N1 then N1 else Inf.

  Lemma widen_1: forall N1 N2, le N1 (widen N1 N2).
  Proof.
    unfold widen; intros. destruct (ble N2 N1) eqn:LE.
    red; auto.
    red; unfold In; auto.
  Qed.

  Lemma widen_1': forall N1 N2, le N2 (widen N1 N2).
  Proof.
    unfold widen; intros. destruct (ble N2 N1) eqn:LE.
    now apply ble_1.
    red; unfold In; auto.
  Qed.

  Definition measure (N: zinf) : nat :=
    match N with Inf => 0%nat | Fin _ => 1%nat end.

  Lemma measure_1: forall N, (measure N <= 1)%nat.
  Proof.
    destruct N; cbn; lia.
  Qed.

  Lemma widen_2:
    forall N1 N2, (measure (widen N1 N2) <= measure N1)%nat.
  Proof.
    intros. unfold widen. destruct (ble N2 N1) eqn:BLE.
  - lia.
  - destruct N1. cbn; lia. destruct N2; discriminate.
  Qed.

  Lemma widen_3: 
    forall N1 N2, ble N2 N1 = false -> (measure (widen N1 N2) < measure N1)%nat.
  Proof.
    destruct N1, N2; cbn; intros; auto; try discriminate. 
    unfold widen. cbn. rewrite H. cbn. lia.
  Qed.

  Definition eqb (N1 N2: zinf): bool :=
    match N1, N2 with
    | Fin n1, Fin n2 => Z.eqb n1 n2
    | Inf, Inf => true
    | _, _ => false
    end.

  Lemma eqb_symm N1 N2: eqb N1 N2 = eqb N2 N1.
  Proof.
    destruct N1, N2; try easy. apply Z.eqb_sym.
  Qed.

  Lemma eqb_iff_true N1 N2: eqb N1 N2 = true <-> N1 = N2.
  Proof.
    Set Printing Coercions.
    destruct N1, N2; try easy; cbn.
    pose proof (Bool.reflect_iff _ _ (Z.eqb_spec h h0)).
    split.
    - intros. cut (h = h0). now intros ->. now apply H.
    - intros [=]; now apply H.
  Qed.
End Zinf.

(** An interval is encoded as a pair of two [zinf].
    The second [zinf] is the upper bound.
    The first [zinf] is the opposite of the lower bound.
    This representation trick makes it possible to have only one
    infinity [Inf], instead of a negative infinity for lower bounds
    and a positive infinity for upper bounds. *)


Module Intervals.

(** The type of abstract values. *)
  Record interval : Type := intv { low: zinf; high: zinf }.
  Definition t := interval.

(** Membership: [n] must be below the upper bound, and the opposite of [n]
    must be below the opposite of the lower bound. *)

  Definition In (n: Z) (N: t) : Prop :=
    Zinf.In n (high N) /\ Zinf.In (-n) (low N).

  Definition inb (n: Z) (N: t): bool :=
    Zinf.inb n (high N) && Zinf.inb (-n) (low N).

  Definition le (N1 N2: t) : Prop :=
    forall n, In n N1 -> In n N2.

(** Test whether an interval is empty. *)

  Definition isempty (N: t) : bool :=
    match N with 
    | {| low := Fin l; high := Fin h |} => h <? (-l)
    | _ => false
    end.

  Lemma isempty_1: forall n N, isempty N = true -> In n N -> False.
  Proof.
    unfold isempty, In; intros. destruct N as [[l|] [h|]]; try discriminate.
    apply Z.ltb_lt in H. cbn in H0. lia.
  Qed.

  Lemma isempty_2: forall N, isempty N = false -> exists n, In n N.
  Proof.
    unfold isempty, In; intros. destruct N as [[l|] [h|]]; cbn.
  - apply Z.ltb_ge in H. exists h; lia.
  - exists (- l); lia.
  - exists h; lia.
  - exists 0; auto.
  Qed.

  Lemma isempty_3: forall N, (forall n, In n N -> False) -> isempty N = true.
  Proof.
    intros * H. case_eq (isempty N); auto. intros Hf. cut False; auto.
    destruct (isempty_2 N Hf) as [n]. now apply (H n).
  Qed.

  Lemma nonempty_le: forall N1 N2,
    le N1 N2 -> isempty N1 = false -> (Zinf.le (high N1) (high N2) /\ Zinf.le (low N1) (low N2)).
  Proof.
    unfold le, In, isempty; intros.
    destruct N1 as [[l1 |] [h1|]]; cbn in *; rewrite ? Zinf.le_Fin, ? Zinf.le_Inf.
  - apply Z.ltb_ge in H0. split.
    + apply H; lia.
    + replace l1 with (- - l1) by lia. apply H. lia.
  - split.
    + apply Zinf.le_is_Inf with (-l1). intros; apply H. intuition lia.
    + replace l1 with (- - l1) by lia. apply H. intuition lia.
  - split.
    + apply H. intuition lia.
    + apply Zinf.le_is_Inf with(- h1).
      intros. replace n with (- - n) by lia. apply H. intuition lia.
  - split; apply Zinf.le_is_Inf with 0; intros.
    + apply H; auto.
    + replace n with (- - n) by lia. apply H. auto.
  Qed.

  Lemma nonempty_le_2: forall N1 N2,
    le N1 N2 -> isempty N1 = false -> isempty N2 = false.
  Proof.
    intros * Hle H1. rewrite <- Bool.not_true_iff_false. intros H2.
    destruct (isempty_2 N1 H1). cut (In x N2); eauto using isempty_1.
  Qed.

  Lemma isempty_le: forall N1 N2,
    le N1 N2 -> isempty N2 = true -> isempty N1 = true.
  Proof.
    intros * Hle H2. case_eq (isempty N1); auto.
    intros H1; now pose proof (nonempty_le_2 _ _ Hle H1) as <-.
  Qed.

(** [ble] is a Boolean-valued function that decides the [le] relation. *)

  Definition ble (N1 N2: t) : bool :=
    isempty N1 || (Zinf.ble (high N1) (high N2) && Zinf.ble (low N1) (low N2)).

  Lemma ble_1: forall N1 N2, ble N1 N2 = true -> le N1 N2.
  Proof.
    unfold ble, le, In; intros.
    destruct (isempty N1) eqn:E.
    elim (isempty_1 _ _ E H0).
    apply andb_prop in H. destruct H as [B1 B2].
    apply Zinf.ble_1 in B1. apply Zinf.ble_1 in B2.
    intuition.
  Qed.

  Lemma ble_2: forall N1 N2, le N1 N2 -> ble N1 N2 = true.
  Proof.
    unfold ble; intros. destruct (isempty N1) eqn:E; auto.
    destruct (nonempty_le N1 N2) as [P Q]; auto.
    apply andb_true_intro; split; apply Zinf.ble_2; auto.
  Qed.

(** [const n] is the abstract value for the singleton set [{n}]. *)
  Definition const (n: Z) : t := {| low := Fin (-n); high := Fin n |}.

  Lemma const_1: forall n, In n (const n).
  Proof.
    unfold const, In, Zinf.In; intros; cbn. lia.
  Qed.

  Lemma const_2: forall n v, In v (const n) -> v = n.
  Proof.
    unfold In, const; cbn. lia.
  Qed.

(** [bot] represents the empty set. *)
  Definition bot: t := {| low := Fin 0; high := Fin (-1) |}.

  Lemma bot_1: forall n, ~(In n bot).
  Proof.
    unfold bot, In, Zinf.In; intros; cbn. lia.
  Qed.

(** [top] represents the set of all integers. *)
  Definition top: t := {| low := Inf; high := Inf |}.

  Lemma top_1: forall n, In n top.
  Proof.
    intros. split; exact I.
  Qed.

  Definition eqb (N1 N2: t): bool :=
    Zinf.eqb (low N1) (low N2) && Zinf.eqb (high N1) (high N2).

  Lemma eqb_symm N1 N2: eqb N1 N2 = eqb N2 N1.
  Proof.
    unfold eqb. now rewrite (Zinf.eqb_symm (low N1)), (Zinf.eqb_symm (high N1)).
  Qed.

  Lemma eqb_iff_true N1 N2: eqb N1 N2 = true <-> N1 = N2.
  Proof.
    destruct N1, N2; try easy. unfold eqb; cbn.
    rewrite Bool.andb_true_iff, ! Zinf.eqb_iff_true. split.
    - now intros [-> ->].
    - now intros [=].
  Qed.

(** [join] computes an upper bound of its two arguments. *)
  Definition join (N1 N2: t) : t :=
    if eqb N2 bot then N1 else
    if isempty N1 then N2 else
    if isempty N2 then N1 else
    {| low := Zinf.max (low N1) (low N2);
       high := Zinf.max (high N1) (high N2) |}.

  Lemma join_1:
    forall n N1 N2, In n N1 -> In n (join N1 N2).
  Proof.
    unfold join; intros; cbn.
    case eqb; auto.
    case (isempty N1) eqn:Hem.
    { exfalso. eapply isempty_1; eauto. }
    case (isempty N2); auto.
    split; apply Zinf.max_1; unfold In in *; tauto.
  Qed.

  Lemma join_2:
    forall n N1 N2, In n N2 -> In n (join N1 N2).
  Proof.
    unfold join; intros; cbn.
    case eqb eqn:Heq.
    { rewrite eqb_iff_true in Heq. subst. exfalso.
      eapply isempty_1; eauto. easy. }
    case (isempty N1).
    { auto. }
    case (isempty N2) eqn:Hem.
    { exfalso. eapply isempty_1; eauto. }
    unfold In in *; split; apply Zinf.max_2; tauto.
  Qed.

  Lemma join_bot_l:
    forall N, join bot N = N.
  Proof.
    unfold join. intros. case eqb eqn:Heq.
    { rewrite <- eqb_iff_true. now rewrite eqb_symm. }
    easy.
  Qed.

  Lemma join_bot_r:
    forall N, join N bot = N.
  Proof.
    easy.
  Qed.

(** The abstract operators for addition and subtraction. *)

  Definition add (N1 N2: t) : t :=
    if isempty N1 || isempty N2 then bot else
    {| low := Zinf.add (low N1) (low N2);
       high := Zinf.add (high N1) (high N2) |}.

  Lemma add_1:
    forall n1 n2 N1 N2, In n1 N1 -> In n2 N2 -> In (n1 + n2) (add N1 N2).
  Proof.
    unfold add; intros.
    destruct (isempty N1) eqn:E1. elim (isempty_1 n1 N1); auto.
    destruct (isempty N2) eqn:E2. elim (isempty_1 n2 N2); auto.
    destruct H; destruct H0; split; cbn.
    apply Zinf.add_1; auto.
    replace (- (n1 + n2)) with ((-n1) + (-n2)) by lia. apply Zinf.add_1; auto.
  Qed.

  Definition opp (v: t) : t := {| low := high v; high := low v |}.

  Lemma opp_1:
    forall n v, In n v -> In (-n) (opp v).
  Proof.
    unfold In, opp; intros; cbn. replace (- - n) with n by lia. tauto.
  Qed.

  Definition sub (N1 N2: t) : t := add N1 (opp N2).

  Lemma sub_1:
    forall n1 n2 N1 N2, In n1 N1 -> In n2 N2 -> In (n1 - n2) (sub N1 N2).
  Proof.
    intros. apply add_1; auto. apply opp_1; auto.
  Qed.

(** [meet] computes a lower bound for its two arguments.*)
  Definition meet (N1 N2: t) : t :=
    {| low := Zinf.min (low N1) (low N2);
       high := Zinf.min (high N1) (high N2) |}.

  Lemma meet_1:
    forall n N1 N2, In n N1 -> In n N2 -> In n (meet N1 N2).
  Proof.
    unfold In, meet; intros; cbn. split; apply Zinf.min_1; tauto. 
  Qed.

(** [inb] tests whether a concrete value belongs to an abstract value. *)

  Lemma inb_iff_true n v: inb n v = true <-> In n v.
  Proof.
    unfold inb, In. rewrite Bool.andb_true_iff.
    intuition now apply Zinf.inb_iff_true.
  Qed.

  Lemma inb_iff_false n v: inb n v = false <-> ~ In n v.
  Proof.
    pose proof (H := inb_iff_true n v).
    destruct (inb n v); intuition discriminate.
  Qed.

(** Abstract operators for inverse analysis of comparisons. *)

  Definition eq_inv (N1 N2: t) : t * t := (meet N1 N2, meet N1 N2).

  Lemma eq_inv_1:
    forall n1 n2 a1 a2,
    In n1 a1 -> In n2 a2 -> n1 = n2 ->
    In n1 (fst (eq_inv a1 a2)) /\ In n2 (snd (eq_inv a1 a2)).
  Proof.
    intros; cbn. subst n2. split; apply meet_1; auto.
  Qed.

  Definition ne_inv (N1 N2: t) : t * t := (N1, N2).

  Lemma ne_inv_1:
    forall n1 n2 a1 a2,
    In n1 a1 -> In n2 a2 -> n1 <> n2 ->
    In n1 (fst (ne_inv a1 a2)) /\ In n2 (snd (ne_inv a1 a2)).
  Proof.
    intros; cbn; auto.
  Qed.

(** For the [<=] comparison, the upper bound of [N1] is at most that of [N2],
    and the lower bound of [N2] is at least that of [N1]. *)

  Definition le_inv (N1 N2: t) : t * t :=
    ( {| low := low N1; high := Zinf.min (high N1) (high N2) |},
      {| low := Zinf.min (low N1) (low N2); high := high N2 |} ).

  Lemma le_inv_1:
    forall n1 n2 a1 a2,
    In n1 a1 -> In n2 a2 -> n1 <= n2 ->
    In n1 (fst (le_inv a1 a2)) /\ In n2 (snd (le_inv a1 a2)).
  Proof.
    unfold In, le_inv; intros; cbn.
    intuition auto; apply Zinf.min_1; auto.
    apply Zinf.In_mono with n2; auto.
    apply Zinf.In_mono with (-n1); auto. lia.
  Qed.

(** For the [>] comparison, the upper bound of [N1] is at least that of [N2],
    and the lower bound of [N2] is at most that of [N1] - 1. *)

  Definition gt_inv (N1 N2: t) : t * t :=
    ( {| low := Zinf.min (low N1) (Zinf.pred (low N2)); high := high N1 |},
      {| low := low N2; high := Zinf.min (high N2) (Zinf.pred (high N1)) |} ).

  Lemma gt_inv_1:
    forall n1 n2 a1 a2,
    In n1 a1 -> In n2 a2 -> n1 > n2 ->
    In n1 (fst (gt_inv a1 a2)) /\ In n2 (snd (gt_inv a1 a2)).
  Proof.
    unfold In, gt_inv; intros; cbn.
    intuition auto; apply Zinf.min_1; auto.
    apply Zinf.In_mono with ((-n2) - 1). lia. apply Zinf.pred_1; auto.
    apply Zinf.In_mono with (n1 - 1). lia. apply Zinf.pred_1; auto.
  Qed.

(** The widening operator. *)

  Definition widen (N1 N2: t) : t :=
    if isempty N1 then N2 else
    if isempty N2 then N1 else
    {| low := Zinf.widen (low N1) (low N2); high := Zinf.widen (high N1) (high N2) |}.

  Lemma widen_1: forall N1 N2, le N1 (widen N1 N2).
  Proof.
    unfold le, widen; intros.
    destruct (isempty N1) eqn:E1. elim (isempty_1 n N1); auto.
    destruct (isempty N2) eqn:E2. auto.
    destruct H. split; apply Zinf.widen_1; auto.
  Qed.

  Definition measure (v: t) : nat :=
    if isempty v then 3%nat else (Zinf.measure (low v) + Zinf.measure (high v))%nat.

  Lemma measure_1 (v: t): (measure v <= 3)%nat.
  Proof.
    unfold measure. case isempty; try lia.
    assert (H1 := Zinf.measure_1 (low v)).
    assert (H2 := Zinf.measure_1 (high v)).
    lia.
  Qed.

  Lemma measure_top: measure top = 0%nat.
  Proof.
    auto.
  Qed.

  Remark isempty_widen: forall N1 N2,
    isempty N1 = false -> isempty N2 = false -> isempty (widen N1 N2) = false.
  Proof.
    intros. destruct (isempty (widen N1 N2)) eqn:E; auto.
    destruct (isempty_2 _ H) as (n1 & IN1).
    elim (isempty_1 n1 _ E). apply widen_1; auto.
  Qed.
  
  Lemma widen_2: 
    forall N1 N2, (measure (widen N1 N2) <= (measure N1))%nat.
  Proof.
    unfold measure; intros.
    destruct (isempty N1) eqn:E1. unfold widen; rewrite E1.
    generalize (Zinf.measure_1 (low N2)) (Zinf.measure_1 (high N2)); intros.
    destruct (isempty N2); lia.
    destruct (isempty N2) eqn:E2. unfold widen; rewrite E1, E2, E1. lia.
    rewrite isempty_widen by auto.
    unfold widen; rewrite E1, E2; cbn.
    generalize (Zinf.widen_2 (low N1) (low N2)) (Zinf.widen_2 (high N1) (high N2)). lia.
  Qed.

  Lemma widen_3: 
    forall N1 N2, ble N2 N1 = false -> (measure (widen N1 N2) < measure N1)%nat.
  Proof.
    unfold ble, measure; intros.
    destruct (isempty N2) eqn:E2. discriminate.
    destruct (isempty N1) eqn:E1.
  - unfold widen; rewrite E1, E2.
    generalize (Zinf.measure_1 (low N2)) (Zinf.measure_1 (high N2)). lia.
  - rewrite isempty_widen by auto.
    unfold widen; rewrite E1, E2. cbn.
    generalize (Zinf.widen_2 (low N1) (low N2)) (Zinf.widen_2 (high N1) (high N2)); intros.
    destruct (Zinf.ble (high N2) (high N1)) eqn:LE.
    + cbn in H. apply Zinf.widen_3 in H. lia.
    + apply Zinf.widen_3 in LE. lia.
  Qed.
End Intervals.
