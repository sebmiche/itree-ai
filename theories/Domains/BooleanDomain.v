(** * Boolean domain over concrete types and lattices.

    Boolean domains are used by conditional constructs. The only operations are
    semi-decidable comparisons against true and false. *)

From ITreeAI Require Import
  Util Lattice LatticePprod LatticeOption NumericalDomain.

Class BooleanDomain V := {
  BooleanDomain_istrue: V -> bool;
  BooleanDomain_isfalse: V -> bool;
}.

Class BooleanDomainConnection A V
    `{BD_V: BooleanDomain V} `{BD_A: BooleanDomain A}
    `{GC: GaloisConnection A V} := {

  BooleanDomain_istrue_sound: forall {a v},
    BooleanDomain_istrue a = true -> galois_in v a ->
    BooleanDomain_istrue v = true;

  BooleanDomain_isfalse_sound: forall {a v},
    BooleanDomain_isfalse a = true -> galois_in v a ->
    BooleanDomain_isfalse v = true;
}.

#[global]
Instance BooleanDomain_NumDomain {V UnOp BinOp} `(NumDomain V UnOp BinOp):
    BooleanDomain V := {
  BooleanDomain_istrue := num_istrue;
  BooleanDomain_isfalse := num_isfalse;
}.

(* ========================================================================== *)
(** ** Boolean domains for classical lattices *)

#[global]
Instance BooleanDomain_option V: BooleanDomain (option V) := {
  BooleanDomain_istrue v :=
    match v with
    | Some _ => true
    | None => false
    end;
  BooleanDomain_isfalse v :=
    match v with
    | Some _ => false
    | None => true
    end;
}.

#[global]
Instance BooleanDomain_optionˣ Vˣ `{Lattice Vˣ}: BooleanDomain (optionˣ Vˣ) := {
  BooleanDomain_istrue v :=
    match fst v with
    | ttˣ => false
    | _ => true
    end;
  (* TODO: Semi-lattice has no decidable guaranteed-emptiness check *)
  BooleanDomain_isfalse v := false;
}.

#[global]
Instance BooleanDomainConnection_optionˣ_option {V Vˣ}
    `{L: Lattice Vˣ} `(GC: GaloisConnection Vˣ V):
    BooleanDomainConnection (optionˣ Vˣ) (option V).
Proof.
  constructor.
  - intros [[]] []; easy.
  - easy.
Defined.
