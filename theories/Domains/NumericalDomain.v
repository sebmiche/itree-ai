(** * Numerical domain over lattices.

    Numerical domains are lattices that additionally implement numeric
    operations in a sound way. They can be used to interpret number-like
    concrete objects. *)

From Coq Require Import Classes.EquivDec ZArith.
From ITreeAI Require Import Util Lattice LatticeConst XL_Intv LatticeIntv.

Class NumDomain V (UnOp BinOp: Type) := {
  num_nat: nat -> V;
  num_eq: V -> V -> bool;
  num_istrue: V -> bool;
  num_isfalse: V -> bool;
  num_unary: UnOp -> V -> V;
  num_binary: BinOp -> V -> V -> V;
}.

Class AbsNumDomain A V UnOp BinOp `{NumDomain V UnOp BinOp} `{NumDomain A _ _}
    `{Lattice A} `{GaloisConnection A V} := {
  num_nat_sound: forall n,
    galois_in (num_nat n: V) (num_nat n: A);
  num_istrue_sound: forall (a:A) (v:V),
    num_istrue a = true -> galois_in v a -> num_istrue v = true;
  num_isfalse_sound: forall (a:A) (v:V),
    num_isfalse a = true -> galois_in v a -> num_isfalse v = true;

  num_unary_sound: forall (a: A) (v: V) op,
    galois_in v a -> galois_in (num_unary op v) (num_unary op a);
  num_binary_sound: forall (a1 a2: A) (v1 v2: V) op,
    galois_in v1 a1 -> galois_in v2 a2 ->
    galois_in (num_binary op v1 v2) (num_binary op a1 a2);
}.

(* ========================================================================== *)
(** ** Numerical domains for classical lattices *)

#[global]
Instance NumDomain_Const V UnOp BinOp `{NumDomain V UnOp BinOp} `{EqDec V eq}:
    NumDomain (Const V) UnOp BinOp := {

  num_nat n :=
    Const_Just (num_nat n: V);
  num_eq c1 c2 :=
    match c1, c2 with
    | Const_Bot, Const_Bot => true
    | Const_Just v1, Const_Just v2 => if v1 == v2 then true else false
    | Const_Top, Const_Top => true
    | _, _ => false
    end;

  num_istrue c :=
    match c with
    | Const_Just v => num_istrue v
    | _ => false
    end;
  num_isfalse c :=
    match c with
    | Const_Just v => num_isfalse v
    | _ => false
    end;

  num_unary op c :=
    match c with
    | Const_Just v => Const_Just (num_unary op v)
    | c => c
    end;

  num_binary op c1 c2 :=
    match c1, c2 with
    | Const_Bot, _ => Const_Bot
    | _, Const_Bot => Const_Bot
    | Const_Just v1, Const_Just v2 => Const_Just (num_binary op v1 v2)
    | _, _ => Const_Top
    end;
}.

#[global]
Instance AbsNumDomain_Const V UnOp BinOp `{EqDec V eq} `{NumDomain V UnOp BinOp}:
    AbsNumDomain (Const V) V UnOp BinOp.
Proof. constructor.
  (* num_nat_sound *)
  - now cbn.
  (* num_istrue_sound *)
  - intros []; cbn. discriminate. intros; now subst. discriminate.
  (* num_isfalse_sound *)
  - intros []; cbn. discriminate. intros; now subst. discriminate.

  (* num_unary_sound *)
  - intros []; cbn; auto. intros; now subst.
  (* num_binary_sound *)
  - intros [] []; cbn; auto. intros; now subst.
Defined.

Inductive IntvUnOp := IntvMinus.
Inductive IntvBinOp := IntvAdd | IntvSub.

#[global]
Instance NumDomain_Intv: NumDomain Intv IntvUnOp IntvBinOp := {
  num_nat n := Intervals.const (Z.of_nat n);
  num_eq := Intervals.eqb;
  num_istrue iv := negb (Intervals.inb 0 iv);
  num_isfalse := Intervals.eqb (Intervals.const 0);

  num_unary op :=
    match op with
    | IntvMinus => Intervals.opp
    end;
  num_binary op :=
    match op with
    | IntvAdd => Intervals.add
    | IntvSub => Intervals.sub
    end;
}.

#[global]
Instance NumDomain_Z: NumDomain Z IntvUnOp IntvBinOp := {
  num_nat := Z.of_nat;
  num_eq := Z.eqb;
  num_istrue iv := negb (Z.eqb 0 iv);
  num_isfalse := Z.eqb 0;

  num_unary op :=
    match op with
    | IntvMinus => Z.opp
    end;
  num_binary op :=
    match op with
    | IntvAdd => Z.add
    | IntvSub => Z.sub
    end;
}.

#[global]
Instance AbsNumDomain_Intv: AbsNumDomain Intv Z IntvUnOp IntvBinOp.
Proof. constructor.
  (* num_nat_sound *)
  - intros; cbn. apply Intervals.const_1.
  (* num_istrue_sound *)
  - intros * Htrue Hin; cbn in *. rewrite Bool.negb_true_iff in *.
    apply Intervals.inb_iff_false in Htrue.
    destruct v; auto || contradiction.
  (* num_isfalse_sound *)
  - intros * Hfalse Hin; cbn in *.
    apply Intervals.eqb_iff_true in Hfalse. rewrite <- Hfalse in Hin.
    apply Intervals.const_2 in Hin. now rewrite Hin.
  (* num_unary_sound *)
  - intros; destruct op.
    * now apply Intervals.opp_1.
  (* num_binary_sound *)
  - intros; destruct op.
    * now apply Intervals.add_1.
    * now apply Intervals.sub_1.
Defined.
