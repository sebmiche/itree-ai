From Coq Require Import
  Morphisms RelationClasses Classes.EquivDec Program.Equality Lists.List Bool
  Lia.
From ExtLib Require Import Structures.Monad.
From ITree Require Import
  ITree ITreeFacts Eq FailFacts Events.State StateFacts Props.Leaf
  Props.HasPost.
Import Basics.Monads.
From ITreeAI.Lattices Require Import
  Lattice LatticeVec LatticePprod LatticeOption LatticeConst LatticeList.
From ITreeAI Require Import
  AbstractMonad NumericalDomain Util Util.List FailA MonadExcept BooleanDomain.
From Equations Require Import Equations.

Import MonadNotation.
Import LeafNotations.
Import PprodNotations.
Import ListNotations.
Local Open Scope monad_scope.

(* BEGIN TODO MOVE *)

#[global] Instance EqDec_Fin {N}: EquivDec.EqDec (Fin.t N) eq := Fin.eq_dec.

(* Tail branch (TB) indicating program flow after executing one of the blocks
   of a local set of N mutually-recursive subprograms (i.e. a CFG). *)
Definition TailBranch (N: nat): Type := unitˣ * listˣ (Fin.t N).

Definition TailBranch_mk_ret {N}: TailBranch N :=
  (top, bot).

Definition TailBranch_mk_branch {N} (l: list (Fin.t N)): TailBranch N :=
  (bot, listˣ_enum l).

Definition TB_returns {N}: TailBranch N -> unitˣ := fst.
Arguments TB_returns {N} _ /.

Definition TB_branches {N}: TailBranch N -> listˣ (Fin.t N) := snd.
Arguments TB_branches {N} _ /.

(* Whether the branch may return. *)
Definition TB_may_return {N} (tb: TailBranch N): bool :=
  unitˣ_to_bool (TB_returns tb).

(* Whether the branch may jump to block [sub-program]. *)
Definition TB_may_branch_to {N} (tb: TailBranch N) (id: Fin.t N): bool :=
  listˣ_in id (TB_branches tb).

(* Add a return path if [err] is not bot. *)
Definition TB_add_fail_path {N} (tb: TailBranch N) (err: unitˣ):
    TailBranch N :=
  (fst tb ⊔ err, snd tb).
Definition TB_add_exception_path {N} (tb: TailBranch N) (err: bool):
    TailBranch N :=
  (if err then ttˣ else fst tb, snd tb).

Lemma TB_add_fail_path_1 {N} (TB: TailBranch N) err:
  TB_may_return TB = true -> TB_may_return (TB_add_fail_path TB err) = true.
Proof. destruct TB as [[] ?], err; auto. Qed.

Lemma TB_add_fail_path_2 {N} (TB: TailBranch N):
  TB_may_return (TB_add_fail_path TB ttˣ) = true.
Proof. destruct TB as [[] ?]; auto. Qed.

Lemma TB_branches_lble {N} (TB₁ TB₂: TailBranch N):
  TB₁ ⊆ TB₂ -> TB_branches TB₁ ⊆ TB_branches TB₂.
Proof.
  intros Hin. destruct TB₁, TB₂; unfold TB_branches; simpl snd.
  cbn in Hin. rewrite andb_true_iff in Hin. apply Hin.
Qed.

Lemma TB_branches_meet {N} (TB₁ TB₂: TailBranch N):
  TB_branches (TB₁ ⊓ TB₂) = TB_branches TB₁ ⊓ TB_branches TB₂.
Proof. destruct TB₁, TB₂; reflexivity. Qed.

Lemma TB_may_branch_to_lble {N} (TB₁ TB₂: TailBranch N) (l: Fin.t N):
  TB₁ ⊆ TB₂ ->
  TB_may_branch_to TB₁ l = true ->
  TB_may_branch_to TB₂ l = true.
Proof.
  intros Hin H. apply TB_branches_lble in Hin.
  eapply listˣ_in_lble; eauto.
Qed.

Lemma TB_may_branch_to_meet {N} (TB₁ TB₂: TailBranch N) (l: Fin.t N):
  TB_may_branch_to TB₁ l = true ->
  TB_may_branch_to TB₂ l = true ->
  TB_may_branch_to (TB₁ ⊓ TB₂) l = true.
Proof.
  unfold TB_may_branch_to. rewrite TB_branches_meet. apply listˣ_in_meet.
Qed.

Lemma TB_may_return_lble {N} (TB₁ TB₂: TailBranch N):
  TB₁ ⊆ TB₂ ->
  TB_may_return TB₁ = true ->
  TB_may_return TB₂ = true.
Proof. destruct TB₁ as [[]], TB₂ as [[]]; intuition. Qed.

Lemma TB_may_return_meet {N} (TB₁ TB₂: TailBranch N):
  TB_may_return TB₁ = true ->
  TB_may_return TB₂ = true ->
  TB_may_return (TB₁ ⊓ TB₂) = true.
Proof. destruct TB₁ as [[]], TB₂ as [[]]; intuition. Qed.
(* END TODO MOVE *)

(** ** Core definitions: aflow type, induction principle, constructors.

    In the abstract world, there are generally two categories of types:
    - The T-types (T₁, T₂, etc...) are source program types. They are pure
      values as far as the abstract interpreter is concerned, and they change
      when the source program adds more data by interpretation.
    - The U-types (U₁, U₂, etc...) are extensions of their respective T-types
      with internal information of the abstract interpreter, typically about
      what control flow paths the source program might take. The extension is
      introspectable so the intepreter knows what paths to join.

   Each program computation generally takes a T-type as input and returns a
   U-type. When the interpreter determines from the U-value that a certain
   control flow path can be taken, it applies a "step function" to strip
   control flow information and get a new T-value out to continue with some
   other part of the source program.

   Note that continuations in aflow are not source program computations; they
   contain abstract intepreter logic (such as calling the step functions),
   which is why they take the full U-value as argument. **)

Inductive TailMRecSpec := TailMRec_None | TailMRec_If.

(* aflow is a tree of control flow combinators (the so-called "primitive"
   ones). The semantics of these combinators are defined by individual files
   in the `Combinators/` folder, but they all have a common interface and a
   unified treatment under aflow.

   Note: we could (should) use each combinator's `Params` type as arguments to
   the constructors instead of exposing the details here. Having the full list
   of parameters explicitly in the definition of aflow is legacy baggage. *)
Unset Elimination Schemes.
Inductive aflow (E: Type -> Type) (R: Type): Type :=
  | aflow_Ret (r: R)
  | aflow_Vis {T} (e: E T) (k: T -> aflow E R)
  | aflow_Seq {U₁ T₁ U₂: Typeˣ}
      (f1: aflow E U₁) (f2: T₁ -> aflow E U₂)
      (step: U₁ -> T₁) (may_exit: U₁ -> bool)
      (merge: bool -> U₁ -> U₂ -> U₂)
      (k: U₂ -> aflow E R)
  | aflow_Fixpoint {T U₁ U₃ Value: Typeˣ} `{BooleanDomain Value}
      (body: T -> aflow E U₁) (step: U₁ -> Value * T * U₃)
      (init: T) (k: U₃ -> aflow E R)
  | aflow_TailMRec {T Tb U Ub: Typeˣ} (spec: TailMRecSpec) (N: nat)
      (scatter: T -> vec Tb N)
      (step: Ub -> TailBranch N * Tb * U)
      (blocks: vec (Tb -> aflow E Ub) N)
      (init: T)
      (k: U -> aflow E R).
Set Elimination Schemes.

Arguments aflow_Ret {E R}.
Arguments aflow_Vis {E R} {T}.
Arguments aflow_Seq {E R} {U₁ T₁ U₂}.
Arguments aflow_Fixpoint {E R} {T U₁ U₃ Value _}.
Arguments aflow_TailMRec {E R} {T Tb U Ub} spec N.

(* BEGIN TODO MOVE *)
Definition pointwise1 {T A: Type} (P: T -> Prop): (A -> T) -> Prop :=
  fun f => forall a, P (f a).
(* END TODO MOVE *)

(* Notation that invokes g explicitly because we'll use it in inductive
   function definitions, where higher-order calls are not allowed. *)
Notation " g ∘ f " := (fun x => g (f x))
  (at level 40, left associativity).

(* ========================================================================= *)

Section aflow_ind.
Variable (E : Type -> Type)
         (P : forall {R}, aflow E R -> Prop).

Hypothesis (Hret: forall {R} r,
  P (@aflow_Ret E R r)).
Hypothesis (Hvis: forall {R T} e k,
  pointwise1 P k ->
  P (@aflow_Vis E R T e k)).
Hypothesis (Hseq: forall {R U₁ T₁ U₂} f1 f2 step may_exit merge k,
  P f1 ->
  pointwise1 P f2 ->
  pointwise1 P k ->
  P (@aflow_Seq E R U₁ T₁ U₂ f1 f2 step may_exit merge k)).
Hypothesis (Hfixpoint: forall {R T U₁ U₃} {Value: Typeˣ}
    `{BooleanDomain Value} body step init k,
  pointwise1 P body ->
  pointwise1 P k ->
  P (@aflow_Fixpoint E R T U₁ U₃ Value _ body step init k)).
Hypothesis (Htailmrec: forall {R T Tb U Ub} spec N
                              scatter step blocks init k,
  pointwise1 P k ->
  vec_all (pointwise1 P) blocks ->
  P (@aflow_TailMRec E R T Tb U Ub spec N scatter step blocks init k)).

Fact Hcons {Tb Ub N} (f: Tb -> aflow E Ub) fs:
  pointwise1 P f ->
  vec_all (N := N) (pointwise1 P) fs ->
  vec_all (pointwise1 P) (vec_cons f fs).
Proof. intros. now simp vec_all. Qed.

Equations aflow_ind {R} (f: aflow E R): P f :=
  aflow_ind (aflow_Ret r) :=
    Hret r;
  aflow_ind (aflow_Vis e k) :=
    Hvis e k (fun t => aflow_ind (k t));
  aflow_ind (aflow_Seq f1 f2 step may_exit merge k) :=
    Hseq f1 f2 step may_exit merge k
      (aflow_ind f1)
      (aflow_ind ∘ f2)
      (aflow_ind ∘ k);
  aflow_ind (aflow_Fixpoint body step init k) :=
    Hfixpoint body step init k
      (aflow_ind ∘ body)
      (aflow_ind ∘ k);
  aflow_ind (aflow_TailMRec spec N scatter step blocks init k) :=
    Htailmrec spec N scatter step blocks init k
      (aflow_ind ∘ k)
      (aux blocks);
where aux {Tb Ub N} (v: vec (Tb -> aflow E Ub) N):
    vec_all (pointwise1 P) v :=
  aux vec_nil := I;
  aux (vec_cons f fs) := Hcons f fs (aflow_ind ∘ f) (aux fs).
End aflow_ind.

(* ========================================================================= *)

Fixpoint aflow_bind {E T R} (t: aflow E T) (k: T -> aflow E R): aflow E R :=
  match t with
  | aflow_Ret r =>
      k r
  | aflow_Vis e k' =>
      aflow_Vis e (fun x => aflow_bind (k' x) k)
  | aflow_Seq f1 f2 step may_exit merge k' =>
      aflow_Seq f1 f2 step may_exit merge (fun x => aflow_bind (k' x) k)
  | aflow_Fixpoint body step init k' =>
      aflow_Fixpoint body step init (fun x => aflow_bind (k' x) k)
  | aflow_TailMRec spec N scatter step blocks init k' =>
      aflow_TailMRec spec N scatter step blocks init
        (fun x => aflow_bind (k' x) k)
  end.

#[export] Instance Monad_aflow {E}: Monad (aflow E) := {
  ret _ := aflow_Ret;
  bind _ _ := aflow_bind;
}.

Definition aflow_map {E T R} (f: T -> R) (t: aflow E T): aflow E R :=
  aflow_bind t (fun x => ret (f x)).

Definition aflow_trigger' {E R} (e: E R) :=
  aflow_Vis e aflow_Ret.

Notation aflow_trigger e := (aflow_trigger' (subevent _ e)).

Definition aflow_seq  {E} {U₁ T₁ U₂: Typeˣ}
    (f1: aflow E U₁) (f2: T₁ -> aflow E U₂)
    (step: U₁ -> T₁) (may_exit: U₁ -> bool)
    (merge: bool -> U₁ -> U₂ -> U₂) :=
  aflow_Seq f1 f2 step may_exit merge ret.

Definition aflow_fixpoint {E} {T U₁ U₃ Value: Typeˣ} `{BooleanDomain Value}
    (body: T -> aflow E U₁) (step: U₁ -> Value * T * U₃) (x: T) :=
  aflow_Fixpoint body step x ret.

Definition aflow_tailmrec {E N} {T Tb U Ub: Typeˣ}
    spec scatter step blocks init :=
  @aflow_TailMRec E _ T Tb U Ub spec N
                  scatter step blocks init ret.

(** ** Non-trivial recursive aflow functions: interp & unfold *)

(* Computation of the abstract program resulting from a round of handling
   events into the identity monad. Since the identify monad doesn't change the
   execution environment, this basically threads the handler `h` to events and
   leaves everything else unchanged. *)
#[derive(eliminator=no)]
Equations aflow_interp {E F} (h: E ~> aflow F) {R}: aflow E R -> aflow F R :=
  aflow_interp h (aflow_Ret r) :=
    ret r;
  aflow_interp h (aflow_Vis e k) :=
    x <- h _ e;; aflow_interp h (k x);
  aflow_interp h (aflow_Seq f1 f2 step may_exit merge k) :=
    aflow_Seq (aflow_interp h f1)
              (aflow_interp h ∘ f2)
              step may_exit merge
              (aflow_interp h ∘ k);
  aflow_interp h (aflow_Fixpoint body step init k) :=
    aflow_Fixpoint (aflow_interp h ∘ body)
                   step init
                   (aflow_interp h ∘ k);
  aflow_interp h (aflow_TailMRec spec N scatter step blocks init k) :=
    aflow_TailMRec spec N scatter step
                   (vec_map (fun b => aflow_interp h ∘ b) blocks)
                   init
                   (aflow_interp h ∘ k).
Arguments aflow_interp {E F} h [R].

(* Computation of the abstract program resulting from a round of handling
   events into the state monad. This applies the handler `h` to events, and
   updates parameters according to each combinator's specifications otherwise.

   Note: despite appearances this is actually a boilerplate function as the new
   parameters for each combinator are given by `<combinator>ˣ_Params_stateT`
   from each combinator's file; it's copied here because legacy bad design. *)
#[derive(eliminator=no)]
Equations aflow_interp_state {E F} {S: Typeˣ}
    (h: E ~> stateT S (aflow F)) [R]: aflow E R -> stateT S (aflow F) R :=
  aflow_interp_state h (aflow_Ret r) :=
      fun s => aflow_Ret (s, r);
  aflow_interp_state h (aflow_Vis e k) :=
      fun s => '(s', x) <- h _ e s;; aflow_interp_state h (k x) s';
  aflow_interp_state h
    (@aflow_Seq _ _ U₁ T₁ U₂ f1 f2 step may_exit merge k) :=
      fun s => aflow_Seq (U₁ := S * U₁) (T₁ := S * T₁)
        (aflow_interp_state h f1 s)
        (fun '(s, t₁) => aflow_interp_state h (f2 t₁) s)
        (fun '(s, u₁) => (s, step u₁))
        (may_exit ∘ snd)
        (fun b '(su₁, u₁) '(su₂, u₂) =>
          (if b then su₁ ⊔ su₂ else su₂, merge b u₁ u₂))
        (fun '(s, x) => aflow_interp_state h (k x) s);
  aflow_interp_state h (@aflow_Fixpoint _ _ T U₁ U₃ _ _ body step init k') :=
      fun s => aflow_Fixpoint (T := S * T) (U₁ := S * U₁) (U₃ := S * U₃)
        (fun '(s, v) => aflow_interp_state h (body v) s)
        (fun '(s, u₁) =>
          let '(v, t, u₃) := step u₁ in (v, (s, t), (s, u₃)))
        (s, init)
        (fun '(s, x) => aflow_interp_state h (k' x) s);
  aflow_interp_state h
    (@aflow_TailMRec _ _ T Tb U Ub spec N scatter step blocks init k') :=
    fun s => aflow_TailMRec
      (T := S * T) (Tb := S * Tb) (U := S * U) (Ub := S * Ub) spec _
      (fun '(s, t₁) => vec_map (fun i => (s, i)) (scatter t₁))
      (fun '(s, o) => let '(tb, i, o) := step o in (tb, (s, i), (s, o)))
      (vec_map (fun block '(s, i) => aflow_interp_state h (block i) s) blocks)
      (s, init)
      (fun '(s, t₂) => aflow_interp_state h (k' t₂) s).

Arguments aflow_interp_state {E F S} h [R].

(* Adds a path from the first half of a sequence (returning eu) to the second
   path (returning et) if the first might fail. *)
Definition failT_post {U T: Typeˣ} (merge: bool -> U -> T -> T):
    bool -> optionˣ U -> optionˣ T -> optionˣ T :=
  fun b '(eu, u) '(et, t) =>
    match eu with
    | unitˣ_bot => (et, merge b u t)
    | ttˣ => (ttˣ, merge true u t)
    end.

(* Combine an error value into a pair. *)
(* TODO: failT_merge and failT_post are basically the same except that the
   latter tracks the existence of an error with a boolean. We might merge these
   when switching to a stronger exception monad. Should we just replace the
   bool with a lattice test [err ⊆? bot] (i.e. do we lose information if we
   don't keep the bool explicitly and look only at the value)? *)
Definition failT_merge {T U: Type} (et: unitˣ * T) (eu: unitˣ * U) :=
  (fst et ⊔ fst eu, snd eu).

(* Do that in a continuation. *)
Definition failT_kmerge {E} {T U: Type}
    (et: unitˣ * T) (f: aflow E (unitˣ * U)): aflow E (unitˣ * U) :=
  aflow_map (failT_merge et) f.

(* Computation of the abstract program resulting from a round of handling
   events into the failure monad. This applies the handler `h` to events, and
   updates parameters according to each combinator's specifications otherwise.

   Note: despite appearances this is actually a boilerplate function as the new
   parameters for each combinator are given by `<combinator>ˣ_Params_failT`
   from each combinator's file; it's copied here because legacy bad design. *)
#[derive(eliminator=no)]
Equations aflow_interp_fail {E F: Type -> Type}
    (h: E ~> failA (aflow F)) [R: Type]:
    aflow E R -> failA (aflow F) R :=
  aflow_interp_fail h (aflow_Ret r) :=
      ret (bot, r);
  aflow_interp_fail h (aflow_Vis e k) :=
      ex <- (h _ e: aflow _ _);;
      failT_kmerge ex (aflow_interp_fail h (k (snd ex)));
  aflow_interp_fail h (@aflow_Seq _ _ U₁ T₁ U₂ f1 f2 step may_exit merge k) :=
      aflow_Seq (U₁ := optionˣ U₁) (U₂ := optionˣ U₂)
                (aflow_interp_fail h f1)
                (aflow_interp_fail h (R := _) ∘ f2)
                (step ∘ snd)
                (fun '(et, t) => unitˣ_to_bool et || may_exit t)
                (failT_post merge)
                (fun ex => failT_kmerge ex (aflow_interp_fail h (k (snd ex))));
  aflow_interp_fail h (@aflow_Fixpoint _ _ T U₁ U₃ _ _ body step init k') :=
      aflow_Fixpoint (T := T) (U₁ := optionˣ U₁) (U₃ := optionˣ U₃)
        (fun u => aflow_interp_fail h (body u))
        (fun '(err, u₁) =>
          let '(v, t, u₃) := step u₁ in (v, t, (err, u₃)))
        init
        (fun ex => failT_kmerge ex (aflow_interp_fail h (k' (snd ex))));
  aflow_interp_fail h
    (@aflow_TailMRec _ _ T Tb U Ub spec N scatter step blocks init k') :=
    aflow_TailMRec (T := T) (Tb := Tb) (U := optionˣ U) (Ub := optionˣ Ub) spec
      _ scatter
      (* Important: before returning, we inspect the block's error value and
         add a return path to the TailBranch if an error might have occurred.
         This adds a control-flow edge based on data only! *)
      (fun '(err, ub) => let '(tb, i, o) := step ub in
        (TB_add_fail_path tb err, i, (err, o)))
      (vec_map (fun block => aflow_interp_fail h (R := _) ∘ block) blocks)
      init
      (fun et₂ => failT_kmerge et₂ (aflow_interp_fail h (k' (snd et₂)))).
Arguments aflow_interp_fail {E F} h [R].

(* Experiments with the exception monad (WIP).
   Ultimately the hope is to define ailTˣ := exceptTˣ unitˣ. *)

(* Adds a path from the first half of a sequence (returning eu) to the second
   path (returning et) if the first might fail. *)
Definition exceptT_post {U T Err: Typeˣ} {LL: LatticeLaws Err}
    (merge: bool -> U -> T -> T):
    bool -> Err * U -> Err * T -> Err * T :=
  fun b '(eu, u) '(et, t) =>
    (eu ⊔ et, merge (is_not_bot eu || b) u t).

(* Merge an error value into a pair. *)
Definition exceptT_merge {T U} {Err: Typeˣ} (et: Err * T) (eu: Err * U) :=
  (fst et ⊔ fst eu, snd eu).

(* Do that in a continuation. *)
Definition exceptT_kmerge {E T U} {Err: Typeˣ}
    (et: Err * T) (f: aflow E (Err * U)): aflow E (Err * U) :=
  aflow_map (exceptT_merge et) f.

#[derive(eliminator=no)]
Equations aflow_interp_except {E F: Type -> Type}
    {Err: Typeˣ} {LL: LatticeLaws Err}
    (h: E ~> exceptTˣ Err (aflow F)) [R: Type]:
    aflow E R -> exceptTˣ Err (aflow F) R :=
  aflow_interp_except h (aflow_Ret r) :=
      ret (⊥, r);
  aflow_interp_except h (aflow_Vis e k) :=
      ex <- (h _ e: aflow _ _);;
      exceptT_kmerge ex (aflow_interp_except h (k (snd ex)));
  aflow_interp_except h (@aflow_Seq _ _ U₁ T₁ U₂ f1 f2 step may_exit merge k) :=
      aflow_Seq (U₁ := Err * U₁) (U₂ := Err * U₂)
                (aflow_interp_except h f1)
                (aflow_interp_except h (R := _) ∘ f2)
                (step ∘ snd)
                (fun '(et, t) => is_not_bot et || may_exit t)
                (exceptT_post merge)
                (fun ex => exceptT_kmerge ex (aflow_interp_except h (k (snd ex))));
  aflow_interp_except h (@aflow_Fixpoint _ _ T U₁ U₃ _ _ body step init k') :=
      aflow_Fixpoint (T := T) (U₁ := Err * U₁) (U₃ := Err * U₃)
        (fun u => aflow_interp_except h (body u))
        (fun '(err, u₁) =>
          let '(v, t, u₃) := step u₁ in (v, t, (err, u₃)))
        init
        (fun ex => exceptT_kmerge ex (aflow_interp_except h (k' (snd ex))));
  aflow_interp_except h
    (@aflow_TailMRec _ _ T Tb U Ub spec N scatter step blocks init k') :=
    aflow_TailMRec (T := T) (Tb := Tb) (U := Err * U) (Ub := Err * Ub) spec _
      scatter
      (* Important: before returning, we inspect the block's error value and
         add a return path to the TailBranch if an error might have occurred.
         This adds a control-flow edge based on data only! *)
      (fun '(err, ub) => let '(tb, i, o) := step ub in
        (TB_add_exception_path tb (is_not_bot err), i, (err, o)))
      (vec_map (fun block => aflow_interp_except h (R := _) ∘ block) blocks)
      init
      (fun et₂ => exceptT_kmerge et₂ (aflow_interp_except h (k' (snd et₂)))).
Arguments aflow_interp_except {E F} {Err _} h [R].

Definition aflow_translate {E F R} (h: E ~> F) (f: aflow E R): aflow F R :=
  aflow_interp (fun _ e => aflow_trigger' (h _ e)) f.

Lemma aflow_translate_Ret {E F X} (rename : E ~> F) (r : X) : aflow_translate rename (aflow_Ret r) = aflow_Ret r.
Proof.
  reflexivity.
Qed.

Lemma aflow_translate_Vis {E F X Y} (rename : E ~> F) (e : E X)
    (k : X -> aflow E Y) :
  aflow_translate rename (aflow_Vis e k)
  = aflow_Vis (rename _ e) (fun x => aflow_translate rename (k x)).
Proof.
  reflexivity.
Qed.

(* Implements the basic fixpoint strategy. `aflow_fixbody body step` computes
   progressively larger inputs to the loop until it reaches a post-fixpoint.
   The `step` function is used to turn the output of an iteration into the
   input to the next. Once a fixpoint is reached, `aflow_fixbody` returns a
   pair (pfp_in, pfp_out) carrying both the fixpoint input and the
   corresponding output of the body. *)
Definition aflow_fixbody {E} {In Out: Typeˣ}
    (body: In -> itree E Out) (step: Out -> In) (x: In):
    itree E (In + In * Out) :=
  y <- body x;;
  let next_x := x ⊔ step y in
  ret (if next_x ⊆? x then inr (x, y) else inl (widen x next_x)).

(* In an aflow_TailMRec, compute the next input state for each block [id] by
   joining the output states of all blocks that may have jumped into [id]. *)
Definition aflow_mrec_step {N} {Tb Ub U: Typeˣ}
    (step: Ub -> TailBranch N * Tb * U) (ubs: vec Ub N): vec Tb N :=
  vec_initf N (fun id =>
    vec_foldr
      (fun ub combined =>
        let '(pred_branch, pred_tb, _) := step ub in
        if TB_may_branch_to pred_branch id then
          combined ⊔ pred_tb
        else
          combined)
      ubs bot).

(* In an aflow_TailMRec, compute the final state based on the PFP output. *)
Definition aflow_mrec_reduce {N} {Tb Ub U: Typeˣ}
    (step: Ub -> TailBranch N * Tb * U) (ubs: vec Ub N): U :=
  vec_foldr join
            (vec_map (fun ub => let '(branch, _, u) := step ub in
                       if TB_may_return branch then u else bot)
                     ubs)
            bot.

Lemma aflow_mrec_reduce_sound {N} {Tb Ub U: Typeˣ}
    (step: Ub -> TailBranch N * Tb * U) (ubs: vec Ub N) (i: Fin.t N)
    branch tb u:
  step (vec_nth ubs i) = (branch, tb, u) ->
  TB_may_return branch = true ->
  u ⊆ aflow_mrec_reduce step ubs.
Proof.
  intros Hstep Hmayreturn; unfold aflow_mrec_reduce.
  apply vec_foldr_iter_after with (index := i).
  - intros b. rewrite vec_nth_map, Hstep, Hmayreturn. apply join_l.
  - intros j b Hinv. eapply lle_trans. apply Hinv. apply join_r.
Qed.

(* Unfold the symbolic control flow combinators (aflow constructors) into an
   executable program (itree). We do this only at the end of event handling
   because event handling affects each combinator's parameters and we can't
   update the parameters after unfolding. This is where we replace each aflow
   constructor with an abstract interpretation algorithm.

   Ideally each combinator's file would provide the algorithm, but in this case
   it's actually a bit tricky to get the interface right. *)
Equations unfold {E R}: aflow E R -> itree E R :=
  unfold (aflow_Ret r) :=
      ret r;
  unfold (aflow_Vis e k) :=
      Vis e (fun x => unfold (k x));
  unfold (aflow_Seq f1 f2 step may_exit merge k) :=
      u₁ <- unfold f1;;
      u₂ <- unfold (f2 (step u₁));;
      unfold (k (merge (may_exit u₁) u₁ u₂));
  unfold (@aflow_Fixpoint _ _ T U₁ U₃ _ _ body step init k) :=
      '(pfp_in, pfp_u₁) <-
        (* TODO: Decidable conditions. So far we just ignore the Value part of
           the step function. *)
        ITree.iter (aflow_fixbody (fun x => unfold (body x))
                                  (fun u₁ => snd (fst (step u₁))))
                   init;;
      let '(pfp_value, pfp_next_t, pfp_out) := step pfp_u₁ in
      unfold (k pfp_out);
  unfold (aflow_TailMRec TailMRec_None N scatter step blocks init k) :=
      let ins := scatter init in
      (* Return both ins and outs to fit the aflow_fixbody pattern *)
      '(_, pfp_outs) <- ITree.iter (fun ins =>
        outs <- unfold_mapM blocks ins;;
        let next_ins := aflow_mrec_step step outs in
        ret (if ins ⊔ next_ins ⊆? ins
             then inr (ins, outs)
             else inl (widen ins (ins ⊔ next_ins)))
      ) ins;;
      unfold (k (aflow_mrec_reduce step pfp_outs));
  unfold (aflow_TailMRec TailMRec_If N scatter step blocks init k) :=
      outs <- unfold_mapM blocks (scatter init);;
      unfold (k (aflow_mrec_reduce step outs));
where unfold_mapM {E N} {Tb Ub: Typeˣ} (blocks_: vec (Tb -> aflow E Ub) N)
    (ins_: vec Tb N): itree E (vec Ub N) :=
  unfold_mapM vec_nil vec_nil :=
    ret vec_nil;
  unfold_mapM (vec_cons b1 blocks_) (vec_cons in1 ins_) :=
    x <- unfold (b1 in1);;
    xs <- unfold_mapM blocks_ ins_;;
    ret (vec_cons x xs).

(* Expose a useful view of unfold (aflow_FixbodyN) which avoids the internal
   sub-function and also introduces a suitable fixbody. *)

Derive Signature for vec.

Equations vec_sequenceM {N} {α: Type} {m} `{Monad m} (v: vec (m α) N):
    m (vec α N) :=
  vec_sequenceM vec_nil :=
    ret vec_nil;
  vec_sequenceM (vec_cons x xs) =>
    a <- x;; as_ <- vec_sequenceM xs;; ret (vec_cons a as_).

Lemma translate_vec_sequenceM : forall E F (rename : E ~> F) N α
    (t : LatticeVec.vec (itree E α) N),
  @translate E F rename _ (@vec_sequenceM N α (itree E) _ t)
  ≈ vec_sequenceM (LatticeVec.vec_map (translate rename (T := _)) t).
Proof.
  induction N as [| N IH].
  - intros; depelim t.
    simp vec_sequenceM.
    now cbn; rewrite translate_ret.
  - intros; depelim t.
    simp vec_map.
    simp vec_sequenceM.
    cbn; rewrite translate_bind.
    apply eutt_eq_bind; intros?.
    rewrite translate_bind, IH.
    apply eutt_eq_bind; intros?.
    now rewrite translate_ret.
Qed.

#[export]
Instance vec_sequenceM_Proper N α E :
  Proper (LatticeVec.vec_eq (eutt eq) ==> eutt eq)
         (@vec_sequenceM N α (itree E) _).
Proof.
  induction N.
  - intros v u eqv.
    depelim v; depelim u; reflexivity.
  - intros v u eqv.
    depelim v; depelim u.
    simp vec_sequenceM.
    apply eutt_eq_bind'. apply (eqv Fin.F1).
    intros ?.
    rewrite (IHN _ _ (fun i => eqv (Fin.FS i))).
    now apply eutt_eq_bind; intros ?.
Qed.

Lemma Leaf_sequenceM {N E R} (v: vec (itree E R) N) (r: vec R N):
  r ∈ vec_sequenceM v ->
  forall i, vec_nth r i ∈ vec_nth v i.
Proof.
  induction N; intros Hr i.
  - dependent destruction i.
  - depelim2 r v;
    simp vec_nth vec_sequenceM in *.
    inversion_Leaf_bind Hr tmp1 Htmp1.
    inversion_Leaf_bind Hr tmp2 Htmp2.
    inversion_Leaf_ret Hr. dependent destruction Hr.
    dependent destruction i; simp vec_nth.
Qed.

Definition aflow_runN {E N} {Tb Ub: Typeˣ}
    (blocks: vec (Tb -> itree E Ub) N) (ins: vec Tb N): itree E (vec Ub N) :=
  vec_sequenceM (vec_map2 id blocks ins).

Lemma unfold_unfold_mapM {E N} {Tb Ub: Typeˣ}:
  forall (blocks: vec (Tb -> aflow E Ub) N) (ins: vec Tb N),
     unfold_mapM blocks ins
     ≈ aflow_runN (vec_map (fun block i => unfold (block i)) blocks) ins.
Proof.
  induction N; intros blocks ins.
  - dependent destruction blocks. dependent destruction ins.
    reflexivity.
  - dependent destruction blocks. dependent destruction ins.
    unfold aflow_runN.
    unfold vec_map2; simp vec_map vec_combine vec_sequenceM; cbn.
    apply eutt_eq_bind; intros.
    apply eutt_eq_bind'; [|reflexivity].
    apply (IHN _ ins).
Qed.

Lemma unfold_tailmrec {E R N} {T Tb U Ub: Typeˣ}
    scatter step init (blocks: vec (Tb -> aflow E Ub) N) (k: U -> aflow E R):
  unfold (aflow_TailMRec (T := T) TailMRec_None N scatter step blocks init k) ≈
      '(_, pfp_outs) <-
        ITree.iter (aflow_fixbody
          (aflow_runN (vec_map (fun block => unfold ∘ block) blocks))
          (aflow_mrec_step step))
        (scatter init);;
      unfold (k (aflow_mrec_reduce step pfp_outs)).
Proof.
  simp unfold; cbn. unfold aflow_fixbody.
  apply eutt_eq_bind'; [|intros []; reflexivity].
  apply eutt_iter; intros ins; cbn.
  apply eutt_eq_bind'; [|reflexivity].
  apply unfold_unfold_mapM.
Qed.

Lemma unfold_trigger {E R} (e: E R):
  unfold (E := E) (aflow_trigger' e) ≈ ITree.trigger e.
Proof. reflexivity. Qed.

Lemma unfold_translate : forall E F X (c : aflow E X) (rename : E ~> F),
    unfold (aflow_translate rename c) ≈ translate rename (unfold c).
Proof.
  intros.
  induction c.
  - simp unfold; cbn; rewrite translate_ret; reflexivity.
  - unfold aflow_translate; simp aflow_interp; cbn; simp unfold; cbn.
    rewrite translate_vis.
    apply eqit_Vis; intros ?.
    apply H.
  - unfold aflow_translate; simp aflow_interp; cbn; simp unfold; cbn.
    rewrite translate_bind.
    apply eutt_eq_bind'; [apply IHc|]; intros.
    rewrite translate_bind.
    apply eutt_eq_bind'; [apply H|]; intros.
    apply H0.
  - unfold aflow_translate; simp aflow_interp; cbn; simp unfold; cbn.
    rewrite translate_bind.
    eapply eutt_eq_bind'.
    + rewrite InterpFacts.translate_iter.
      apply KTreeFacts.eutt_iter.
      intros ?. cbn.
      rewrite translate_bind.
      eapply eutt_eq_bind'.
      apply H0.
      intros ?.
      rewrite translate_ret; reflexivity.
    + intros [? u₁]. destruct (step u₁) as [[? ?] ?].
      apply H1.
  - unfold aflow_translate; simp aflow_interp; cbn.
    destruct spec.
    + rewrite !unfold_tailmrec; cbn.
      rewrite translate_bind. apply eutt_eq_bind'.
      { rewrite InterpFacts.translate_iter. apply KTreeFacts.eutt_iter.
        intros ins; cbn.
        rewrite translate_bind. apply eutt_eq_bind'.
        * unfold aflow_runN. rewrite translate_vec_sequenceM.
          apply vec_sequenceM_Proper. intros i; cbn.
          rewrite vec_nth_map2, !vec_nth_map, vec_nth_map2, vec_nth_map.
          rewrite vec_all_forall in H0. apply H0.
        * intros outs. now rewrite translate_ret. }
      intros [pfp_in pfp_out]; cbn. eauto.
    + simp unfold; cbn. rewrite translate_bind. apply eutt_eq_bind'.
      { rewrite ! unfold_unfold_mapM, vec_map_map.
        unfold aflow_runN; rewrite translate_vec_sequenceM.
        apply vec_sequenceM_Proper; intros i.
        rewrite vec_nth_map2, ! vec_nth_map, vec_nth_map2, vec_nth_map.
        rewrite vec_all_forall in H0. apply H0. }
      eauto.
Qed.

(** ** Fixpoint theorems (TODO: move) *)

Theorem iter_aflow_fixbody_pfp {E} {U T: Typeˣ}
    (body: U -> itree E T) (shuffle: T -> U):
  forall x pfp_in pfp_out,
    (pfp_in, pfp_out) ∈ ITree.iter (aflow_fixbody body shuffle) x ->
    inr (pfp_in, pfp_out) ∈ aflow_fixbody body shuffle pfp_in
    /\ x ⊆ pfp_in.
Proof.
  (* This is by well-founded induction on the initial state. *)
  induction x as [x Hwf] using (well_founded_induction widen_order_wf).
  intros * Hfp.

  (* We unfold the first iteration of the loop. *)
  rewrite unfold_iter in Hfp.
  inversion_Leaf_bind Hfp y_first Hfirst; cbn in Hfirst.
  inversion_Leaf_bind Hfirst y_body Hy_body.
  inversion_Leaf_ret Hfirst.
  case (x ⊔ shuffle y_body ⊆? x) eqn:Hpfp.

  (* If the loop finishes, compare the last round side-by-side with the extra
     body, then apply the monotony property. *)
  - inversion_Leaf_ret Hfp; cbn. split; [|apply lle_refl].
    eapply Leaf_bind; eauto.
    rewrite Hpfp; apply Leaf_Ret.
  (* Otherwise, carry the invariant over the next iteration *)
  - apply Leaf_Tau_inv in Hfp.
    assert (Hwo: widen_order (widen x (x ⊔ shuffle y_body)) x).
    { econstructor; split; [reflexivity|]. now apply Bool.not_true_iff_false. }
    pose proof (H := Hwf _ Hwo _ _ Hfp). split.
    * apply H.
    * eapply lle_trans. apply widen_l. apply H.
Qed.

Theorem unfold_aflow_fixpoint_pfp {E} {T U₁ U₃ Value: Typeˣ}
    `{BD: BooleanDomain Value}
    (body: T -> aflow E U₁) (step: U₁ -> Value * T * U₃):
  forall x pfp_out,
    pfp_out ∈ unfold (aflow_fixpoint body step x) ->
    exists pfp_in pfp_u₁,
    inr (pfp_in, pfp_u₁) ∈
      aflow_fixbody (fun x => unfold (body x))
                    (fun u₁ => snd (fst (step u₁)))
                    pfp_in
    /\ x ⊆ pfp_in
    /\ pfp_out = snd (step pfp_u₁).
Proof.
  intros x tmp_out. unfold aflow_fixpoint, ret. simp unfold.
  intros H. inversion_Leaf_bind H [pfp_in pfp_u₁] Hpfp.
  destruct (step pfp_u₁) as [[pfp_value pfp_next_t] pfp_out] eqn:Hu₁.
  simp unfold in H. inversion_Leaf_ret H.
  exists pfp_in, pfp_u₁. split; [|split].
  1, 2: eapply iter_aflow_fixbody_pfp; apply Hpfp.
  now rewrite Hu₁.
Qed.

Theorem unfold_aflow_tailmrec_pfp {E N} {T Tb U Ub: Typeˣ}
    scatter step (blocks: vec (Tb -> aflow E Ub) N):
  forall x y,
    y ∈ unfold (aflow_tailmrec (T := T) (U := U) TailMRec_None
                scatter step blocks x) ->
    exists pfp_ins pfp_outs,
    inr (pfp_ins, pfp_outs) ∈ aflow_fixbody
      (aflow_runN (vec_map (fun block i => unfold (block i)) blocks))
      (aflow_mrec_step step) pfp_ins
    /\ y = aflow_mrec_reduce step pfp_outs
    /\ (scatter x) ⊆ pfp_ins.
Proof.
  intros x y. unfold aflow_tailmrec. rewrite unfold_tailmrec.
  intros H. inversion_Leaf_bind H [pfp_in pfp_out] Hpfp.
  simp unfold in H. inversion_Leaf_ret H.
  apply iter_aflow_fixbody_pfp in Hpfp.
  exists pfp_in, pfp_out.
  split; [|split]; apply Hpfp || reflexivity.
Qed.

(** ** Equality for programs and vectors of programs *)

Unset Elimination Schemes.
Inductive aflow_eq {E R}: aflow E R -> aflow E R -> Prop :=
  | aflowEqRet (r: R):
      aflow_eq (aflow_Ret r) (aflow_Ret r)
  | aflowEqVis {T} e k k'
      (Hk: pointwise aflow_eq k k'):
      aflow_eq (@aflow_Vis E R T e k)
               (@aflow_Vis E R T e k')
  | aflowEqSeq {U₁ T₁ U₂} f1 f2 k f1' f2' k' step may_exit merge
      (Hf1: aflow_eq f1 f1')
      (Hf2: pointwise aflow_eq f2 f2')
      (Hk: pointwise aflow_eq k k'):
      aflow_eq (@aflow_Seq E R U₁ T₁ U₂ f1  f2  step may_exit merge k)
               (@aflow_Seq E R U₁ T₁ U₂ f1' f2' step may_exit merge k')
  | aflowEqFixpoint {T U₁ U₃} {Value: Typeˣ} `{BooleanDomain Value}
      body step k body' step' k' init
      (Hbody: pointwise aflow_eq body body')
      (Hstep: forall t, step t = step' t)
      (Hk: pointwise aflow_eq k k'):
      aflow_eq (@aflow_Fixpoint E R T U₁ U₃ Value _ body step init k)
               (@aflow_Fixpoint E R T U₁ U₃ Value _ body' step' init k')
  | aflowEqTailMRec {T Tb U Ub N}
        spec scatter step init blocks k blocks' k'
      (Hblocks: vec_eq (pointwise aflow_eq) blocks blocks')
      (Hk: pointwise aflow_eq k k'):
      aflow_eq (@aflow_TailMRec E R T Tb U Ub spec N
                  scatter step blocks init k)
               (aflow_TailMRec spec N scatter step blocks' init k').
Set Elimination Schemes.

Section aflow_eq_ind.
Variable (E: Type -> Type)
         (P: forall {R}, aflow E R -> aflow E R -> Prop).

Hypothesis (HEqRet: forall {R} r,
  P (@aflow_Ret E R r)
    (@aflow_Ret E R r)).
Hypothesis (HEqVis: forall {R T} e k k',
    pointwise aflow_eq k k' ->
    pointwise P k k' ->
  P (@aflow_Vis E R T e k)
    (@aflow_Vis E R T e k')).
Hypothesis (HEqSeq: forall {R U₁ T₁ U₂ f1 f2 k f1' f2' k'} step may_exit merge,
    aflow_eq f1 f1' ->
    P f1 f1' ->
    pointwise aflow_eq f2 f2' ->
    pointwise P f2 f2' ->
    pointwise aflow_eq k k' ->
    pointwise P k k' ->
  P (@aflow_Seq E R U₁ T₁ U₂ f1 f2 step may_exit merge k)
    (@aflow_Seq E R U₁ T₁ U₂ f1' f2' step may_exit merge k')).
Hypothesis (HEqFixpoint: forall {R T U₁ U₃} {Value: Typeˣ}
    `{BooleanDomain Value} {body body' step step' k k'} init,
    pointwise aflow_eq body body' ->
    pointwise P body body' ->
    pointwise eq step step' ->
    pointwise aflow_eq k k' ->
    pointwise P k k' ->
  P (@aflow_Fixpoint E R T U₁ U₃ Value _ body step init k)
    (@aflow_Fixpoint E R T U₁ U₃ Value _ body' step' init k')).
Hypothesis (HEqTailMRec:
    forall {R T Tb U Ub N blocks blocks' k k' spec} scatter step init,
    vec_eq (pointwise aflow_eq) blocks blocks' ->
    vec_eq (pointwise P) blocks blocks' ->
    pointwise aflow_eq k k' ->
    pointwise P k k' ->
  P (@aflow_TailMRec E R T Tb U Ub spec N scatter step blocks init k)
    (@aflow_TailMRec E R T Tb U Ub spec N scatter step blocks' init k')).

Equations aflow_eq_ind {R} {f₁ f₂: aflow E R} (Heq: aflow_eq f₁ f₂): P f₁ f₂ :=
  aflow_eq_ind (aflowEqRet r) :=
    HEqRet r;
  aflow_eq_ind (aflowEqVis _ k k' Hk) :=
    HEqVis _ k k' Hk (aflow_eq_ind ∘ Hk);
  aflow_eq_ind (aflowEqSeq f1 f2 k f1' f2' k' _ _ _ Hf1 Hf2 Hk):=
    HEqSeq _ _ _ Hf1 (aflow_eq_ind Hf1)
                 Hf2 (aflow_eq_ind ∘ Hf2)
                 Hk  (aflow_eq_ind ∘ Hk);
  aflow_eq_ind (aflowEqFixpoint body step k body' step' k' _ Hbody Hstep Hk) :=
    HEqFixpoint _ Hbody (aflow_eq_ind ∘ Hbody)
                  Hstep
                  Hk (aflow_eq_ind ∘ Hk);
  aflow_eq_ind (aflowEqTailMRec _ _ _ _ blocks blocks' k k' Hblocks Hk) :=
    HEqTailMRec _ _ _ Hblocks (fun i => aflow_eq_ind ∘ Hblocks i)
                      Hk (aflow_eq_ind ∘ Hk).
End aflow_eq_ind.

#[export, refine]
Instance Reflexive_aflow_eq {E R}: Reflexive (@aflow_eq E R) := {}.
Proof.
  intros f; induction f; constructor; auto.
  - intro. rewrite vec_all_forall in H0. apply H0.
Qed.

#[export, refine]
Instance Symmetric_aflow_eq {E R}: Symmetric (@aflow_eq E R) := {}.
Proof.
  intros f f' eq. induction eq; constructor; auto.
Qed.

#[export, refine]
Instance Transitive_aflow_eq {E R}: Transitive (@aflow_eq E R) := {}.
Proof.
  intros f₁ f₂ f₃ H H'; revert f₃ H'.
  induction H; intros; try dependent destruction H';
    try constructor; unfold pointwise in *; auto.
  - intros. rewrite <- Hstep. apply H2.
  - intros i te. apply H0, Hblocks.
Qed.

#[export]
Instance Equivalence_aflow_eq {E R}: Equivalence (@aflow_eq E R) := {}.

#[export]
Instance Proper_aflow_Seq {E R} {U₁ T₁ U₂: Typeˣ}:
  Proper (aflow_eq ==> pointwise aflow_eq ==> eq ==> eq ==> eq ==>
          pointwise aflow_eq ==> aflow_eq)
         (@aflow_Seq E R U₁ T₁ U₂).
Proof. repeat intro; subst; now constructor. Qed.

#[export]
Instance Proper_aflow_seq {E} {U₁ T₁ U₂: Typeˣ}:
  Proper (aflow_eq ==> pointwise aflow_eq ==> eq ==> eq ==> eq ==> aflow_eq)
         (@aflow_seq E U₁ T₁ U₂).
Proof. repeat intro; subst; now constructor. Qed.

#[export]
Instance Proper_aflow_Fixpoint {E R} {T U₁ U₃ Value: Typeˣ}
     `{BooleanDomain Value}:
  Proper (pointwise aflow_eq ==> pointwise eq ==> eq ==> pointwise aflow_eq ==>
          aflow_eq)
         (@aflow_Fixpoint E R T U₁ U₃ Value _).
Proof. repeat intro; subst; now constructor. Qed.

#[export]
Instance Proper_aflow_fixpoint {E} {T U₁ U₃ Value: Typeˣ}
     `{BooleanDomain Value}:
  Proper (pointwise aflow_eq ==> pointwise eq ==> eq ==> aflow_eq)
         (@aflow_fixpoint E T U₁ U₃ Value _).
Proof. repeat intro; subst; now constructor. Qed.

#[export]
Instance Proper_aflow_TailMRec {E R spec N} {T Tb U Ub: Typeˣ}:
  (* Everything fixed except blocks and the continuation *)
  Proper (eq ==> eq ==> vec_eq (pointwise aflow_eq) ==> eq
             ==> pointwise aflow_eq ==> aflow_eq)
         (@aflow_TailMRec E R T Tb U Ub spec N).
Proof. repeat intro; subst; now constructor. Qed.

#[export]
Instance Proper_aflow_tailmrec {E spec N} {T Tb U Ub: Typeˣ}:
  (* Everything fixed except blocks *)
  Proper (eq ==> eq ==> vec_eq (pointwise aflow_eq) ==> eq ==> aflow_eq)
         (@aflow_tailmrec E N T Tb U Ub spec).
Proof. repeat intro; subst; now constructor. Qed.


(** ** Properties of bind, unfold, inter wrt. equality *)

Lemma aflow_Vis_trigger {E T R} (e: E T) (k: T -> aflow E R):
  aflow_eq (aflow_Vis e k) (aflow_bind (aflow_trigger' e) k).
Proof. reflexivity. Qed.

#[export]
Instance Proper_aflow_eq_bind {E T R}:
  Proper (@aflow_eq E T ==> (eq ==> @aflow_eq E R) ==> @aflow_eq E R)
         aflow_bind.
Proof.
  intros ? ? Hu ? ? Hk; red in Hk.
  induction Hu; cbn; try constructor; unfold pointwise in *; auto.
Qed.

Theorem aflow_eq_bind' {E T R} u u' (k k': T -> aflow E R):
  aflow_eq u u' ->
  pointwise aflow_eq k k' ->
  aflow_eq (aflow_bind u k) (aflow_bind u' k').
Proof. intros; apply Proper_aflow_eq_bind; [auto|now intros ? ? ->]. Qed.

Theorem aflow_eq_bind {E T R} u (k k': T -> aflow E R):
  pointwise aflow_eq k k' ->
  aflow_eq (aflow_bind u k) (aflow_bind u k').
Proof. now apply aflow_eq_bind'. Qed.

Lemma aflow_bind_ret_l {E T R}
    (k: T -> aflow E R) (x: T):
  aflow_eq (aflow_bind (aflow_Ret x) k) (k x).
Proof. reflexivity. Qed.

Lemma aflow_bind_ret_r {E T} (f: aflow E T):
  aflow_eq (aflow_bind f (fun x => ret x)) f.
Proof. induction f; try constructor; auto || reflexivity. Qed.

Lemma failT_kmerge_bot {E T R} (t: T) (f: aflow E (optionˣ R)):
  aflow_eq (failT_kmerge (bot, t) f) f.
Proof.
  rewrite <- (aflow_bind_ret_r f) at 2. apply aflow_eq_bind. now intros [].
Qed.

Lemma failT_merge_bot_l {T R} (t: T) (er: optionˣ R):
  failT_merge (bot, t) er = er.
Proof. now destruct er. Qed.

Lemma failT_merge_bot_r {T R} (et: optionˣ T) (r: R):
  failT_merge et (bot, r) = (fst et, r).
Proof. now destruct et as [[] ?]. Qed.

Theorem unfold_bind {E T R} (f: aflow E T) (k: T -> aflow E R):
  unfold (aflow_bind f k) ≈ ITree.bind (unfold f) (fun x => unfold (k x)).
Proof.
  revert k; induction f; intros; cbn; simp unfold; cbn.
  - now rewrite bind_ret_l.
  - rewrite bind_vis. hnf in H. now setoid_rewrite H.
  - rewrite bind_bind. apply eutt_eq_bind; intros.
    rewrite bind_bind. apply eutt_eq_bind; intros; auto.
  - rewrite bind_bind. apply eutt_eq_bind; intros [].
    case step; cbn; intros [] ?; auto.
  - destruct spec; simp unfold; cbn.
    * rewrite bind_bind. apply eutt_eq_bind; intros []; auto.
    * rewrite bind_bind. now apply eutt_eq_bind.
Qed.

Lemma aflow_runN_eutt {E N} {Tb Ub: Typeˣ}
    {v₁ v₂: vec (Tb -> itree E Ub) N}:
  vec_eq (pointwise (eutt eq)) v₁ v₂ ->
  forall t, aflow_runN v₁ t ≈ aflow_runN v₂ t.
Proof.
  induction N; intros Heq t.
  - depelim3 v₁ v₂ t. reflexivity.
  - depelim3 v₁ v₂ t. unfold aflow_runN.
    unfold vec_map2 in *; simp vec_combine vec_map vec_sequenceM; cbn.
    apply eutt_eq_bind'. apply (Heq Fin.F1). intros.
    apply eutt_eq_bind'. apply IHN. intros i; apply (Heq (Fin.FS i)).
    reflexivity.
Qed.

#[export]
Instance Proper_aflow_map {E R T f}:
  Proper (@aflow_eq E R ==> @aflow_eq E T) (aflow_map f).
Proof.
  induction 1; cbn; try constructor; unfold pointwise in *; eauto.
Qed.

#[export]
Instance Proper_aflow_eq_unfold {E R}:
  Proper (@aflow_eq E R ==> eutt eq) unfold.
Proof.
  induction 1.
  1-5: simp unfold; cbn.
  - reflexivity.
  - hnf in H0. now setoid_rewrite H0.
  - apply eutt_eq_bind'; [easy|]; intros.
    apply eutt_eq_bind'; easy.
  - apply eutt_eq_bind'; cycle 1.
    { intros []. rewrite <- H2. case step. now intros [] ?. }
    apply eutt_iter; intro. cbn.
    hnf in H1; rewrite H1.
    apply eutt_eq_bind; intros.
    hnf in H2. now rewrite ! H2.
  - destruct spec.
    * rewrite !unfold_tailmrec.
      apply eutt_eq_bind'; [|now intros []].
      apply eutt_iter; intro ins.
      apply eutt_eq_bind'; [|reflexivity].
      apply aflow_runN_eutt; intros i te.
      rewrite !vec_nth_map. apply H0.
    * simp unfold.
      apply eutt_eq_bind'; [|auto].
      rewrite ! unfold_unfold_mapM; unfold aflow_runN.
      apply vec_sequenceM_Proper. intros i.
      rewrite ! vec_nth_map2, ! vec_nth_map. apply H0.
Qed.

Theorem eutt_unfold {E R} (f f': aflow E R):
  aflow_eq f f' -> unfold f ≈ unfold f'.
Proof. now intros ->. Qed.

Theorem aflow_eq_interp {E F R} (f f': aflow E R) (h: E ~> aflow F):
  aflow_eq f f' -> aflow_eq (aflow_interp h f) (aflow_interp h f').
Proof.
  induction 1;
    simp aflow_interp; cbn; try constructor; auto using aflow_eq_bind.
  - intros i te. rewrite ! vec_nth_map. apply H0.
Qed.

Theorem aflow_eq_interp_state {E F R} {S: Typeˣ}
  (f f': aflow E R) (h: E ~> stateT S (aflow F)) s:
  aflow_eq f f' ->
  aflow_eq (aflow_interp_state h f s) (aflow_interp_state h f' s).
Proof.
  intros Heq; revert s.
  induction Heq;
    intros; simp aflow_interp_state; cbn;
    constructor || apply aflow_eq_bind; auto; try now intros [].
  - intros []; hnf in H2; now rewrite H2.
  - intros i []. rewrite ! vec_nth_map. apply H0.
Qed.

Lemma aflow_eq_failT_kmerge {E T U}
    (et: unitˣ * T) (f f': aflow E (unitˣ * U)):
  aflow_eq f f' ->
  aflow_eq (failT_kmerge et f) (failT_kmerge et f').
Proof. intros Heq; now apply aflow_eq_bind'. Qed.

Theorem aflow_eq_interp_fail {E F R}
  (f f': aflow E R) (h: E ~> failA (aflow F)):
  aflow_eq f f' ->
  aflow_eq (aflow_interp_fail h f) (aflow_interp_fail h f').
Proof.
  induction 1; intros; simp aflow_interp_fail; cbn.
  - try constructor.
  - apply aflow_eq_bind. intro. apply aflow_eq_failT_kmerge, H0.
  - constructor; hnf; eauto using aflow_eq_failT_kmerge.
  - constructor; auto.
    * intros []. rewrite <- H2. case step; now intros [].
    * intros []; apply aflow_eq_failT_kmerge; auto.
  - constructor; [|intro; now apply aflow_eq_failT_kmerge].
    intros l i; cbn. rewrite ! vec_nth_map. apply H0.
Qed.

#[export]
Instance aflow_interp_cong_aflow_eq {E F} (h: E ~> aflow F) [R]:
  Proper (@aflow_eq E R ==> @aflow_eq F R) (aflow_interp h (R := R)).
Proof. repeat intro. now apply aflow_eq_interp. Qed.

#[export]
Instance aflow_interp_state_cong_aflow_eq {E F} {S: Typeˣ}
    (h: E ~> stateT S (aflow F)) [R]:
  Proper (@aflow_eq E R ==> eq ==> @aflow_eq F _)
         (aflow_interp_state h (R := R)).
Proof. repeat intro. subst. now apply aflow_eq_interp_state. Qed.

#[export]
Instance aflow_interp_fail_cong_aflow_eq {E F}
    (h: E ~> failA (aflow F)) [R]:
  Proper (@aflow_eq E R ==> @aflow_eq F _)
         (aflow_interp_fail h (R := R)).
Proof. repeat intro. subst. now apply aflow_eq_interp_fail. Qed.

Lemma aflow_bind_bind {E T U R}
    (u: aflow E T) (k: T -> aflow E U) (k': U -> aflow E R):
  aflow_eq
    (aflow_bind (aflow_bind u k) k')
    (aflow_bind u (fun x => aflow_bind (k x) k')).
Proof.
  induction u; intros; cbn; try constructor; unfold pointwise in *;
    reflexivity || auto.
Qed.

Lemma aflow_interp_bind {E F T R} (u: aflow E T) (k: T -> aflow E R)
    (h: E ~> aflow F):
  aflow_eq
    (aflow_interp h (aflow_bind u k))
    (aflow_bind (aflow_interp h u) (fun x => aflow_interp h (k x))).
Proof.
  revert k. induction u; intros; cbn; simp aflow_interp; cbn.
  - reflexivity.
  - rewrite aflow_bind_bind. now apply aflow_eq_bind.
  - constructor; unfold pointwise in *; reflexivity || auto.
  - constructor; unfold pointwise in *; reflexivity || auto.
  - constructor; unfold pointwise in *; reflexivity || auto.
Qed.

Lemma aflow_interp_state_bind {E F T R} {S: Typeˣ}
    (h: E ~> stateT S (aflow F))
    (u: aflow E T) (k: T -> aflow E R) s:
  aflow_eq
    (aflow_interp_state h (aflow_bind u k) s)
    (aflow_bind (aflow_interp_state h u s)
               (fun '(s, x) => aflow_interp_state h (k x) s)).
Proof.
  revert k s. induction u; intros; cbn; simp aflow_interp_state; cbn.
  - reflexivity.
  - rewrite aflow_bind_bind. apply aflow_eq_bind. now intros [].
  - constructor; reflexivity || now intros [].
  - constructor; reflexivity || now intros [].
  - constructor; reflexivity || now intros [].
Qed.

Lemma failT_merge_comm {T U R: Type}
    (et: optionˣ T) (eu: optionˣ U) (er: optionˣ R):
  failT_merge et (failT_merge eu er)
  = failT_merge (failT_merge et eu) er.
Proof. now destruct et as [[] ?], eu as [[] ?], er as [[] ?]. Qed.

(* A bit awkward as we have to specialize the continuation to include
   aflow_interp_fail. This is because we need to expose "fun ex =>" to get the
   second kmerge and then it becomes difficult to match the function body. *)
Lemma failT_kmerge_bind {E F} {X T R: Type} (ex: optionˣ X)
    (f: aflow F (optionˣ T)) (k: T -> aflow E R) (h: E ~> failA (aflow F)):
  aflow_eq
    (failT_kmerge ex (aflow_bind f (fun ex =>
      failT_kmerge ex (aflow_interp_fail h (k (snd ex))))))
    (aflow_bind (failT_kmerge ex f) (fun ex =>
      failT_kmerge ex (aflow_interp_fail h (k (snd ex))))).
Proof.
  unfold failT_kmerge, aflow_map.
  rewrite !aflow_bind_bind. apply aflow_eq_bind. intros []; cbn.
  rewrite !aflow_bind_bind. apply aflow_eq_bind. intros []; cbn.
  now rewrite failT_merge_comm.
Qed.

Lemma aflow_interp_fail_bind {E F T R}
    (h: E ~> failA (aflow F))
    (f: aflow E T) (k: T -> aflow E R):
  aflow_eq
    (aflow_interp_fail h (aflow_bind f k))
    (aflow_bind (aflow_interp_fail h f)
               (fun ex => failT_kmerge ex (aflow_interp_fail h (k (snd ex))))).
Proof.
  revert k. induction f; intros; cbn; simp aflow_interp_fail; cbn.
  - rewrite <- aflow_bind_ret_r at 1.
    apply aflow_eq_bind. now intros [].
  - rewrite aflow_bind_bind. apply aflow_eq_bind. intros [u ?]; cbn.
    rewrite H. unfold failT_kmerge, aflow_map. rewrite ! aflow_bind_bind.
    apply aflow_eq_bind. intros []. rewrite aflow_bind_bind; cbn.
    apply aflow_eq_bind. intros []. now destruct u, u0.
  - constructor; try reflexivity. intro; rewrite H0. apply failT_kmerge_bind.
  - constructor; try reflexivity. intro; rewrite H1. apply failT_kmerge_bind.
  - constructor; try reflexivity. intro; rewrite H. apply failT_kmerge_bind.
Qed.

(* --- *)

(* The traditional soundness predicate for abstract interpretation. Values
   computed by the abstract program should be related, by a Galois connection,
   with values computed by the concrete program. When using the monadic style,
   the final return value of programs include the entire state, error flags,
   etc. so we just need to check the final return value.

   Note: the "x ∈ p" in this definition is "p returns value x". *)
Definition sound {R1 R2 E1 E2} `{GaloisConnection R2 R1}
    (t1: itree E1 R1) (t2: aflow E2 R2) :=
  forall x1 x2, x1 ∈ t1 -> x2 ∈ unfold t2 -> galois_in x1 x2.

#[export] Instance sound_eutt {E1 E2 R1 R2} `{GaloisConnection R2 R1}:
  Proper (eutt eq ==> aflow_eq ==> iff) (@sound R1 R2 E1 E2 _ _).
Proof.
  intros t1 t1' Ht1 p2 p2' Hp2. split; intros Hsound x1 x2 Hx1 Hx2.
  * rewrite <- Ht1 in Hx1. rewrite <- Hp2 in Hx2. apply Hsound; auto.
  * rewrite Ht1 in Hx1. rewrite Hp2 in Hx2. apply Hsound; auto.
Qed.
