(* ** High-level abstract types and monads

This function defines the category Typeˣ of types equipped with lattices, and
builds up the monadic stack on top of it, including classes such as Eqˣ,
Functorˣ and Monadˣ.

Typeˣ is of course a record that carries a Type and a Lattice on it. But it
comes with pretty great automation and is basically transparently a type for
which there is a Lattice instance:

- A Typeˣ can be used where a Type is expected, or where a Lattice is expected;
- A Typeˣ counts as a Lattice instance for typeclass resolution;
- Any Type can be used where a Typeˣ is expected if a Lattice instance can be
  synthesized by typeclass resolution.
- Any Lattice can be used where a Typeˣ is expected.

Essentially if I planned correctly none of the Typeˣ constructors/projections
should ever need to be used. *)

From ITreeAI.Lattices Require Import 
  Lattice LatticePprod LatticeOption. 
From ITreeAI Require Import 
  Psum Util.
From Coq Require Import Program.Basics Morphisms RelationClasses.

Set Universe Polymorphism.

Import PprodNotations.
Import PsumNotations.
Local Open Scope program_scope.

(** * Typeˣ: sub-sort of types with lattices *)

(* This is equivalent to `sigT Lattice` but we use a custom definition because:
   - sigT is template polymorphic and I want full universe polymorphism;
   - This lets us define reverse coercions for transparency, see below;
   - It lets us have more custom Lattice inference. *)
#[projections(primitive=yes)]
Cumulative Record Typeˣ: Type := mkTypeˣ_nonCanonical {
  Typeˣ_Type: Type;
  Typeˣ_Lattice: Lattice Typeˣ_Type;
}.

(* This coercion means
   1. Forward direction: we can write functions that return T when (T: Typeˣ)
      and it will use the underlying type [Typeˣ_Type T]. This works in any
      context where a type is expected, because [Typeˣ_Type T: Type] is a type,
      but [T: Typeˣ: Type] isn't (it's a record).
   2. Reverse direction: we can use a Type directly where a Typeˣ is expected
      and it will synthesize the Typeˣ from typeclass resolution. (!!) *)
#[reversible]
Coercion Typeˣ_Type: Typeˣ >-> Sortclass.

(* This instance allows us to use Lattice and LatticeBase operations on values
   of a Typeˣ. *)
#[global]
Existing Instance Typeˣ_Lattice.

(* This coercion allows us to specify a Typeˣ where a Lattice is expected. In
   the reverse direction, it lets us use a Lattice to build a Typeˣ. *)
#[reversible]
Coercion Typeˣ_Lattice: Typeˣ >-> Lattice.

(* This coercion allows us to specify a Typeˣ where a LatticeBase is expected.
   Note: this is not automatic because Lattice_LatticeBase is a typeclass
   instance but not a coercion. I think I should keep it that way. *)
Definition Typeˣ_LatticeBase (T: Typeˣ): LatticeBase T :=
  (* - First T coerces to the Type, ie. Typeˣ_Type T;
     - Second T coerces to the Lattice instance, ie. Typeˣ_Lattice T.
     Not good style to use in other files but for fun here :) *)
  @Lattice_LatticeBase T T.
Coercion Typeˣ_LatticeBase: Typeˣ >-> LatticeBase.

Canonical Structure mkTypeˣ {T} (LT: Lattice T) := mkTypeˣ_nonCanonical T LT.

(* Interpret notation involving Typeˣ terms, such as X * Y, in type_scope. *)
Bind Scope type_scope with Typeˣ.

(**** Demo of typical uses cases, uncomment to test

(* A Type can be used where a Typeˣ is expected if a Lattice instance can be
   synthesized. This uses the reverse direction of the Typeˣ_Type coercion. *)
Type (unitˣ: Typeˣ).
  (* mkTypeˣ Lattice_unitˣ : Typeˣ *)

(* A Lattice can also be used to specify a Typeˣ if there would be ambiguities
   in typeclass resolution (typically multiple instances for one type). *)
Type (Lattice_unitˣ: Typeˣ).
  (* mkTypeˣ Lattice_unitˣ : Typeˣ *)

(* A Typeˣ can be used as a Type. This uses the forward direction of the
   Typeˣ_Type coercion. *)
Definition getBot (T: Typeˣ): T := bot.
Type getBot.
  (* getBot: forall T: Typeˣ, T *)

(* A Typeˣ also implicitly counts as an instance of the LatticeBase and Lattice
   classes. This uses the typeclass instance Typeˣ_Lattice. *)
Definition getJoin (T: Typeˣ): (T -> T -> T) := join.
Definition getWiden (T: Typeˣ): (T -> T -> T) := widen.
Type getJoin.
  (* getJoin: forall T: Typeˣ, T -> T -> T *)
Type getWiden.
  (* getWiden: forall T: Typeˣ, T -> T -> T *)

(* We can specify a Typeˣ where a Lattice is expected. This uses the forward
   directions of the Typeˣ_Lattice and Typeˣ_LatticeBase coercions. *)
Definition getJoin' (T: Typeˣ): (T -> T -> T) := join (LatticeBase := T).
Definition getWiden' (T: Typeˣ): (T -> T -> T) := widen (Lattice := T).
Type getJoin'.
  (* getJoin': forall T: Typeˣ, T -> T -> T *)
Type getWiden'.
  (* getWiden': forall T: Typeˣ, T -> T -> T *)

****)


(** * Non-trivial equality on abstract type families *)

Class Eqˣ (F: Typeˣ -> Type) :=
  eqˣ: forall A, F A -> F A -> Prop.

Arguments eqˣ {F _} {A}.
Infix "≈ˣ" := eqˣ (at level 70): monad_scope.

Class EqˣEquivalence (F: Typeˣ -> Type) {EQ: Eqˣ F}: Prop :=
  eqˣ_equiv :: forall A, Equivalence (@eqˣ _ _ A).

Notation "E ~>ˣ F" := (forall (T: Typeˣ), E T -> F T)
  (at level 99, right associativity, only parsing): type_scope.


(** * Functors on abstract types *)

Class Functorˣ (F: Typeˣ -> Type) :=
  fmapˣ: forall {A B: Typeˣ}, (A -> B) -> (F A -> F B).
Arguments fmapˣ {F _} {A B}.

(* TODO: ∘ (compose) is not universe polymorphic, dropping FunctorˣLaws *)
Class FunctorˣLaws (F: Typeˣ -> Type) {FF: Functorˣ F} {EQF: Eqˣ F}: Prop := {
  fmapˣ_id {A: Typeˣ}:
    fmapˣ (@id A) = id;
  fmapˣ_compose: forall {A B C: Typeˣ} (f: A -> B) (g: B -> C),
    fmapˣ (F := F) (g ∘ f) = fmapˣ g ∘ fmapˣ f;
}.


(** * Monads on abstract types

Interestingly we do not have an Applicative layer because lattices on functions
are kind of strange beasts so we don't have any default instance. *)

Class Monadˣ (m: Typeˣ -> Type) := {
  retˣ: forall {T: Typeˣ}, T -> m T;
  bindˣ: forall {T U: Typeˣ}, m T -> (T -> m U) -> m U;
}.

Definition mcomposeˣ (m: Typeˣ -> Type) {M: Monadˣ m} {T U V: Typeˣ}
    (f: T -> m U) (g: U -> m V) (x: T): m V :=
  bindˣ (f x) g.

Module MonadˣNotations.
  Notation "f <$>ˣ x" := (fmapˣ f x)
    (at level 59, right associativity): monad_scope.
  Notation "x >>=ˣ k" := (bindˣ x k)
    (at level 58, left associativity): monad_scope.
  Notation "f >=>ˣ g" := (mcomposeˣ f g)
    (at level 61, right associativity): monad_scope.
  Notation "k =<<ˣ x" := (bindˣ x k)
    (at level 61, right associativity): monad_scope.
  Notation "v <-ˣ x ;; y" := (bindˣ x (fun v => y))
    (at level 61, x at next level, right associativity): monad_scope.
  Notation "' pat <-ˣ x ;; y" :=
    (bindˣ x (fun v => match v with | pat => y end))
    (at level 61, pat pattern, x at next level, right associativity): monad_scope.
End MonadˣNotations.

Import MonadˣNotations.
Local Open Scope monad_scope.

Definition liftMˣ {m: Typeˣ -> Type} {M: Monadˣ m} {T U: Typeˣ}
    (f: T -> U) (x: m T): m U :=
  x >>=ˣ (fun t => retˣ (f t)).

Definition liftM2ˣ {m: Typeˣ -> Type} {M: Monadˣ m} {T U V: Typeˣ}
    (f: T -> U -> V) (x: m T) (y: m U): m V :=
  t <-ˣ x;;
  u <-ˣ y;;
  retˣ (f t u).

Definition liftM3ˣ {m: Typeˣ -> Type} {M: Monadˣ m} {T U V W: Typeˣ}
    (f: T -> U -> V -> W) (x: m T) (y: m U) (z: m V): m W :=
  t <-ˣ x;;
  u <-ˣ y;;
  v <-ˣ z;;
  retˣ (f t u v).

#[global]
Instance Functorˣ_Monadˣ (m: Typeˣ -> Type) {M: Monadˣ m}: Functorˣ m := {
  fmapˣ _ _ := liftMˣ;
}.

#[global]
Instance Proper_retˣ {m} `{Monadˣ m} `{Eqˣ m} `{EqˣEquivalence m} {A: Typeˣ}:
  Proper (@eq A ==> eqˣ) retˣ.
Proof.
  repeat intro. now subst.
Qed.

#[global]
Instance Proper_bindˣ_1 {m} `{Monadˣ m} `{Eqˣ m} `{EqˣEquivalence m}
    {A B: Typeˣ}:
  Proper (eq ==> eq ==> eqˣ) (@bindˣ _ _ A B).
Proof.
  repeat intro. now subst.
Qed.

Class MonadˣLaws (m: Typeˣ -> Type) {M: Monadˣ m} {EQm: Eqˣ m}: Prop := {
  bind_ret_lˣ: forall {A B: Typeˣ} (f: A -> m B) (x: A),
    bindˣ (retˣ x) f ≈ˣ f x;
  bind_ret_rˣ: forall {A: Typeˣ} (x: m A),
    bindˣ x (fun y => retˣ y) ≈ˣ x;
  bind_bindˣ: forall {A B C: Typeˣ} (x: m A) (f: A -> m B) (g: B -> m C),
    bindˣ (bindˣ x f) g ≈ˣ bindˣ x (fun y => bindˣ (f y) g);
  Proper_bindˣ: forall {A B: Typeˣ},
    (@Proper (m A -> (A -> m B) -> m B)
             (eqˣ ==> pointwise_relation _ eqˣ ==> eqˣ)
             bindˣ);
}.
Arguments bind_ret_lˣ {m _ _ _} {A B}.
Arguments bind_ret_rˣ {m _ _ _} {A}.
Arguments bind_bindˣ {m _ _ _} {A B C}.
Arguments Proper_bindˣ {m _ _ _} {A B} {x y} _ {x y} _.

#[global]
Existing Instance Proper_bindˣ.

Lemma eqˣ_bindˣ' {m} {EQm: Eqˣ m} {M: Monadˣ m} {Lm: MonadˣLaws m} {T U: Typeˣ}:
  forall {x y: m T} (Hxy: x ≈ˣ y)
         {kx ky: T -> m U} (Hkxky: forall (t: T), kx t ≈ˣ ky t),
  bindˣ x kx ≈ˣ bindˣ y ky.
Proof.
  now apply @Proper_bindˣ.
Qed.

Lemma eqˣ_bindˣ {m} {EQm: Eqˣ m} {EQE: EqˣEquivalence m}
                {M: Monadˣ m} {Lm: MonadˣLaws m} {T U: Typeˣ}:
  forall {x: m T}
         {k₁ k₂: T -> m U} (Hkxky: forall (t: T), k₁ t ≈ˣ k₂ t),
  bindˣ x k₁ ≈ˣ bindˣ x k₂.
Proof.
  intros; now apply eqˣ_bindˣ'.
Qed.


(** * Iterative monads *)

(* Note: This is one of few constructions where we'd like the full monadic
   power of working with any type. [I + R] doesn't have a very natural lattice,
   so we use a trivial one where everyone is top. *)
Class MonadIterˣ (m: Typeˣ -> Type) :=
  iterˣ: forall {R I: Typeˣ},
                (I -> m (Lattice_trivial (I +++ R) (pinl top))) -> I -> m R.


(** * Abstract monad transformers *)

Class MonadTransˣ
      (tm: Typeˣ -> Type) (m: Typeˣ -> Type): Type :=
    liftˣ: forall {T: Typeˣ}, m T -> tm T.

(** * Abstract monad transformers: stateTˣ *)

Class MonadStateˣ (S: Typeˣ) (m: Typeˣ -> Type) := {
  mgetˣ: m S;
  mputˣ: S -> m unitˣ;
}.
Arguments mgetˣ {S m _}.
Arguments mputˣ {S m _}.

Definition mmodifyˣ {S m} {M: Monadˣ m} {MS: MonadStateˣ S m}
    (f: S -> S): m S :=
  x <-ˣ mgetˣ;;
  _ <-ˣ mputˣ (f x);;
  retˣ x.

Definition mgetsˣ {S m} {M: Monadˣ m} {MS: MonadStateˣ S m}
    {T: Typeˣ} (f: S -> T): m T :=
  f <$>ˣ mgetˣ.

(* stateT, and with polymorphic products, thank you very much *)
Definition stateTˣ (S: Typeˣ) (m: Typeˣ -> Type) (T: Typeˣ) :=
  S -> m (S ** T).

Definition runStateˣ {S m T} (x: stateTˣ S m T): S -> m (S ** T) := x.

Definition liftStateˣ {S F} {FF: Functorˣ F} {T} (x: F T): stateTˣ S F T :=
  fun s => ppair s <$>ˣ x.

#[global]
Instance MonadTransˣ_stateTˣ {S m} {M: Monadˣ m}:
    MonadTransˣ (stateTˣ S m) m := {
  liftˣ := @liftStateˣ _ _ _;
}.

#[global]
Instance Functorˣ_stateTˣ {F S} {FF: Functorˣ F}:
    Functorˣ (stateTˣ S F) := {
  fmapˣ _ _ f := fun x s => fmapˣ (fun st => ⦇pfst st, f (psnd st)⦈) (x s)
}.

#[global]
Instance Monadˣ_stateTˣ {m S} {M: Monadˣ m}:
    Monadˣ (stateTˣ S m) := {
  retˣ _ r := fun s => retˣ ⦇s, r⦈;
  bindˣ _ _ x k := fun s =>
    st <-ˣ x s;;
    k (psnd st) (pfst st);
}.

#[global]
Instance Eqˣ_stateTˣ {m S} {EQm: Eqˣ m}: Eqˣ (stateTˣ S m) := {
  eqˣ _ x₁ x₂ := pointwise eqˣ x₁ x₂;
}.

#[global]
Instance EqˣEquivalence_stateTˣ {m S} {EQm: Eqˣ m} {EQE: EqˣEquivalence m}:
    EqˣEquivalence (stateTˣ S m) := {}.

#[global, refine]
Instance MonadˣLaws_stateTˣ {S m} {EQm: Eqˣ m} {EEm: EqˣEquivalence m}
                            {M: Monadˣ m} {Lm: MonadˣLaws m}:
    MonadˣLaws (stateTˣ S m) := {}.
Proof.
  - intros. intro s; cbn. now rewrite bind_ret_lˣ.
  - intros. intro s; cbn. setoid_rewrite ppair_canon. apply bind_ret_rˣ.
  - intros. intro s; cbn. now rewrite bind_bindˣ.
  - repeat intro; cbn. apply eqˣ_bindˣ'; auto.
    intro sa. apply (H0 (psnd sa)).
Qed.

#[global]
Instance MonadItemˣ_stateTˣ {S m} {M: Monadˣ m} {MI: MonadIterˣ m}:
    MonadIterˣ (stateTˣ S m) := {
  iterˣ _ _ step i s :=
    iterˣ (m := m) (fun si =>
      s_ir <-ˣ step (psnd si) (pfst si);;
      retˣ match psnd s_ir with
           | pinl i' => pinl ⦇pfst s_ir, i'⦈
           | pinr r  => pinr ⦇pfst s_ir, r⦈
           end
    ) ⦇s,i⦈;
}.


(** * Abstract monad transformers: failTˣ *)

Definition failTˣ (m: Typeˣ -> Type) (T: Typeˣ) :=
  m (unitˣ ** T).

Definition runFailTˣ {m T} (x: failTˣ m T): m (unitˣ ** T) := x.

Definition liftFailˣ {F} {FF: Functorˣ F} {T} (x: F T): failTˣ F T :=
  ppair bot <$>ˣ x.

#[global]
Instance MonadTransˣ_failTˣ {m} {M: Monadˣ m}:
    MonadTransˣ (failTˣ m) m := {
  liftˣ := @liftFailˣ _ _;
}.

#[global]
Instance Functorˣ_failTˣ {F} {FF: Functorˣ F}:
    Functorˣ (failTˣ F) := {
  fmapˣ _ _ f := fmapˣ (fun et => ⦇pfst et, f (psnd et)⦈)
}.

#[global]
Instance Monadˣ_failTˣ {m} {M: Monadˣ m}:
    Monadˣ (failTˣ m) := {
  retˣ _ r := retˣ (m := m) ⦇bot, r⦈;
  bindˣ _ _ x k :=
    bindˣ (m := m) x
          (fun y => bindˣ (m := m) (k (psnd y))
                          (fun z => retˣ ⦇pfst y ⊔ pfst z, psnd z⦈));
}.

#[global]
Instance Eqˣ_failTˣ {m} {EQm: Eqˣ m}: Eqˣ (failTˣ m) := {
  eqˣ _ x₁ x₂ := eqˣ (F := m) x₁ x₂;
}.

#[global, refine]
Instance EqˣEquivalence_failTˣ {m} {EQm: Eqˣ m} {EQE: EqˣEquivalence m}:
    EqˣEquivalence (failTˣ m) := {}.
Proof.
  intros. apply (eqˣ_equiv (F := m)).
Qed.

#[global, refine]
Instance MonadˣLaws_failTˣ {m} {EQm: Eqˣ m} {EEm: EqˣEquivalence m}
                           {M: Monadˣ m} {Lm: MonadˣLaws m}:
    MonadˣLaws (failTˣ m) := {}.
Proof.
  - intros; cbn. rewrite bind_ret_lˣ; cbn.
    setoid_rewrite ppair_canon. now setoid_rewrite bind_ret_rˣ.
  - intros; cbn. setoid_rewrite bind_ret_lˣ; cbn.
    rewrite <- (bind_ret_rˣ (m := m) x) at 2.
    apply (eqˣ_bindˣ (m := m)). now intros [[] ?].
  - intros; cbn.
    rewrite ! bind_bindˣ. apply (eqˣ_bindˣ (m := m)). intros [ea a]; cbn.
    rewrite ! bind_bindˣ. apply (eqˣ_bindˣ (m := m)). intros [eb b]; cbn.
    rewrite ! bind_bindˣ, ! bind_ret_lˣ; cbn.
      apply (eqˣ_bindˣ (m := m)). intros [ec c]; cbn.
    rewrite bind_ret_lˣ. now destruct ea, eb, ec.
  - repeat intro; cbn.
    apply (eqˣ_bindˣ' (m := m) H). intros [ea a].
    unshelve eapply (eqˣ_bindˣ' (m := m) _); [eauto|]. intros [eb b].
    destruct ea; cbn; unfold "≈ˣ"; reflexivity.
Qed.

#[global]
Instance MonadIterˣ_failTˣ {m} {M: Monadˣ m} {MI: MonadIterˣ m}:
    MonadIterˣ (failTˣ m) := {
  iterˣ R I step i :=
    iterˣ (m := m) (fun ei =>
      bindˣ (m := m) (step (psnd ei))
                     (fun e_jr => match psnd e_jr with
                                  | pinl j => retˣ (pinl ⦇pfst ei ⊔ pfst e_jr, j⦈)
                                  | pinr r => retˣ (pinr ⦇pfst ei ⊔ pfst e_jr, r⦈)
                                  end)
    ) ⦇bot, i⦈;
}.
