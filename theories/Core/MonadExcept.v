From Paco Require Import paco.
From ITreeAI Require Import Lattice LatticePprod LatticeOption AbstractMonad.
From ITree Require Import
  Basics CategoryOps CategoryKleisli CategoryTheory Monad ITree
  Events.FailFacts.
From ExtLib Require Import Functor.
Import Basics.Monads.
Import MonadNotation.
Import PprodNotations.
Local Open Scope monad_scope.

Definition exceptTˣ (err: Typeˣ) (m : Type -> Type) : Type -> Type :=
  fun R => m ((err * R)%type).

#[global] Instance exceptTˣ_monad {err: Typeˣ} {m} `{Monad m} :
  Monad (exceptTˣ err m) :=
  {|
    ret _ x := ret (bot, x);
    bind _ _ x k :=
      bind (m := m) x
        (fun y => bind (m := m) (k (snd y))
                 (fun z => ret (fst y ⊔ fst z, snd z)))
  |}.

#[export] Instance Functor_exceptTˣ {err: Typeˣ} {m : Type -> Type}
    `{Functor m} : Functor (exceptTˣ err m) :=
  {| fmap _ _ f := fmap (fun '(x, y) => (x, f y)) |}.

Section ExceptT.

  Variable m : Type -> Type.
  Context {EQm : Eq1 m}.
  Context {Hm: Monad m}.
  Context {HEQP: @Eq1Equivalence m _ EQm}.
  Context {mL: @MonadLawsE m _ Hm}.

  Global Instance Eq1_exceptTˣ {err} : Eq1 (exceptTˣ err m).
  Proof.
    refine (fun a => eq1 (M := m)).
  Defined.

  Global Instance Eq1Equivalence_exceptTˣ {err}:
    @Eq1Equivalence (exceptTˣ err m) _ Eq1_exceptTˣ.
  Proof.
  constructor.
  - repeat red.
    reflexivity.
  - repeat red. intros. symmetry. apply H.
  - repeat red. intros. etransitivity; eauto.
  Qed.

  Instance MonadLawsE_exceptTˣ {err: Typeˣ} {LL: LatticeLaws err}:
    @MonadLawsE (exceptTˣ err m) _ _.
  Proof.
  constructor.
  - cbn. intros a b f x.
    rewrite bind_ret_l.
    cbn.
    pose proof bind_ret_r (M := m) _ (f x).
    rewrite <- H at 2.
    apply (Proper_bind (M := m)).
    reflexivity.
    intros []; cbn. now rewrite join_bot_l.
  - cbn. intros a x. setoid_rewrite Monad.bind_ret_l.
    pose proof Monad.bind_ret_r (M := m) _ x.
    rewrite <- H at 2.
    apply (Proper_bind (M := m)).
    reflexivity.
    intros []; cbn. now rewrite join_bot_r.
  - cbn. intros a b c x f g.
    rewrite !bind_bind.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_bind.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_ret_l, !bind_bind.
    cbn.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_ret_l.
    cbn. now rewrite join_assoc.
  - repeat red. intros a b x y H x0 y0 H0.
    cbn; apply Proper_bind.
    + apply H.
    + intros []; cbn.
      cbn; apply Proper_bind.
      apply H0.
      intros []; cbn.
      reflexivity.
  Qed.

  Section Iter1.

    Context {CM': MonadIter m}.
    Context {CM: Iterative (Kleisli m) sum}.

    Definition iter_exceptTˣ {err} : MonadIter (exceptTˣ err m) :=
      fun [R I] body i =>
        iter (Iter := @Iter_Kleisli m _)
          (fun '(x,i) => bind (m := m) (body i)
                        (fun '(y,jr) => match jr with
                                     | inl j => ret (inl (x ⊔ y, j))
                                     | inr r => ret (inr (x ⊔ y, r))
                                     end))
          (bot, i).

    Arguments iter_exceptTˣ [_ _].

  End Iter1.

End ExceptT.

From ITree Require Import ITree Eq KTree KTreeFacts.

Lemma iter_exceptTˣ_unfold_aux {E I R} {Err: Typeˣ} {LL: LatticeLaws Err}
  (body: I -> itree E (Err * (I + R)))
  (l: Err) (i0: I):
  ITree.iter
    (fun '(x, i1) =>
       pat0 <- body i1;;
       match pat0 with
       | (y, inl j) => ret (inl (x ⊔ y, j))
       | (y, inr r) => ret (inr (x ⊔ y, r))
       end) (l, i0)
    ≅ z <-
    ITree.iter
      (fun '(x, i1) =>
         pat0 <- body i1;;
         match pat0 with
         | (y, inl j) => ret (inl (x ⊔ y, j))
         | (y, inr r) => ret (inr (x ⊔ y, r))
         end) (bot, i0);; ret (l ⊔ fst z, snd z).
Proof.
  cbn.
  match goal with
  | |- ITree.iter ?BODY1 (?X1, ?Y1) ≅
        ITree.bind (ITree.iter ?BODY2 ?I2) ?K =>
      set (body1 := BODY1); set (body2 := BODY2); set (k := K)
  end.
  rewrite <- bind_ret_r at 1.
  set (RR := fun (rl rr: Err * R) => ret rl ≅ k rr).
  set (RI := fun (il ir: Err * I) =>
               fst il = l ⊔ fst ir /\ snd il = snd ir).
  apply eq_itree_clo_bind with (UU := RR).
  - apply eq_itree_iter' with (RI := RI).
    * intros [err1 j1] [err2 j2] HRI. subst RI; cbn in *.
      apply eq_itree_clo_bind with (UU := eq). now destruct HRI as [_ ->].
      intros [? []] [? []] H; cbn in *; inversion_clear H.
      all: apply eqit_Ret; cbn; constructor; cbn.
      { destruct HRI as [-> ->]. split; [|auto].
        apply join_assoc. }
      { destruct HRI as [-> ->]. subst RR k; cbn. apply eqit_Ret.
        now rewrite join_assoc. }
    * subst RI; cbn. split; [|auto]. now rewrite join_bot_r.
  - subst RR; now intros [] [].
Qed.

Opaque join.
Lemma iter_exceptTˣ_unfold {err: Typeˣ} {LL: LatticeLaws err} {E} {R I: Type}
  body i :
  iter_exceptTˣ (itree E) R I body i ≅
    bind (m := exceptTˣ err _) (body i)
    (fun jr => match jr with
            | inl j => Tau (iter_exceptTˣ _ _ _ body j)
            | inr r => ret r
            end).
Proof.
  unfold iter_exceptTˣ.
  unfold iter, Iter_Kleisli, Basics.iter, MonadIter_itree.
  rewrite unfold_iter.
  cbn.
  rewrite bind_bind.
  apply eq_itree_clo_bind with (UU := eq); [reflexivity |].
  intros [? []] ? <-; cbn; rewrite ?bind_ret_l.
  - cbn. rewrite bind_tau.
    apply eqit_Tau. rewrite join_bot_l.
    pose proof (H := @iter_exceptTˣ_unfold_aux).
    cbn in H. now apply H.
  - cbn. now rewrite join_bot_l, join_bot_r.
Qed.
Transparent join.

Lemma iter_exceptTˣ_unfold_eutt {E} {Err: Typeˣ} {LL: LatticeLaws Err}
    {R I: Type} body i :
  iter_exceptTˣ (itree E) R I body i ≈
    bind (m := exceptTˣ Err _) (body i)
    (fun jr => match jr with
            | inl j => iter_exceptTˣ _ _ _ body j
            | inr r => ret r
            end).
Proof.
  rewrite iter_exceptTˣ_unfold.
  apply eutt_eq_bind.
  intros [? []]; cbn; try reflexivity.
  rewrite bind_tau, tau_euttge.
  reflexivity.
Qed.

#[export] Existing Instance iter_exceptTˣ.
#[export] Instance Iter_exceptTˣ {E} {Err: Typeˣ}:
  Iter (Kleisli (exceptTˣ Err (itree E))) sum := _.

#[export] Instance IterUnfold_exceptTˣ {E} {Err: Typeˣ} {LL: LatticeLaws Err}:
  IterUnfold (Kleisli (exceptTˣ Err (itree E))) sum.
Proof.
  unfold IterUnfold.
  intros a b f ?.
  apply iter_exceptTˣ_unfold_eutt.
Qed.

Definition interp_exceptTˣ {E M} {Err}
           {FM : Functor M} {MM : Monad M}
           {IM : MonadIter M} (h : E ~> exceptTˣ Err M) :
  itree E ~> exceptTˣ Err M := interp h.
Arguments interp_exceptTˣ {E M Err FM MM IM} h [T].

(* (** Unfolding of [interp_fail]. *) *)
Definition _interp_fail {E F R Err}
  (f : E ~> exceptTˣ Err (itree F)) (ot : itreeF E R _)
  : exceptTˣ Err (itree F) R :=
  match ot with
  | RetF r => Ret (bot, r)
  | TauF t => Tau (interp_exceptTˣ f t)
  | VisF e k =>
      bind (m := exceptTˣ Err _) (f _ e)
           (fun jr => Tau (interp_exceptTˣ f (k jr)))
  end.

(** Unfold lemma. *)
Lemma unfold_interp_exceptTˣ {E F R} {Err: Typeˣ} {LL: LatticeLaws Err}
  (f : E ~> exceptTˣ Err (itree F)) (t : itree E R) :
  interp_exceptTˣ f t ≅ _interp_fail f (observe t).
Proof.
  unfold interp_exceptTˣ, interp, Basics.iter.
  rewrite iter_exceptTˣ_unfold.
  destruct (observe t).
  + repeat (cbn; rewrite !bind_ret_l). now rewrite join_bot_l.
  + repeat (cbn; rewrite ?bind_ret_l).
    rewrite bind_tau.
    apply eqitree_Tau.
    symmetry.
    rewrite <- bind_ret_r.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-. now rewrite join_bot_l.
  + cbn; rewrite bind_map.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-.
    cbn.
    rewrite !bind_tau.
    apply eqitree_Tau.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-; reflexivity.
Qed.

Arguments join : simpl nomatch.

Lemma interp_exceptTˣ_ret : forall {E F R} {Err: Typeˣ} {LL: LatticeLaws Err}
    (h : E ~> exceptTˣ Err (itree F)) (r : R),
    interp_exceptTˣ h (Ret r) ≅ ret (bot, r).
Proof.
  intros; rewrite unfold_interp_exceptTˣ; reflexivity.
Qed.

Lemma interp_exceptTˣ_vis :
  forall {E F X R} {Err: Typeˣ} {LL: LatticeLaws Err}
    (h : E ~> exceptTˣ Err (itree F)) (e : E X) (k : X -> itree _ R),
    interp_exceptTˣ h (Vis e k) ≈
      bind (m := exceptTˣ Err _) (h _ e) (fun y => (interp_exceptTˣ h (k y))).
Proof.
  intros; rewrite unfold_interp_exceptTˣ.
  cbn.
  now setoid_rewrite tau_euttge.
Qed.
