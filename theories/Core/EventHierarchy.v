(** * Additional utilities to compose interpreters *)

(* begin hide *)
From Paco Require Import paco.
From ITree Require Import
  ITree ITreeFacts Eq
  Events.State Events.StateFacts Events.FailFacts.

Import Basics.Monads.

From ITreeAI.Lattices Require Import
  Lattice LatticeOption.
From ITreeAI Require Import
  AflowMonad AbstractMonad
  Events
  Soundness
  Util FailA.
Import MonadNotation.

From Equations Require Import Equations.
(* end hide *)

Definition inj_void {E} : void1 ~> E := subevent.
Arguments inj_void {E} [T].

Definition caseC {E F} (h: E ~> itree void1): E +' F ~> itree F :=
  fun _ effect =>
    match effect with
    | inl1 e => translate inj_void (h _ e)
    | inr1 e => ITree.trigger e
    end.
Arguments caseC {E F} h [T].

Definition caseA {E F} (h: E ~> aflow void1): E +' F ~> aflow F :=
  fun _ effect =>
    match effect with
    | inl1 e => aflow_translate inj_void (h _ e)
    | inr1 e => aflow_trigger e
    end.
Arguments caseA {E F} h [T].

(* TODO: Use MonadLift to write case' generically *)
Definition case_stateC {E F S} (h: E ~> stateT S (itree void1)):
    E +' F ~> stateT S (itree F) :=
  fun _ effect =>
    match effect with
    | inl1 e => (fun s => translate subevent (h _ e s))
    | inr1 e => (fun s => x <- ITree.trigger e;; ret (s, x))
  end.
Arguments case_stateC {E F S} h [T].

Definition case_stateA {E F S} (h: E ~> stateT S (aflow void1)):
    E +' F ~> stateT S (aflow F) :=
  fun _ effect =>
    match effect with
    | inl1 e => (fun s => aflow_translate subevent (h _ e s))
    | inr1 e => (fun s => x <- aflow_trigger e;; ret (s, x))
  end.
Arguments case_stateA {E F S} h [T].

Definition case_failC {E F} (h: E ~> failT (itree void1)):
    E +' F ~> failT (itree F) :=
  fun _ effect =>
    match effect with
    | inl1 e => translate subevent (h _ e)
    | inr1 e =>  x <- ITree.trigger e;; ret (Some x)
  end.
Arguments case_failC {E F} h [T].

Definition case_failA {E F} (h: E ~> failA (aflow void1)):
  E +' F ~> failA (aflow F) :=
  fun _ effect =>
    match effect with
    | inl1 e => aflow_translate subevent (h _ e)
    | inr1 e => x <- aflow_trigger' e;;
               ret (bot, x)
    end.
Arguments case_failA {E F} h [T].

Lemma sound'_translate {F1 F2} {EvF: F1 =# F2}
      {R1} {R2: Typeˣ} {GC_R: GaloisConnection R2 R1}
      (t1: itree void1 R1) (p2: aflow void1 R2)
      (h1: void1 ~> F1) (h2: void1 ~> F2):
  sound' t1 p2 ->
  sound' (translate h1 t1) (aflow_translate h2 p2).
Proof.
  intros. rewrite translate_to_interp; unfold aflow_translate.
  apply interp_sound_idT; auto.
  - intros _ _ [].
Qed.

Lemma handler_sound_idT_case' : forall E1 E2 F1 F2 h g `{E1 =# E2} `{F1 =# F2},
    handler_sound_idT h g ->
    handler_sound_idT (@caseC E1 F1 h) (@caseA E2 F2 g).
Proof.
  intros * HS; red; intros *.
  destruct e as [e|e], eˣ as [eˣ|eˣ]; try now cbn in *; contradiction.
  - cbn. apply sound'_translate, HS.
  - cbn in *. apply sound'_trigger.
Qed.

Class MonadHoist (t: (Type -> Type) -> (Type -> Type)) := {
  hoist: forall (m n: Type -> Type) (h: m ~> n), t m ~> t n;
}.
Arguments hoist {t _ m n} h [_].

Definition idT (m: Type -> Type): Type -> Type := m.

#[export] Instance hoist_idT: MonadHoist idT := {
  hoist m n h r x := h _ x;
}.

#[export] Instance hoist_stateT {S}: MonadHoist (stateT S) := {
  hoist m n h r x := fun s => h _ (x s);
  }.

#[export] Instance hoist_failT : MonadHoist failT :=
  { hoist m n h T c := h _ c }.

#[export] Instance hoist_failA : MonadHoist failA :=
  {| hoist _ _ h _ c := h _ c |}.

Lemma handler_sound_case :
  forall {E1 E2 F1 F2}
    h g `{E1 =# E2} `{F1 =# F2},
    handler_sound_idT h g ->
    handler_sound_idT (@caseC E1 F1 h) (@caseA E2 F2 g).
Proof.
  intros * HS; hnf; intros *.
  destruct e, eˣ; cbn in *; try contradiction.
  - apply sound'_translate, HS.
  - apply sound'_trigger.
Qed.

Lemma handler_sound_failT_case :
  forall {E1 E2 F1 F2}
    h g `{E1 =# E2} `{F1 =# F2},
    handler_sound_failT h g ->
    handler_sound_failT (@case_failC E1 F1 h) (@case_failA E2 F2 g).
Proof.
  intros * HS; red; intros *.
  destruct e as [e|e], eˣ as [eˣ|eˣ]; try now cbn in *; contradiction.
  - cbn. apply sound'_translate, HS.
  - unfold case_failC, case_failA. cbn in H1.
    apply (sound'_map (GC_T := evl_galois H1)); auto. apply sound'_trigger.
Qed.

Lemma handler_sound_stateT_case :
  forall {E1 E2 F1 F2 S1 S2} {L_A : Lattice S2}
    {G: GaloisConnection S2 S1}
    h g `{E1 =# E2} `{F1 =# F2},
    handler_sound_stateT h g ->
    handler_sound_stateT (@case_stateC E1 F1 S1 h) (@case_stateA E2 F2 S2 g).
Proof.
  intros * HS; red; intros * Hgalois.
  destruct e as [e|e], eˣ as [eˣ|eˣ]; try now cbn in *; contradiction.
  - cbn; now apply sound'_translate, HS.
  - unfold case_stateC, case_stateA. cbn in Hin.
    eapply (sound'_map (GC_T := evl_galois Hin)).
    apply sound'_trigger. cbn; auto.
Qed.
