(* ** Modular soundness

Soundness for the abstract program wrt. the concrete program is a predicate
about the values and states of the abstract program. In a monadic setup like
this one, it all comes down to the concrete/abstract programs' return values.
However, this is only true after interpreting all events, because interpreting
adds new code through events and new control flow through monadic effects.

Both of these problems are solved by tracking the *syntactic* correspondance
between the concrete and abstract program through interpretation and studying
return values only at the end.

The syntactic correspondance matches concrete and abstract effects, which we
can then relate through an "event lattice" (see Events.v), noted "E =# Eˣ" for
E and Eˣ a concrete and abstract family of events respectively.

It also matches control flow structures, which is absolutely crucial because
interpreting changes control flow structures (not their general shapes, but
many of their details) and tracking the correspondance is needed to allow the
proof of the abstract interpretation algorithms (sound_unfold) to conclude.

This file proceeds as follows.
1. First, we define sound', the syntactic matching predicate.
2. We then define handler_sound_*, the soundness predicate for handlers for
   each monad, in terms of sound'.
3. We continue by showing that sound' is preserved by interpretation, which is
   the crucial property.
4. Finally, we prove that after intepretation sound' implies the return-value-
   based predicate "sound". *)

From Coq Require Import Morphisms.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Functor Structures.Monad.
From ITree Require Import
    ITree ITreeFacts Interp.Interp Events.State Events.StateFacts
    Events.FailFacts Props.Leaf.
Import Basics.Monads.

From ITreeAI Require Import
  Lattice LatticeOption LatticeVec BooleanDomain
  Events Util AflowMonad AbstractMonad FailA.
From ITreeAI.Combinators Require Import Seq Do TailMRec If.

Import MonadNotation.
Import LeafNotations.
Local Open Scope monad_scope.

(* Syntactic soundness predicate. This records the fact that two partially-
   handled concrete and abstract programs have the same control flow structure,
   with the return values and events related through Galois connections. The
   key property of this predicate (illustrated through Figure 6) is that it's
   stable with each event handling round, because the parameterized algorithms
   behind each combinator absorb the monadic changes from the event handling
   and rephrase them as a simple change to their parameters. *)
Inductive sound' {E Eˣ: Type -> Type} {EvE: E =# Eˣ}
                 {R} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R}:
    itree E R -> aflow Eˣ Rˣ -> Prop :=
  | SoundRet
      r1 r2
      (Hin: galois_in r1 r2)
      {p} (Hp: p ≈ Ret r1)
      {pˣ} (Hpˣ: aflow_eq pˣ (aflow_Ret r2)):
      sound' p pˣ
  | SoundTrigger {U} {Uˣ: Typeˣ}
      (* Introduce type-changing continuations U -> R, Uˣ -> Rˣ just so that
         the Lattices on U/Uˣ, which are given by the EventLattice, can be
         syntactically different from the Lattices on R/Rˣ, which is given by
         sound'. In practice they'll always be the same *)
      (e: E U) (eˣ: Eˣ Uˣ) (f: U -> R) (fˣ: Uˣ -> Rˣ)
      (Hevlin: evl_in e eˣ)
      (Hf: forall u uˣ,
        galois_in (GC := evl_galois Hevlin) u uˣ ->
        galois_in (f u) (fˣ uˣ))
      {p} (Hp: p ≈ u <- ITree.trigger e;; ret (f u))
      {pˣ} (Hpˣ: aflow_eq pˣ
        (aflow_bind (aflow_trigger' eˣ) (fun uˣ => aflow_Ret (fˣ uˣ)))):
      sound' p pˣ
  | SoundMap {T} {Tˣ: Typeˣ} {GC_T: Tˣ <#> T}
      (u: itree E T) (f: T -> R) (uˣ: aflow Eˣ Tˣ) (fˣ: Tˣ -> Rˣ)
      (Hsound_u: sound' u uˣ)
      (Hsound_f: forall t tˣ, galois_in t tˣ -> galois_in (f t) (fˣ tˣ))
      {p} (Hp: p ≈ ITree.bind u (fun t => Ret (f t)))
      {pˣ} (Hpˣ: aflow_eq pˣ (aflow_bind uˣ (fun tˣ => aflow_Ret (fˣ tˣ)))):
      sound' p pˣ
  | SoundSeq {T₁ U₁} {T₁ˣ U₁ˣ: Typeˣ}
      {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}
      (P: seqᵒ_Params T₁ U₁ R) f k
      (Pˣ: seqˣ_Params T₁ˣ U₁ˣ Rˣ) fˣ kˣ
      (Hsound_f: sound' f fˣ)
      (Hsound_k: forall t tˣ, galois_in t tˣ -> sound' (k t) (kˣ tˣ))
      (HSE: seq_sound_effects P Pˣ)
      {p} (Hp: p ≈ seqᵒ P f k)
      {pˣ} (Hpˣ: aflow_eq pˣ (seqˣ Pˣ fˣ kˣ)):
      sound' p pˣ
  | SoundDo {T U₁ U₂ Value} {Tˣ U₁ˣ U₂ˣ Valueˣ: Typeˣ}
      `{BooleanDomain Value} `{BooleanDomain Valueˣ}
      {GC_T: Tˣ <#> T} {GC_U₁: U₁ˣ <#> U₁} {GC_U₂: U₂ˣ <#> U₂}
      {GC_Value: Valueˣ <#> Value}
      (P: doᵒ_Params T U₁ U₂ R Value) body init
      (Pˣ: doˣ_Params Tˣ U₁ˣ Rˣ Valueˣ) bodyˣ initˣ
      (Hinit: galois_in init initˣ)
      (Hsound_body: forall x xˣ,
        galois_in x xˣ -> sound' (body x) (bodyˣ xˣ))
      (HSE: do_sound_effects P Pˣ)
      {p} (Hp: p ≈ doᵒ P body init)
      {pˣ} (Hpˣ: aflow_eq pˣ (doˣ Pˣ bodyˣ initˣ)):
      sound' p pˣ
  | SoundTailMRec_None {N Tb Ub} {Tˣ Tbˣ Ubˣ: Typeˣ}
      {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_Ub: Ubˣ <#> Ub}
      (P: tailmrecᵒ_Params N Tb Ub R) blocks entry init
      (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Rˣ Ubˣ) blocksˣ initˣ
      (Hinit: galois_in init initˣ)
      (Hsound_blocks:
        vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
                  galois_in tb tbˣ -> sound' (b tb) (bˣ tbˣ))
               blocks blocksˣ)
      (HSE: tailmrec_sound_effects P entry Pˣ)
      {p} (Hp: p ≈ tailmrecᵒ P blocks entry init)
      {pˣ} (Hpˣ: aflow_eq pˣ (tailmrecˣ Pˣ blocksˣ initˣ)):
      sound' p pˣ
  | SoundTailMRec_If {Tb Ub} {Tˣ Tbˣ Ubˣ: Typeˣ}
      {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_Ub: Ubˣ <#> Ub}
      (P: tailmrecᵒ_Params 2 Tb Ub R) blocks entry init
      (Pˣ: tailmrecˣ_Params 2 Tˣ Tbˣ Rˣ Ubˣ) blocksˣ initˣ
      (Hinit: galois_in init initˣ)
      (Hsound_blocks:
        vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
                  galois_in tb tbˣ -> sound' (b tb) (bˣ tbˣ))
               blocks blocksˣ)
      (HSE: if_shape_effects P entry Pˣ)
      {p} (Hp: p ≈ tailmrecᵒ P blocks entry init)
      {pˣ} (Hpˣ: aflow_eq pˣ (tailmrecˣ Pˣ blocksˣ initˣ)):
      sound' p pˣ.

Section sound'_basics.
Context {E Eˣ: Type -> Type} {EvE: E =# Eˣ}.
Context {R: Type} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R}.

(* Basic constructors that avoid evars when the program is in the right form *)

Lemma sound'_ret r rˣ:
  galois_in r rˣ ->
  sound' (Ret r) (aflow_Ret rˣ).
Proof. intros. eapply SoundRet; now eauto. Qed.

(* Notice how the lattice and Galois connections being used here come from EvE
   instead of the contextual Rˣ and GC_R. In user code it's usually necessary
   to bridge these through a morphism, as in sound'_trigger_2. This version of
   the lemma is useful to prove handlers sound because that predicate uses the
   connection from EvE directly. *)
Lemma sound'_trigger {U Uˣ: Type} {e: E U} {eˣ: Eˣ Uˣ} (Hevlin: evl_in e eˣ):
  sound' (Rˣ := evl_lattice eˣ) (GC_R := evl_galois Hevlin)
         (ITree.trigger e) (aflow_trigger' eˣ).
Proof.
  eapply SoundTrigger with (Hevlin := Hevlin) (f := id).
  - intros * Hu; unfold id. apply Hu.
  - unfold id; cbn; now rewrite bind_ret_r.
  - now rewrite aflow_bind_ret_r.
Qed.

(* In this version we bridge the conflicting lattice and Galois connection on
   R/Rˣ (the ones from EvE and the contextual GC_R) via an explicit hypothesis.
   At the top level, the first normally reduces to the second if the user is
   consistent with their datatype setup, so the subproof is easy. *)
Lemma sound'_trigger_2 {e: E R} {eˣ: Eˣ Rˣ} (Hevlin: evl_in e eˣ)
  (Hequiv: forall r rˣ, galois_in (GC := evl_galois Hevlin) r rˣ ->
                        galois_in (GC := GC_R) r rˣ):
  sound' (GC_R := GC_R) (ITree.trigger e) (aflow_trigger' eˣ).
Proof.
  eapply SoundTrigger with (Hevlin := Hevlin) (f := id) (fˣ := id); auto.
  - unfold id; cbn; now rewrite bind_ret_r.
  - now rewrite aflow_bind_ret_r.
Qed.

Lemma sound'_map {T} {Tˣ: Typeˣ} {GC_T: Tˣ <#> T}
    {t tˣ} {f: T -> R} {fˣ: Tˣ -> Rˣ}:
  sound' t tˣ ->
  (forall t tˣ, galois_in t tˣ -> galois_in (f t) (fˣ tˣ)) ->
  sound' (ITree.bind t (fun x => Ret (f x)))
         (aflow_bind tˣ (fun xˣ => aflow_Ret (fˣ xˣ))).
Proof. intros. eapply (SoundMap t _ tˣ); auto; reflexivity. Qed.

Lemma sound'_map_2 {T} {Tˣ: Typeˣ} {GC_T: Tˣ <#> T}
    {t tˣ k kˣ} {f: T -> R} {fˣ: Tˣ -> Rˣ}:
  sound' t tˣ ->
  (forall x, k x ≈ Ret (f x)) ->
  (forall xˣ, aflow_eq (kˣ xˣ) (aflow_Ret (fˣ xˣ))) ->
  (forall t tˣ, galois_in t tˣ -> galois_in (f t) (fˣ tˣ)) ->
  sound' (ITree.bind t k) (aflow_bind tˣ kˣ).
Proof.
  intros. eapply (SoundMap t _ tˣ); auto.
  - apply eutt_eq_bind. auto.
  - apply aflow_eq_bind. auto.
Qed.

Lemma sound'_seq
  {T₁ U₁} {T₁ˣ U₁ˣ: Typeˣ} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}
  (P: seqᵒ_Params T₁ U₁ R) (f: itree E U₁) (k: T₁ -> itree E R)
  (Pˣ: seqˣ_Params T₁ˣ U₁ˣ Rˣ) fˣ kˣ
  (Hsound_f: sound' f fˣ)
  (Hsound_k: forall t tˣ, galois_in t tˣ -> sound' (k t) (kˣ tˣ))
  (HSE: seq_sound_effects P Pˣ):
  sound' (seqᵒ P f k) (seqˣ Pˣ fˣ kˣ).
Proof.
  now apply (SoundSeq _ _ _ _ _ _ Hsound_f Hsound_k HSE).
Qed.

Lemma sound'_do {T U₁ U₂ Value} {Tˣ U₁ˣ U₂ˣ Valueˣ: Typeˣ}
  `{BooleanDomain Value} `{BooleanDomain Valueˣ}
  {GC_T: Tˣ <#> T} {GC_U₁: U₁ˣ <#> U₁} {GC_U₂: U₂ˣ <#> U₂}
  {GC_Value: Valueˣ <#> Value}
  (P: doᵒ_Params T U₁ U₂ R Value) (body: T -> itree E U₁) (init: T)
  (Pˣ: doˣ_Params Tˣ U₁ˣ Rˣ Valueˣ) bodyˣ initˣ
  (Hinit: galois_in init initˣ)
  (Hsound_body: forall x xˣ,
    galois_in x xˣ -> sound' (body x) (bodyˣ xˣ))
  (HSE: do_sound_effects P Pˣ):
  sound' (doᵒ P body init) (doˣ Pˣ bodyˣ initˣ).
Proof.
  intros.
  eapply (@SoundDo E Eˣ _ R Rˣ _ T U₁ U₂ Value Tˣ U₁ˣ U₂ˣ Valueˣ);
  [..|reflexivity|reflexivity]; auto.
Qed.

Lemma sound'_tailmrec {N Tb Ub} {Tˣ Tbˣ Ubˣ: Typeˣ}
  {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_Ub: Ubˣ <#> Ub}
  (P: tailmrecᵒ_Params N Tb Ub R)
  (blocks: vec (Tb -> itree E Ub) N) (entry: Fin.t N) (init: Tb)
  (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Rˣ Ubˣ)
  (blocksˣ: vec (Tbˣ -> aflow Eˣ Ubˣ) N) (initˣ: Tˣ)
  (Hinit: galois_in init initˣ)
  (Hsound_blocks:
    vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
              galois_in tb tbˣ -> sound' (b tb) (bˣ tbˣ))
           blocks blocksˣ)
  (HSE: tailmrec_sound_effects P entry Pˣ):
  sound' (tailmrecᵒ P blocks entry init) (tailmrecˣ Pˣ blocksˣ initˣ).
Proof.
  intros; eapply (SoundTailMRec_None (Tb := Tb) (Tˣ := Tˣ)); easy || eauto.
Qed.

Lemma sound'_if
    {Value} `{BooleanDomain Value}
    {Valueˣ: Typeˣ} `{BooleanDomain Valueˣ}
    {GC_Value: Valueˣ <#> Value}
    (cond: Value) (thenᵒ elseᵒ: itree E R)
    (condˣ: Valueˣ) (thenˣ elseˣ: aflow Eˣ Rˣ)
    (Hsound_cond: galois_in cond condˣ)
    (Hsound_then: sound' thenᵒ thenˣ)
    (Hsound_else: sound' elseᵒ elseˣ):
  sound' (ifᵒ cond thenᵒ elseᵒ)
         (ifˣ condˣ thenˣ elseˣ).
Proof.
  intros.
  eapply (SoundTailMRec_If (Tb := unit) (Ub := R) (Tˣ := unitˣ) (Tbˣ := unitˣ));
    [..|unfold ifᵒ; reflexivity
       |unfold ifˣ; reflexivity]; [easy|..].
  - intros i tb tbˣ Htb.
    depelim i; [|depelim i; [|depelim i]]; rewrite ? vec_nth_FS, ? vec_nth_F1.
    apply Hsound_then. apply Hsound_else.
  - apply if_shape_effects_init.
Qed.

Lemma sound'_eutt {t t' pˣ}:
  t ≈ t' -> sound' t pˣ -> sound' t' pˣ.
Proof.
  intros * Ht. induction 1.
  - eapply SoundRet; eauto. now rewrite <- Ht.
  - eapply SoundTrigger with (f := f); eauto. now rewrite <- Ht.
  - eapply SoundMap; eauto. now rewrite <- Ht.
  - eapply SoundSeq with (f := f); eauto. now rewrite <- Ht.
  - eapply (SoundDo (T := T) (U₁ := U₁) (U₂ := U₂) (Value := Value)); eauto.
    rewrite <- Ht; eauto.
  - eapply (SoundTailMRec_None (Tb := Tb) (Tˣ := Tˣ)); eauto. now rewrite <- Ht.
  - eapply (SoundTailMRec_If (Tb := Tb) (Tˣ := Tˣ)); eauto. now rewrite <- Ht.
Qed.

Lemma sound'_aflow_eq {t pˣ pˣ'}:
  aflow_eq pˣ pˣ' -> sound' t pˣ -> sound' t pˣ'.
Proof.
  intros * Hpˣ. induction 1.
  - eapply SoundRet; eauto. now rewrite <- Hpˣ.
  - eapply SoundTrigger with (fˣ := fˣ); eauto. now rewrite <- Hpˣ.
  - eapply SoundMap; eauto. now rewrite <- Hpˣ.
  - eapply SoundSeq with (f := f); eauto.
    rewrite <- Hpˣ; eauto.
  - eapply (SoundDo (T := T) (U₁ := U₁) (U₂ := U₂) (Value := Value)); eauto.
    rewrite <- Hpˣ; eauto.
  - eapply (SoundTailMRec_None (Tb := Tb) (Tˣ := Tˣ)); eauto. now rewrite <- Hpˣ.
  - eapply (SoundTailMRec_If (Tb := Tb) (Tˣ := Tˣ)); eauto. now rewrite <- Hpˣ.
Qed.

#[global]
Instance sound'_eq: Proper (eutt eq ==> aflow_eq ==> iff) sound'.
Proof.
  intros t t' Ht pˣ pˣ' Hpˣ; split.
  - intros. eapply sound'_eutt. apply Ht. eapply sound'_aflow_eq; eauto.
  - intros. eapply sound'_eutt. symmetry; apply Ht.
    eapply sound'_aflow_eq; [symmetry|]; eauto.
Qed.
End sound'_basics.

Section interp_sound.
Context {E Eˣ F Fˣ: Type -> Type} {EvE: E =# Eˣ} {EvF: F =# Fˣ}.

(* Now define the soundness for handlers as well, and show that this is enough
   to preserve soundness across all intepretation layers. *)
Definition handler_sound_idT
  (h: E ~> itree F) (hˣ: Eˣ ~> aflow Fˣ) :=
  forall U Uˣ (e: E U) (eˣ: Eˣ Uˣ) (H: evl_in e eˣ),
    sound' (Rˣ := evl_lattice eˣ) (GC_R := evl_galois H)
           (h _ e) (hˣ _ eˣ).

Definition handler_sound_stateT {S} {Sˣ: Typeˣ} {GC_S: Sˣ <#> S}
    (h: E ~> stateT S (itree F)) (hˣ: Eˣ ~> stateT Sˣ (aflow Fˣ)) :=
  forall U Uˣ (e: E U) (eˣ: Eˣ Uˣ) (Hin: evl_in e eˣ) (i: S) (iˣ: Sˣ),
    galois_in i iˣ ->
    sound' (Rˣ := @Lattice_prod _ _ Sˣ (evl_lattice eˣ))
           (GC_R := GaloisConnection_prod _ (evl_galois Hin))
           (h _ e i) (hˣ _ eˣ iˣ).

Definition handler_sound_failT
  (h: E ~> failT (itree F)) (hˣ: Eˣ ~> failA (aflow Fˣ)) :=
  forall U Uˣ (e: E U) (eˣ: Eˣ Uˣ) (H: evl_in e eˣ),
  sound' (GC_R := @GaloisConnection_optionˣ_option _ _ (evl_lattice eˣ) _
                  (evl_galois H))
         (h _ e) (hˣ _ eˣ).

(* Proof that handling a family of events into the identity monad (i.e. no
   transformer) preserves soundness. The argument is the same for all
   combinators: handling events modifies the combinators' parameters
   (<combinator>ᵒ_interp, <combinator>ˣ_interp) and the updated parameters on
   the concrete and abstract sides are still sound with respect to each
   other (predicate sound'_<combinator>). For the identity monad this is
   especially trivial because the parameters don't change at all. *)
Lemma interp_sound_idT {R} {Rˣ: Typeˣ} {GC: Rˣ <#> R}
    (h: E ~> itree F) (hˣ: Eˣ ~> aflow Fˣ)
    (p: itree E R) (pˣ: aflow Eˣ Rˣ):
  handler_sound_idT h hˣ ->
  sound' p pˣ ->
  sound' (interp h p) (aflow_interp hˣ pˣ).
Proof.
  intros Hhandler Hsound.
  induction Hsound as [*
                      |*
                      |* ? IH_u
                      |* ? IH_f ? IH_k
                      |* ? IH_body
                      |* ? ? IH_blocks
                      |* ? ? IH_blocks];
    rewrite Hp, Hpˣ; clear p pˣ Hp Hpˣ.

  (* Ret *)
  - rewrite interp_ret; simp aflow_interp. now apply sound'_ret.
  (* Trigger *)
  - cbn; rewrite interp_bind, interp_trigger. setoid_rewrite interp_ret.
    eapply (SoundMap (Tˣ := evl_lattice eˣ) (GC_T := evl_galois Hevlin));
      now auto.
  (* BindPureR *)
  - rewrite interp_bind, aflow_interp_bind.
    eapply sound'_map_2; auto; [intros; now rewrite interp_ret|easy].
  (* Combinators *)
  - rewrite seqᵒ_interp, seqˣ_interp. apply sound'_seq; eauto.
  - rewrite doᵒ_interp, doˣ_interp. apply sound'_do; eauto.
  - rewrite tailmrecᵒ_interp, tailmrecˣ_interp. apply sound'_tailmrec; eauto.
    intros j x xˣ Hx. rewrite ! vec_nth_map. now apply IH_blocks.
  - rewrite tailmrecᵒ_interp, tailmrecˣ_interp. apply sound'_tailmrec; eauto.
    intros j x xˣ Hx. rewrite ! vec_nth_map. now apply IH_blocks.
    apply if_SE_tailmrec, HSE.
Qed.

(* Proof that handling a family of events into the state monad preserves
   soundness. The argument is the same for all combinators: handling events
   modifies the combinators' parameters (<combinator>ᵒ_interp_state,
   <combinator>ˣ_interp_state) and the updated parameters on the concrete and
   abstract sides are still sound with respect to each other (predicate
   sound'_<combinator> with judiciously-chosen parameters). *)
Lemma interp_sound_stateT {R S} {Rˣ Sˣ: Typeˣ} {GC_R: Rˣ <#> R} {GC_S: Sˣ <#> S}
    (h: E ~> stateT S (itree F)) (hˣ: Eˣ ~> stateT Sˣ (aflow Fˣ))
    (t: itree E R) (pˣ: aflow Eˣ Rˣ) s sˣ:
  galois_in s sˣ ->
  handler_sound_stateT h hˣ ->
  sound' t pˣ ->
  sound' (interp_state h t s) (aflow_interp_state hˣ pˣ sˣ).
Proof.
  intros Hinitial Hhandler Hsound. revert s sˣ Hinitial.
  induction Hsound as [*
                      |*
                      |* ? IH_u
                      |* ? IH_f ? IH_k
                      |* ? IH_body
                      |* ? ? IH_blocks
                      |* ? ? IH_blocks];
    intros; rewrite Hp, Hpˣ; clear p pˣ Hp Hpˣ.

  (* Ret *)
  - rewrite interp_state_ret. simp aflow_interp_state.
    apply sound'_ret. split; auto.
  (* Trigger *)
  - cbn; rewrite interp_state_bind, interp_state_trigger.
    eapply (sound'_map_2
      (f := fun '((s, u): S * U) => (s, f u))
      (fˣ := fun '((sˣ, uˣ): Sˣ * Uˣ) => (sˣ, fˣ uˣ))).
    * now apply Hhandler.
    * intros []; now rewrite interp_state_ret.
    * intros []; easy.
    * intros [] []; cbn. intuition.
  (* BindPureR *)
  - rewrite interp_state_bind, aflow_interp_state_bind.
    eapply (sound'_map_2
      (f := fun '((s, t): S * T) => (s, f t))
      (fˣ := fun '((sˣ, tˣ): Sˣ * Tˣ) => (sˣ, fˣ tˣ)));
    repeat intros []; cbn; auto.
    * now rewrite interp_state_ret.
    * easy.
  (* Seq *)
  - rewrite seqᵒ_interp_state, seqˣ_interp_state.
    apply sound'_seq; eauto.
    * intros [] []; cbn; intuition auto.
    * now apply seq_sound_effects_stateT.
  (* Do *)
  - rewrite doᵒ_interp_state, doˣ_interp_state.
    apply sound'_do; cbn; eauto.
    * repeat intros []; auto.
    * now apply do_sound_effects_stateT.
  (* TailMRec_None *)
  - rewrite tailmrecᵒ_interp_state, tailmrecˣ_interp_state.
    eapply (sound'_tailmrec (Tb := S * Tb) (Ub := S * Ub)); eauto.
    * cbn; auto.
    * intros i [] [] []. rewrite ! vec_nth_map; auto.
    * now apply tailmrec_sound_effects_stateT.
  (* TailMRec_If *)
  - rewrite tailmrecᵒ_interp_state, tailmrecˣ_interp_state.
    eapply (sound'_tailmrec (Tb := S * Tb) (Ub := S * Ub)); eauto.
    * cbn; auto.
    * intros i [] [] []. rewrite ! vec_nth_map; auto.
    * now apply tailmrec_sound_effects_stateT, if_SE_tailmrec.
Qed.

(* Proof that handling a family of events into the failure monad preserves
   soundness. The argument is the same for all combinators: handling events
   modifies the combinators' parameters (<combinator>ᵒ_interp_fail,
   <combinator>ˣ_interp_fail) and the updated parameters on the concrete and
   abstract sides are still sound with respect to each other (predicate
   sound'_<combinator> with judiciously-chosen parameters). *)
Lemma interp_sound_failT {R} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R}
  (h: E ~> failT (itree F)) (hˣ: Eˣ ~> failA (aflow Fˣ))
  (t: itree E R) (pˣ: aflow Eˣ Rˣ):
  handler_sound_failT h hˣ ->
  sound' t pˣ ->
  sound' (interp_fail h t) (aflow_interp_fail hˣ pˣ).
Proof.
  intros Hhandler Hsound.
  induction Hsound as [
                      |
                      |* ? IH_u
                      |* ? IH_f ? IH_k
                      |* ? IH_body
                      |* ? IH_blocks
                      |* ? IH_blocks];
    intros; rewrite Hp, Hpˣ; clear p pˣ Hp Hpˣ.

  (* Ret *)
  - rewrite interp_fail_ret; simp aflow_interp_fail.
    now apply sound'_ret.
  (* Trigger *)
  - cbn; rewrite interp_fail_bind.
    eapply (SoundMap
     (GC_T := GaloisConnection_optionˣ_option (evl_galois Hevlin))) with
     (f := fun o_u => fmap f o_u)
     (fˣ := fun err_u => _); cycle -2.
    * apply eutt_eq_bind. intros []; now rewrite ? interp_fail_ret.
    * apply aflow_eq_bind. now intros [].
    * rewrite interp_fail_trigger. apply Hhandler.
    * intros [] [] Hin; rewrite ? interp_fail_ret; cbn; auto.
      inversion Hin; now destruct u.
  (* BindPureR *)
  - rewrite interp_fail_bind, aflow_interp_fail_bind.
    eapply SoundMap with
     (f := fun (o_u: option _) => fmap f o_u)
     (fˣ := fun '(err, t) => (err, fˣ t)); cycle -2.
    * apply eutt_eq_bind. intros []; now rewrite ? interp_fail_ret.
    * apply aflow_eq_bind. intros []; cbn. now rewrite failT_merge_bot_r.
    * auto.
    * intros [] [] Hin; cbn. eauto. now inversion Hin.
  (* Seq *)
  - rewrite seqᵒ_interp_fail, seqˣ_interp_fail.
    apply sound'_seq; eauto. now apply seq_sound_effects_failT.
  (* Do *)
  - rewrite doᵒ_interp_fail, doˣ_interp_fail.
    apply sound'_do; eauto. now apply do_sound_effects_failT.
  (* TailMRec_None *)
  - rewrite tailmrecᵒ_interp_fail, tailmrecˣ_interp_fail.
    apply sound'_tailmrec; eauto.
    * intros i * ?. rewrite ! vec_nth_map; eauto.
    * now apply tailmrec_sound_effects_failT.
  (* TailMRec_If *)
  - rewrite tailmrecᵒ_interp_fail, tailmrecˣ_interp_fail.
    apply sound'_tailmrec; eauto.
    * intros i * ?. rewrite ! vec_nth_map; eauto.
    * now apply tailmrec_sound_effects_failT, if_SE_tailmrec.
Qed.

Lemma sound_bind {R U} {Rˣ Uˣ: Typeˣ} {GC_R: Rˣ <#> R} {GC_U: Uˣ <#> U}
    t pˣ (k: U -> itree E R) (kˣ: Uˣ -> aflow Eˣ Rˣ):
  sound t pˣ ->
  (forall u uˣ, galois_in u uˣ -> sound (k u) (kˣ uˣ)) ->
  sound (ITree.bind t k) (aflow_bind pˣ kˣ).
Proof.
  intros Hsound Hcont x xˣ Hx Hxˣ.
  apply Leaf_bind_inv in Hx. destruct Hx as [u [Hu Hx]].
  rewrite unfold_bind in Hxˣ.
  apply Leaf_bind_inv in Hxˣ. destruct Hxˣ as [uˣ [Huˣ Hxˣ]].
  red in Hcont. eapply Hcont; eauto.
Qed.
End interp_sound.

(* Proof that unfolding the control flow combinators in the abstract program
   turns our control-flow-structure soundness predicate (sound') into the usual
   return-values-obey-Galois-connection soundness predicate (sound). This is
   what the correctness of lattice operations and fixpoint approximation
   schemes leads to. This particular definition is boilerplate because each
   combinator provides this theorem individually (sound_<combinator>). See the
   files in `Combinators/` for the "real" abstract interpretation proofs. *)
Proposition sound_unfold {R} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R} {Evv: void1 =# void1}
    (t: itree void1 R) (pˣ: aflow void1 Rˣ):
  sound' t pˣ -> sound t pˣ.
Proof.
  intros Hsound.
  induction Hsound as [*
                      |*
                      |* ? IH_u
                      |* ? IH_tf ? IH_k
                      |* ? ? IH_then ? IH_else
                      |* ? IH_body
                      |* ? ? IH_blocks];
    intros; rewrite Hp, Hpˣ; clear p pˣ Hp Hpˣ.

  - intros x xˣ Hx Hxˣ.
    inversion_Leaf_ret Hx.
    simp unfold in Hxˣ.
    now inversion_Leaf_ret Hxˣ.
  - destruct e.
  - intros x xˣ Hx Hxˣ.
    rewrite unfold_bind in Hxˣ.
    inversion_Leaf_bind Hxˣ vˣ Hvˣ.
    simp unfold in Hxˣ.
    inversion_Leaf_ret Hxˣ.
    inversion_Leaf_bind Hx v Hv.
    inversion_Leaf_ret Hx.
    auto.
  - eapply (sound_seq (U₁ := U₁)); cycle -2.
    reflexivity. reflexivity. all: eauto.
  - eapply (sound_do (T := T) (U₁ := U₁) (U₂ := U₂) (Value := Value)); cycle -2.
    reflexivity. reflexivity. all: eauto.
  - eapply (sound_tailmrec (Tb := Tb) (Tˣ := Tˣ)); cycle -2.
    reflexivity. reflexivity. all: eauto.
  - eapply (sound_tailmrec (Tb := Tb) (Tˣ := Tˣ)); cycle -2.
    reflexivity. reflexivity. all: eauto using @if_SE_tailmrec.
Qed.
