From ITree Require Import
  ITree ITreeFacts Core.Subevent.
From ITreeAI Require Import
  Lattice LatticeOption Util AbstractMonad AflowMonad FailA.
From ExtLib Require Import Structures.Monad.
Import Basics.Monads.

(* We first define a notion of *event abstraction*, which is used to relate
   events of the concrete and abstract worlds. Usually the features of the
   language remain the same during abstract interpretation so the types of
   events that can be triggered are morally the same; however the domains of
   values that are used as parameters and return values are likely to change.

   The event abstraction captures how parameters and return values of abstract
   events abstract their concrete counterparts, and also allows the families to
   differ when it is needed. *)
Class EventLattice (E1 E2: Type -> Type) := {

  (* Abstraction of events and their parameters *)
  evl_in: forall [U1 U2], E1 U1 -> E2 U2 -> Prop;

  (* Lattice of return values *)
  evl_lattice: forall [U2], E2 U2 -> Lattice U2;

  (* Abstraction of return values. We pass a proof of [evl_in e1 e2] so that
     when e1 and e2 are not related (evl_in e1 e2 = False), we can match on it
     and not define a connection. *)
  evl_galois: forall [U1 U2] [e1: E1 U1] [e2: E2 U2],
    evl_in e1 e2 -> @GaloisConnection U2 U1 (evl_lattice e2);

  (* However, we want evl_ret_lattice to only depend on whether e1 and e2 are
     related, not on the particular proof of that relation. *)
  evl_galois_unique: forall [U1 U2] [e1: E1 U1] [e2: E2 U2] H1 H2,
    @evl_galois _ _ e1 e2 H1 = @evl_galois _ _ e1 e2 H2;

  (* Associated RAns relation for SoundTrigger. The lemma [evl_RAns_make] below
     should be used to prove evl_RAns to avoid the dependency on H. *)
  evl_RAns R1 R2 (e1: E1 R1) (r1: R1) (e2: E2 R2) (r2: R2) :=
    forall (H: evl_in e1 e2), @galois_in _ _ _ (evl_galois H) r1 r2;
}.

Infix "=#" := EventLattice (at level 60).
(* Arguments evl_in {_ _ _} [_ _] _ _/. *)
(* Arguments galois_in {_ _ _ _} _ _/. *)

(* Note the difference with evl_RAns: here the forall captures the conclusion,
   so we can prove [lin] with any H and get it for all H. *)
Lemma evl_RAns_make {E1 E2 R1 R2} `{E1 =# E2} (e1: E1 R1) r1 (e2: E2 R2) r2:
  forall (H: evl_in e1 e2),
  @galois_in _ _ _ (evl_galois H) r1 r2 -> evl_RAns _ _ e1 r1 e2 r2.
Proof.
  (* Set Printing Implicit. *)
  repeat intro. erewrite evl_galois_unique. eauto.
Qed.

(* Initially the language semantics should define event abstractions for
   families of events individually; then we lift it automatically to sums. *)
#[global, refine]
Instance sum1_EventLattice {E1 F1 E2 F2} (EvE: E1 =# E2) (EvF: F1 =# F2):
    (E1 +' F1) =# (E2 +' F2) := {

  evl_in _ _ e1 e2 :=
    match e1, e2 with
    | inl1 l1, inl1 l2 => evl_in l1 l2
    | inr1 r1, inr1 r2 => evl_in r1 r2
    | _, _ => False
    end;

  evl_lattice _ e2 :=
    match e2 with
    | inl1 l2 => evl_lattice l2
    | inr1 r2 => evl_lattice r2
    end;

  evl_galois _ _ e1 e2 Heq :=
    match e1, e2, Heq with
    | inl1 l1, inl1 l2, Heq => evl_galois Heq
    | inr1 r1, inr1 r2, Heq => evl_galois Heq
    | _, _, Heq => match Heq with end
    end;
}.
Proof. intros U1 U2 [] [] H1 H2; intuition; apply evl_galois_unique. Defined.

(* Once execution finished we are left with void1 events. *)
#[global, refine]
Instance void1_EventLattice: EventLattice void1 void1 := {}.
Proof. all: intros; match goal with v: void1 _ |- _ => destruct v end. Defined.


(* Now that we have relations between concrete and abstract event families, we
   are almost able to interpret events with handlers. There is however an extra
   step; when we chain event handlers to create a semantics, the handler
   usually outputs programs with different events (here F1) which need to be
   injected into remaining set of events:

     t: itree (E1 +' E2 +' E3) R
     h: E1 ~> itree F1
     interp h t: itree F1 R
     [interp h t] is injected into itree (F1 +' E2 +' E3) R

   Because this injection changes the set of events, thus the event abstraction
   in use, we need to make sure that the new event abstraction is compatible,
   which is the purpose of the class below. It's basically a pair of -< (one
   for each world) bundled with a proof that the abstraction is preserved.

   Note that eve_inj1 and eve_inj2 are pretty annoying as instances of -< and
   often cause the typeclass engine to loop even with low priority, so we make
   sure to not declare them as instances and name them when we need them. *)
Class EventExtends {F1 F2 G1 G2} (EvF: F1 =# F2) (EvG: G1 =# G2) := {
  eve_inj1: F1 -< G1;
  eve_inj2: F2 -< G2;

  (* Injecting equal events should yield equal events *)
  eve_in: forall {U1 U2} (e1: F1 U1) (e2: F2 U2),
    evl_in e1 e2 -> evl_in (subevent _ e1) (subevent _ e2);

  (* The abstraction injection must preserve the lttices *)
  eve_lattice: forall {U2} (e2: F2 U2),
    evl_lattice e2 = evl_lattice (subevent _ e2);

  (* The connections for equal events and their injected counterparts should be
     the same. We can't assert equality between both GaloisConnection instances
     because that would require dependent transport of eve_lattice, but all we
     need is a pointwise equivalence. *)
  eve_galois: forall {U1 U2} (e1: F1 U1) (e2: F2 U2),
    forall (HeqF: evl_in e1 e2) (HeqG: evl_in (eve_inj1 _ e1) (eve_inj2 _ e2)),
    forall (x1: U1) (x2: U2),
    @galois_in _ _ _ (evl_galois HeqF) x1 x2 <->
    @galois_in _ _ _ (evl_galois HeqG) x1 x2;
}.

(* #[local] Existing Instance eve_inj1 | 20.
#[local] Existing Instance eve_inj2 | 20. *)

Arguments eve_inj1 {F1 F2 G1 G2} {EvF EvG} EventExtends.
Arguments eve_inj2 {F1 F2 G1 G2} {EvF EvG} EventExtends.

Infix "=<" := EventExtends (at level 50).

(* This lemma propagates evl_RAns between extended event families. *)
Lemma eve_ans {F1 F2 G1 G2} {EvF: F1 =# F2} {EvG: G1 =# G2} `{EvF =< EvG}:
  forall {U1 U2} (e1: F1 U1) r1 (e2: F2 U2) r2,
    evl_in e1 e2 ->
    evl_RAns _ _ (eve_inj1 _ _ e1) r1 (eve_inj2 _ _ e2) r2 ->
    evl_RAns _ _ e1 r1 e2 r2.
Proof.
  (* Set Printing Implicit. *)
  intros * Hin Hans. unshelve eapply evl_RAns_make. apply Hin.
  unshelve eapply eve_galois. now apply eve_in. apply Hans.
Qed.

(* We prove the extension property for the common instances of =#, meaning that
   for sum1-based constructions the extension can be inferred. A language only
   needs to prove the extension if the events of two different handlers
   interact in other non-trivial ways. *)

#[global, refine]
Instance sum1_EventExtends {E1 E2 G1 G2 F1 F2 H1 H2}
    {EvE: E1 =# E2} {EvF: F1 =# F2} {EvG: G1 =# G2} {EvH: H1 =# H2}
    (EvEG: EvE =< EvG) (EvFH: EvF =< EvH):
  sum1_EventLattice EvE EvF =< sum1_EventLattice EvG EvH := {}.
Proof.
  - pose proof (eve_inj1 EvEG).
    pose proof (eve_inj1 EvFH).
    typeclasses eauto.
  - pose proof (eve_inj2 EvEG).
    pose proof (eve_inj2 EvFH).
    typeclasses eauto.
  - intros; destruct e1, e2; cbn; auto; now apply eve_in.
  - intros; destruct e2; cbn; apply eve_lattice.
  - intros; destruct e1, e2; try destruct HeqF, HeqG; cbn; apply eve_galois.
Defined.
