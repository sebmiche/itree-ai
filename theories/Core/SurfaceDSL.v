From Coq Require Import
  Strings.String Lists.List Classes.EquivDec DecidableType Program.Equality
  Arith.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad.
From Paco Require Import paco.

From ITree Require Import ITree ITreeFacts.
Import Basics.Monads.

From ITreeAI.Lattices Require Import
  Lattice LatticeConst LatticeOption LatticeVec.
From ITreeAI Require Import
  Soundness BooleanDomain
  Events Util AflowMonad AbstractMonad NumericalDomain.
From ITreeAI.Combinators Require Import Return Seq Do TailMRec If.

Definition csum (R Rˣ: Type): bool -> Type := fun b => if b then Rˣ else R.

Equations mksum {R Rˣ} b (r : R) (rˣ : Rˣ) : csum R Rˣ b :=
  mksum false r _  := r;
  mksum true  _ rˣ := rˣ
.
Arguments mksum {_ _ b}.

Equations app_csum {T Tˣ} {R Rˣ} b:
    csum (T -> R) (Tˣ -> Rˣ) b -> csum T Tˣ b -> csum R Rˣ b :=
  app_csum false f t => f t;
  app_csum true fˣ tˣ => fˣ tˣ.
Arguments app_csum {_ _ _ _ b}.

Equations bimap_csum {T Tˣ} {R Rˣ} b:
    csum T Tˣ b -> (T -> R) -> (Tˣ -> Rˣ) -> csum R Rˣ b :=
  bimap_csum false t f fˣ => f t;
  bimap_csum true tˣ f fˣ => fˣ tˣ.
Arguments bimap_csum {_ _ _ _ b}.

Equations bimap2_csum {T Tˣ} {U Uˣ} {R Rˣ} b:
    csum T Tˣ b -> csum U Uˣ b -> (T -> U -> R) -> (Tˣ -> Uˣ -> Rˣ) ->
    csum R Rˣ b :=
  bimap2_csum false t u f fˣ := f t u;
  bimap2_csum true t u f fˣ := fˣ t u.
Arguments bimap2_csum {_ _ _ _ _ _ b}.

(* Datatype for a dual concrete and abstract denotation. This is a tree of
   control flow structures with pairs of ret-statements or events as leaves.
   The dual typing uses the `csum` type to select between the concrete and
   abstract type based on the value of the boolean b. Note that an instance of
   `SurfaceAST E Eˣ R Rˣ false` is only a concrete interpreter, and an instance
   of `SurfaceAST E Eˣ R Rˣ true` is only an abstract interpreter. We say that
   a SurfaceAST is both because constructors and notations for building
   SurfaceASTs all provide both b=true and b=false. Hence there is implicit
   quantification on b for most definitions. *)
Inductive SurfaceAST {E Eˣ: Type -> Type} (R Rˣ: Type): bool -> Type :=
  | AST_Ret {b} (_: csum R Rˣ b): SurfaceAST R Rˣ b
  | AST_Event {b} (_: csum (E R) (Eˣ Rˣ) b): SurfaceAST R Rˣ b
  | AST_Seq {b} {T} {Tˣ: Typeˣ}
      (p: SurfaceAST T Tˣ b)
      (k: csum T Tˣ b -> SurfaceAST R Rˣ b):
      SurfaceAST R Rˣ b
  | AST_If {b} {Value} {Valueˣ: Typeˣ} `{BooleanDomain Value}
      `{BooleanDomain Valueˣ} {GC_Value: Valueˣ <#> Value}
      (v: csum Value Valueˣ b) (pthen pelse: SurfaceAST R Rˣ b):
      SurfaceAST R Rˣ b
  | AST_Do {b}
     {Value U} {Valueˣ Uˣ: Typeˣ} `{BooleanDomain Value}
     `{BooleanDomain Valueˣ} {GC_Value: Valueˣ <#> Value}
      (init : csum R Rˣ b)
      (dist : csum (U -> Value * R * R) (Uˣ -> Valueˣ * Rˣ * Rˣ) b)
      (body : csum R Rˣ b -> SurfaceAST U Uˣ b):
      SurfaceAST R Rˣ b
  | AST_CFG {b Tb} {Tbˣ: Typeˣ} (N : nat)
      (bks : vec (csum Tb Tbˣ b -> SurfaceAST (Fin.t (S N) * Tb + R) (TailBranch (S N) * Tbˣ * Rˣ) b) (S N))
      (entry : Fin.t (S N))
      (init : csum Tb Tbˣ b) :
    SurfaceAST R Rˣ b
.

Arguments SurfaceAST E Eˣ R Rˣ: clear implicits.
Arguments AST_Ret   {E Eˣ} {R Rˣ} {b}.
Arguments AST_Event {E Eˣ} {R Rˣ} {b}.
Arguments AST_Seq   {E Eˣ} {R Rˣ} {b} {T Tˣ}.
Arguments AST_If    {E Eˣ} {R Rˣ} {b} {Value Valueˣ} {_ _ _}.
Arguments AST_Do    {E Eˣ} {R Rˣ} {b} {Value U Valueˣ Uˣ} {_ _ _}.
Arguments AST_CFG   {E Eˣ} {R Rˣ} {b Tb Tbˣ} N bks entry init.

(* AST macro for the while loop. We expand `while <b> do <c>` into the block
   `if <c> (do <b> while <c>)`, which is functionally equivalent. Doing this at
   the SurfaceAST level means that we cannot implement a specialized analysis
   for while, it will always use the analyses for if and do. *)
Definition AST_While {E Eˣ} {b} {Value R U} {Valueˣ Rˣ Uˣ: Typeˣ}
    {BD: BooleanDomain Value} {BDˣ: BooleanDomain Valueˣ}
    {GC_Value: Valueˣ <#> Value}
    (init : csum R Rˣ b)
    (dist : csum (U -> Value * R * R) (Uˣ -> Valueˣ * Rˣ * Rˣ) b)
    (cond body : csum R Rˣ b -> SurfaceAST E Eˣ U Uˣ b):
    SurfaceAST E Eˣ R Rˣ b :=
  AST_Seq (cond init) (fun c =>
    let value   := bimap_csum (app_csum dist c) (fst ∘ fst) (fst ∘ fst) in
    let r_true  := bimap_csum (app_csum dist c) (snd ∘ fst) (snd ∘ fst) in
    let r_false := bimap_csum (app_csum dist c) snd snd in
    AST_If value (AST_Do r_true dist body)
                 (AST_Ret r_false)).

Definition AST_While_unit {E Eˣ} {b} {Value} {Valueˣ: Typeˣ}
  `{BD : BooleanDomain Value} `{BDˣ : BooleanDomain Valueˣ}
  {GC_Value: Valueˣ <#> Value}
  (cond : SurfaceAST E Eˣ Value Valueˣ b)
  (body : SurfaceAST E Eˣ unit unitˣ b):
  SurfaceAST E Eˣ unit unitˣ b :=
  AST_While (Value := Value)
    (mksum tt ttˣ)
    (mksum (fun v => (v,tt,tt)) (fun v => (v,ttˣ,ttˣ)))
    (fun _ => cond)
    (fun _ => AST_Seq body (fun _ => cond)).

Definition AST_CFG_unit {E Eˣ R Rˣ} {b} (N : nat)
  (* (bks : vec (SurfaceAST E Eˣ (Fin.t (S N) + R) (TailBranch (S N) * Rˣ) b) (S N)) *)
  (bks : vec (csum unit unitˣ b -> SurfaceAST E Eˣ (Fin.t (S N) * unit + R) (TailBranch (S N) * unitˣ * Rˣ) b) (S N))
  (entry : Fin.t (S N))
  init
  :
  SurfaceAST E Eˣ R Rˣ b :=
  AST_CFG N bks entry init.

(* Definition make_block {E N} `{@memE reg value unit -< E} *)
(*     {Value} `{NumDomain Value} (cblk: itree E (blockres Value)): *)
(*     unit -> itree E (Fin.t N * unit + unit) := *)
(*   fun _ => ITree.map blockres_to_tailmrecᵒ_ret cblk. *)


(* Projection of a SurfaceAST to a concrete interpreter as an itree. *)
Equations ast2itree {E Eˣ} {R Rˣ} (ast: SurfaceAST E Eˣ R Rˣ false): itree E R :=
  ast2itree (AST_Ret r) :=
    returnᵒ id r;
  ast2itree (AST_Event e) :=
    ITree.trigger e;
  ast2itree (AST_Seq p k) :=
    cseq (ast2itree p)
      (fun t => ast2itree (k t));
  ast2itree (AST_If value pthen pelse) :=
    ifᵒ value (ast2itree pthen) (ast2itree pelse);
  ast2itree (AST_Do init dist body) :=
    doᵒ
      (doᵒ_Params_init dist)
      (fun r => ast2itree (body r))
      init;
  ast2itree (AST_CFG N bks entry init) := (tailmrecᵒ (tailmrecᵒ_Params_init_cfg _ _ _) (aux bks) entry init);
where aux {E Eˣ} {Ub Ubˣ} {Tb N} (v: vec (Tb -> SurfaceAST E Eˣ Ub Ubˣ false) N):
  vec (Tb -> itree E Ub) N :=
  aux vec_nil := vec_nil;
  aux (vec_cons f fs) := vec_cons (ast2itree ∘ f) (aux fs).

(* Projection of a SurfaceAST to an abstract interpreter as an aflow. In this
   definition we split Rˣ and its Lattice instance (as with every other
   computational definition that needs a lattice) because extraction would be
   lost if a Typeˣ was involved. *)
Equations ast2aflow {E Eˣ} {EvE: E =# Eˣ} {R} {Rˣ: Type} {L_R: Lattice Rˣ}
    (ast: SurfaceAST E Eˣ R Rˣ true): aflow Eˣ Rˣ :=
  ast2aflow (AST_Ret r) :=
    returnˣ (returnˣ_Params_init _) r;
  ast2aflow (AST_Event e) :=
    aflow_trigger' e;
  ast2aflow (AST_Seq p k) :=
    aseq (ast2aflow p)
         (fun t => ast2aflow (k t));
  ast2aflow (AST_If value pthen pelse) :=
    ifˣ value (ast2aflow pthen) (ast2aflow pelse);
  ast2aflow (AST_Do init dist body) :=
    doˣ
      (doˣ_Params_init dist)
      (fun r => ast2aflow (body r))
      init;
  ast2aflow (AST_CFG N bks entry init) :=
    tailmrecˣ
      (tailmrecˣ_Params_init_cfg _ _ _ entry)
      (aux' bks)
      init;
where aux' {E Eˣ} {EvE : E =# Eˣ} {Ub} {Ubˣ Tbˣ : Typeˣ} {N} (v: vec (Tbˣ -> SurfaceAST E Eˣ Ub Ubˣ true) N):
  vec (Tbˣ -> aflow Eˣ Ubˣ) N :=
  aux' vec_nil := vec_nil;
  aux' (vec_cons f fs) := vec_cons (ast2aflow ∘ f) (aux' fs).

Inductive SoundAST {E Eˣ} {EvE: E =# Eˣ} {R: Type} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R}:
      SurfaceAST E Eˣ R Rˣ false -> SurfaceAST E Eˣ R Rˣ true -> Prop :=
  | SoundAST_Ret
      (r: R) (rˣ: Rˣ)
      (Hin: galois_in r rˣ):
      SoundAST (AST_Ret (b := false) r) (AST_Ret (b := true) rˣ)
  | SoundAST_Event
      (e: csum (E R) (Eˣ Rˣ) false) (eˣ: csum (E R) (Eˣ Rˣ) true)
      (Hevlin: evl_in e eˣ)
      (* Make sure the lattices used in EvE and GC_R are compatible. This will
         be the same lattice unless the user messes up. *)
      (Hcompat: forall r rˣ, galois_in (GC := evl_galois Hevlin) r rˣ ->
                             galois_in (GC := GC_R) r rˣ):
      SoundAST (AST_Event e) (AST_Event eˣ)
  | SoundAST_Seq {T} {Tˣ: Typeˣ} {GC_T: Tˣ <#> T}
      (p1: SurfaceAST E Eˣ T Tˣ false)
      (p2: SurfaceAST E Eˣ T Tˣ true)
      (k1: T -> SurfaceAST E Eˣ R Rˣ false)
      (k2: Tˣ -> SurfaceAST E Eˣ R Rˣ true)
      (Hp: SoundAST p1 p2)
      (Hk: forall t tˣ, galois_in t tˣ ->
             SoundAST (k1 t) (k2 tˣ)):
      SoundAST (AST_Seq p1 k1)
               (AST_Seq p2 k2)
  | SoundAST_If {Value} {Valueˣ: Typeˣ} `{BooleanDomain Value}
      `{BooleanDomain Valueˣ} {GC_Value: Valueˣ <#> Value}
      (pthen1 pelse1: SurfaceAST E Eˣ R Rˣ false)
      (pthen2 pelse2: SurfaceAST E Eˣ R Rˣ true)
      (value1: Value)
      (value2: Valueˣ)
      (Hin: galois_in value1 value2)
      (Hpthen: SoundAST pthen1 pthen2)
      (Hpelse: SoundAST pelse1 pelse2):
      SoundAST (AST_If (b := false) value1 pthen1 pelse1)
               (AST_If (b := true) value2 pthen2 pelse2)
  | SoundAST_Do {U Value} {Uˣ Valueˣ : Typeˣ}
      `{BooleanDomain Value} `{BooleanDomain Valueˣ}
      {GC_U: Uˣ <#> U} {GC_Value: Valueˣ <#> Value}
      (init1 : R)
      (init2 : Rˣ)
      (dist1 : U -> Value * R * R)
      (dist2 : Uˣ -> Valueˣ * Rˣ * Rˣ)
      (body1 : R -> SurfaceAST E Eˣ U Uˣ false)
      (body2 : Rˣ -> SurfaceAST E Eˣ U Uˣ true)
      (Hinit: galois_in init1 init2)
      (Hdist: forall u uˣ, galois_in u uˣ ->
          galois_in (dist1 u) (dist2 uˣ))
      (Hbody: forall r rˣ, galois_in r rˣ ->
          SoundAST (body1 r) (body2 rˣ)):
      SoundAST (AST_Do (b := false) init1 dist1 body1)
               (AST_Do (b := true) init2 dist2 body2)
  | SoundAST_CFG {Tb U} {Tbˣ Uˣ : Typeˣ} {N : nat}
      {GC_U : Uˣ <#> U} {GC_Tb : Tbˣ <#> Tb}
      entry
      (init1 : Tb)
      (init2 : Tbˣ)
      (bks1 : vec (Tb -> SurfaceAST E Eˣ _ _ false) (S N))
      (bks2 : vec (Tbˣ -> SurfaceAST E Eˣ _ _ true) (S N))
      (Hinit: galois_in init1 init2)
      (Hbody: vec_eq (fun b bˣ => forall r rˣ, galois_in r rˣ -> SoundAST (b r) (bˣ rˣ)) bks1 bks2):
      SoundAST (AST_CFG (b := false) N bks1 entry init1)
               (AST_CFG (b := true)  N bks2 entry init2).

Lemma SoundAST_While {E Eˣ R} {Rˣ: Typeˣ} {EvE: E =# Eˣ}
      {U Value} {Uˣ Valueˣ : Typeˣ}
      `{BooleanDomain Value} `{BooleanDomain Valueˣ}
      {GC_U: Uˣ <#> U} {GC_Value: Valueˣ <#> Value} {GC_R: Rˣ <#> R}
      (init1 : R)
      (init2 : Rˣ)
      (dist1 : U -> Value * R * R)
      (dist2 : Uˣ -> Valueˣ * Rˣ * Rˣ)
      (cond1 body1 : R -> SurfaceAST E Eˣ U Uˣ false)
      (cond2 body2 : Rˣ -> SurfaceAST E Eˣ U Uˣ true)
      (Hinit: galois_in init1 init2)
      (Hdist: forall u uˣ, galois_in u uˣ ->
          galois_in (dist1 u) (dist2 uˣ))
      (Hcond: forall r rˣ, galois_in r rˣ ->
          SoundAST (cond1 r) (cond2 rˣ))
      (Hbody: forall r rˣ, galois_in r rˣ ->
          SoundAST (body1 r) (body2 rˣ)):
      SoundAST (AST_While (b := false) init1 dist1 cond1 body1)
               (AST_While (b := true) init2 dist2 cond2 body2).
Proof.
  econstructor; auto.
  intros t tˣ Hin.
  simp bimap_csum app_csum.
  apply Hdist in Hin.
  destruct (dist1 t) as [[value rtrue] rfalse].
  destruct (dist2 tˣ) as [[valueˣ rtrueˣ] rfalseˣ]. cbn.
  econstructor.
  - apply Hin.
  - econstructor; auto. apply Hin.
  - constructor. apply Hin.
Qed.

Lemma sound'_aux {N Nvec: nat} {Tb R} {Tbˣ Rˣ: Typeˣ} {E Eˣ} {EvE: E =# Eˣ}
  {GC_Tb: Tbˣ <#> Tb} {GC_R: Rˣ <#> R}
  (bks1 : vec (Tb -> SurfaceAST E Eˣ (Fin.t N * Tb + R)
                        (TailBranch N * Tbˣ * Rˣ) false) Nvec)
  (bks2 : vec (Tbˣ -> SurfaceAST E Eˣ (Fin.t N * Tb + R)
                        (TailBranch N * Tbˣ * Rˣ) true) Nvec):
  (forall (i : Fin.t Nvec) (r : Tb) (rˣ : Tbˣ),
   galois_in r rˣ ->
   sound' (ast2itree (vec_nth bks1 i r)) (ast2aflow (vec_nth bks2 i rˣ))) ->
  vec_eq
    (fun b bˣ => forall tb tbˣ, galois_in tb tbˣ -> sound' (b tb) (bˣ tbˣ))
    (aux bks1) (aux' bks2).
Proof.
  induction Nvec as [|Nvec IH]; cbn; intros.
  * intros i; depelim3 i bks1 bks2.
  * intros i; depelim3 i bks1 bks2.
    intros; cbn; rewrite !vec_nth_F1; apply (H Fin.F1); auto.
    intros; cbn; rewrite !vec_nth_FS. apply IH; eauto.
    intros i r rˣ Hin. apply (H (Fin.FS i) _ _ Hin).
Qed.

(* Proof that the concrete and abstract interpreters generated by ast2itree
   and ast2aflow from the shared SurfaceAST notation are initially sound, i.e.
   they have the same control flow structure and their events/return values are
   related by Galois connections. *)
Theorem sound_ast2itree_ast2aflow {E Eˣ} {EvE: E =# Eˣ} {R} {Rˣ: Typeˣ} {GC_R: Rˣ <#> R}
    (p:  SurfaceAST E Eˣ R Rˣ false)
    (pˣ: SurfaceAST E Eˣ R Rˣ true):
  SoundAST p pˣ ->
  sound' (ast2itree p) (ast2aflow pˣ).
Proof.
  induction 1; simp ast2itree ast2aflow.
  - apply sound'_ret. apply Hin.
  - eapply SoundTrigger with (f := id) (fˣ := id); [exact Hcompat|..].
    * cbn; now rewrite bind_ret_r.
    * now rewrite aflow_bind_ret_r.
  - apply sound'_seq; eauto. apply seq_sound_effects_init.
  - eapply (sound'_if (Valueˣ := Valueˣ)); eauto.
  - eapply sound'_do; eauto. apply do_sound_effects_init. auto.
  - eapply sound'_tailmrec; eauto. now apply sound'_aux.
    apply tailmrec_sound_effects_init_cfg.
Qed.

Module surface.
  Declare Scope surface.
  Bind Scope surface with SurfaceAST.

  Notation "'ret' x 'and' y" :=
    (AST_Ret (mksum x y))
      (at level 49, x at next level, y at next level, no associativity) : surface.
  Notation "'ret' f 'and' g 'on' x" :=
    (AST_Ret (bimap_csum x f g))
      (at level 49, f at next level, x at next level, g at next level, no associativity): surface.
  Notation "'ret' f 'and' g 'on' x 'and' y" :=
    (AST_Ret (bimap2_csum x y f g))
      (at level 49, f at next level, x at next level, y at next level, g at next level, no associativity): surface.
  Notation "'do' x 'and' y 'on' z 'and' u" :=
    (AST_Event
       (bimap2_csum z u (fun a b => (subevent _ (x a b))) (fun a b => (subevent _ (y a b)))))
      (at level 49, x at next level, y at next level, z at next level, u at next level, no associativity) : surface.
   Notation "'do' x 'and' y 'on' z" :=
    (AST_Event
       (bimap_csum z (fun a => (subevent _ (x a))) (fun a => (subevent _ (y a)))))
      (at level 49, x at next level, y at next level, z at next level, no associativity) : surface.
  Notation "'do' x 'and' y" :=
    (AST_Event (mksum (subevent _ x) (subevent _ y)))
      (at level 49, x at next level, y at next level, no associativity) : surface.
  Notation "x <- t ;; u"   := (AST_Seq t (fun x => u))
    (at level 61, t at next level, right associativity) : surface.
  Notation "t ;; u"   := (AST_Seq t (fun _ => u)) (at level 61, right associativity): surface.
  Notation "'if' v 'then' x 'else' y"  := (AST_If v x y) (at level 40) : surface.
  (* Notation "'while' b 'do' c" := (AST_While b c) (at level 40) : surface. *)
  Notation "'while_u' b 'do' c" := (AST_While_unit b c) (at level 40) : surface.
End surface.
