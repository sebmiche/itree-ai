(* ** Monad iterators

This module provides functions to iterate monadically on lists and vectors.
(Not all of them are used currently, some are there for historical reasons.)

On lists:
* mapiM: monadic map with index (f: nat -> α -> m β)
* foldriM: monadic fold with index (f: nat -> α -> β -> m β)

On vectors:
* vec_mapM: monadic map (f: α -> m β)
* vec_mapiM: monadic map with index (f: nat -> α -> m β)
* vec_foldrM: monadic fold (f: α -> β -> m β)
* vec_foldriM: monadic fold with index (f: nat -> α -> β -> m β)

These come with iteration principles, equality up-to eutt and state eutt, and
transformation under interp and interp_state. *)

From Coq Require Import Lists.List Lia Bool Program.Equality.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad.
From ITree Require Import ITree ITreeFacts Events.State Events.StateFacts.
From ITreeAI Require Import LatticeVec LatticePprod AflowMonad Util Util.List.

Import PeanoNat.Nat.
Import Basics.Monads.
Import ListNotations.
Import MonadNotation.
Import PprodNotations.
Local Open Scope list_scope.
Local Open Scope monad_scope.
Local Open Scope bool_scope.

(* ============================================================================
** Monadic map with index on lists *)

Fixpoint mapiM_aux {α β: Type} {m} `{Monad m}
    (i: nat) (f: nat -> α -> m β) (l: list α): m (list β) :=
  match l with
  | [] => ret []
  | a :: as_ => b <- f i a;; bs <- mapiM_aux (i + 1) f as_;; ret (b :: bs)
  end.

Definition mapiM {α β m} `{Monad m} (f: nat -> α -> m β) l :=
  mapiM_aux 0 f l.

(* This induction principle comes with extra spice because the parameter `i` of
   `f` stays within the original 0..N range even as the size of `l` varies.
   The same applies to the values, so we explicitly write out the property that
   the inductive list is a sublist of `l_orig` and bridge in `mapiM_iter`. *)
Lemma mapiM_aux_iter {α β} {m} `{Monad m} (f: nat -> α -> m β) l_orig N i l
    (I: m (list β) -> Prop):
  N = length l_orig ->
  I (ret []) ->
  (exists l', l' ++ l = l_orig) ->
  i + length l = N ->
  (forall i l, i + length l = N -> (exists l', l' ++ l = l_orig) ->
    forall x j a,
      I x -> j < length l -> nth_error l j = Some a ->
      I (b <- f (i + j) a;; bs <- x;; ret (b :: bs))) ->
  I (mapiM_aux i f l).
Proof.
  intros HN Hret Hl' Hi Hstep; revert i Hi Hl'.
  induction l; auto; intros; cbn.
  rewrite <- (add_0_r i) at 1.
  apply (Hstep _ _ Hi); auto; [|cbn; lia].
  apply IHl. cbn in Hi; lia.
  destruct Hl' as [l' Hl'].
  exists (l' ++ [a]). now rewrite <- app_assoc.
Qed.

Lemma mapiM_iter {α β} {m} `{Monad m} (f: nat -> α -> m β) l
    (I: m (list β) -> Prop):
  I (ret []) ->
  (forall x i a,
    I x -> i < length l -> nth_error l i = Some a ->
    I (b <- f i a;; bs <- x;; ret (b :: bs))) ->
  I (mapiM f l).
Proof.
  unfold mapiM; intros Hret Hf.
  apply (mapiM_aux_iter f l (length l) 0 l); auto. now exists [].
  intros i lr Hi [ll Hll] x j a Hx Hj Hnth.
  assert (i = length ll) as -> by (rewrite <- Hll, app_length in Hi; lia).
  apply Hf; auto; [lia|].
  rewrite <- Hll. rewrite nth_error_app2; [|lia].
  now assert (length ll + j - length ll = j) as -> by lia.
Qed.

Lemma mapiM_iter_state {α β F S} (f: nat -> α -> stateT S (itree F) β) l
    (I: stateT S (itree F) (list β) -> Prop):
  I (ret []) ->
  (forall x i a,
    I x -> i < length l -> nth_error l i = Some a ->
    I (b <- f i a;; bs <- x;; ret (b :: bs))) ->
  I (mapiM f l).
Proof. apply mapiM_iter. Qed.

Lemma mapiM_aux_eutt {α α' β E}
  (f: nat -> α -> itree E β) (f': nat -> α' -> itree E β) l l' i j lr lr':
  (forall i lr lr',
    (exists ll, length ll = i /\ ll ++ lr = l) ->
    (exists ll', length ll' = i /\ ll' ++ lr' = l') ->
    forall j a a',
      nth_error lr j = Some a ->
      nth_error lr' j = Some a' ->
      f (i + j) a ≈ f' (i + j) a') ->
  i + j = length l ->
  i + j = length l' ->
  length lr = j ->
  length lr' = j ->
  (exists ll, length ll = i /\ ll ++ lr = l) ->
  (exists ll', length ll' = i /\ ll' ++ lr' = l') ->
  mapiM_aux i f lr ≈ mapiM_aux i f' lr'.
Proof.
  intros Hf; revert i lr lr'.
  induction j.
  - intros. rewrite length_zero_iff_nil in *; now subst.
  - intros i lr lr' Hlen Hlen' Hj Hj' [ll [Hll Hl]] [ll' [Hll' Hl']].
    destruct lr as [|x lr]; inversion Hj.
    destruct lr' as [|x' lr']; inversion Hj'. cbn.
    apply eutt_eq_bind'.
    { rewrite <- (add_0_r i). apply (Hf i (x::lr) (x'::lr')); try easy.
      exists ll; auto. exists ll'; auto. }
    intros b.
    apply eutt_eq_bind'; [|reflexivity].
    apply IHj; auto; try lia.
    * exists (ll ++ [x]). rewrite app_length, <- app_assoc; cbn; auto.
    * exists (ll' ++ [x']). rewrite app_length, <- app_assoc; cbn; auto.
Qed.

Lemma mapiM_eutt {α α' β E}
  (f: nat -> α -> itree E β) (f': nat -> α' -> itree E β) l l':
  length l = length l' ->
  (forall i a a',
    nth_error l i = Some a ->
    nth_error l' i = Some a' ->
    f i a ≈ f' i a') ->
  mapiM f l ≈ mapiM f' l'.
Proof.
  intros Hlen Hf.
  apply (mapiM_aux_eutt f f' l l' 0 (length l) l l');
    try lia; try now exists [].
  intros i lr lr' [ll [Hll Hl]] [ll' [Hll' Hl']] j a a' Ha Ha'.
  apply Hf.
  - rewrite <- Hl, nth_error_app2; [| lia].
    now assert (i + j - length ll = j) as -> by lia.
  - rewrite <- Hl', nth_error_app2; [| lia].
    now assert (i + j - length ll' = j) as -> by lia.
Qed.

Lemma interp_mapiM {α β E F} (h: E ~> itree F) (f: nat -> α -> itree E β) l:
  interp h (mapiM f l) ≈ mapiM (fun i a => interp h (f i a)) l.
Proof.
  unfold mapiM. generalize 0 as i_start.
  induction l; intros; cbn.
  - now rewrite interp_ret.
  - rewrite interp_bind. apply eutt_eq_bind; intros b'.
    rewrite interp_bind. apply eutt_eq_bind'.
    apply IHl. intros; now rewrite interp_ret.
Qed.

Lemma interp_state_mapiM {α β E S F} (h: E ~> stateT S (itree F))
  (f: nat -> α -> itree E β) l s:
  interp_state h (mapiM f l) s ≈ mapiM (fun i a => interp_state h (f i a)) l s.
Proof.
  unfold mapiM. generalize 0 as i_start. revert s.
  induction l; intros; cbn.
  - now rewrite interp_state_ret.
  - rewrite interp_state_bind. apply eutt_eq_bind; intros [s' b'].
    rewrite interp_state_bind. apply eutt_eq_bind'.
    apply IHl. intros []; now rewrite interp_state_ret.
Qed.


(* ============================================================================
** Monadic fold with index on lists *)

Fixpoint foldriM_aux {α β} {m} `{Monad m}
    (i: nat) (args: list α) (b: β) (f: nat -> α -> β -> m β): m β :=
  match args with
  | [] => ret b
  | a :: args => b' <- foldriM_aux (S i) args b f;; f i a b'
  end.

Definition foldriM {α β} {m} `{Monad m}
    (args: list α) (b: β) (f: nat -> α -> β -> m β): m β :=
  foldriM_aux 0 args b f.

Lemma foldriM_aux_iter {α β} {m} `{Monad m} (f: nat -> α -> β -> m β)
    l N i lr b (I: m β -> Prop):
  N = length l ->
  I (ret b) ->
  (exists ll, ll ++ lr = l) ->
  i + length lr = N ->
  (forall i lr, i + length lr = N -> (exists ll, ll ++ lr = l) ->
    forall x j a,
      I x -> nth_error lr j = Some a ->
      I (b' <- x;; f (i + j) a b')) ->
  I (foldriM_aux i lr b f).
Proof.
  intros HN Hret Hl Hi Hf; revert i Hi b Hret Hl.
  induction lr; auto; intros; cbn.
  rewrite <- (add_0_r i).
  apply Hf with (lr := a::lr); auto.
  apply IHlr; auto. cbn in Hi; lia.
  destruct Hl as [ll Hll].
  exists (ll ++ [a]); now rewrite <- app_assoc.
Qed.

Lemma foldriM_iter {α β} {m} `{Monad m} (f: nat -> α -> β -> m β)
    (l: list α) (b: β) (I: m β -> Prop):
  I (ret b) ->
  (forall x i a, I x -> nth_error l i = Some a -> I (b' <- x;; f i a b')) ->
  I (foldriM l b f).
Proof.
  intros Hret Hf. unfold foldriM.
  apply (foldriM_aux_iter f l (length l) 0 l b); auto.
  now exists [].
  intros i lr Hi [ll Hll] x j a Hx Ha.
  apply Hf; auto.
  assert (i = length ll) as -> by (rewrite <- Hll, app_length in Hi; lia).
  rewrite <- Hll, nth_error_app2; [|lia].
  now assert (length ll + j - length ll = j) as -> by lia.
Qed.

Lemma foldriM_iter_state {α β S F} (f: nat -> α -> β -> stateT S (itree F) β)
    (l: list α) (b: β) (I: (stateT S (itree F) β) -> Prop):
  I (ret b) ->
  (forall x i a, I x -> nth_error l i = Some a -> I (b' <- x;; f i a b')) ->
  I (foldriM l b f).
Proof. apply foldriM_iter. Qed.

Lemma foldriM_aux_eutt {α α' β E}
  (f: nat -> α -> β -> itree E β) (f': nat -> α' -> β -> itree E β)
    l l' i j lr lr' b:
  (forall i lr lr',
    (exists ll, length ll = i /\ ll ++ lr = l) ->
    (exists ll', length ll' = i /\ ll' ++ lr' = l') ->
    forall j b a a',
      nth_error lr j = Some a ->
      nth_error lr' j = Some a' ->
      f (i + j) a b ≈ f' (i + j) a' b) ->
  i + j = length l ->
  i + j = length l' ->
  length lr = j ->
  length lr' = j ->
  (exists ll, length ll = i /\ ll ++ lr = l) ->
  (exists ll', length ll' = i /\ ll' ++ lr' = l') ->
  foldriM_aux i lr b f ≈ foldriM_aux i lr' b f'.
Proof.
  intros Hf; revert i b lr lr'.
  induction j.
  - intros. rewrite length_zero_iff_nil in *; now subst.
  - intros i b lr lr' Hlen Hlen' Hj Hj' [ll [Hll Hl]] [ll' [Hll' Hl']].
    destruct lr as [|x lr]; inversion Hj.
    destruct lr' as [|x' lr']; inversion Hj'. cbn.
    apply eutt_eq_bind'.
    { apply IHj; auto; try lia.
      * exists (ll ++ [x]). rewrite app_length, <- app_assoc.
        cbn; split; [lia|auto].
      * exists (ll' ++ [x']). rewrite app_length, <- app_assoc.
        cbn; split; [lia|auto]. }
    intros b'.
    rewrite <- (add_0_r i). apply (Hf i (x::lr) (x'::lr')); try easy.
    exists ll; auto. exists ll'; auto.
Qed.

Lemma foldriM_eutt {α α' β E}
  (f: nat -> α -> β -> itree E β) (f': nat -> α' -> β -> itree E β) l l' b:
  length l = length l' ->
  (forall i b a a',
    nth_error l i = Some a ->
    nth_error l' i = Some a' ->
    f i a b ≈ f' i a' b) ->
  foldriM l b f ≈ foldriM l' b f'.
Proof.
  intros Hlen Hf.
  apply (foldriM_aux_eutt f f' l l' 0 (length l) l l' b); auto;
    try now exists [].
  intros i lr lr' [ll [Hll Hl]] [ll' [Hll' Hl']] j b' a a' Ha Ha'.
  apply Hf.
  - rewrite <- Hl, nth_error_app2; [|lia].
    now assert (i + j - length ll = j) as -> by lia.
  - rewrite <- Hl', nth_error_app2; [|lia].
    now assert (i + j - length ll' = j) as -> by lia.
Qed.

Lemma interp_foldriM {α β E F} (h: E ~> itree F)
    (args: list α) (b: β) (f: nat -> α -> β -> itree E β):
  interp h (foldriM args b f) ≈
  foldriM args b (fun i a b => interp h (f i a b)).
Proof.
  unfold foldriM. generalize 0 as i. revert b.
  induction args; intros; cbn.
  - now rewrite interp_ret.
  - rewrite interp_bind. now rewrite IHargs.
Qed.

Lemma interp_state_foldriM {α β E S F} (h: E ~> stateT S (itree F))
    (args: list α) (b: β) (f: nat -> α -> β -> itree E β) s:
  interp_state h (foldriM args b f) s ≈
  foldriM args b (fun b i a => interp_state h (f b i a)) s.
Proof.
  unfold foldriM. generalize 0 as i. revert b s.
  induction args; intros; cbn.
  - now rewrite interp_state_ret.
  - rewrite interp_state_bind. now rewrite IHargs.
Qed.


(* ============================================================================
** Monadic map on vectors *)

Equations vec_mapM {N} {α β: Type} {m} `{Monad m} (v: vec α N) (f: α -> m β): m (vec β N) :=
  vec_mapM vec_nil f := ret vec_nil;
  vec_mapM (vec_cons a as_) f => b <- f a;; bs <- vec_mapM as_ f;; ret (vec_cons b bs).

Lemma vec_mapM_iter {α β: Type} {m} `{Monad m} (f: α -> m β) N (v: vec α N)
    (I: forall N', m (vec β N') -> Prop):
  I 0 (ret vec_nil) ->
  (forall N' x i,
    I N' x ->
    I (S N') (b <- f (vec_nth v i);; bs <- x;; ret (vec_cons b bs))) ->
  I N (vec_mapM v f).
Proof.
  intros H0. revert v. induction N; intros v HS.
  - dependent destruction v; auto.
  - dependent destruction v; cbn. simp vec_mapM. apply (HS N _ Fin.F1).
    apply IHN. intros N' x i. apply (HS N' x (Fin.FS i)).
Qed.

Lemma vec_mapM_iter_state {α β F S} (f: α -> stateT S (itree F) β)
    {N} (v: vec α N) (I: forall N', stateT S (itree F) (vec β N') -> Prop):
  I 0 (ret vec_nil) ->
  (forall N' x i,
    I N' x ->
    I (Datatypes.S N') (b <- f (vec_nth v i);; bs <- x;; ret (vec_cons b bs))) ->
  I N (vec_mapM v f).
Proof. apply vec_mapM_iter. Qed.

Lemma vec_mapM_eutt {α α' β E} (f: α -> itree E β) (f': α' -> itree E β)
    {N} (v: vec α N) (v': vec α' N):
  (forall i, f (vec_nth v i) ≈ f' (vec_nth v' i)) ->
  vec_mapM v f ≈ vec_mapM v' f'.
Proof.
  revert v v'. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_mapM.
    apply eutt_eq_bind'. apply (Heq Fin.F1). intros b.
    rewrite IHN. reflexivity.
    intros i. apply (Heq (Fin.FS i)).
Qed.

Lemma vec_mapM_state_eutt {α α' β S E}
    (f: α -> stateT S (itree E) β) (f': α' -> stateT S (itree E) β)
    {N} (v: vec α N) (v': vec α' N) s:
  (forall i s, f (vec_nth v i) s ≈ f' (vec_nth v' i) s) ->
  vec_mapM v f s ≈ vec_mapM v' f' s.
Proof.
  revert v v' s. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_mapM.
    apply eutt_eq_bind'. apply (Heq Fin.F1). intros [s' b]. cbn.
    rewrite IHN. reflexivity.
    intros i. apply (Heq (Fin.FS i)).
Qed.

Lemma vec_mapM_aflow_eq {α α' β E} (f: α -> aflow E β) (f': α' -> aflow E β)
    {N} (v: vec α N) (v': vec α' N):
  (forall i, aflow_eq (f (vec_nth v i)) (f' (vec_nth v' i))) ->
  aflow_eq (vec_mapM v f) (vec_mapM v' f').
Proof.
  revert v v'. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_mapM.
    apply aflow_eq_bind'. apply (Heq Fin.F1). intros b.
    rewrite IHN. reflexivity.
    intros i. apply (Heq (Fin.FS i)).
Qed.

Lemma interp_vec_mapM {α β E F} (h: E ~> itree F) (f: α -> itree E β) {N}
    (v: vec α N):
  interp h (vec_mapM v f) ≈ vec_mapM v (fun a => interp h (f a)).
Proof.
  revert v. induction N; intros.
  - dependent destruction v; simp vec_mapM; cbn. now rewrite interp_ret.
  - dependent destruction v; simp vec_mapM; cbn.
    rewrite interp_bind. apply eutt_eq_bind; intros b.
    rewrite interp_bind. apply eutt_eq_bind'. apply IHN.
    intros; now rewrite interp_ret.
Qed.

Lemma interp_state_vec_mapM {α β E S F} (h: E ~> stateT S (itree F))
    (f: α -> itree E β) {N} (v: vec α N) s:
  interp_state h (vec_mapM v f) s
  ≈ vec_mapM v (fun a => interp_state h (f a)) s.
Proof.
  revert v s. induction N; intros.
  - dependent destruction v; simp vec_mapM; cbn. now rewrite interp_state_ret.
  - dependent destruction v; simp vec_mapM; cbn.
    rewrite interp_state_bind. apply eutt_eq_bind; intros [s' b'].
    rewrite interp_state_bind. apply eutt_eq_bind'; cbn. apply IHN.
    intros []; now rewrite interp_state_ret.
Qed.

Lemma aflow_interp_vec_mapM {α β E F} (h: E ~> aflow F) (f: α -> aflow E β) {N}
    (v: vec α N):
  aflow_eq
    (aflow_interp h (vec_mapM v f))
    (vec_mapM v (fun a => aflow_interp h (f a))).
Proof.
  revert v. induction N; intros.
  - dependent destruction v; now simp vec_mapM.
  - dependent destruction v; simp vec_mapM; cbn.
    rewrite aflow_interp_bind. apply aflow_eq_bind; intros b.
    rewrite aflow_interp_bind. apply aflow_eq_bind'. apply IHN.
    reflexivity.
Qed.


(* ============================================================================
** Monadic map with index on vectors *)

Definition vec_mapiM {α β: Type} {N m} `{Monad m} (v: vec α N)
    (f: nat -> α -> m β): m (vec β N) :=
  vec_mapM (vec_combine (vec_range N) v) (fun '(ppair i a) => f i a).

Lemma vec_mapiM_iter {α β: Type} {m} `{Monad m} (f: nat -> α -> m β) {N}
    (v: vec α N) (I: forall N', m (vec β N') -> Prop):
  I 0 (ret vec_nil) ->
  (forall N' x i,
    I N' x ->
    I (S N') (b <- f (fin_nat i) (vec_nth v i);; bs <- x;; ret (vec_cons b bs))) ->
  I N (vec_mapiM v f).
Proof.
  intros H0 HS. unfold vec_mapiM. apply vec_mapM_iter; auto.
  intros. rewrite vec_nth_combine, vec_nth_range. now apply HS.
Qed.

Lemma vec_mapiM_iter_state {α β F S} (f: nat -> α -> stateT S (itree F) β)
    {N} (v: vec α N) (I: forall N', stateT S (itree F) (vec β N') -> Prop):
  I 0 (ret vec_nil) ->
  (forall N' x i,
    I N' x ->
    I (Datatypes.S N')
      (b <- f (fin_nat i) (vec_nth v i);; bs <- x;; ret (vec_cons b bs))) ->
  I N (vec_mapiM v f).
Proof. apply vec_mapiM_iter. Qed.

Lemma vec_mapiM_eutt {α α' β E N} (f: nat -> α -> itree E β)
    (f': nat -> α' -> itree E β) (v: vec α N) (v': vec α' N):
  (forall i, f (fin_nat i) (vec_nth v i) ≈ f' (fin_nat i) (vec_nth v' i)) ->
  vec_mapiM v f ≈ vec_mapiM v' f'.
Proof.
  intros. apply vec_mapM_eutt. intros.
  rewrite ! vec_nth_combine, ! vec_nth_range. auto.
Qed.

Lemma vec_mapiM_aflow_eq {α α' β E N} (f: nat -> α -> aflow E β)
    (f': nat -> α' -> aflow E β) (v: vec α N) (v': vec α' N):
  (forall i, aflow_eq
    (f (fin_nat i) (vec_nth v i))
    (f' (fin_nat i) (vec_nth v' i))) ->
  aflow_eq (vec_mapiM v f) (vec_mapiM v' f').
Proof.
  intros. apply vec_mapM_aflow_eq. intros.
  rewrite ! vec_nth_combine, ! vec_nth_range. auto.
Qed.

Lemma interp_vec_mapiM {α β E F} (h: E ~> itree F) (f: nat -> α -> itree E β)
    {N} (v: vec α N):
  interp h (vec_mapiM v f) ≈ vec_mapiM v (fun i a => interp h (f i a)).
Proof.
  unfold vec_mapiM. rewrite interp_vec_mapM. apply vec_mapM_eutt. intros.
  rewrite ! vec_nth_combine, ! vec_nth_range. reflexivity.
Qed.

Lemma interp_state_vec_mapiM {α β E S F} (h: E ~> stateT S (itree F))
  (f: nat -> α -> itree E β) {N} (v: vec α N) s:
  interp_state h (vec_mapiM v f) s
  ≈ vec_mapiM v (fun i a => interp_state h (f i a)) s.
Proof.
  unfold vec_mapiM. rewrite interp_state_vec_mapM.
  apply vec_mapM_state_eutt. intros i s'.
  rewrite ! vec_nth_combine, ! vec_nth_range. reflexivity.
Qed.

Lemma aflow_interp_vec_mapiM {α β E F} (h: E ~> aflow F)
    (f: nat -> α -> aflow E β) {N} (v: vec α N):
  aflow_eq
    (aflow_interp h (vec_mapiM v f))
    (vec_mapiM v (fun i a => aflow_interp h (f i a))).
Proof.
  unfold vec_mapiM. rewrite aflow_interp_vec_mapM. apply vec_mapM_aflow_eq.
  intros. rewrite ! vec_nth_combine, ! vec_nth_range. reflexivity.
Qed.


(* ============================================================================
** Monadic fold on vectors *)

Equations vec_foldrM {α β} {m} `{Monad m} {N} (v: vec α N) (b: β)
    (f: α -> β -> m β): m β :=
  vec_foldrM vec_nil b f := ret b;
  vec_foldrM (vec_cons a u) b f := b' <- vec_foldrM u b f;; f a b'.

Lemma vec_foldrM_iter {α β} {m} `{Monad m} (f: α -> β -> m β) {N}
    (v: vec α N) (b: β) (I: m β -> Prop):
  I (ret b) ->
  (forall x i, I x -> I (b' <- x;; f (vec_nth v i) b')) ->
  I (vec_foldrM v b f).
Proof.
  revert b v. induction N; intros * H0 HS.
  - now dependent destruction v.
  - dependent destruction v; simp vec_foldrM. apply (HS _ Fin.F1).
    apply IHN; auto. intros. now apply (HS _ (Fin.FS i)).
Qed.

Lemma vec_foldrM_iter_state {α β S F} (f: α -> β -> stateT S (itree F) β) {N}
    (v: vec α N) (b: β) (I: (stateT S (itree F) β) -> Prop):
  I (ret b) ->
  (forall x i, I x -> I (b' <- x;; f (vec_nth v i) b')) ->
  I (vec_foldrM v b f).
Proof. apply vec_foldrM_iter. Qed.

Lemma vec_foldrM_eutt {α α' β E} {N} (f: α -> β -> itree E β)
    (f': α' -> β -> itree E β) (v: vec α N) (v': vec α' N) b:
  (forall i b, f (vec_nth v i) b ≈ f' (vec_nth v' i) b) ->
  vec_foldrM v b f ≈ vec_foldrM v' b f'.
Proof.
  revert v v' b. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_foldrM; cbn.
    apply eutt_eq_bind'. apply IHN. intros; apply (Heq (Fin.FS i)).
    intros. apply (Heq Fin.F1).
Qed.

Lemma vec_foldrM_aflow_eq {α α' β E} {N} (f: α -> β -> aflow E β)
    (f': α' -> β -> aflow E β) (v: vec α N) (v': vec α' N) b:
  (forall i b, aflow_eq
    (f (vec_nth v i) b)
    (f' (vec_nth v' i) b)) ->
  aflow_eq (vec_foldrM v b f) (vec_foldrM v' b f').
Proof.
  revert v v' b. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_foldrM; cbn.
    apply aflow_eq_bind'. apply IHN. intros; apply (Heq (Fin.FS i)).
    hnf; intros. apply (Heq Fin.F1).
Qed.

Lemma vec_foldrM_state_eutt {α α' β S E}
    (f: α -> β -> stateT S (itree E) β) (f': α' -> β -> stateT S (itree E) β)
    {N} (v: vec α N) (v': vec α' N) (b: β) s:
  (forall i b s, f (vec_nth v i) b s ≈ f' (vec_nth v' i) b s) ->
  vec_foldrM v b f s ≈ vec_foldrM v' b f' s.
Proof.
  revert v v' b s. induction N; intros * Heq.
  - dependent destruction v; now dependent destruction v'.
  - dependent destruction v; dependent destruction v'; simp vec_foldrM; cbn.
    apply eutt_eq_bind'. rewrite IHN. reflexivity.
    intros i. apply (Heq (Fin.FS i)).
    intros []; cbn; apply (Heq Fin.F1).
Qed.

Lemma interp_vec_foldrM {α β E F} (h: E ~> itree F) {N} (v: vec α N) (b: β)
    (f: α -> β -> itree E β):
  interp h (vec_foldrM v b f) ≈
  vec_foldrM v b (fun a b => interp h (f a b)).
Proof.
  revert v b. induction N; intros.
  - dependent destruction v; simp vec_foldrM; cbn. now rewrite interp_ret.
  - dependent destruction v; simp vec_foldrM; cbn.
    rewrite interp_bind. apply eutt_eq_bind'. apply IHN.
    intros; reflexivity.
Qed.

Lemma interp_state_vec_foldrM {α β E S F} (h: E ~> stateT S (itree F))
    {N} (v: vec α N) (b: β) (f: α -> β -> itree E β) s:
  interp_state h (vec_foldrM v b f) s ≈
  vec_foldrM v b (fun b a => interp_state h (f b a)) s.
Proof.
  revert v b. induction N; intros.
  - dependent destruction v; simp vec_foldrM; cbn. now rewrite interp_state_ret.
  - dependent destruction v; simp vec_foldrM; cbn.
    rewrite interp_state_bind. apply eutt_eq_bind'. apply IHN.
    intros []; reflexivity.
Qed.

Lemma aflow_interp_vec_foldrM {α β E F} (h: E ~> aflow F) {N} (v: vec α N)
    (b: β) (f: α -> β -> aflow E β):
  aflow_eq
    (aflow_interp h (vec_foldrM v b f))
    (vec_foldrM v b (fun a b => aflow_interp h (f a b))).
Proof.
  revert v b. induction N; intros.
  - now dependent destruction v.
  - dependent destruction v; simp vec_foldrM; cbn.
    rewrite aflow_interp_bind. apply aflow_eq_bind'. apply IHN. reflexivity.
Qed.


(* ============================================================================
** Monadic fold with index on vectors *)

Definition vec_foldriM {α β} {N m} `{Monad m} (v: vec α N) (b: β)
    (f: nat -> α -> β -> m β): m β :=
  vec_foldrM (vec_combine (vec_range N) v) b (fun '(ppair i a) b => f i a b).

Lemma vec_foldriM_iter {α β} {N m} `{Monad m} (f: nat -> α -> β -> m β)
    (v: vec α N) (b: β) (I: m β -> Prop):
  I (ret b) ->
  (forall x i, I x -> I (b' <- x;; f (fin_nat i) (vec_nth v i) b')) ->
  I (vec_foldriM v b f).
Proof.
  intros H0 HS. unfold vec_foldriM. apply vec_foldrM_iter; auto.
  intros. rewrite vec_nth_combine, vec_nth_range. now apply HS.
Qed.

Lemma vec_foldriM_iter_state {α β N S F}
    (f: nat -> α -> β -> stateT S (itree F) β)
    (v: vec α N) (b: β) (I: stateT S (itree F) β -> Prop):
  I (ret b) ->
  (forall x i, I x -> I (b' <- x;; f (fin_nat i) (vec_nth v i) b')) ->
  I (vec_foldriM v b f).
Proof. apply vec_foldriM_iter. Qed.

Lemma vec_foldriM_eutt {α α' β N E}
    (f: nat -> α -> β -> itree E β) (f': nat -> α' -> β -> itree E β)
    (v: vec α N) (v': vec α' N) b:
  (forall i b,
    f (fin_nat i) (vec_nth v i) b ≈ f' (fin_nat i) (vec_nth v' i) b) ->
  vec_foldriM v b f ≈ vec_foldriM v' b f'.
Proof.
  intros Heq. apply vec_foldrM_eutt. intros.
  rewrite ! vec_nth_combine, ! vec_nth_range. apply Heq.
Qed.

Lemma vec_foldriM_aflow_eq {α α' β N E}
    (f: nat -> α -> β -> aflow E β) (f': nat -> α' -> β -> aflow E β)
    (v: vec α N) (v': vec α' N) b:
  (forall i b, aflow_eq
    (f (fin_nat i) (vec_nth v i) b)
    (f' (fin_nat i) (vec_nth v' i) b)) ->
  aflow_eq (vec_foldriM v b f) (vec_foldriM v' b f').
Proof.
  intros Heq. apply vec_foldrM_aflow_eq. intros.
  rewrite ! vec_nth_combine, ! vec_nth_range. apply Heq.
Qed.

Lemma interp_vec_foldriM {α β E F N} (h: E ~> itree F)
    (v: vec α N) (b: β) (f: nat -> α -> β -> itree E β):
  interp h (vec_foldriM v b f) ≈
  vec_foldriM v b (fun i a b => interp h (f i a b)).
Proof.
  unfold vec_foldriM. rewrite interp_vec_foldrM.
  apply vec_foldrM_eutt. intros.
  now rewrite ! vec_nth_combine, ! vec_nth_range.
Qed.

Lemma interp_state_vec_foldriM {α β E S F N} (h: E ~> stateT S (itree F))
    (v: vec α N) (b: β) (f: nat -> α -> β -> itree E β) s:
  interp_state h (vec_foldriM v b f) s ≈
  vec_foldriM v b (fun b i a => interp_state h (f b i a)) s.
Proof.
  unfold vec_foldriM. rewrite interp_state_vec_foldrM.
  apply vec_foldrM_state_eutt. intros.
  now rewrite ! vec_nth_combine, ! vec_nth_range.
Qed.

Lemma aflow_interp_vec_foldriM {α β E F N} (h: E ~> aflow F)
    (v: vec α N) (b: β) (f: nat -> α -> β -> aflow E β):
  aflow_eq
    (aflow_interp h (vec_foldriM v b f))
    (vec_foldriM v b (fun i a b => aflow_interp h (f i a b))).
Proof.
  unfold vec_foldriM. rewrite aflow_interp_vec_foldrM.
  apply vec_foldrM_aflow_eq. intros.
  now rewrite ! vec_nth_combine, ! vec_nth_range.
Qed.
