(* ** Finite integer vectors

This file defines a `natvec n` type which is a vector of n natural integers.
Such types are used as measures for lattices. Using integers directly is too
restrictive, as not all well-founded sets have integral measures (an easy
example is ℕ ∪ {+∞}; a lattice example is the lattice of maps).

More complex structures could be used instead (a promising idea seems to be to
copy the lattice structure, eg. to have a map of measures for the measure of a
map), but integer vectors strike a reasonable compromise with minimal
complexity involved.

We equip the vectors with lexicographic order, element-wise addition (which is
compatible with the order) and a maximum operation which forces lengths to
match and is useful to combine lattice measures of differing vector size. *)

From Equations Require Import Equations.
From Coq Require Import ZArith DecidableType Lia.

(* The set of natural vectors of size `n` equipped with lexicographic order and
   element-wise addition. *)

Fixpoint natvec n: Set :=
  match n with
  | 0 => unit
  | S m => nat * natvec m
  end.

Fixpoint natvec_zero n: natvec n :=
  match n return natvec n with
  | 0 => tt
  | S m => (0, natvec_zero m)
  end.

Fixpoint natvec_le {n} (u v: natvec n): Prop :=
  match n, u, v with
  | 0, tt, tt => True
  | S m, (xu, u'), (xv, v') => (xu < xv) \/ ((xu = xv) /\ natvec_le u' v')
  end.

Fixpoint natvec_lt {n} (u v: natvec n): Prop :=
  match n, u, v with
  | 0, tt, tt => False
  | S m, (xu, u'), (xv, v') => (xu < xv) \/ ((xu = xv) /\ natvec_lt u' v')
  end.

Fixpoint natvec_add {n} (x y: natvec n): natvec n :=
  match n, x, y with
  | 0, tt, tt => tt
  | S m, (x0, x), (y0, y) => (x0+y0, natvec_add x y)
  end.

Infix "≺" := natvec_lt (at level 80).
Infix "≼" := natvec_le (at level 80).
Infix "⊕" := natvec_add (at level 60).

(* Basic properties of orders *)

#[global]
Instance natvec_lt_Transitive {n}: Transitive (@natvec_lt n).
Proof.
  induction n.
  - intros [] [] []. now cbn.
  - intros [x0 x] [y0 y] [z0 z] [|[]] [|[]]; try (left; lia).
    right; split. lia. eapply IHn; eauto.
Defined.

#[global]
Instance natvec_le_Reflexive {n}: Reflexive (@natvec_le n).
Proof.
  induction n.
  - intros []. now cbn.
  - intros [x0 x]. right; auto.
Defined.

Lemma natvec_le': forall {n} (u v: natvec n),
  u ≼ v <-> u ≺ v \/ u = v.
Proof.
  induction n.
  - intros [] []. intuition (easy || auto).
  - intros [u0 u] [v0 v]. split.
    intros [Hle|[? Hle]]. left. now left. apply IHn in Hle. intuition.
      left. now right. subst; now right.
    intros [[]|Heq]. now left. right; intuition auto with *.
    now rewrite Heq.
Qed.

#[global]
Instance natvec_le_Transitive {n}: Transitive (@natvec_le n).
Proof.
  hnf. intros *. rewrite ! natvec_le'. intros [] []; subst; auto.
  left. now (transitivity y).
Defined.

Lemma natvec_le_of_lt {n}: forall (u v: natvec n),
  u ≺ v -> u ≼ v.
Proof. intros. rewrite natvec_le'. auto. Qed.

Lemma natvec_le_lt {n}: forall {x: natvec n} y {z},
  x ≼ y -> y ≺ z -> x ≺ z.
Proof.
  setoid_rewrite natvec_le'. intros * [] ?. etransitivity; eauto. now subst.
Qed.

Lemma natvec_lt_le {n}: forall {x: natvec n} y {z},
  x ≺ y -> y ≼ z -> x ≺ z.
Proof.
  setoid_rewrite natvec_le'. intros * ? []. etransitivity; eauto. now subst.
Qed.

Lemma natvec_zero_le {n}: forall x, natvec_zero n ≼ x.
Proof.
  induction n.
  - intros []. now cbn.
  - intros [[] x]. right; auto. left; lia.
Qed.

(* The lexicographic order is well-founded *)

(* Propagating Acc through ≼ rather than ≺. This is possible because we can
   simply go down one level and use transitivity with one ≺ and one ≼ *)
Lemma natvec_Acc_le {n}: forall {x: natvec n} y,
  x ≼ y -> Acc natvec_lt y -> Acc natvec_lt x.
Proof.
  intros * Hlexy HAccy. inversion_clear HAccy as [HAcclty].
  constructor. intros z Hltzx. apply HAcclty. now apply (natvec_lt_le x).
Qed.

(* This lemma allows well-founded induction on objects that have a natvec
   measure rather than on the measure itself. It's like [ltof]. *)
Definition natvecof {T n} (measure: T -> natvec n) :=
  fun x y => measure x ≺ measure y.

Lemma well_founded_natvecof_ {T n} (measure: T -> natvec n):
  well_founded (@natvec_lt n) ->
  well_founded (natvecof measure).
Proof.
  intros Hwf.
  assert (Huniv: forall m x, measure x ≺ m -> Acc (natvecof measure) x).
  { induction m as [m IHm] using (well_founded_induction Hwf).
    intros x Hx. constructor. now apply IHm. }
  (* We don't directly have a measure above (measure x) so unfold one step *)
  intro. constructor. intros. eapply Huniv. eauto.
Qed.

Proposition natvec_wf {n}:
  well_founded (@natvec_lt n).
Proof.
  induction n.
  - intros []. constructor. intros [] [].
  - intros u.
    induction u as [u IH1] using
      (well_founded_induction (well_founded_ltof (natvec (S n)) fst)).
    induction u as [[u0 u] IH2] using
      (well_founded_induction (well_founded_natvecof_ snd IHn)).
    constructor. intros [v0 v] [Hvu|[-> Hvu]].
    * apply IH1. now unfold ltof.
    * apply IH2. now unfold natvecof. apply IH1.
Qed.

Definition well_founded_natvecof {T n} (measure: T -> natvec n) :=
  well_founded_natvecof_ measure (@natvec_wf n).

(* Compatibility of addition with order *)


Lemma natvec_add_lt {n}: forall (x y z t: natvec n),
  x ≺ z -> y ≺ t -> x ⊕ y ≺ z ⊕ t.
Proof.
  induction n.
  - intros [] [] [] []; auto.
  - intros [x0 x] [y0 y] [z0 z] [t0 t] [|[]] [|[]]; try (left; lia).
    subst; right; auto.
Qed.

Lemma natvec_add_lt_r {n}: forall (x y z: natvec n),
  x ≺ y -> x ⊕ z ≺ y ⊕ z.
Proof.
  induction n.
  - intros [] [] []; auto.
  - intros [x0 x] [y0 y] [z0 z] [|[]]. left; lia. right; subst; auto.
Qed.

Lemma natvec_add_lt_l {n}: forall (x y z: natvec n),
  y ≺ z -> x ⊕ y ≺ x ⊕ z.
Proof.
  induction n.
  - intros [] [] []; auto.
  - intros [x0 x] [y0 y] [z0 z] [|[]]. left; lia. right; subst; auto.
Qed.

#[export] Hint Resolve natvec_add_lt: core.
#[export] Hint Resolve natvec_add_lt_r: core.
#[export] Hint Resolve natvec_add_lt_l: core.

Lemma natvec_add_le {n}: forall (x y z t: natvec n),
  x ≼ z -> y ≼ t -> x ⊕ y ≼ z ⊕ t.
Proof.
  intros * Hxz Hyt. rewrite natvec_le' in *. destruct Hxz, Hyt; subst; auto.
Qed.

Lemma natvec_add_le_lt {n}: forall (x y z t: natvec n),
  x ≼ z -> y ≺ t -> x ⊕ y ≺ z ⊕ t.
Proof.
  intros * Hxz Hyt. rewrite natvec_le' in Hxz. destruct Hxz; subst; auto.
Qed.

Lemma natvec_add_lt_le {n}: forall (x y z t: natvec n),
  x ≺ z -> y ≼ t -> x ⊕ y ≺ z ⊕ t.
Proof.
  intros * Hxz Hyt. rewrite natvec_le' in Hyt. destruct Hyt; subst; auto.
Qed.

#[export] Hint Resolve natvec_add_le: core.
#[export] Hint Resolve natvec_add_le_lt: core.
#[export] Hint Resolve natvec_add_lt_le: core.

(* Injection into longer vectors. This is used when combining two measures that
   might not use the same vector length. *)

(* Dependent transport along an equality *)
Definition _tp {n m} (H: n=m): natvec n -> natvec m :=
  fun x => eq_rect _ _ x _ H.
(* And the tactic to eliminate it, which is possible because equality on the
   parameter (n: nat) is decidable.*)
Ltac _depeq :=
  match goal with
  | |- context[_tp ?EQPROOF _] =>
    unfold _tp;
    rewrite <- EQPROOF;
    erewrite <- ! Eqdep_dec.eq_rect_eq_dec; try apply Nat.eq_dec
  end.

Fixpoint natvec_extend {n} m (x: natvec n): natvec (n+m) :=
  match m with
  | 0 => _tp (plus_n_O n) x
  | S p => _tp (plus_n_Sm n p) (0, natvec_extend p x)
  end.

Lemma le_plus_minus_r n m : n <= m -> n + (m - n) = m.
Proof. lia. Qed.

Definition natvec_extend' {n m}: n <= m -> natvec n -> natvec m :=
  fun H x => _tp (le_plus_minus_r n m H) (natvec_extend (m-n) x).

Definition natvec_max_l n m (x: natvec n): natvec (max n m) :=
  match le_ge_dec n m with
  | left Pn_le_m  => _tp (eq_sym (max_r n m Pn_le_m)) (natvec_extend' Pn_le_m x)
  | right Pm_le_n => _tp (eq_sym (max_l n m Pm_le_n)) x
  end.

Definition natvec_max_r n m (x: natvec m): natvec (max n m) :=
  match le_ge_dec n m with
  | left  Pn_le_m  => _tp (eq_sym (max_r n m Pn_le_m)) x
  | right Pm_le_n => _tp (eq_sym (max_l n m Pm_le_n)) (natvec_extend' Pm_le_n x)
  end.

Lemma natvec_max_l_lt {n m}: forall (x y: natvec n),
  x ≺ y -> natvec_max_l n m x ≺ natvec_max_l n m y.
Proof.
  unfold natvec_max_l; destruct (le_ge_dec n m).
  - intros. _depeq. unfold natvec_extend'. _depeq.
    generalize (m - n) as p. induction p.
    * intros. cbn. now _depeq.
    * intros. cbn. _depeq. right; intuition.
  - intros. now _depeq.
Qed.

Lemma natvec_max_r_lt {n m}: forall (x y: natvec m),
  x ≺ y -> natvec_max_r n m x ≺ natvec_max_r n m y.
Proof.
  unfold natvec_max_r; destruct (le_ge_dec n m).
  - intros. now _depeq.
  - intros. _depeq. unfold natvec_extend'. _depeq.
    generalize (n - m) as p. induction p.
    * intros. cbn. now _depeq.
    * intros. cbn. _depeq. right; intuition.
Qed.

#[export] Hint Resolve natvec_max_l_lt: core.
#[export] Hint Resolve natvec_max_r_lt: core.

Lemma natvec_max_l_le {n m}: forall (x y: natvec n),
  x ≼ y -> natvec_max_l n m x ≼ natvec_max_l n m y.
Proof. setoid_rewrite natvec_le'. intros * []; subst; auto. Qed.

Lemma natvec_max_r_le {n m}: forall (x y: natvec m),
  x ≼ y -> natvec_max_r n m x ≼ natvec_max_r n m y.
Proof. setoid_rewrite natvec_le'. intros * []; subst; auto. Qed.

#[export] Hint Resolve natvec_max_l_le: core.
#[export] Hint Resolve natvec_max_r_le: core.

Lemma natvec_add_comm: forall {n} (a b : natvec n), a ⊕ b = b ⊕ a.
Proof.
  induction n; intros.
  - depelim a; depelim b; auto.
  - depelim a; depelim b.
    specialize (IHn n1 n3).
    simpl; f_equal; auto with arith.
Qed.

Lemma natvec_add_assoc: forall {n} (a b c : natvec n), (a ⊕ b) ⊕ c = a ⊕ (b ⊕ c).
Proof.
  induction n; intros.
  - depelim a; depelim b; depelim c; auto.
  - depelim a; depelim b; depelim c.
    cbn.
    specialize (IHn n1 n3 n5).
    simpl; f_equal; auto with arith.
Qed.

Lemma natvec_add_increasing_l: forall {n} (a b : natvec n), b ≼ a ⊕ b.
Proof.
  induction n; intros.
  - depelim a; depelim b; cbn; auto.
  - depelim a; depelim b; cbn.
    specialize (IHn n1 n3).
    destruct n0 as [| n0].
    2: left; lia.
    right; auto.
Qed.

