(* ** Universe-polymorphic sums

The same issues tht arise with prod also arise to a smaller extent with sums,
due to the iteration operation of monads hardcoding it. This makes interp
pretty tense by design since the inner iteration in interp returns an itree on
its left side. *)

From Coq Require Import PeanoNat Bool List Lia Program.Equality Morphisms.
From ITreeAI Require Import Lattice Util.Natvec Util.List.
From ITree Require Import Basics.Category Basics.Function.
Import Nat.
Open Scope signature_scope.

(* ============================================================================
** Polymorphic sums *)

Set Universe Polymorphism.

Inductive psum (α β: Type): Type :=
  | pinl (_: α)
  | pinr (_: β).

Arguments pinl {α β}.
Arguments pinr {α β}.

Module PsumNotations.
Notation "x +++ y" := (psum x y) (at level 45): type_scope.
End PsumNotations.

Import PsumNotations.

Definition pcase {α β γ: Type} (fl: α -> γ) (fr: β -> γ): α +++ β -> γ :=
  fun ab =>
    match ab with
    | pinl a => fl a
    | pinr b => fr b
    end.

#[global] Instance Case_psum : Case Fun psum := @pcase.
#[global] Instance Inl_psum : Inl Fun psum := @pinl.
#[global] Instance Inr_psum : Inr Fun psum := @pinr.


(* There is no canonical lattice on sums because there isn't a sufficiently-
   generic option. *)
