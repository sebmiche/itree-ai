From Paco Require Import paco.
From ITreeAI.Lattices Require Import Lattice LatticePprod LatticeOption.

From ITree Require Import Basics CategoryOps CategoryKleisli CategoryTheory Monad ITree Events.FailFacts.
From ExtLib Require Import Functor.
Import Basics.Monads.
Import MonadNotation.
Import PprodNotations.
Local Open Scope monad_scope.

Definition failA (m : Type -> Type) : Type -> Type :=
  fun R => m ((unitˣ * R)%type).

#[global] Instance failA_monad {m} `{Monad m} : Monad (failA m) :=
  {|
    ret _ x := ret (bot, x);
    bind _ _ x k :=
      bind (m := m) x
        (fun y => bind (m := m) (k (snd y))
                 (fun z => ret (fst y ⊔ fst z, snd z)))
  |}.

#[export] Instance Functor_failA {m : Type -> Type} `{Functor m} :
    Functor (failA m) :=
  {| fmap _ _ f := fmap (fun '(x, y) => (x, f y)) |}.

Section FailA.

  Variable m : Type -> Type.
  Context {EQm : Eq1 m}.
  Context {Hm: Monad m}.
  Context {HEQP: @Eq1Equivalence m _ EQm}.
  Context {mL: @MonadLawsE m _ Hm}.

  Global Instance Eq1_failAM : Eq1 (failA m).
  Proof.
    refine (fun a => eq1 (M := m)).
  Defined.

  Global Instance Eq1Equivalence_failAM : @Eq1Equivalence (failA m) _ Eq1_failAM.
  Proof.
  constructor.
  - repeat red.
    reflexivity.
  - repeat red. intros. symmetry. apply H.
  - repeat red. intros. etransitivity; eauto.
  Qed.

  Instance MonadLawsE_stateTM : @MonadLawsE (failA m) _ _.
  Proof.
  constructor.
  - cbn. intros a b f x.
    rewrite bind_ret_l.
    cbn.
    pose proof bind_ret_r (M := m) _ (f x).
    rewrite <- H at 2.
    apply (Proper_bind (M := m)).
    reflexivity.
    intros [[]]; cbn; reflexivity.
  - cbn. intros a x. setoid_rewrite Monad.bind_ret_l.
    pose proof Monad.bind_ret_r (M := m) _ x.
    rewrite <- H at 2.
    apply (Proper_bind (M := m)).
    reflexivity.
    intros [[]]; cbn; reflexivity.
  - cbn. intros a b c x f g.
    rewrite !bind_bind.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_bind.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_ret_l, !bind_bind.
    cbn.
    apply (Proper_bind (M := m)); [reflexivity | intros []; cbn ].
    rewrite !bind_ret_l.
    cbn.
    destruct u,u0,u1; cbn; reflexivity.
  - repeat red. intros a b x y H x0 y0 H0.
    cbn; apply Proper_bind.
    + apply H.
    + intros []; cbn.
      cbn; apply Proper_bind.
      apply H0.
      intros []; cbn.
      reflexivity.
  Qed.

  Section Iter1.

    Context {CM': MonadIter m}.
    Context {CM: Iterative (Kleisli m) sum}.

    Definition iter_failA : MonadIter (failA m) :=
      fun [R I] body i =>
        iter (Iter := @Iter_Kleisli m _)
          (fun '(x,i) => bind (m := m) (body i)
                        (fun '(y,jr) => match jr with
                                     | inl j => ret (inl (x ⊔ y, j))
                                     | inr r => ret (inr (x ⊔ y, r))
                                     end))
          (bot, i).

    Arguments iter_failA [_ _].

  End Iter1.

End FailA.

From ITree Require Import ITree Eq KTree KTreeFacts.

Lemma iter_failA_unfold_aux {E I R}
  (body: I -> itree E (unitˣ * (I + R)))
  (l: unitˣ) (i0: I):
  ITree.iter
    (fun '(x, i1) =>
       pat0 <- body i1;;
       match pat0 with
       | (y, inl j) => ret (inl (x ⊔ y, j))
       | (y, inr r) => ret (inr (x ⊔ y, r))
       end) (l, i0)
    ≅ z <-
    ITree.iter
      (fun '(x, i1) =>
         pat0 <- body i1;;
         match pat0 with
         | (y, inl j) => ret (inl (x ⊔ y, j))
         | (y, inr r) => ret (inr (x ⊔ y, r))
         end) (bot, i0);; ret (l ⊔ fst z, snd z).
Proof.
  cbn.
  match goal with
  | |- ITree.iter ?BODY1 (?X1, ?Y1) ≅
        ITree.bind (ITree.iter ?BODY2 ?I2) ?K =>
      set (body1 := BODY1); set (body2 := BODY2); set (k := K)
  end.
  rewrite <- bind_ret_r at 1.
  set (RR := fun (rl rr: unitˣ * R) => ret rl ≅ k rr).
  set (RI := fun (il ir: unitˣ * I) =>
               fst il = l ⊔ fst ir /\ snd il = snd ir).
  apply eq_itree_clo_bind with (UU := RR).
  - apply eq_itree_iter' with (RI := RI).
    * intros [err1 j1] [err2 j2] HRI. subst RI; cbn in *.
      apply eq_itree_clo_bind with (UU := eq). now destruct HRI as [_ ->].
      intros [? []] [? []] H; cbn in *; inversion_clear H.
      all: apply eqit_Ret; cbn; constructor; cbn.
      { destruct HRI as [-> ->]. split; [|auto].
        destruct l, err2; try reflexivity. }
      { destruct HRI as [-> ->]. subst RR k; cbn. apply eqit_Ret.
        destruct l, err2; reflexivity. }
    * subst RI; cbn. split; [|auto]. now destruct l.
  - subst RR; now intros [[]] [[]].
Qed.

Opaque join.
Lemma iter_failA_unfold {E} {R I: Type} body i :
  iter_failA (itree E) R I body i ≅
    bind (m := failA _) (body i)
    (fun jr => match jr with
            | inl j => Tau (iter_failA _ _ _ body j)
            | inr r => ret r
            end).
Proof.
  unfold iter_failA.
  unfold iter, Iter_Kleisli, Basics.iter, MonadIter_itree.
  rewrite unfold_iter.
  cbn.
  rewrite bind_bind.
  apply eq_itree_clo_bind with (UU := eq); [reflexivity |].
  intros [? []] ? <-; cbn; rewrite ?bind_ret_l.
  - cbn. rewrite bind_tau.
    apply eqit_Tau.
    pose proof (H := @iter_failA_unfold_aux).
    cbn in H. apply H.
  - cbn. now destruct u.
Qed.
Transparent join.

Lemma iter_failA_unfold_eutt {E} {R I: Type} body i :
  iter_failA (itree E) R I body i ≈
    bind (m := failA _) (body i)
    (fun jr => match jr with
            | inl j => iter_failA _ _ _ body j
            | inr r => ret r
            end).
Proof.
  rewrite iter_failA_unfold.
  apply eutt_eq_bind.
  intros [? []]; cbn; try reflexivity.
  rewrite bind_tau, tau_euttge.
  reflexivity.
Qed.

#[export] Existing Instance iter_failA.
#[export] Instance Iter_failAM {E} : Iter (Kleisli (failA (itree E))) sum.
typeclasses eauto.
Defined.

#[export] Instance IterUnfold_failAM {E} : IterUnfold (Kleisli (failA (itree E))) sum.
Proof.
  unfold IterUnfold.
  intros a b f ?.
  apply iter_failA_unfold_eutt.
Qed.

Definition interp_failA {E M}
           {FM : Functor M} {MM : Monad M}
           {IM : MonadIter M} (h : E ~> failA M) :
  itree E ~> failA M := interp h.
Arguments interp_failA {E M FM MM IM} h [T].

(* (** Unfolding of [interp_fail]. *) *)
Definition _interp_fail {E F R} (f : E ~> failA (itree F)) (ot : itreeF E R _)
  : failA (itree F) R :=
  match ot with
  | RetF r => Ret (bot, r)
  | TauF t => Tau (interp_failA f t)
  | VisF e k =>
      bind (m := failA _) (f _ e) (fun jr => Tau (interp_failA f (k jr))
        )
  end.

(** Unfold lemma. *)
Lemma unfold_interp_failA {E F R} (f : E ~> failA (itree F)) (t : itree E R) :
  interp_failA f t ≅ _interp_fail f (observe t).
Proof.
  unfold interp_failA, interp, Basics.iter.
  rewrite iter_failA_unfold.
  destruct (observe t).
  + repeat (cbn; rewrite !bind_ret_l); reflexivity.
  + repeat (cbn; rewrite ?bind_ret_l).
    rewrite bind_tau.
    apply eqitree_Tau.
    symmetry.
    rewrite <- bind_ret_r.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-; reflexivity.
  + cbn; rewrite bind_map.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-.
    cbn.
    rewrite !bind_tau.
    apply eqitree_Tau.
    apply eq_itree_clo_bind with (UU := eq).
    reflexivity.
    intros [? ?] ? <-; reflexivity.
Qed.

Arguments join : simpl nomatch.

Lemma interp_failA_ret : forall {E F R} (h : E ~> failA (itree F)) (r : R),
    interp_failA h (Ret r) ≅ ret (bot, r).
Proof.
  intros; rewrite unfold_interp_failA; reflexivity.
Qed.

Lemma interp_failA_vis :
  forall {E F X R} (h : E ~> failA (itree F))
    (e : E X) (k : X -> itree _ R),
    interp_failA h (Vis e k) ≈
      bind (m := failA _) (h _ e) (fun y => (interp_failA h (k y))).
Proof.
  intros; rewrite unfold_interp_failA.
  cbn.
  now setoid_rewrite tau_euttge.
Qed.
