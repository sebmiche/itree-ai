(* ** List utilities

More functions and theorems on generic lists. *)

From Coq Require Import List Lia Program Bool Classes.EquivDec PeanoNat.
Import ListNotations.

(* Decidable `In` for natural integers. *)

Definition inb {α: Type} `{EqDec α eq} (x: α) (l: list α): bool :=
  existsb (fun y => if y == x then true else false) l.

#[local] Lemma eqe {T} {ED: EqDec T eq}: forall x y,
  ((if ED x y then true else false) = true) <-> (x = y).
Proof. split; intros; destruct (ED x y); (auto || discriminate). Qed.

#[local] Lemma eqe_refl {T} {ED: EqDec T eq}: forall x,
  (if ED x x then true else false) = true.
Proof. intros; now rewrite eqe. Qed.

Lemma inb_In {α: Type} {ED: EqDec α eq} (x: α) (l: list α):
  inb x l = true <-> In x l.
Proof.
  split.
  - induction l; [easy|].
    cbn. destruct (a == x) eqn:H; auto.
  - induction l; [easy|].
    intros [].
    * subst; cbn. now rewrite eqe_refl.
    * cbn. rewrite orb_true_iff. right. now apply IHl.
Qed.

Lemma forallb_monotone {α: Type} {P Q: α -> bool}:
  (forall x, P x = true -> Q x = true) ->
  forall (l: list α),
  forallb P l = true -> forallb Q l = true.
Proof.
  intros Hmono l. induction l; intros.
  - reflexivity.
  - cbn in *. rewrite andb_true_iff in *; split.
    apply Hmono, H. apply IHl, H.
Qed.

Lemma forallb_inb_self {α: Type} {ED: EqDec α eq} (l: list α):
  forallb (fun x => inb x l) l = true.
Proof.
  induction l.
  - easy.
  - cbn. rewrite eqe_refl, orb_true_l, andb_true_l.
    unshelve eapply (forallb_monotone _ _ IHl).
    intros. cbn in *. apply orb_true_iff. right. apply H.
Qed.

(* Lists as sets. *)

Fixpoint join_lists {α: Type} {ED: EqDec α eq} (l l': list α) :=
  match l with
  | [] => l'
  | x::l => if inb x l' then join_lists l l' else x :: join_lists l l'
  end.

Lemma join_lists_spec {α: Type} {ED: EqDec α eq} {l l': list α} {x}:
  In x (join_lists l l') <-> In x l \/ In x l'.
Proof.
  split.
  - revert l'; induction l; intuition.
    cbn in H. destruct (inb a l') eqn:Ha.
    { destruct (IHl _ H) as [Hx|Hx']; [left; now constructor 2|now right]. }
    cbn; rewrite or_assoc. destruct (x == a) eqn:Heq.
    { left. symmetry. apply e. }
    right. apply IHl. inversion_clear H; [exfalso; now apply c|easy].
  - revert l'; induction l.
    * intros l' [[]|]. easy.
    * intros l' [[Hx|Hx]|Hx]; cbn.
      { subst. case inb eqn:Hin; [|now constructor].
        apply IHl. right. now rewrite <- inb_In. }
      { case inb; [|constructor 2]. all: apply IHl; now left. }
      { case inb; [|constructor 2]. all: apply IHl; now right. }
Qed.

Lemma join_lists_specb {α: Type} {ED: EqDec α eq} {l l': list α} {x}:
  inb x (join_lists l l') = inb x l || inb x l'.
Proof.
  apply Bool.eq_true_iff_eq. rewrite Bool.orb_true_iff.
  rewrite ! inb_In.
  apply join_lists_spec.
Qed.

Lemma join_lists_nil_l {α: Type} {ED: EqDec α eq} {l: list α}:
  join_lists [] l = l.
Proof. easy. Qed.

Lemma join_lists_nil_r {α: Type} {ED: EqDec α eq} {l: list α}:
  join_lists l [] = l.
Proof. induction l. easy. cbn. now rewrite IHl. Qed.

Fixpoint meet_lists {α: Type} {ED: EqDec α eq} (l l': list α) :=
  match l with
  | [] => []
  | x::l => if inb x l' then x :: meet_lists l l' else meet_lists l l'
  end.

Lemma meet_lists_spec {α: Type} {ED: EqDec α eq} {l l': list α} {x}:
  In x (meet_lists l l') <-> In x l /\ In x l'.
Proof.
  split.
  - revert l'; induction l; intuition auto with *.
    * cbn in *. destruct (a == x) eqn:Heq.
      { left; apply e. }
      case (inb a l') eqn:Ha.
      { inversion_clear H. subst; now left. right; now apply (IHl l'). }
      right. now apply (IHl l').
    * cbn in *. case (inb a l') eqn:Ha.
      { cbn in H. destruct H. subst; now apply inb_In. now apply (IHl l'). }
      { now apply (IHl l'). }
  - revert l'; induction l; intros l' [Hl Hl']; intuition auto with *.
    cbn. case (inb a l') eqn:Ha.
    * destruct (a == x) eqn:Heq. left; apply e.
      destruct Hl; [easy|right; auto].
    * apply (IHl l'). destruct Hl; auto.
      subst; apply inb_In in Hl'; rewrite Hl' in *; inversion Ha.
Qed.

Lemma meet_lists_specb {α: Type} {ED: EqDec α eq}  {l l': list α} {x}:
  inb x (meet_lists l l') = inb x l && inb x l'.
Proof.
  apply Bool.eq_true_iff_eq. rewrite Bool.andb_true_iff.
  rewrite ! inb_In.
  apply meet_lists_spec.
Qed.

(* Indexed lists (of a known size N) *)

Fixpoint ilist_init_aux {α} (N index: nat) (f: nat -> α): list α :=
  match N with
  | 0 => []
  | S Nm1 => f index :: ilist_init_aux Nm1 (S index) f
  end.

Definition ilist_init {α} N (f: nat -> α): list α :=
  ilist_init_aux N 0 f.

Lemma length_ilist_init_aux {α N index f}:
  length (@ilist_init_aux α N index f) = N.
Proof. revert index. induction N; cbn; auto. Qed.

Lemma length_ilist_init {α N f}: length (@ilist_init α N f) = N.
Proof. apply length_ilist_init_aux. Qed.

Lemma In_ilist_init_aux {α N index f} {a: α}:
  In a (ilist_init_aux N index f) <->
  (exists i, i >= index /\ i < index+N /\ f i = a).
Proof.
  revert a index. induction N.
  - intuition auto with *. destruct H; lia.
  - intros a index. split.
    * cbn. intros [H|H].
      { exists index. split. lia. split. lia. auto. }
      { rewrite (IHN a (S index)) in H.
       destruct H as [i [? []]]. exists i. split. lia. split. lia. auto. }
    * intros [i [? []]]. cbn.
      case (Nat.eqb i index) eqn:Hi.
      { apply Nat.eqb_eq in Hi. left. now rewrite <- Hi. }
      { rewrite Nat.eqb_neq in Hi. right. destruct (IHN a (S index)) as [_ IH].
        apply IH. exists i. split. lia. split. lia. auto. }
Qed.

Lemma In_ilist_init {α N f} {a: α}:
  In a (ilist_init N f) <-> (exists i, i < N /\ f i = a).
Proof.
  unfold ilist_init. rewrite In_ilist_init_aux.
  split; intros [i Hi]; exists i; intuition lia.
Qed.

#[global] Hint Rewrite @length_ilist_init: core.

Lemma nth_error_ilist_init_aux {α N index f i}:
  i < N -> nth_error (@ilist_init_aux α N index f) i = Some (f (index+i)).
Proof.
  revert i index; induction N; intros * Hi; cbn.
  - inversion Hi.
  - destruct i as [|j]; cbn.
    * now rewrite Nat.add_0_r.
    * rewrite <- Nat.add_succ_comm at 1. apply IHN. lia.
Qed.

Lemma nth_error_ilist_init {α N f i}:
  i < N -> nth_error (@ilist_init α N f) i = Some (f i).
Proof. intros; unfold ilist_init; now rewrite nth_error_ilist_init_aux. Qed.

#[global] Hint Rewrite @nth_error_ilist_init: core.

Fixpoint ilist_set {α} (index: nat) (l: list α) (a: α): list α :=
  match l with
  | [] => []
  | head::tail =>
      match index with
      | 0 => a::tail
      | S index' => head :: ilist_set index' tail a
      end
  end.

Lemma nth_error_ilist_set {α index l a i}:
  index < length l -> nth_error (@ilist_set α index l a) i =
  if index =? i then Some a else nth_error l i.
Proof.
  revert i index; induction l; intros * Hi; cbn.
  - inversion Hi.
  - destruct index as [|index'], i as [|j]; cbn; auto.
    apply IHl. inversion Hi; lia.
Qed.

Lemma combine_nth_error1 {α β} (l: list α) (l': list β) n a b:
  nth_error l n = Some a ->
  nth_error l' n = Some b ->
  nth_error (combine l l') n = Some (a, b).
Proof.
  revert a b l l'; induction n; intros * Ha Hb.
  all: destruct l as [|x l]; inversion Ha;
       destruct l' as [|y l']; inversion Hb.
  - now subst.
  - cbn. now erewrite IHn.
Qed.

Lemma combine_nth_error2 {α β} (l: list α) (l': list β) n a b:
  nth_error (combine l l') n = Some (a, b) <->
  nth_error l n = Some a /\ nth_error l' n = Some b.
Proof.
  split.
  - revert a b l l'; induction n; intros * Hab.
    all: destruct l as [|x l]; inversion Hab;
         destruct l' as [|y l']; inversion Hab;
         cbn; auto.
  - intuition auto using combine_nth_error1.
Qed.
