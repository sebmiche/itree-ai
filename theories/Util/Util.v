#[global] Set Warnings "-intuition-auto-with-star".

From Coq Require Import
  ZArith String FMaps DecidableType Datatypes Lia Program.Equality
  Relations Relation_Operators Wellfounded.Lexicographic_Product.
From ExtLib Require Import Structures.Monad.
From ITree Require Import
  ITree Eq.Eqit Interp.Interp Events.State Events.StateFacts KTreeFacts
  Props.Finite Props.HasPost Props.Leaf InterpFacts TranslateFacts FailFacts.
Import Basics.Monads.
From Paco Require Import paco.

Import LeafNotations.
Import MonadNotation.
Import HasPostNotations.

Open Scope monad_scope.

Definition reassoc3 {α β γ: Type}: α * (β * γ) -> α * β * γ :=
  fun '(a, (b, c)) => (a, b, c).

(* Inverts [H: x ∈ Ret y] into a substitution x -> y, including destructing
   tuples and clearing trivial equalities. *)
Ltac inversion_Leaf_ret H :=
  simpl in H;
  apply Leaf_Ret_inv in H;
  repeat (setoid_rewrite pair_equal_spec in H; destruct H as [H ?]);
  subst;
  repeat match goal with Huseless: ?x = ?x |- _ => clear Huseless end.

(* Inverses [H: x ∈ (a <- t;; k)] into [a], [Ha: a ∈ t] and [H: x ∈ k a]. H is
   modified in-place. *)
Tactic Notation "inversion_Leaf_bind"
    ident(H) simple_intropattern(a) ident(Ha) :=
  simpl in H;
  apply Leaf_bind_inv in H; destruct H as [a [Ha H]].

(* Apply a step of computation on [all_finite (bind _ _)], introducing the
   temporary value with pattern a. *)
Tactic Notation "all_finite_bind" simple_intropattern_list(a) :=
  eapply all_finite_bind; try intros a; try intros.

(* Apply a step of computation on [has_post (bind _ _) S] using the same post-
   condition for the intermediate program. Introduces the temporary values
   with x. *)
Tactic Notation "has_post_bind" simple_intropattern_list(x) :=
  match goal with
  | |- has_post _ ?PostCond =>
         eapply has_post_bind with (S := PostCond);
         try intros x
  end.


(* Pointwise relation *)
(* Can't use pointwise_relation because .:*' universe inconsistencies '*:. *)

Definition pointwise {α β} (R: relation β): relation (α -> β) :=
  fun f g => forall a, R (f a) (g a).

#[export]
Instance Reflexive_pointwise {α β} {R: relation β} `{Reflexive _ R}:
  Reflexive (@pointwise α β R).
Proof. repeat intro; reflexivity. Qed.

#[export]
Instance Symmetric_pointwise {α β} {R: relation β} `{Symmetric _ R}:
  Symmetric (@pointwise α β R).
Proof. repeat intro; symmetry. intuition. Qed.

#[export]
Instance Transitive_pointwise {α β} {R: relation β} `{Transitive _ R}:
  Transitive (@pointwise α β R).
Proof. repeat intro; etransitivity; intuition. Qed.

#[export]
Instance Equivalence_pointwise {α β} {R: relation β} `{Equivalence _ R}:
  Equivalence (@pointwise α β R) := {}.


(* Lemmas on interp_fail *)

Lemma eutt_eq_interp_fail_iter {E F I R}
    (f: E ~> failT (itree F)) (t: I -> itree E (I + R)) (i: I):
  interp_fail f (ITree.iter t i)
  ≈ Basics.iter (fun i => interp_fail f (t i)) i.
Proof.
  unfold Basics.iter, failT_iter, Basics.iter, MonadIter_itree.
  revert t i. ginit. gcofix CIH; intros t i.
  rewrite 2 unfold_iter; cbn.
  rewrite !bind_bind.
  rewrite interp_fail_bind.
  guclo eqit_clo_bind; econstructor; eauto. reflexivity.
  intros [u1|] [u2|] [].
  - destruct u1; cbn.
    rewrite bind_ret_l, interp_fail_tau.
    gstep; constructor. gfinal; left; apply CIH.
    rewrite bind_ret_l, interp_fail_ret.
    gstep; now constructor.
  - rewrite bind_ret_l. gstep; now constructor.
Qed.


(* Modules for decidable types *)

Module NatDecidable.
  Definition t := nat.
  Definition eq := @eq t.
  Definition eq_refl := @eq_refl t.
  Definition eq_sym := @eq_sym t.
  Definition eq_trans := @eq_trans t.
  Definition eq_dec := Nat.eq_dec.
End NatDecidable.

Module ZDecidable.
  Definition t := Z.
  Definition eq := @eq t.
  Definition eq_refl := @eq_refl t.
  Definition eq_sym := @eq_sym t.
  Definition eq_trans := @eq_trans t.
  Definition eq_dec := Z.eq_dec.
End ZDecidable.

Module StringDecidable.
  Definition t := string.
  Definition eq := @eq t.
  Definition eq_refl := @eq_refl t.
  Definition eq_sym := @eq_sym t.
  Definition eq_trans := @eq_trans t.
  Definition eq_dec := string_dec.
End StringDecidable.


(* Finiteness is stable by interpretation in basic cases *)

Lemma all_finite_interp {E F R} {t: itree E R} {h: E ~> itree F}:
  all_finite t ->
  (forall R (e: E R), all_finite (h _ e)) ->
  all_finite (interp h t).
Proof.
  intros Hfin Hhandler. induction Hfin; setoid_rewrite (itree_eta t); rewrite H.
  - rewrite interp_ret. apply all_finite_Ret.
  - rewrite interp_tau. now apply all_finite_Tau.
  - rewrite interp_vis. all_finite_bind; [apply Hhandler|].
    now apply all_finite_Tau.
Qed.

Lemma all_finite_interp_state {E F R S}
    {t: itree E R} {h: E ~> stateT S (itree F)} {s}:
  all_finite t ->
  (forall R (e: E R) s, all_finite (h _ e s)) ->
  all_finite (interp_state h t s).
Proof.
  intros Hfin Hhandler; revert s.
  induction Hfin; intros s; setoid_rewrite (itree_eta t); rewrite H.
  - rewrite interp_state_ret. apply all_finite_Ret.
  - rewrite interp_state_tau. now apply all_finite_Tau.
  - rewrite interp_state_vis. all_finite_bind []; [apply Hhandler|].
    now apply all_finite_Tau.
Qed.

Lemma all_finite_interp_fail {E F R} {t: itree E R} {h: E ~> failT (itree F)}:
  all_finite t ->
  (forall R (e: E R), all_finite (h _ e)) ->
  all_finite (interp_fail h t).
Proof.
  intros Hfin Hhandler. induction Hfin; setoid_rewrite (itree_eta t); rewrite H.
  - rewrite interp_fail_ret. apply all_finite_Ret.
  - rewrite interp_fail_tau. now apply all_finite_Tau.
  - rewrite interp_fail_vis. all_finite_bind []; [apply Hhandler|..].
    * now apply all_finite_Tau.
    * apply all_finite_Ret.
Qed.
