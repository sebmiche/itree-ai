(* ** Lattice combinators: option lattice

This is a dual-channel sum lattice, with
  * The unitˣ channel covers error paths (None)
  * The T channel covers successful paths (Some)
This induces the following interpretation of cases:
  * (bot, bot): Inaccessible (lattice bottom)
  * (bot, non-bot): Cannot fail, always returns successfully
  * (non-bot, bot): Always fails, never returns successfully
  * (non-bot, non-bot): Both paths are possible. *)

From ExtLib.Structures Require Import Functor.
From ITreeAI Require Import Lattice.

Set Universe Polymorphism.

Definition optionˣ (T: Type): Type := unitˣ * T.

Definition optionˣ_none {T} `{Lattice T}: optionˣ T :=
  (ttˣ, bot).

Definition optionˣ_some {T} (x: T): optionˣ T :=
  (bot, x).

#[global] Instance Functor_optionˣ: Functor optionˣ := {
  fmap _ _ f oˣ := (fst oˣ, f (snd oˣ));
}.

#[global] Instance LatticeBase_optionˣ {T} `(L: Lattice T):
  LatticeBase (optionˣ T) := _.
#[global] Instance Lattice_optionˣ {T} `(L: Lattice T):
  Lattice (optionˣ T) := _.

#[global, refine] Instance GaloisConnection_optionˣ_option {Tˣ T}
    `{L: Lattice Tˣ} `(GC: GaloisConnection Tˣ T):
    GaloisConnection (optionˣ Tˣ) (option T) := {
  galois_in c a :=
    match c with
    | Some c => galois_in c (snd a)
    | None => fst a = ttˣ
    end;
}.
Proof.
  - intros [[] ?] [[] ?] []; cbn; easy || eauto using galois_lble.
  - intros [[] ?] [[] ?] []; cbn; easy || eauto using galois_meet.
  - intros []; eauto using galois_top.
Defined.
