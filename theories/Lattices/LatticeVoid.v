From ITreeAI Require Import Lattice Util.
From ITree Require Import Basics.

(* void lattice: only one element (bot) *)
Definition Lvoid := unit.

#[global, refine]
Instance LatticeBase_Lvoid: LatticeBase Lvoid := {
  lble _ _ := true;
  bot := tt;
  top := tt;
  join _ _ := tt;
  meet _ _ := tt;
}.
Proof. all: intuition. Defined.

#[global, refine]
Instance Lattice_Lvoid: Lattice Lvoid := {
  widen := join;
  measure_N := 0;
  measure _ := tt;
}.
Proof. all: repeat intros []; intuition; cbn in *; auto. Defined.

#[global, refine]
Instance GaloisConnection_Lvoid_void: GaloisConnection Lvoid void := {
  galois_in _ _ := False;
}.
Proof. all: repeat intros []. Defined.