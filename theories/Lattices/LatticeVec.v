(* ** Lattices of fixed-sized lists (vectors)

The vector type implemented here is for a fixed size and pointwise reasoning.
It is mainly used in the CFG combinator to store per-block data.

There are many approaches to implementing fixed-size vectors. This one makes a
point of having an nth function which doesn't require any extra hypotheses, for
ease of rewriting. Things tested include:

- { l: list α | length l = N } : requires (H: i < N) for the nth function.
  Proving that the function is independent of H is really difficult, and
  carrying around H's is annoying.

- Structural vec with `nth: nat -> α` with Inhabited hypothesis: doesn't work
  for complex functions like map, where for out-of-bounds indices there are no
  laws on the default value (eg. we want `nth (map f v) i = f (nth v i)` but
  for i ≥ N this gives `@default β = f (@default α)`, which isn't true).

- Structural vec with `nth: Fin.t N -> α`: works, as long as the function is
  written with Fin's induction principle and not a normal match.

Equations makes this a lot simpler so that's the current implementation.

This file consists of the `vec` type, a bunch of sequence processing functions,
iteration principles, and a Lattice instance. Most of the functions are
characterized by a `vec_nth` law, e.g. `vec_nth (vec_initf N f) i = f i`.
Monadic iterators can be found in Util.MonadicIterators. *)

From Coq Require Import PeanoNat Bool List Lia Program.Equality Morphisms.
From Equations Require Import Equations.
From ITreeAI Require Import Lattice LatticePprod Util.Natvec Util.List.
Import Nat.
Import PprodNotations.
Open Scope signature_scope.

(* Simplifications for Fin.to_nat while hiding the sigma. *)

Definition fin_nat {N} (i: Fin.t N) := proj1_sig (Fin.to_nat i).

Arguments fin_nat: simpl never.

Lemma fin_nat_F1 {N}: @fin_nat (S N) Fin.F1 = 0.
Proof. reflexivity. Qed.

Lemma fin_nat_FS {N i}: @fin_nat (S N) (Fin.FS i) = S (fin_nat i).
Proof.
  destruct i; cbn. reflexivity.
  unfold fin_nat; cbn. destruct (Fin.to_nat i); cbn. reflexivity.
Qed.

Tactic Notation "depelim1" constr(t) :=
  dependent elimination t.
Tactic Notation "depelim2" constr(t) constr(u) :=
  dependent elimination t; dependent elimination u.
Tactic Notation "depelim3" constr(t) constr(u) constr(v) :=
  dependent elimination t; dependent elimination u; dependent elimination v.

(* The vector type and nth function. *)

Set Universe Polymorphism.

Inductive vec (α: Type): nat -> Type :=
  | vec_nil: vec α 0
  | vec_cons {N} (a: α) (u: vec α N): vec α (S N).

Arguments vec_nil {α}.
Arguments vec_cons {α} {N}.
Derive Signature for vec.
Derive Signature for Fin.t.

Equations vec_nth {α N} (v: vec α N) (i: Fin.t N): α :=
vec_nth (vec_cons a u) Fin.F1 := a;
vec_nth (vec_cons a u) (Fin.FS i) := vec_nth u i.

Arguments vec_nth: simpl never.

Module VecNotations.
Notation "[ x , y ]" := (vec_cons x y).
End VecNotations.
Import VecNotations.

Lemma vec_nth_F1 {α N} {a: α} {u: vec α N}:
  vec_nth [a, u] Fin.F1 = a.
Proof. now simp vec_nth. Qed.

Lemma vec_nth_FS {α N} {a: α} {u: vec α N} {i}:
  vec_nth [a, u] (Fin.FS i) = vec_nth u i.
Proof. now simp vec_nth. Qed.

Theorem vec_extensionality {α N} {u v: vec α N}:
  (forall i, vec_nth u i = vec_nth v i) -> u = v.
Proof.
  induction N.
  - dependent destruction u. now dependent destruction v.
  - dependent destruction u. dependent destruction v. intros Hi.
    pose proof (H1 := Hi Fin.F1). rewrite ! vec_nth_F1 in H1. subst.
    rewrite (IHN u v); [reflexivity|].
    intros i. apply (Hi (Fin.FS i)).
Qed.

(* vec_of_list *)

Fixpoint vec_of_list {α} (l: list α): vec α (length l) :=
  match l with
  | nil => vec_nil
  | a::l => [a, vec_of_list l]
  end.

(* vec_init *)

Fixpoint vec_init_aux {α} (N index: nat) (f: nat -> α): vec α N :=
  match N with
  | 0 => vec_nil
  | S N' => [f index, vec_init_aux N' (S index) f]
  end.

Definition vec_init {α} N f := @vec_init_aux α N 0 f.

Lemma vec_init_aux_nth {α N index i} {f: nat -> α}:
  vec_nth (vec_init_aux N index f) i = f (index + fin_nat i).
Proof.
  revert i index. induction N.
  - inversion i.
  - intros i index. dependent destruction i; cbn. now rewrite add_0_r.
    rewrite vec_nth_FS; cbn. rewrite IHN. f_equal. rewrite fin_nat_FS. lia.
Qed.

Lemma vec_nth_init {α N i} {f: nat -> α}:
  vec_nth (vec_init N f) i = f (fin_nat i).
Proof. unfold vec_init. rewrite vec_init_aux_nth; try f_equal; lia. Qed.

(* vec_range *)

Definition vec_range N: vec nat N :=
  vec_init N id.

Lemma vec_nth_range {N i}: vec_nth (vec_range N) i = fin_nat i.
Proof. unfold vec_range. now rewrite vec_nth_init. Qed.

(* vec_rangef *)

#[local]
Definition nat_to_fin_mod (N0: nat): nat -> Fin.t (S N0) :=
  fun p => Fin.of_nat_lt (mod_upper_bound p (S N0) (neq_succ_0 N0)).

Equations vec_rangef (N: nat): vec (Fin.t N) N :=
  vec_rangef 0 := vec_nil;
  vec_rangef (S N') := vec_init (S N') (nat_to_fin_mod N').

#[local]
Lemma fold_modulo (i N: nat): N - snd (divmod i N 0 N) = i mod (S N).
Proof. reflexivity. Qed.

#[local]
Lemma fin_nat_upper_bound (N: nat):
  forall (i: Fin.t N), fin_nat i < N.
Proof.
  induction N.
  - inversion i.
  - intros i. dependent destruction i.
    * rewrite fin_nat_F1. lia.
    * rewrite fin_nat_FS. specialize (IHN i). lia.
Qed.

Lemma vec_nth_rangef {N i}: vec_nth (vec_rangef N) i = i.
Proof.
  destruct N.
  - inversion i.
  - simp vec_rangef. rewrite vec_nth_init; cbn. apply Fin.to_nat_inj.
    rewrite Fin.to_nat_of_nat; cbn. rewrite fold_modulo.
    rewrite mod_small; auto. apply fin_nat_upper_bound.
Qed.

(* vec_map *)

Section VecMap.
Variables (α β: Type) (f: α -> β).
Equations vec_map {N} (v: vec α N): vec β N :=
  vec_map vec_nil := vec_nil;
  vec_map [a,u] := [f a, vec_map u].
End VecMap.
Arguments vec_map {α β} f {N}.

Lemma vec_nth_map {N α β} {f: α -> β} {v: vec α N} i:
  vec_nth (vec_map f v) i = f (vec_nth v i).
Proof.
  revert i v. induction N.
  - inversion i.
  - intros i v. dependent elimination v.
    dependent elimination i; now simp vec_nth vec_map.
Qed.

Lemma vec_map_map {N α β γ} {f: α -> β} {g: β -> γ} {v: vec α N}:
  vec_map g (vec_map f v) = vec_map (fun a => g (f a)) v.
Proof.
  apply vec_extensionality. intros i. now rewrite ! vec_nth_map.
Qed.

Lemma vec_map_cong {N α β} {f g: α -> β}:
  (forall a, f a = g a) -> forall (v: vec α N), vec_map f v = vec_map g v.
Proof.
  intros Hfg v. apply vec_extensionality. intros. now rewrite ! vec_nth_map.
Qed.

(* vec_initf *)

Definition vec_initf {α} N (f: Fin.t N -> α): vec α N :=
  vec_map f (vec_rangef N).

Lemma vec_nth_initf {α} N (f: Fin.t N -> α) i:
  vec_nth (vec_initf N f) i = f i.
Proof.
  unfold vec_initf. now rewrite vec_nth_map, vec_nth_rangef.
Qed.

(* vec_combine *)

Equations vec_combine {N α β} (va: vec α N) (vb: vec β N): vec (α ** β) N :=
  vec_combine vec_nil vec_nil := vec_nil;
  vec_combine [a, ua] [b, ub] := [ppair a b, vec_combine ua ub].

Lemma vec_nth_combine {N α β} {va: vec α N} {vb: vec β N} {i}:
  vec_nth (vec_combine va vb) i = ppair (vec_nth va i) (vec_nth vb i).
Proof.
  revert i va vb. induction N.
  - inversion i.
  - intros i u v. depelim3 u v i; now simp vec_nth vec_combine.
Qed.

(* vec_split *)

Equations vec_split {N α β} (vab: vec (α ** β) N): vec α N ** vec β N :=
  vec_split vec_nil := ppair vec_nil vec_nil;
  vec_split [ppair a b, uab] := let '(ppair ua ub) := vec_split uab in ppair [a, ua] [b, ub].

Lemma vec_nth_fst_split {N α β} {vab: vec (α ** β) N} {i}:
  vec_nth (pfst (vec_split vab)) i = pfst (vec_nth vab i).
Proof.
  induction vab as [|N [a b] u].
  - inversion i.
  - simp vec_split. destruct (vec_split u).
    dependent elimination i; cbn; now simp vec_nth.
Qed.

Lemma vec_nth_snd_split {N α β} {vab: vec (α ** β) N} {i}:
  vec_nth (psnd (vec_split vab)) i = psnd (vec_nth vab i).
Proof.
  induction vab as [|N [a b] u].
  - inversion i.
  - simp vec_split. destruct (vec_split u).
    dependent elimination i; cbn; now simp vec_nth.
Qed.

(* vec_map2 *)

Definition vec_map2 {N α β γ} (f: α -> β -> γ)
    (v1: vec α N) (v2: vec β N): vec γ N :=
  vec_map (fun '(ppair a b) => f a b) (vec_combine v1 v2).

Lemma vec_nth_map2 {N α β γ} {f: α -> β -> γ} {va: vec α N} {vb} i:
  vec_nth (vec_map2 f va vb) i = f (vec_nth va i) (vec_nth vb i).
Proof.
  (* Getting there was pretty damn hard, but finally! A clean API. *)
  unfold vec_map2.
  rewrite vec_nth_map.
  rewrite vec_nth_combine.
  reflexivity.
Qed.

(* vec_mapi *)

Definition vec_mapi {α β: Type} {N} (f: nat -> α -> β) (v: vec α N): vec β N :=
  vec_map2 f (vec_range N) v.

Lemma vec_nth_mapi {N α β} {f: nat -> α -> β} {v: vec α N} i:
  vec_nth (vec_mapi f v) i = f (fin_nat i) (vec_nth v i).
Proof.
  unfold vec_mapi. now rewrite vec_nth_map2, vec_nth_range.
Qed.

(* vec_mapf *)

Definition vec_mapf {α β: Type} {N} (f: Fin.t N -> α -> β) (v: vec α N):
    vec β N :=
  vec_map2 f (vec_rangef N) v.

Lemma vec_nth_mapf {α β: Type} {N} (f: Fin.t N -> α -> β) (v: vec α N) i:
  vec_nth (vec_mapf f v) i = f i (vec_nth v i).
Proof.
  unfold vec_mapf. now rewrite vec_nth_map2, vec_nth_rangef.
Qed.

(* vec_all *)

Equations vec_all {N α} (f: α -> Prop) (v: vec α N): Prop :=
  vec_all f vec_nil := True;
  vec_all f [a, u] := f a /\ vec_all f u.

Lemma vec_all_forall {N α} {f: α -> Prop} {v: vec α N}:
  vec_all f v <->
  (forall i: Fin.t N, f (vec_nth v i)).
Proof.
  split.
  - induction N.
    * inversion i.
    * dependent elimination v. simp vec_all. intros [Ha Hall] i.
      dependent destruction i; simp vec_nth.
  - induction N.
    * depelim1 v. now simp vec_all.
    * depelim1 v. simp vec_all. intros Hall. split.
      { apply (Hall Fin.F1). }
      { apply IHN. intros i. apply (Hall (Fin.FS i)). }
Qed.

(* vec_all2 *)
(* TODO: Replace all uses of vec_all2 with vec_eq (which is equivalent *)
Definition vec_all2 {N α β} (f: α -> β -> Prop) (va: vec α N) (vb: vec β N) :=
  vec_all (fun '(ppair a b) => f a b) (vec_combine va vb).

Lemma vec_all2_forall {N α β} {f: α -> β -> Prop} {va: vec α N} {vb: vec β N}:
  vec_all2 f va vb <->
  (forall i: Fin.t N, f (vec_nth va i) (vec_nth vb i)).
Proof.
  unfold vec_all2. rewrite vec_all_forall.
  split; intros H i.
  - specialize (H i). now rewrite vec_nth_combine in H.
  - now rewrite vec_nth_combine.
Qed.

(* vec_allb *)

Equations vec_allb {N α} (f: α -> bool) (v: vec α N): bool :=
  vec_allb f vec_nil := true;
  vec_allb f [a, u] := f a && vec_allb f u.

Lemma vec_allb_forall {N α} {f: α -> bool} {v: vec α N}:
  vec_allb f v = true <->
  (forall i: Fin.t N, f (vec_nth v i) = true).
Proof.
  split.
  - induction N.
    * intros ? abs; inversion abs.
    * depelim v.
      simp vec_allb.
      intros H i.
      rewrite andb_true_iff in H; destruct H as [Ha Hall].
      depelim i.
      { rewrite vec_nth_F1. apply Ha. }
      { rewrite vec_nth_FS; cbn. apply IHN, Hall. }
  - induction N.
    * depelim v; cbn; auto.
    * depelim v; cbn in *. intros Hall.
      simp vec_allb.
      apply andb_true_iff. split.
      { apply (Hall Fin.F1). }
      { apply IHN. intros i. apply (Hall (Fin.FS i)). }
Qed.

Lemma vec_allb_false {N α} {f: α -> bool} {v: vec α N}:
  vec_allb f v = false <->
  (exists i: Fin.t N, f (vec_nth v i) = false).
Proof.
  split.
  - induction N.
    * depelim v; discriminate.
    * depelim v. intros Hfalse.
      case (f a) eqn:Ha.
      { simp vec_allb in Hfalse. rewrite Ha, andb_true_l in Hfalse.
        destruct (IHN _ Hfalse) as [i Hi].
        exists (Fin.FS i). rewrite vec_nth_FS; apply Hi. }
      { exists Fin.F1. apply Ha. }
  - induction N.
    * intros [i]. inversion i.
    * depelim v. intros [i Hi]. cbn.
      depelim i.
      { rewrite vec_nth_F1 in Hi. simp vec_allb. rewrite Hi.
        now rewrite andb_false_l. }
      { rewrite vec_nth_FS in Hi. simp vec_allb. rewrite andb_false_iff.
        right. apply IHN. exists i. apply Hi. }
Qed.

(* vec_foldr *)

Equations vec_foldr {N α β} (f: α -> β -> β) (v: vec α N) (b: β): β :=
  vec_foldr f vec_nil b := b;
  vec_foldr f [a, u] b := f a (vec_foldr f u b).

Lemma vec_foldr_iter {N α β} {f: α -> β -> β} {v: vec α N} {b} (I: β -> Prop):
  I b ->
  (forall i b, I b -> I (f (vec_nth v i) b)) ->
  I (vec_foldr f v b).
Proof.
  intros H0 HS.
  revert b H0; induction N; intros.
  - dependent destruction v. simp vec_foldr.
  - dependent destruction v. simp vec_foldr. apply (HS Fin.F1).
    apply IHN; auto. intros; now apply (HS (Fin.FS i)).
Qed.

(* Prove that invariant I is obtained after step "index" and then maintained
   until the end of the fold. *)
Lemma vec_foldr_iter_after {N α β} {f: α -> β -> β} {v: vec α N} {b}
  (I: β -> Prop) (index: Fin.t N):
  (forall b, I (f (vec_nth v index) b)) ->
  (forall i b, I b -> I (f (vec_nth v i) b)) ->
  I (vec_foldr f v b).
Proof.
  intros Hindex HS.
  induction index.
  - dependent destruction v; simp vec_foldr.
  - dependent destruction v; simp vec_foldr.
    apply (HS Fin.F1). apply IHindex; auto.
    intros i *; apply (HS (Fin.FS i)).
Qed.

Lemma vec_foldr_compat_le {N α} {f: α -> α -> α} {va vb: vec α N} {i1 i2}
  (LE: α -> α -> Prop)
  (Hcompat: Proper (LE ==> LE ==> LE) f):
  LE i1 i2 ->
  vec_all2 LE va vb ->
  LE (vec_foldr f va i1) (vec_foldr f vb i2).
Proof.
  revert va vb. induction N.
  - intros a b; depelim a; depelim b; intros Hi _; auto.
  - intros a b; depelim a; depelim b; intros Hi Hall. rewrite vec_all2_forall in Hall.
    simp vec_foldr.
    apply Hcompat. apply (Hall Fin.F1).
    apply IHN; auto. rewrite vec_all2_forall.
    intros i. apply (Hall (Fin.FS i)).
Qed.

Lemma vec_foldr_compat_lt {N α} (f: α -> α -> α) (va vb: vec α N) i1 i2
  (LE LT: α -> α -> Prop)
  (Sub: subrelation LT LE)
  (HcompatLE: Proper (LE ==> LE ==> LE) f)
  (HcompatLT_r: Proper (LE ==> LT ==> LT) f)
  (HcompatLT_l: Proper (LT ==> LE ==> LT) f):
  LE i1 i2 ->
  vec_all2 LE va vb ->
  (exists (i: Fin.t N), LT (vec_nth va i) (vec_nth vb i)) ->
  LT (vec_foldr f va i1) (vec_foldr f vb i2).
Proof.
  revert va vb. induction N.
  - intros a b; depelim a; depelim b; intros _ _ [x _]; depelim x.
  - intros a b; depelim a; depelim b; intros Hinit Hall [i Hi].
    depelim i.
    (* When i=0, we gain LT immediately, and finish immediately by carrying it
       through LE using vec_foldr_compat_le. *)
    { rewrite ! vec_nth_F1 in Hi. simp vec_foldr.
      apply HcompatLT_l. apply Hi.
      apply (vec_foldr_compat_le _ HcompatLE Hinit). apply Hall. }
    (* When i>0, we continue with the induction hypothesis. *)
    { rewrite ! vec_nth_FS in Hi. simp vec_foldr.
      apply HcompatLT_r. apply Hall.
      apply IHN; auto. apply Hall. now exists i. }
Qed.

(* ============================================================================
** Equality *)

Definition vec_eq {α β} (R: α -> β -> Prop) {N} (v₁: vec α N) (v₂: vec β N) :=
  forall i, R (vec_nth v₁ i) (vec_nth v₂ i).

#[export]
Instance Reflexive_vec_eq {α N} (R: α -> α -> Prop) `{Reflexive _ R}:
  Reflexive (vec_eq (N := N) R).
Proof. repeat intro; reflexivity. Qed.

#[export]
Instance Symmetric_vec_eq {α N} (R: α -> α -> Prop) `{Symmetric _ R}:
  Symmetric (vec_eq (N := N) R).
Proof. repeat intro; now symmetry. Qed.

#[export]
Instance Transitive_vec_eq {α N} (R: α -> α -> Prop) `{Transitive _ R}:
  Transitive (vec_eq (N := N) R).
Proof. repeat intro; etransitivity; eauto. Qed.

#[export]
Instance Equivalence_vec_eq {α N} (R: α -> α -> Prop) `{Equivalence _ R}:
  Equivalence (vec_eq (N := N) R) := {}.

#[export]
Instance Proper_vec_map {α β} {Rα: α -> α -> Prop} {Rβ: β -> β -> Prop}:
  Proper ((Rα ==> Rβ) ==>
          forall_relation (fun N =>
            vec_eq (N := N) Rα ==> vec_eq (N := N) Rβ))
         vec_map.
Proof.
  intros f₁ f₂ Hf N v₁ v₂ Hv i.
  rewrite ! vec_nth_map.
  apply Hf, (Hv i).
Qed.

Lemma vec_eq_eq {α N} (v₁ v₂: vec α N):
  v₁ = v₂ <-> vec_eq Logic.eq v₁ v₂.
Proof.
  split. now intros ->.
  intros Heq.
  induction N.
  - dependent destruction v₁; dependent destruction v₂. reflexivity.
  - dependent destruction v₁; dependent destruction v₂.
    assert (a = a0) as -> by apply (Heq Fin.F1).
    assert (v₁ = v₂) as ->.
    { apply IHN. intros i. apply (Heq (Fin.FS i)). }
    reflexivity.
Qed.

(* weaker version just in case inference on the strong one fails
#[export]
Instance Proper_vec_map {α β} {Rα: α -> α -> Prop} {Rβ: β -> β -> Prop} {f} {N}:
  Proper (Rα ==> Rβ) f ->
  Proper (vec_eq (N := N) Rα ==> vec_eq (N := N) Rβ) (vec_map f).
Proof.
  intros Pf v₁ v₂ Heq i.
  rewrite ! vec_nth_map.
  apply Pf, (Heq i).
Qed. *)

(* ============================================================================
** Lattice definition *)

#[export, refine]
Instance LatticeBase_vec (N: nat) (α: Type) {L: LatticeBase α}:
    LatticeBase (vec α N) := {

  lble v1 v2 := vec_allb id (vec_map2 lble v1 v2);
  bot := vec_init N (fun _ => bot);
  top := vec_init N (fun _ => top);
  join := vec_map2 join;
  meet := vec_map2 meet;
}.
Proof.
  (* Reflexivity of ⊆ *)
  - intros v. apply vec_allb_forall. intros.
    rewrite vec_nth_map2. apply lle_refl.
  (* Transitivity of ⊆ *)
  - intros v1 v2 v3. rewrite ! vec_allb_forall.
    setoid_rewrite vec_nth_map2.
    intros H12 H23 i. eapply lle_trans. apply (H12 i). apply (H23 i).
  (* Join bounds *)
  - intros v1 v2. apply vec_allb_forall. intros.
    rewrite ! vec_nth_map2. apply join_l.
  - intros v1 v2. apply vec_allb_forall. intros.
    rewrite ! vec_nth_map2. apply join_r.
Defined.

Lemma vec_lle_extensionality {N α} `{L: LatticeBase α} {v₁ v₂: vec α N}:
  v₁ ⊆ v₂ <-> forall i, vec_nth v₁ i ⊆ vec_nth v₂ i.
Proof.
  cbn. rewrite vec_allb_forall. unfold id, vec_map2.
  now setoid_rewrite vec_nth_map; setoid_rewrite vec_nth_combine.
Qed.

Lemma vec_lble_extensionality {N α} `{L: LatticeBase α} {v₁ v₂: vec α N}:
  v₁ ⊆? v₂ = true <-> forall i, vec_nth v₁ i ⊆? vec_nth v₂ i = true.
Proof.
  apply vec_lle_extensionality.
Qed.

Definition vec_measure {N α} {L: Lattice α} (v: vec α N):
    natvec (measure_N L) :=
  vec_foldr (fun m m' => m ⊕ m') (vec_map measure v) (natvec_zero _).

Lemma vec_measure_top {N α} {L: Lattice α} (v: vec α N):
  vec_measure (N := N) top ≼ vec_measure v.
Proof.
  apply vec_foldr_compat_le with (LE := natvec_le).
  - repeat intro. now apply natvec_add_le.
  - apply natvec_zero_le.
  - rewrite vec_all2_forall. intros i. rewrite ! vec_nth_map.
    cbn. rewrite vec_nth_init. apply measure_top.
Qed.

Lemma vec_measure_le {N α} {L: Lattice α} (v1 v2: vec α N):
  v1 ⊆ v2 -> vec_measure v2 ≼ vec_measure v1.
Proof.
  intros Hin.
  apply vec_foldr_compat_le with (LE := natvec_le).
  - repeat intro. now apply natvec_add_le.
  - apply natvec_zero_le.
  - rewrite vec_all2_forall. intros i. rewrite ! vec_nth_map.
    cbn in Hin. rewrite vec_allb_forall in Hin. specialize (Hin i).
    rewrite vec_nth_map2 in Hin.
    apply measure_sound_le. apply Hin.
Qed.

Lemma vec_measure_lt {N α} {L: Lattice α} (v1 v3: vec α N):
  (exists v2, v3 = vec_map2 widen v1 v2 /\ ~(v2 ⊆ v1)) ->
  vec_measure v3 ≺ vec_measure v1.
Proof.
  intros [v2 [-> Hv2]]. cbn in Hv2.
  rewrite not_true_iff_false, vec_allb_false in Hv2.
  apply vec_foldr_compat_lt with (LT := natvec_lt) (LE := natvec_le).
  - repeat intro. now apply natvec_le_of_lt.
  - repeat intro. now apply natvec_add_le.
  - repeat intro. now apply natvec_add_le_lt.
  - repeat intro. now apply natvec_add_lt_le.
  - apply natvec_zero_le.
  - rewrite vec_all2_forall. intros i. rewrite ! vec_nth_map.
    rewrite vec_nth_map2. apply measure_sound_le. apply widen_l.
  - destruct Hv2 as [i Hi]. exists i.
    rewrite ! vec_nth_map. rewrite vec_nth_map2 in Hi. rewrite vec_nth_map2.
    apply measure_sound_lt. exists (vec_nth v2 i).
    split; [reflexivity|cbn]. unfold id in Hi. now rewrite Hi.
Qed.

#[export, refine]
Instance Lattice_vec (N: nat) (α: Type) {L: Lattice α}: Lattice (vec α N) := {
  widen := vec_map2 widen;
  measure_N := measure_N L;
  measure := vec_measure;
}.
Proof.
  (* Widen bounds *)
  - intros v1 v2. unfold lle, lble, LatticeBase_vec.
    apply vec_allb_forall. intros i.
    rewrite ! vec_nth_map2. apply widen_l.
  - intros v1 v2.  unfold lle, lble, LatticeBase_vec.
    apply vec_allb_forall. intros i.
    rewrite ! vec_nth_map2. apply widen_r.
  (* Measure properties *)
  - apply vec_measure_top.
  - apply vec_measure_le.
  - apply vec_measure_lt.
Defined.

#[export, refine]
Instance GaloisConnection_vec_vec {N: nat} {α γ}
    {L: Lattice α} {GC: GaloisConnection α γ}:
    GaloisConnection (vec α N) (vec γ N) := {
  galois_in := vec_eq galois_in;
}.
Proof.
  (* Soundness of ⊆ *)
  - intros v u c. cbn. rewrite vec_allb_forall.
    intros Hlle Hv i; specialize (Hlle i); specialize (Hv i).
    eapply galois_lble. rewrite vec_nth_map2 in Hlle. apply Hlle. apply Hv.
  (* Meet inequality *)
  - intros v u c.
    intros Hv Hu i; specialize (Hv i); specialize (Hu i).
    cbn. rewrite vec_nth_map2.
    now apply galois_meet.
  (* Top property *)
  - repeat intro. cbn. rewrite vec_nth_init. apply galois_top.
Defined.
