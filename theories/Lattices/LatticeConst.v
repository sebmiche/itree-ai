(* ** Lattice of constants

The lattice of constants of type `T` is a trivial lattice suitable for constant
propagation. It is defined for any T with decidable equality. *)

From Coq Require Import ZArith Classes.EquivDec Bool.Bool Lia String.
From ITreeAI Require Import Lattice Util.

Inductive Const (T: Type): Type :=
  | Const_Bot
  | Const_Just (t: T)
  | Const_Top.

Arguments Const_Bot  {T}.
Arguments Const_Just {T}.
Arguments Const_Top  {T}.

Lemma eqe {T} `{H: EqDec T eq}: forall x y,
  ((if H x y then true else false) = true) <-> (x = y).
Proof. split; intros; destruct (H x y); (auto || discriminate). Qed.

#[global, refine]
Instance LatticeBase_Const (T: Type) `{EqDec T eq}: LatticeBase (Const T) := {

  lble x y :=
    match x, y with
    | Const_Bot, _ => true
    | Const_Just u, Const_Just v => if u == v then true else false
    | _, Const_Top => true
    | _, _ => false
    end;

  bot := Const_Bot;
  top := Const_Top;

  join a1 a2 :=
    match a1, a2 with
    | Const_Bot, _ => a2
    | _, Const_Bot => a1
    | Const_Top, _ => Const_Top
    | _, Const_Top => Const_Top
    | Const_Just v1, Const_Just v2 =>
        if v1 == v2 then Const_Just v1 else Const_Top
    end;

  meet a1 a2 :=
    match a1, a2 with
    | Const_Bot, _ => Const_Bot
    | _, Const_Bot => Const_Bot
    | Const_Top, _ => a2
    | _, Const_Top => a1
    | Const_Just v1, Const_Just v2 =>
        if v1 == v2 then Const_Just v2 else Const_Bot
    end;
}.
Proof.
  (* Reflexivity of ⊆ *)
  - intros []; auto. now rewrite eqe.
  (* Transitivity of ⊆ *)
  - intros [] [] []; intuition.
    destruct (t == t0) as [Heq1|], (t0 == t1) as [Heq2|]; intuition.
    inversion Heq1; inversion Heq2; subst.
    destruct (t1 == t1); intuition.
  (* Join bounds *)
  - intros [] []; intuition. now rewrite eqe.
    destruct (t == t0); auto. now rewrite eqe.
  - intros [] []; intuition. now rewrite eqe.
    destruct (t == t0); auto. now rewrite eqe.
Defined.

#[global, refine]
Instance Lattice_Const (T: Type) `{EqDec T eq}: Lattice (Const T) := {
  widen (x y: Const T) := join x y;
  measure_N := 1;
  measure x :=
    match x with
    | Const_Bot     => (2, tt)
    | Const_Just _  => (1, tt)
    | Const_Top     => (0, tt)
    end;
}.
Proof.
  (* Upper bounds on widen *)
  - apply join_l.
  - apply join_r.

  (* measure_top *)
  - intros []; cbn; lia.
  (* measure_le *)
  - intros [] []; cbn; intuition.
  (* measure_lt *)
  - intros [] [] [[] []]; cbn; try (left; lia); cbn in *; try discriminate;
    try now intuition.
    * destruct (t == t0); discriminate.
    * destruct (t == t1); try discriminate.
      inversion e; subst. rewrite eqe in H1. destruct (H1 eq_refl).
Defined.

#[global, refine]
Instance GaloisConnection_Const (T: Type) `{EqDec T eq}:
    GaloisConnection (Const T) T := {

  galois_in t x :=
    match x with
    | Const_Bot => False
    | Const_Just u => t = u
    | Const_Top => True
    end;
}.
Proof.
  - intros [] []; auto; intuition. cbn in *. rewrite eqe in H0. now subst.
  - intros [] []; intuition. cbn.
    destruct (t == t0). now transitivity t0. subst; intuition.
  - now cbn.
Defined.

(* ============================================================================
** Common instances *)

#[global]
Instance EqDec_Z: EqDec Z eq.
Proof.
  compute. intros n m.
  destruct (Z_dec n m); intuition.
Qed.

#[global]
Instance EqDec_string: EqDec string eq := string_dec.
