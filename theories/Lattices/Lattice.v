(* ** A short theory of lattices for abstract interpretation

Lattices are used to represent sets of elements of a non-empty base type to
approximate collecting semantics.

We define several classes:
- `Lattice` is the main class, which defines a lattice over a type. Lattices in
   this framework must include a widening operator.
- `LatticeBase` is a lattice without widening; we provide a few universal (but
   mostly bad) widening operators to extend it to a full Lattice.
- `GaloisConnection` connects a lattice to a concrete type, and axiomatizes
   parts of the properties of a Galois connection.

TODO: Provide an automatic extension of LatticeBase into Lattice with the
universal widening operators. *)

From Coq Require Import DecidableType FMaps Classes.EquivDec Program.Tactics.
From ExtLib Require Import Functor.
From ITreeAI Require Import Util.Natvec MemoryDomain.

(* ============================================================================
** Lattices with widening *)

Reserved Infix "⊆"  (at level 70).
Reserved Infix "⊆?" (at level 70).
Reserved Infix "⊔"  (at level 40).
Reserved Infix "⊓"  (at level 35).
Reserved Notation "⊤".
Reserved Notation "⊥".

(* `LatticeBase T` is a lattice without widening on type T. *)
Class LatticeBase (T: Type) := {

  (* A decidable lattice order (Lattice Boolean LE). This must be computable,
     but it only needs to semi-decide the order in the Galois connection. *)
  lble: T -> T -> bool where "x ⊆? y" := (lble x y);
  (* Shorthand notation for asserting order in Prop. *)
  lle x y := x ⊆? y = true where "x ⊆ y" := (lle x y);

  lle_refl :: Reflexive lle;
  lle_trans :: Transitive lle;

  (* Bottom and top elements. There are no requirements on the order, but there
     are some on the measure, which is related to the order via join. *)
  bot: T where "⊥" := bot;
  top: T where "⊤" := top;

  (* Join and meet operations. There are no properties on the meet because the
     semi-decidable nature of ⊆ (ie. x ⊆ y → γ(x) ⊆ γ(y), but not the converse)
     makes it too weak to reason about elements that get smaller. The property
     is categorized with the `GaloisConnection` instead. *)
  join: T -> T -> T where "x ⊔ y" := (join x y);
  meet: T -> T -> T where "x ⊓ y" := (meet x y);

  join_l: forall x y, x ⊆ (x ⊔ y);
  join_r: forall x y, y ⊆ (x ⊔ y);
}.

Declare Scope lattice_scope.
Infix "⊆?" := lble (at level 70): lattice_scope.
Infix "⊆"  := lle  (at level 70): lattice_scope.
Infix "⊔"  := join (at level 40): lattice_scope.
Infix "⊓"  := meet (at level 35): lattice_scope.
Notation "⊤" := top: lattice_scope.
Notation "⊥" := bot: lattice_scope.

(* Make lle unfold automatically with simpl or cbn. *)
Arguments lle /.

Open Scope lattice_scope.

(* `Lattice T` is a full lattice with widening on type T. *)
Class Lattice (T: Type) := {
  Lattice_LatticeBase :: LatticeBase T;

  (* Widening operator *)
  widen: T -> T -> T;
  widen_l: forall x y, x ⊆ widen x y;
  widen_r: forall x y, y ⊆ widen x y;

  (* The widening order, which relates inputs and outputs of the widening
     operator in a well-founded way *)
  widen_order z x := exists y, z = widen x y /\ ~(y ⊆ x);

  (* Measure function, which proves that the widening order is well-founded *)
  measure_N: nat;
  measure: T -> natvec measure_N;

  measure_top: forall x, measure ⊤ ≼ measure x;
  measure_sound_le: forall {x y}, x ⊆ y -> measure y ≼ measure x;
  measure_sound_lt: forall {x z}, widen_order z x -> measure z ≺ measure x;
}.

Arguments measure_N {T} (_).

Proposition widen_order_wf {T} `{Lattice T}:
  well_founded widen_order.
Proof.
  intros x.
  induction x as [x IH] using
    (well_founded_ind (well_founded_natvecof measure)).
  constructor; intros z Hwiden.
  apply IH. now apply measure_sound_lt.
Qed.

(* Additional lattice laws that are needed for some lattices. *)
Class LatticeLaws {T: Type} (L_T: Lattice T) := {
  (* Decide whether a value is ⊥. *)
  bot_dec: forall (t: T), {t = ⊥} + {t <> ⊥};
  is_not_bot t := if bot_dec t then false else true;
  (* Neutrality of ⊥ in join. *)
  join_bot_l: forall x, ⊥ ⊔ x = x;
  join_bot_r: forall x, x ⊔ ⊥ = x;
  (* Associativity of join. *)
  join_assoc: forall x y z, (x ⊔ y) ⊔ z = x ⊔ (y ⊔ z);
}.

(* ============================================================================
** Galois connections between lattice elements and concrete sets *)

(* A Galois connection between abstract type A (which must be a lattice) and
   concrete type C. *)
Class GaloisConnection (A C: Type) `{L_A: Lattice A} := {

  (* Subset concretization predicate. `galois_in c a` means c ∈ γ(a). *)
  galois_in: C -> A -> Prop;

  (* We require the lattice order to semi-decide the set order on γ. *)
  galois_lble: forall {x y c}, x ⊆ y -> galois_in c x -> galois_in c y;

  (* We require the meet to be compatible with galois_in. If (⊆) decided the
     order on γ, this would be a consequence of (x ⊓ y ⊆ x /\ x ⊓ y ⊆ y). *)
  galois_meet: forall {x y c},
    galois_in c x -> galois_in c y -> galois_in c (x ⊓ y);

  galois_top: forall c, galois_in c ⊤;
}.

Arguments galois_in {A C L_A} {GC} _ _ : rename.

Notation "Tˣ <#> T" := (GaloisConnection Tˣ T)
  (at level 40, no associativity).

#[refine] Local Instance GaloisConnection_morphism {T₁ T₂ Tˣ}
    {L_Tˣ: Lattice Tˣ} {GC_T₁: GaloisConnection Tˣ T₁} (f: T₂ -> T₁):
    Tˣ <#> T₂ := {
  galois_in t₂ tˣ := galois_in (f t₂) tˣ;
}.
Proof.
  - intros * Hlble Hin. eapply galois_lble; eauto.
  - intros * Hx Hy. now apply galois_meet.
  - intros *. now apply galois_top.
Defined.

#[refine] Local Instance GaloisConnection_morphismˣ {T T₁ˣ T₂ˣ}
    {L_T₁ˣ: Lattice T₁ˣ} {L_T₂ˣ: Lattice T₂ˣ}
    {GC_T₁: GaloisConnection T₁ˣ T} (f: T₂ˣ -> T₁ˣ)
    (Hf_lble: forall x y, x ⊆ y -> f x ⊆ f y)
    (Hf_meet: forall x y, f (x ⊓ y) = f x ⊓ f y)
    (Hf_top: f ⊤ = ⊤):
    T₂ˣ <#> T := {
  galois_in t t₂ˣ := galois_in t (f t₂ˣ);
}.
Proof.
  - intros * Hlble Hin. eapply galois_lble; [apply Hf_lble|]; eauto.
  - intros * Hx Hy. rewrite Hf_meet. now apply galois_meet.
  - intros *. rewrite Hf_top. now apply galois_top.
Defined.


(* ============================================================================
** Trivial lattice *)

(* A trivial lattice carrying no information. This is not an instance for
   obvious reasons. *)
Program Definition LatticeBase_trivial (T: Type) (t: T): LatticeBase T := {|
  lble _ _ := true;
  bot := t;
  top := t;
|}.
Solve Obligations with intuition.


Program Definition Lattice_trivial (T: Type) (t: T): Lattice T := {|
  Lattice_LatticeBase := LatticeBase_trivial T t;
  widen x _ := x;
  measure_N := 0;
  measure _ := tt;
|}.


(* ============================================================================
** Unit lattice *)

(* A lattice over a unit value. Because the unit type only has one value, there
   is no need to include a top value.. This lattice only contains bot and the
   abstract unit ttˣ. *)
Inductive unitˣ := ttˣ | unitˣ_bot.

Definition unitˣ_to_bool (u: unitˣ): bool :=
  match u with
  | ttˣ => true
  | _ => false
  end.

Ltac inv_unitˣ :=
  match goal with _x: unitˣ |- _ => destruct _x end.

#[global, refine]
Instance LatticeBase_unitˣ: LatticeBase unitˣ := {
  lble u1 u2 :=
    match u1, u2 with
    | ttˣ, unitˣ_bot => false
    | _, _ => true
    end;
  bot := unitˣ_bot;
  top := ttˣ;
  join u1 u2 :=
    match u1 with
    | ttˣ => ttˣ
    | _ => u2
    end;
  meet u1 u2 :=
    match u1 with
    | ttˣ => u2
    | _ => unitˣ_bot
    end;
}.
Proof. all: repeat intro; now repeat inv_unitˣ. Defined.

#[global, refine]
Instance Lattice_unitˣ: Lattice unitˣ := {
  widen := join;
  measure_N := 1;
  measure u :=
    match u with
    | ttˣ => (0, tt)
    | _ => (1, tt)
    end;
}.
Proof.
  all: repeat intro; repeat inv_unitˣ; cbn; intuition.
  (* Some cases left in measure_lt *)
  all: destruct H as [? []]; inv_unitˣ; cbn in *; intuition || inversion H.
Defined.

#[global, refine]
Instance LatticeLaws_unitˣ: LatticeLaws Lattice_unitˣ := {}.
Proof.
  - decide equality.
  - now intros [].
  - now intros [].
  - now intros [] [] [].
Defined.

#[global, refine]
Instance GaloisConnection_unitˣ_unit: GaloisConnection unitˣ unit := {
  galois_in _ u :=
    match u with
    | ttˣ => True
    | _ => False
    end;
}.
Proof. all: repeat intro; repeat inv_unitˣ; intuition || now cbn. Defined.


(* ============================================================================
** Lattice combinators: product lattice

This is one quite straightforward, because a product always consists of two
elements, so there no information to abstract about what elements we have. We
simply map to a product of lattices. *)

#[global, refine]
Instance LatticeBase_prod {T U} {LB_T: LatticeBase T} {LB_U: LatticeBase U}:
    LatticeBase (T * U) := {

  lble '(x1, x2) '(y1, y2) := (x1 ⊆? y1) && (x2 ⊆? y2);

  top := (⊤, ⊤);
  bot := (⊥, ⊥);

  join '(x1, x2) '(y1, y2) := (x1 ⊔ y1, x2 ⊔ y2);
  meet '(x1, x2) '(y1, y2) := (x1 ⊓ y1, x2 ⊓ y2);
}.
Proof.
  - intros [t u]. now rewrite ! lle_refl.
  - intros [] [] []. rewrite ! andb_true_iff.
    intuition; eapply lle_trans; eauto.
  - intros [] []. now rewrite ! join_l.
  - intros [] []. now rewrite ! join_r.
Defined.

#[global, refine]
Instance Lattice_prod T U {L_T: Lattice T} {L_U: Lattice U}:
    Lattice (T * U) := {

  widen '(u1, t1) '(u2, t2) := (widen u1 u2, widen t1 t2);
  measure_N := max (measure_N L_T) (measure_N L_U);
  measure '(t, u) :=
      natvec_max_l _ _ (measure t)
    ⊕ natvec_max_r _ _ (measure u);
}.
  (* Widen bounds *)
  - intros [] []. cbn. now rewrite ! widen_l.
  - intros [] []. cbn. now rewrite ! widen_r.

  (* measure_top *)
  - intros [t u]. cbn. apply natvec_add_le.
    apply natvec_max_l_le, measure_top.
    apply natvec_max_r_le, measure_top.

  (* measure_sound_le *)
  - intros [xt xu] [yt yu] Hle.
    cbn in Hle. rewrite andb_true_iff in Hle. apply natvec_add_le.
    now apply natvec_max_l_le, measure_sound_le.
    now apply natvec_max_r_le, measure_sound_le.

  (* measure_sound_lt *)
  - intros [xt xu] [zt zu] [[yt yu] [Hwiden Hnle]].
    rewrite pair_equal_spec in Hwiden. destruct Hwiden as [-> ->].
    unfold lle in Hnle. cbn in Hnle.
    rewrite not_true_iff_false, andb_false_iff in Hnle.
    destruct Hnle as [Hnle|Hnle].
    * apply natvec_add_lt_le.
      apply natvec_max_l_lt, measure_sound_lt.
      { exists yt; intuition. now rewrite H in *. }
      apply natvec_max_r_le, measure_sound_le, widen_l.
    * apply natvec_add_le_lt.
      apply natvec_max_l_le, measure_sound_le, widen_l.
      apply natvec_max_r_lt, measure_sound_lt.
      { exists yu; intuition. now rewrite H in *. }
Defined.

#[global, refine]
Instance GaloisConnection_prod {TA TC UA UC}
    {L_T: Lattice TA} (GC_T: GaloisConnection TA TC)
    {L_U: Lattice UA} (GC_U: GaloisConnection UA UC):
    GaloisConnection (TA * UA) (TC * UC) := {

  galois_in '(tc, uc) '(ta, ua) := galois_in tc ta /\ galois_in uc ua;
}.
Proof.
  - intros [t1 u1] [t2 u2] [tc uc] Hle [Hin1 Hin2].
    cbn in Hle. rewrite andb_true_iff in Hle. destruct Hle.
    split; eapply galois_lble; eauto.
  - repeat intros []; split; now apply galois_meet.
  - intros []; split; apply galois_top.
Defined.

Lemma prod_lle_spec {T U} {L_T: Lattice T} {L_U: Lattice U} x y:
  @lle _ (@Lattice_LatticeBase _ (@Lattice_prod _ _ L_T L_U)) x y
  <-> fst x ⊆ fst y /\ snd x ⊆ snd y.
Proof.
  destruct x, y; cbn; now rewrite andb_true_iff.
Qed.

(* ============================================================================
** Lattice combinators: single-channel sum lattice

For sum types there are different ways to approach the abstraction. Because the
sum type can hold either one of two elements, we end up approximating not only
the value of these elements, but also the information of which element is held.

This version of the sum lattice expresses that information with three options:
1. We know the left type is held, with some abstraction of its value.
2. We know the right type is held, with some abstraction of its value.
3. We don't know which type is held and get no information on either. *)

Inductive LSum {T U: Type} :=
  | LSum_Bot
  | LSum_Left (t: T)
  | LSum_Right (u: U)
  | LSum_Top.
Arguments LSum: clear implicits.

#[global, refine]
Instance LatticeBase_LSum T U {LB_T: LatticeBase T} {LB_U: LatticeBase U}:
    LatticeBase (LSum T U) := {

  lble x y :=
    match x, y with
    | LSum_Bot, _ => true
    | LSum_Left x1, LSum_Left y1 => x1 ⊆? y1
    | LSum_Right x2, LSum_Right y2 => x2 ⊆? y2
    | _, LSum_Top => true
    | _, _ => false
    end;

  top := LSum_Top;
  bot := LSum_Bot;

  join x y :=
    match x, y with
    | LSum_Bot, _ => y
    | _, LSum_Bot => x
    | LSum_Left x1, LSum_Left y1 => LSum_Left (x1 ⊔ y1)
    | LSum_Right x2, LSum_Right y2 => LSum_Right (x2 ⊔ y2)
    | _, _ => LSum_Top
    end;
  meet x y :=
    match x, y with
    | LSum_Bot, _ => LSum_Bot
    | _, LSum_Bot => LSum_Bot
    | LSum_Left x1, LSum_Left y1 => LSum_Left (x1 ⊓ y1)
    | LSum_Right x2, LSum_Right y2 => LSum_Right (x2 ⊓ y2)
    | _, LSum_Top => x
    | LSum_Top, _ => y
    | _, _ => LSum_Left bot
    end;
}.
Proof.
  - intros []; now rewrite ? lle_refl.
  - intros [] [] []; intuition; eapply lle_trans; eauto.
  - intros [] []; auto; apply join_l || apply lle_refl.
  - intros [] []; auto; apply join_r || apply lle_refl.
Defined.

#[global, refine]
Instance Lattice_LSum T U {L_T: Lattice T} {L_U: Lattice U}:
    Lattice (LSum T U) := {

  widen x y :=
    match x, y with
    | LSum_Bot, _ => y
    | _, LSum_Bot => x
    | LSum_Left x1, LSum_Left y1 => LSum_Left (widen x1 y1)
    | LSum_Right x2, LSum_Right y2 => LSum_Right (widen x2 y2)
    | _, _ => LSum_Top
    end;

  measure_N := S (max (measure_N L_T) (measure_N L_U));

  measure x :=
    match x with
    | LSum_Bot => (2, natvec_max_l _ _ (natvec_zero _))
    | LSum_Left x1 => (1, natvec_max_l _ _ (measure x1))
    | LSum_Right x2 => (1, natvec_max_r _ _ (measure x2))
    | LSum_Top => natvec_zero _
    end;
}.
Proof.
  (* Widen bounds *)
  - intros [] []; cbn; auto; apply widen_l || apply lle_refl.
  - intros [] []; cbn; auto; apply widen_r || apply lle_refl.

  (* measure_top *)
  - intros []; cbn; auto. right; intuition.

  (* measure_sound_le *)
  - intros [|x1|x2|] [|y1|y2|]; intros Hle;
    cbn in Hle; try apply measure_sound_le in Hle; cbn; try intuition.

  (* measure_sound_lt *)
  - intros [|x1|x2|] [|z1|z2|] [[|y1|y2|] [Hwiden Hnle]]; try discriminate;
    inversion_clear Hwiden; cbn in *; try intuition.
    * right; intuition. apply natvec_max_l_lt, measure_sound_lt. now exists y1.
    * right; intuition. apply natvec_max_r_lt, measure_sound_lt. now exists y2.
Defined.

#[global, refine]
Instance GaloisConnection_LSum TA TC UA UC
    {L_T: Lattice TA} {GC_T: GaloisConnection TA TC}
    {L_U: Lattice UA} {GC_U: GaloisConnection UA UC}:
    GaloisConnection (LSum TA UA) (TC + UC) := {

  galois_in b a :=
    match b, a with
    | inl b1, LSum_Left a1 => galois_in b1 a1
    | inr b2, LSum_Right a2 => galois_in b2 a2
    | _, LSum_Top => True
    | _, _ => False
    end;
}.
Proof.
  - intros [|x1|x2|] [|y1|y2|]; try (now intuition); intros []; auto; try easy.
    * cbn. apply galois_lble.
    * cbn. apply galois_lble.
  - intros [|x1|x2|] [|y1|y2|] [b1|b2] Hbx Hby; auto;
      (contradiction || now apply galois_meet).
  - now intros [].
Defined.


(* ============================================================================
** Lattice combinators: dual-channel sum lattice

This version of the sum lattic does not track which element of the sum is being
used, and instead provides an approximation for both, using a product. We reuse
`Lattice_prod` for the lattice definition and only need to provide the Galois
connection. *)

#[global, refine]
Instance GaloisConnection_dualsum TA TC UA UC
    {L_T: Lattice TA} {GC_T: GaloisConnection TA TC}
    {L_U: Lattice UA} {GC_U: GaloisConnection UA UC}:
    GaloisConnection (TA * UA) (TC + UC) := {

  galois_in b '(x1, x2) :=
    match b with
    | inl b1 => galois_in b1 x1
    | inr b2 => galois_in b2 x2
    end;
}.
Proof.
  - intros [x1 x2] [y1 y2] b Hinc.
    cbn in Hinc; apply andb_true_iff in Hinc.
    destruct b as [b1|b2]; now apply galois_lble.
  - intros [x1 x2] [y1 y2] [b1|b2] Hbx Hby; now apply galois_meet.
  - intros []; now apply galois_top.
Defined.


(* ========================================================================== *)
(** ** Combinators: map lattice *)

(* The WS module is an instance of FMapWeakList.Make. *)
Module MapLattice (Map: WS).
  Module MapFact := FMapFacts.WFacts(Map).
  Module MapProp := FMapFacts.WProperties(Map).
  Import Map.
  Import MapFact.
  Import MapProp.

  (* Additional map facts *)

  Lemma Add_MapsTo {T} (m m': Map.t T) k e:
    Add k e m m' -> MapsTo k e m'.
  Proof.
    intros Hadd. specialize (Hadd k). apply find_2.
    now rewrite add_eq_o in Hadd.
  Qed.

  Lemma find_Empty: forall {T} {m: t T} (H: Empty m) k, find k m = None.
  Proof.
    intros * H *. specialize (H k). setoid_rewrite find_mapsto_iff in H.
    destruct (find k m); auto. now elim (H t0).
  Qed.

  Lemma Add_remove {T} (m: Map.t T) (k: key) (e: T):
    find k m = Some e -> Add k e (remove k m) m.
  Proof.
    intros Hk k'. destruct (E.eq_dec k k') as [<-|].
    - now rewrite add_eq_o.
    - now rewrite add_neq_o, remove_neq_o.
  Qed.

  Lemma for_all_false_iff: forall {T} f (m: Map.t T),
    Proper (E.eq ==> eq ==> eq) f ->
    for_all f m = false <-> (exists k e, MapsTo k e m /\ f k e = false).
  Proof.
    intros. split.
    (* Subtle direction. Can't use for_all_iff, we need to quantify more
       precisely which set of keys is being iterated on; forall is too rough *)
    - unfold for_all. apply fold_rec.
      * discriminate.
      * intros k e acc m' m'' Hk HnotIn HAdd IH Hacc. destruct acc.
        + exists k, e. split. now eapply Add_MapsTo. now destruct (f k e).
        + destruct (IH eq_refl) as (k' & e' & Hk' & He'); clear IH Hacc.
          exists k', e'. split; [|auto].
          apply find_2. rewrite (HAdd k'). destruct (E.eq_dec k k').
          (* k=k': absurd, we can't have seen that key before *)
          { absurd (In k m'); auto. eapply In_iff; [eassumption|].
            exists e'. apply Hk'. }
          (* k != k': then we fall back to m' *)
          { rewrite add_neq_o; auto. now apply find_1. }
    (* Easy direction *)
    - intros (k & x & Hk & Hx). rewrite <- not_true_iff_false.
      intros Hall. rewrite for_all_iff in Hall; auto.
      specialize (Hall k x Hk). rewrite Hall in Hx; discriminate.
  Qed.

  Definition for_all_2 {T} f (m1 m2: Map.t T) :=
    for_all (fun _ b => b) (Map.map2 (fun o1 o2 => match o1, o2 with
      | None, None => None | _, _ => Some (f o1 o2) end) m1 m2).

  Lemma for_all_2_iff: forall {T} f (m1 m2: Map.t T),
    f None None = true ->
    for_all_2 f m1 m2 = true <-> forall k, f (find k m1) (find k m2) = true.
  Proof.
    intros. unfold for_all_2. rewrite for_all_iff. 2: compute; auto.
    setoid_rewrite find_mapsto_iff. split; intros Htrue k.
    * specialize (Htrue k). setoid_rewrite map2_1bis in Htrue; auto.
      destruct (find k m1), (find k m2); auto.
    * intros []; auto. rewrite map2_1bis; auto. specialize (Htrue k).
      destruct (find k m1), (find k m2); rewrite ? Htrue; discriminate.
  Qed.

  Lemma for_all_2_false_iff: forall {T} f (m1 m2: Map.t T),
    f None None = true ->
    for_all_2 f m1 m2 = false <-> exists k, f (find k m1) (find k m2) = false.
  Proof.
    intros. unfold for_all_2. rewrite for_all_false_iff. 2: compute; auto.
    setoid_rewrite find_mapsto_iff. split.
    - intros (k & ? & Hk & ->). exists k.
      rewrite map2_1bis in Hk; auto.
      destruct (find k m1), (find k m2); inversion Hk; auto.
    - intros [k Hk]. exists k, false; split; [|auto].
      rewrite map2_1bis; auto.
      destruct (find k m1), (find k m2); inversion Hk; auto.
      rewrite Hk in *; discriminate.
  Qed.

  (* Map Lattice type *)

  Inductive MapL (A: Type) :=
    | Bot
    | TopExcept (m: Map.t A).
  Arguments Bot {A}.
  Arguments TopExcept {A}.

  Definition findL {T} k (m: MapL T) :=
    match m with
    | Bot => None
    | TopExcept m => Map.find k m
    end.

  Definition addL {T} k x (m: MapL T) :=
    match m with
    | Bot => TopExcept (Map.add k x (Map.empty T))
    | TopExcept m => TopExcept (Map.add k x m)
    end.

  (* Properties about lattice operations *)

  Definition lble_aux {T} {L: Lattice T} (ot ot': option T): bool :=
    match ot, ot' with
    | Some t, Some t' => t ⊆? t'
    | _, None => true
    | None, _ => false
    end.

  Definition join_aux {T} {L: Lattice T} (ot ot': option T): option T :=
    match ot, ot' with
    | Some t, Some t' => Some (t ⊔ t')
    | _, _ => None
    end.

  Definition meet_aux {T} {L: Lattice T} (ot ot': option T): option T :=
    match ot, ot' with
    | Some t, Some t' => Some (t ⊓ t')
    | _, None => ot
    | None, _ => ot'
    end.

  Definition widen_aux {T} {L: Lattice T} (ot ot': option T) : option T :=
    match ot, ot' with
    | Some t, Some t' => Some (widen t t')
    | None, _ => None
    | _, None => None
    end.

  #[global, refine]
  Instance LB T {L_T: Lattice T}: LatticeBase (MapL T) := {
    lble em1 em2 :=
      match em1, em2 with
      | Bot, _ => true
      | TopExcept am1, Bot => false
      | TopExcept am1, TopExcept am2 => for_all_2 lble_aux am1 am2
      end;

    top := TopExcept (Map.empty T);
    bot := Bot;

    join em1 em2 :=
      match em1, em2 with
      | Bot, _ => em2
      | _, Bot => em1
      | TopExcept am1, TopExcept am2 => TopExcept (Map.map2 join_aux am1 am2)
      end;

    meet em1 em2 :=
      match em1, em2 with
      | Bot, _ => Bot
      | _, Bot => Bot
      | TopExcept am1, TopExcept am2 => TopExcept (Map.map2 meet_aux am1 am2)
      end;
  }.
  Proof.
    (* Reflexivity of ⊆? *)
    - intros []; auto. rewrite for_all_2_iff; auto.
      intros. destruct (find k m); cbn; auto. apply lle_refl.
    (* Transitivity of ⊆? *)
    - intros [] [] []; intuition. rewrite for_all_2_iff in *; auto.
      intros; specialize (H k); specialize (H0 k).
      destruct (find k m), (find k m0), (find k m1); cbn in *; intuition.
      eapply lle_trans; eauto.
    (* Join bounds *)
    - intros [|amx] [|amy]; auto; rewrite for_all_2_iff; auto.
      * intros. destruct (find k amx); cbn; auto. apply lle_refl.
      * intros. rewrite map2_1bis; auto.
        destruct (find k amx), (find k amy); cbn; auto. apply join_l.
    - intros [|amx] [|amy]; auto; rewrite for_all_2_iff; auto.
      * intros. destruct (find k amy); cbn; auto. apply lle_refl.
      * intros. rewrite map2_1bis; auto.
        destruct (find k amx), (find k amy); cbn; auto. apply join_r.
  Defined.

  Definition map_widen {T} `{Lattice T} (emx emy: MapL T): MapL T :=
    match emx, emy with
    | Bot, _ => emy
    | _, Bot => emx
    | TopExcept amx, TopExcept amy => TopExcept (Map.map2 widen_aux amx amy)
    end.

  Lemma map_widen_l {T} `{Lattice T}:
    forall (emx emy: MapL T), emx ⊆ map_widen emx emy.
  Proof.
    intros [|amx] [|amy]; cbn; auto; rewrite for_all_2_iff; auto; intros.
    - destruct (find k amx); cbn; auto. apply lle_refl.
    - unfold widen_aux. rewrite map2_1bis; auto.
      destruct (find k amx), (find k amy); auto. cbn. apply widen_l.
  Qed.

  Lemma map_widen_r {T} `{Lattice T}:
    forall (emx emy: MapL T), emy ⊆ map_widen emx emy.
  Proof.
    intros [|amx] [|amy]; cbn; auto; rewrite for_all_2_iff; auto; intros.
    - destruct (find k amy); cbn; auto. apply lle_refl.
    - unfold widen_aux. rewrite map2_1bis; auto.
      destruct (find k amx), (find k amy); auto. cbn. apply widen_r.
  Qed.

  (* Properties about the measure *)

  Definition measure_acc {T} `{Lattice T} (_: key) (x: T) (acc: natvec _) :=
    ((1, measure x): natvec (S _)) ⊕ acc.

  (* Bottom has the highest measure, enforced by its leading 1. Other maps have
     the sum of their elements' measure, except that the second integer in the
     vector counts the elements, so that going from Some top (explicit top) to
     None (top by omission) results in a decrease in measure. *)
  Definition map_measure {T} `{Lattice T} (emx: MapL T): natvec (S (S _)) :=
    match emx with
    | Bot => (1, (0, natvec_zero _))
    | TopExcept amx => (0, fold measure_acc amx (natvec_zero _))
    end.

  (* The measure accumulator is associative-commutative, which allows us to
     iterate over keys in any order *)
  Lemma measure_acc_Proper {T} `{Lattice T}:
    Proper (E.eq ==> eq ==> eq ==> eq) (@measure_acc T _).
  Proof. repeat intro; now subst. Qed.
  #[local] Hint Resolve measure_acc_Proper: core.

  Lemma measure_acc_transpose_neqkey {T} `{Lattice T}:
    transpose_neqkey eq (@measure_acc T _).
  Proof.
    repeat intro; subst; unfold measure_acc.
    destruct a; cbn. rewrite pair_equal_spec; split; [easy|].
    now rewrite <- natvec_add_assoc,
                   (natvec_add_comm (measure e)),
                   natvec_add_assoc.
  Qed.
  #[local] Hint Resolve measure_acc_transpose_neqkey: core.

  Lemma measure_Add {T} `{Lattice T} k (m1 m2: Map.t T) e i:
    ~ In k m1 -> Add k e m1 m2 ->
    fold measure_acc m2 i = measure_acc k e (fold measure_acc m1 i).
  Proof.
    intros. apply fold_Add; auto.
  Qed.

  Lemma measure_split {T} `{Lattice T} k e (m: Map.t T) i:
    find k m = Some e ->
    fold measure_acc m i = measure_acc k e (fold measure_acc (remove k m) i).
  Proof.
    intros. erewrite measure_Add. reflexivity.
    now apply remove_1. now apply Add_remove.
  Qed.

  Lemma map_measure_le_1 {T} `{Lattice T}: forall amx amy,
    TopExcept amx ⊆ TopExcept amy ->
    fold measure_acc amy (0, natvec_zero _)
    ≼ fold measure_acc amx (0, natvec_zero _).
  Proof.
    intros amx amy Hle; cbn in Hle.
    rewrite for_all_2_iff in Hle; auto.
    revert amy Hle. apply fold_rec.

    (* Empty amy *)
    - intros. unfold lble_aux in Hle. setoid_rewrite find_Empty in Hle; auto.
      apply fold_rec; [reflexivity|].
      intros k value meas m1 m2; intros.
      rewrite find_mapsto_iff in H1. specialize (Hle k).
      rewrite H1 in Hle; discriminate.

    (* Nonempty amy *)
    - intros k value m am1 am2 Hmaps Hnin Hadd IH amy Hle; clear amx Hmaps.
      (* amy above am2 means that it might not have k despite am2 having it *)
      destruct (find k amy) as [v' |] eqn:EQy.
      + (* if it does, then we can remove it! *)
        specialize (IH (remove k amy)).
        assert (INEQ: value ⊆ v').
        {
          specialize (Hle k).
          unfold Add in Hadd; rewrite Hadd, add_eq_o, EQy in Hle; eauto.
        }
        rewrite (measure_split k v' amy); auto.
        apply natvec_add_le.
        right; split; [easy|].
        apply measure_sound_le; auto.
        apply IH.
        intros k'.
        case (eq_dec k  k'); intros ?.
        rewrite remove_eq_o; auto.
        apply not_find_in_iff in Hnin.
        erewrite find_o in Hnin; eauto.
        rewrite Hnin; auto.
        rewrite remove_neq_o; auto.
        red in Hadd.
        specialize (Hadd k').
        rewrite add_neq_o in Hadd; auto.
        rewrite <- Hadd; auto.
      + (* if it doesn't, then we still remove it though *)
        specialize (IH amy).
        match type of IH with | ?H -> _ => assert H end.
        {
          intros k'.
          case (F.eq_dec k k'); intros.
          - erewrite find_o; [| symmetry; eassumption].
            erewrite (@find_o _ _ k'); [| symmetry; eassumption].
            rewrite EQy.
            apply not_find_in_iff in Hnin; rewrite Hnin; auto.
          - specialize  (Hle k').
            assert (EQ: find k' am1 = find k' am2).
            { red in Hadd; specialize (Hadd k'); rewrite add_neq_o in Hadd; auto. }
            rewrite EQ.
            destruct (find k' amy) eqn:EQy', (find k' am2) eqn:EQ2; intuition.
        }
        specialize (IH H0).
        rewrite IH.
        apply natvec_add_increasing_l.
  Qed.

  Lemma map_measure_le {T} `{Lattice T}: forall x y,
    x ⊆ y -> map_measure y ≼ map_measure x.
  Proof.
    intros [|amx] [|amy]; try now (cbn; intuition auto using natvec_zero_le).
    intros Hle; right; intuition.
    now apply map_measure_le_1.
  Qed.

  Lemma map_measure_lt {T} `{Lattice T} x z y:
    z = map_widen x y /\ ~ y ⊆ x ->
    map_measure z ≺ map_measure x.
  Proof.
    revert x y z.
    intros [|amx] [|amy] [|amz]; try now (cbn; intuition auto).
    (* Do not simplify natvec to not unfold (natvec (S _) into nat * _) *)
    cbn -["≺" natvec]. intros [Hamz Hle]; right; intuition.
    inversion_clear Hamz.

    (* Since ~ y ⊆ x, they must differ on a key k where y is larger *)
    apply not_true_is_false, for_all_2_false_iff in Hle; [| reflexivity].
    destruct Hle as [k Hk].
    (* ... and k must be in x (otherwise it'd map to top by default, which
       couldn't be extended by y *)
    assert (exists vx, find k amx = Some vx) as [vx EQx].
    { destruct (find k amx); eauto.
      destruct (find k amy); inversion Hk. }

    (* Extract k from the right side *)
    rewrite (measure_split k vx amx); auto.

    destruct (find k amy) as [vy |] eqn:EQy.
    - (* When k is present in amy, we compare its contribution in amx and its
         contribution in amy, then remove from both sides to recurse *)
      rewrite (measure_split k (widen vx vy) _); cycle 1.
      { now rewrite map2_1bis, EQx, EQy. }

      apply natvec_add_lt_le.
      { right; split; [easy|].
        apply measure_sound_lt. exists vy; split; auto.
        rewrite EQx in Hk; cbn in Hk. now cbn; rewrite Hk. }

      apply map_measure_le_1.
      { cbn. rewrite for_all_2_iff; auto.
        intros x.
        case (F.eq_dec k x); intros.
        * rewrite ?remove_eq_o; auto.
        * rewrite ?remove_neq_o; auto.
          pose proof (H0 := map_widen_l (TopExcept amx) (TopExcept amy)).
          cbn in H0. now rewrite for_all_2_iff in H0. }

    - (* When k isn't present in amy, we compare its contribution in amx to 0
         and recurse after removing k only from the amx side *)
      assert (Hzero: forall n (nv: natvec n), natvec_zero _ ⊕ nv = nv).
       { induction n; destruct nv. easy. cbn. rewrite pair_equal_spec.
         split; [easy|]. apply IHn. }
      rewrite <- Hzero at 1.

      apply natvec_add_lt_le; [left; auto|].
      apply map_measure_le_1.
      { cbn. rewrite for_all_2_iff; auto.
        intros x.
        case (F.eq_dec k x); intros.
        * rewrite ?remove_eq_o; auto. rewrite map2_1bis; auto.
          now rewrite <- e, EQx, EQy.
        * rewrite ?remove_neq_o; auto.
          pose proof (H0 := map_widen_l (TopExcept amx) (TopExcept amy)).
          cbn in H0. now rewrite for_all_2_iff in H0. }
  Qed.

  #[global, refine]
  Instance L (T: Type) {L_T: Lattice T}: Lattice (MapL T) := {
    widen := map_widen;
    measure := map_measure;
  }.
  Proof.
    (* Widen bounds *)
    - apply map_widen_l.
    - apply map_widen_r.
    (* Measure properties *)
    - intros [|amx]. simpl; auto. right; intuition.
      rewrite fold_1, elements_empty; cbn.
      destruct (fold _ _) as [[] v'].
      * right; split; [easy|]. apply natvec_zero_le.
      * left. apply PeanoNat.Nat.lt_0_succ.
    - apply map_measure_le.
    - intros * []. eapply map_measure_lt; eauto.
  Defined.

  #[global, refine]
  Instance GC TA TC {L_T: Lattice TA} {GC_T: GaloisConnection TA TC}:
      GaloisConnection (MapL TA) (Map.t TC) := {

    galois_in cm em :=
      match em with
      | Bot => False
      | TopExcept am => forall key,
        match Map.find key cm, Map.find key am with
        | Some b, Some x => galois_in b x
        | None, Some _ => False
        | _, _ => True
        end
      end;
  }.
  Proof.
    (* Soundness of ⊆? *)
    - intros [|amx] [|amy]; cbn; try now intuition.
      intros cm Hble Hin_amx k. specialize (Hin_amx k).
      rewrite for_all_2_iff in Hble; auto.
      case_eq (find k cm).
      * intros b Hb; rewrite Hb in *.
        specialize (Hble k). destruct (find k amy), (find k amx); intuition.
      * intros Hnone; rewrite Hnone in *; clear Hnone.
        specialize (Hble k). destruct (find k amy), (find k amx); intuition.
    (* Meet inequality *)
    - intros emx emy cm Hinx Hiny.
      destruct emx as [| amx]; try contradiction.
      destruct emy as [| amy]; try contradiction.
      intros k. specialize (Hinx k). specialize (Hiny k).
      destruct (find k cm).
      * rewrite map2_1bis; auto. destruct (find k amx), (find k amy); auto.
        simpl. now apply galois_meet.
      * rewrite map2_1bis; auto. destruct (find k amx), (find k amy); auto.
    (* Top property *)
    - cbn. intros. rewrite empty_o. now case find.
  Defined.

  (* This can't be specified in MemoryDomain.v because modules with parameters
     can't be extended/built upon after they're defined *)
  #[global] Instance MD {T}: MemDomain (MapL T) Map.key T := {
    mem_store m addr val := addL addr val m;
    mem_query m addr := findL addr m;
  }.

  (* Properties about map operations *)

  Lemma map_find_sound {T Tˣ} {L: Lattice Tˣ} {GC: GaloisConnection Tˣ T}
      (s: Map.t T) (sˣ: MapL Tˣ) (a: key) (d: T):
    galois_in s sˣ ->
    galois_in (match Map.find a s with Some v => v | _ => d end)
              (match findL a sˣ   with Some v => v | _ => top end).
  Proof.
    intros Hs.
    destruct sˣ as [|emˣ]; cbn.
    - apply galois_top.
    - specialize (Hs a).
      destruct (find a s), (find a emˣ); intuition auto using galois_top.
  Qed.

  Lemma map_add_sound {T Tˣ} {L: Lattice Tˣ} {GC: GaloisConnection Tˣ T}
      (s: Map.t T) (sˣ: MapL Tˣ) (a: key) (v: T) (vˣ: Tˣ):
    galois_in s sˣ ->
    galois_in v vˣ ->
    galois_in (Map.add a v s) (addL a vˣ sˣ).
  Proof.
    intros Hs Hv. destruct sˣ as [|emˣ].
    - intros k. case (E.eq_dec a k).
      * intros Heq. now rewrite ! add_eq_o.
      * intros Hneq. rewrite ! add_neq_o, ! empty_o; auto. now case find.
    - intros k. case (E.eq_dec a k).
      * intros Heq. now rewrite ! add_eq_o.
      * intros Hneq. rewrite ! add_neq_o; auto. apply (Hs k).
  Qed.
End MapLattice.
