(* ** Lattice structure over a finite list of concrete values.

This lattice is suitable for representing finite sets of any (finite or
infinite) concrete type. Its values are all finite sets and top.

We also a little bit of precision for finite types because the set consisting
of all elements is considered smaller than top. *)

From Coq Require Import Lists.List Classes.EquivDec Lia.
From ITreeAI Require Import Lattice Util Util.List.

Import ListNotations.

Inductive listˣ {T: Type}: Type :=
  | listˣ_enum (s: list T)
  | listˣ_all.
Arguments listˣ (T): clear implicits.

Definition listˣ_in {T: Type} {EQ: EqDec T eq} (t: T) (l: listˣ T): bool :=
  match l with
  | listˣ_enum s => inb t s
  | listˣ_all => true
  end.

#[global, refine]
Instance LatticeBase_listˣ (T: Type) {EQ: EqDec T eq}:
    LatticeBase (listˣ T) := {
  lble l₁ l₂ :=
    match l₁, l₂ with
    | listˣ_enum s₁, listˣ_enum s₂ => forallb (fun t => inb t s₂) s₁
    | _, listˣ_all => true
    | _, _ => false
    end;
  bot := listˣ_enum [];
  top := listˣ_all;
  join l₁ l₂ :=
    match l₁, l₂ with
    | listˣ_enum s₁, listˣ_enum s₂ => listˣ_enum (@join_lists _ EQ s₁ s₂)
    | _, _ => listˣ_all
    end;
  meet l₁ l₂ :=
    match l₁, l₂ with
    | _, listˣ_all => l₁
    | listˣ_all, _ => l₂
    | listˣ_enum s₁, listˣ_enum s₂ => listˣ_enum (@meet_lists _ EQ s₁ s₂)
    end;
}.
Proof.
  - intros []; auto. apply forallb_inb_self.
  - intros [] [] []; discriminate || auto.
    rewrite ! forallb_forall. intros. apply H0, inb_In, H, H1.
  - intros [] []; intuition. apply forallb_forall; intros.
    rewrite join_lists_specb, Bool.orb_true_iff. left; now apply inb_In.
  - intros [] []; intuition. apply forallb_forall; intros.
    rewrite join_lists_specb, Bool.orb_true_iff. right; now apply inb_In.
Defined.

#[global, refine]
Instance Lattice_listˣ (T: Type) {EQ: EqDec T eq}: Lattice (listˣ T) := {
  widen l₁ l₂ :=
    match l₁, l₂ with
    | listˣ_enum s₁, listˣ_enum s₂ =>
        (* TODO: Try to keep the lists when there is a finite guarantee? *)
        listˣ_all
    | _, _ => listˣ_all
    end;
  measure_N := 1;
  measure l :=
    match l with
    | listˣ_enum _ => (1, tt)
    | listˣ_all => (0, tt)
    end;
}.
Proof.
  (* Widen bounds *)
  - intros [] []; cbn; auto.
  - intros [] []; cbn; auto.
  (* measure_top *)
  - intros []; cbn; lia.
  (* measure_le *)
  - intros [] [] H; cbn in *; lia.
  (* measure_lt *)
  - intros [] [] [[] [H1 H2]]; cbn in *; discriminate || contradiction || auto.
Defined.

#[global, refine]
Instance GaloisConnection_listˣ (T: Type) {EQ: EqDec T eq}:
    GaloisConnection (listˣ T) T := {
  galois_in t l :=
    match l with
    | listˣ_enum s => In t s
    | listˣ_all => True
    end;
}.
Proof.
  - intros [] [] ? Hin; intuition. cbn in Hin. rewrite forallb_forall in Hin.
    setoid_rewrite inb_In in Hin; auto.
  - intros [] [] ? Hin; cbn; intuition. now apply meet_lists_spec.
  - easy.
Defined.

Lemma listˣ_in_galois_in {T} {EQ: EqDec T eq} (t: T) (l: listˣ T):
  listˣ_in t l = true <-> galois_in t l.
Proof. destruct l; cbn; intuition (now try apply inb_In). Qed.

Lemma listˣ_in_lble {T} {EQ: EqDec T eq} (t: T) (l₁ l₂: listˣ T):
  listˣ_in t l₁ = true -> l₁ ⊆ l₂ -> listˣ_in t l₂ = true.
Proof.
  destruct l₁, l₂; cbn; intuition.
  rewrite forallb_forall in H0. apply H0. now rewrite <- inb_In.
Qed.

Lemma listˣ_in_meet {T} {EQ: EqDec T eq} (t: T) (l₁ l₂: listˣ T):
  listˣ_in t l₁ = true -> listˣ_in t l₂ = true -> listˣ_in t (l₁ ⊓ l₂) = true.
Proof.
  destruct l₁, l₂; cbn; intuition.
  rewrite inb_In in *. now apply meet_lists_spec.
Qed.
