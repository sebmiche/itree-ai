(* ** Universe-polymorphic products

When writing the CFG combinator, we run into all sorts of problems because we
have both aflows that return vectors of states, and vectors of aflows (for the
blocks themselves). This puts a lot of universe pressure on all the definitions
related to these two types, the crucial one being `prod`, as it is used both
for aflow (through stateT) and in the definition of vec as an iterated product.

To combat this issue, we define polymorphic products and ensure that all of the
definitions related to `vec` use it and remain universe polymorphic. *)

From Coq Require Import PeanoNat Bool List Lia Program.Equality Morphisms.
From ITreeAI Require Import Lattice Util.Natvec Util.List.
Import Nat.
Open Scope signature_scope.

(* ============================================================================
** Polymorphic products *)

Set Universe Polymorphism.

(* We want to make both vectors of aflow and instances of aflow that return
   pairs. If we define vectors using `prod` this will create unsolvable
   universe constraints on it because it's not universe polymorphic. Instead we
   use a polymorphic product. (A monomorphic copy would've worked too.) *)
Inductive pprod (α β: Type): Type :=
  | ppair (_: α) (_: β).
Arguments ppair {α β}.

Definition pfst {α β} (p: pprod α β): α :=
  match p with
  | ppair a _ => a
  end.

Definition psnd {α β} (p: pprod α β): β :=
  match p with
  | ppair _ b => b
  end.

Module PprodNotations.
Notation "x ** y" := (pprod x y) (at level 40): type_scope.
Notation "⦇ x , y ⦈" := (ppair x y) (format "'⦇' x ','  y '⦈'").
End PprodNotations.

Import PprodNotations.

Lemma ppair_equal_spec {α β} (a1 a2: α) (b1 b2: β):
  ⦇a1, b1⦈ = ⦇a2, b2⦈ <-> a1 = a2 /\ b1 = b2.
Proof. split. intros H; now inversion H. now intros [-> ->]. Qed.

Lemma ppair_canon {α β} (p: α ** β):
  ⦇pfst p, psnd p⦈ = p.
Proof. now destruct p. Qed.


(* ============================================================================
** Polymorphic product lattice

THis is exactly the same as `Lattice_prod` (see `Lattice.v`). *)

#[global, refine]
Instance LatticeBase_pprod {T U} {LB_T: LatticeBase T} {LB_U: LatticeBase U}:
    LatticeBase (T ** U) := {

  lble '⦇x1, x2⦈ '⦇y1, y2⦈ := (x1 ⊆? y1) && (x2 ⊆? y2);

  top := ⦇top, top⦈;
  bot := ⦇bot, bot⦈;

  join '⦇x1, x2⦈ '⦇y1, y2⦈ := ⦇x1 ⊔ y1, x2 ⊔ y2⦈;
  meet '⦇x1, x2⦈ '⦇y1, y2⦈ := ⦇x1 ⊓ y1, x2 ⊓ y2⦈;
}.
Proof.
  - intros [t u]. now rewrite ! lle_refl.
  - intros [] [] []. rewrite ! andb_true_iff.
    intuition; eapply lle_trans; eauto.
  - intros [] []. now rewrite ! join_l.
  - intros [] []. now rewrite ! join_r.
Defined.

#[global, refine]
Instance Lattice_pprod T U {L_T: Lattice T} {L_U: Lattice U}:
    Lattice (T ** U) := {

  widen '⦇u1, t1⦈ '⦇u2, t2⦈ := ⦇widen u1 u2, widen t1 t2⦈;
  measure_N := max (measure_N L_T) (measure_N L_U);
  measure '⦇t, u⦈ :=
      natvec_max_l _ _ (measure t)
    ⊕ natvec_max_r _ _ (measure u);
}.
  (* Widen bounds *)
  - intros [] []. cbn. now rewrite ! widen_l.
  - intros [] []. cbn. now rewrite ! widen_r.

  (* measure_top *)
  - intros [t u]. cbn. apply natvec_add_le.
    apply natvec_max_l_le, measure_top.
    apply natvec_max_r_le, measure_top.

  (* measure_sound_le *)
  - intros [xt xu] [yt yu] Hle.
    cbn in Hle. rewrite andb_true_iff in Hle. apply natvec_add_le.
    now apply natvec_max_l_le, measure_sound_le.
    now apply natvec_max_r_le, measure_sound_le.

  (* measure_sound_lt *)
  - intros [xt xu] [zt zu] [[yt yu] [Hwiden Hnle]].
    rewrite ppair_equal_spec in Hwiden. destruct Hwiden as [-> ->].
    unfold lle in Hnle. cbn in Hnle.
    rewrite not_true_iff_false, andb_false_iff in Hnle.
    destruct Hnle as [Hnle|Hnle].
    * apply natvec_add_lt_le.
      apply natvec_max_l_lt, measure_sound_lt.
      { exists yt; intuition. now rewrite H in *. }
      apply natvec_max_r_le, measure_sound_le, widen_l.
    * apply natvec_add_le_lt.
      apply natvec_max_l_le, measure_sound_le, widen_l.
      apply natvec_max_r_lt, measure_sound_lt.
      { exists yu; intuition. now rewrite H in *. }
Defined.

#[global, refine]
Instance GaloisConnection_pprod {TA TC UA UC}
    {L_T: Lattice TA} (GC_T: GaloisConnection TA TC)
    {L_U: Lattice UA} (GC_U: GaloisConnection UA UC):
    GaloisConnection (TA ** UA) (TC ** UC) := {

  galois_in '⦇tc, uc⦈ '⦇ta, ua⦈ := galois_in tc ta /\ galois_in uc ua;
}.
Proof.
  - intros [t1 u1] [t2 u2] [tc uc] Hle [Hin1 Hin2].
    cbn in Hle. rewrite andb_true_iff in Hle. destruct Hle.
    split; eapply galois_lble; eauto.
  - repeat intros []; split; now apply galois_meet.
  - intros []; split; apply galois_top.
Defined.
