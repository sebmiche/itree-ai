(** * Lattice/Domain of integer intervals *)

From Coq Require Import ZArith Classes.EquivDec Lia.
From ITreeAI Require Import Util Lattice XL_Intv.

Definition Intv := Intervals.t.

Definition Intv_eq i j :=
  (Intervals.isempty i = true /\ Intervals.isempty j = true) \/ i = j.

#[global,refine] Instance Intv_eq_Equivalence: Equivalence Intv_eq := {}.
Proof.
  all: unfold Intv_eq.
  - hnf. intuition.
  - hnf. intuition.
  - intros i j k. repeat (intros [| ->]; auto). intuition.
Defined.

Lemma widen_1': forall N1 N2, Intervals.le N2 (Intervals.widen N1 N2).
Proof.
  unfold Intervals.le, Intervals.widen; intros.
  destruct (Intervals.isempty N1) eqn:E1; auto.
  destruct (Intervals.isempty N2) eqn:E2. elim (Intervals.isempty_1 n N2); auto.
  destruct H. split; apply Zinf.widen_1'; auto.
Qed.

#[global, refine]
Instance LatticeBase_Intv: LatticeBase Intv := {
  lble  := Intervals.ble;
  top   := Intervals.top;
  bot   := Intervals.bot;
  join  := Intervals.join;
  meet  := Intervals.meet;
}.
Proof.
  Import Intervals.

  (* Reflexivity of ⊆ *)
  - hnf. intros. apply ble_2. now intro.
  (* Transitivity of ⊆? *)
  - hnf. intros * H1 H2. apply Bool.orb_true_intro. pose proof (H1' := H1).
    apply Bool.orb_prop in H1; destruct H1 as [Hex|H1]; auto.
    apply Bool.orb_prop in H2; destruct H2 as [Hey|H2].
    * left. apply (isempty_le _ y); auto. now apply ble_1.
    * apply andb_prop in H1; destruct H1 as [H1h H1l].
      apply andb_prop in H2; destruct H2 as [H2h H2l].
      right; apply andb_true_intro.
      split; eapply Zinf.ble_trans; eauto.
  (* Join properties *)
  - intros x y. apply ble_2. intro. apply join_1.
  - intros x y. apply ble_2. intro. apply join_2.
Defined.


#[global, refine]
Instance Lattice_Intv: Lattice Intv := {
  widen     := Intervals.widen;
  measure_N := 1;
  measure x := (Intervals.measure x, tt);
}.
Proof.
  Import Intervals.

  (* Widen upper bound *)
  - intros. apply ble_2. apply widen_1.
  - intros. apply ble_2. apply widen_1'.

  (* Measure properties *)
  - intros. cbn. destruct (measure x); lia.
  - intros x y. case_eq (isempty x); intro Hex.
    { intros Hle. unfold measure at 2; rewrite Hex. cbn.
      assert (H3 := measure_1 y). lia. }
    case_eq (isempty y); intro Hey.
    { intros Hle. cbn in Hle. apply ble_1, isempty_le in Hle; auto.
      rewrite Hle in *; discriminate. }
    intros Hble. unfold measure. rewrite Hex, Hey.
    destruct x as [[] []], y as [[] []]; cbn in *; lia.
  - intros x z [y [Hy1 Hy2]]. cbn in *; left. subst. apply widen_3.
    destruct (ble y x); intuition.
Defined.

#[global, refine]
Instance GaloisConnection_Intv: GaloisConnection Intv Z := {
  galois_in := Intervals.In;
}.
Proof.
  Import Intervals.

  (* Soundness of ⊆ *)
  - intros * Hle; revert c. now apply ble_1.
  (* Meet inequality *)
  - intros x y n Hx Hy. now apply meet_1.
  (* Top property *)
  - intros; apply top_1.
Defined.
