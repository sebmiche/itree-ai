(* ** Sequence combinator

This control flow combinator represents a sequence in the source program in a
continuation style, i.e. the second half of the computation has access to the
return value of the first half.

       Concrete version: seqᵒ           Abstract version: seqˣ

             ┌─────────┐                  ┌─────────┐
             │    t    │                  │    f    │
             └─────────┘                  └─────────┘
               U₁ │                         U₁ ├─────────┬── may_exit
                 step                          │         │     ▼ bool
        OK:       ▼       ERROR:              step       │
         T₁ ┌──── ? ────┐ U₂                T₁ ▼         │
            ▼           │                 ┌─────────┐    │
       ┌─────────┐      │                 │    k    │    │
       │    k    │      │                 └─────────┘    │
       └─────────┘      │                   U₂ │         │
         U₂ ▼           ▼ U₂                 merge ──────┘
                                            U₂ ▼

One might wonder why this combinator even needs to exist when monads natively
have a bind operation specifically for sequencing computations. The short
answer is that in the abstract world, monadic bind is for sequencing internal
computations of the abstract interpreter only, and monadic binds do _not_
carry new monadic effects when interpreting.

The existence of two different types of sequence is fundamental to the design.
The abstract program can be thought of as an abstract interpreter partially
evaluated on a given source program; as a result, it has a mix of control flow
from the abstract interpreter and from the source program. Sequence of the
first kind is monadic bind in aflow, sequence of the second kind is this
combinator. *)

(* TODO [Seq]: Rename dist into step to match the abstract naming *)

From Coq Require Import Morphisms.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad Structures.Functor.
From ITree Require Import
  ITree ITreeFacts Props.Leaf
  Interp.InterpFacts Events.State Events.StateFacts Events.FailFacts.
From ITreeAI.Lattices Require Import
  Lattice LatticeOption.
From ITreeAI Require Import
  BooleanDomain AflowMonad AbstractMonad Util Events
  FailA.

Import Basics.Monads.
Local Open Scope bool_scope.

Inductive ConcreteSeqResult {T₁ U₂: Type} :=
  | CSR_Continue (t₁: T₁)
  | CSR_Fail (err: U₂).
Arguments ConcreteSeqResult: clear implicits.

(* Just the step function. *)
Definition seqᵒ_Params T₁ U₁ U₂ :=
  U₁ -> ConcreteSeqResult T₁ U₂.

Definition seqᵒ_Params_init T₁ U₂: seqᵒ_Params T₁ (*U₁:=*)T₁ U₂ :=
  CSR_Continue.

Definition seqᵒ_Params_stateT {T₁ U₁ U₂ S}:
    seqᵒ_Params T₁ U₁ U₂ ->
    seqᵒ_Params (S * T₁) (S * U₁) (S * U₂) :=
  fun step '(s, u₁) =>
    match step u₁ with
    | CSR_Continue t₁ => CSR_Continue (s, t₁)
    | CSR_Fail err => CSR_Fail (s, err)
    end.

Definition seqᵒ_Params_failT {T₁ U₁ U₂}:
    seqᵒ_Params T₁ U₁ U₂ ->
    seqᵒ_Params T₁ (option U₁) (option U₂) :=
  fun step o_u₁ =>
    match fmap step o_u₁ with
    | Some (CSR_Continue t₁) => CSR_Continue t₁
    | Some (CSR_Fail err) => CSR_Fail (Some err)
    | None => CSR_Fail None
    end.

(* TODO: This is not complete. We use unit as the exception type because failT
   doesn't carry any. *)
Definition seqᵒ_Params_failT_catch {T₁ U₁ U₂}:
    seqᵒ_Params T₁ U₁ U₂ ->
    seqᵒ_Params (T₁ + (* TODO *)unit) (option U₁) U₂ :=
  fun step o_u₁ =>
    match fmap step o_u₁ with
    | Some (CSR_Continue t₁) => CSR_Continue (inl t₁)
    (* Error captured by a *previous* failT handler *)
    | Some (CSR_Fail err) => CSR_Fail err
    (* Error captured by this handler *)
    | None => CSR_Continue (inr tt)
    end.

Definition seqᵒ {E T₁ U₁ U₂} (P: seqᵒ_Params T₁ U₁ U₂)
    (t: itree E U₁) (k: T₁ -> itree E U₂) :=
  ITree.bind t (fun result =>
    match P result with
    | CSR_Continue value => k value
    | CSR_Fail err => ret err
    end).

#[global] Instance seqᵒ_eutt {E T₁ U₁ U₂}:
  Proper (eq ==> eutt eq ==> (eq ==> eutt eq) ==> eutt eq)
         (@seqᵒ E T₁ U₁ U₂).
Proof.
  intros P _ <- t t' Ht k k' Hk.
  apply eutt_eq_bind'; auto; intros; now case P; auto.
Qed.

Lemma seqᵒ_interp {E F T₁ U₁ U₂} (h: E ~> itree F) P t k:
  interp h (seqᵒ P t k)
  ≈ @seqᵒ F T₁ U₁ U₂ P (interp h t) (fun x => interp h (k x)).
Proof.
  unfold seqᵒ; cbn.
  rewrite interp_bind. apply eutt_eq_bind; intros; case P.
  - reflexivity.
  - intros; now rewrite interp_ret.
Qed.

Lemma seqᵒ_interp_state {E F T₁ U₁ U₂ S} (h: E ~> stateT S (itree F))
    P t k s:
  interp_state h (@seqᵒ E T₁ U₁ U₂ P t k) s
  ≈ seqᵒ (seqᵒ_Params_stateT P)
         (interp_state h t s)
         (fun '(s, t) => interp_state h (k t) s).
Proof.
  unfold seqᵒ.
  rewrite interp_state_bind. apply eutt_eq_bind; intros []; cbn; case P.
  - easy.
  - intros; cbn. now rewrite interp_state_ret.
Qed.

Lemma seqᵒ_interp_fail {E F T₁ U₁ U₂} (h: E ~> failT (itree F)) P t k:
  interp_fail h (@seqᵒ E T₁ U₁ U₂ P t k)
  ≈ seqᵒ (seqᵒ_Params_failT P)
         (interp_fail h t)
         (interp_fail h (T := _) ∘ k).
Proof.
  unfold seqᵒ.
  rewrite interp_fail_bind. apply eutt_eq_bind; intros []; [cbn; case P|].
  - reflexivity.
  - intros; now rewrite interp_fail_ret.
  - reflexivity.
Qed.

(* ========================================================================= *)

Record seqˣ_Params {T₁ U₁ U₂: Typeˣ} := {
  (* Function extracting the visible part of U₁ (the non-exception value) into
     T₁ to run the second half of the sequence. The name “step” refers to the
     idea that we perform a control flow step by not throwing/aborting/etc. *)
  seqˣ_step: U₁ -> T₁;
  (* Function checking whether a value of type U₁ may have an early exit path
     that leaves the sequence without running the continuation. This is a
     conservative approximation. *)
  seqˣ_may_exit: U₁ -> bool;
  (* Function joining the early-exit/exception path (if there is one) with the
     normal output. *)
  seqˣ_merge: bool -> U₁ -> U₂ -> U₂;
}.
Arguments seqˣ_Params (T₁ U₁ U₂): clear implicits.

Definition seqˣ {E} {T₁ U₁ U₂: Typeˣ} (P: seqˣ_Params T₁ U₁ U₂)
    (f: aflow E U₁) (k: T₁ -> aflow E U₂) :=
  aflow_seq f k (seqˣ_step P) (seqˣ_may_exit P) (seqˣ_merge P).

Definition seqˣ_Params_init T₁ U₂: seqˣ_Params T₁ ((* U₁ := *) T₁) U₂ :=
  Build_seqˣ_Params _ _ _ id (fun _ => false) (fun b t r => r).

Lemma seqˣ_aflow_eq {E T₁ U₁ U₂ P f f' k k'}:
  aflow_eq f f' ->
  pointwise aflow_eq k k' ->
  aflow_eq (@seqˣ E T₁ U₁ U₂ P f  k)
           (@seqˣ E T₁ U₁ U₂ P f' k').
Proof.
  intros; now apply aflowEqSeq.
Qed.

(* Natural intepretation *)

Lemma seqˣ_interp {E F T₁ U₁ U₂} (h: E ~> aflow F) P f k:
  aflow_eq
    (aflow_interp h (@seqˣ E T₁ U₁ U₂ P f k))
    (seqˣ P (aflow_interp h f) (aflow_interp h (R := _) ∘ k)).
Proof.
  reflexivity.
Qed.

(* Intepretation into stateT *)

Definition seqˣ_Params_stateT {T₁ U₁ U₂} {S: Typeˣ} (P: seqˣ_Params T₁ U₁ U₂):
    seqˣ_Params (S * T₁) (S * U₁) (S * U₂) := {|
  seqˣ_step '(s, u₁) := (s, seqˣ_step P u₁);
  seqˣ_may_exit := seqˣ_may_exit P ∘ snd;
  seqˣ_merge b '(s₁,u₁) '((s₂,u₂): S * U₂) :=
    (if b then s₁ ⊔ s₂ else s₂, seqˣ_merge P b u₁ u₂)
|}.

Lemma seqˣ_interp_state {E F T₁ U₁ U₂} {S: Typeˣ}
    (h: E ~> stateT S (aflow F)) P f k s:
  aflow_eq
    (aflow_interp_state h (@seqˣ E T₁ U₁ U₂ P f k) s)
    (seqˣ (U₁ := S * U₁) (U₂ := S * U₂)
          (seqˣ_Params_stateT P)
          (aflow_interp_state h f s)
          (fun '(s, x) => aflow_interp_state h (k x) s)).
Proof.
  apply aflowEqSeq; reflexivity || now intros [].
Qed.

(* Intepretation into failT *)

Definition seqˣ_Params_failT {T₁ U₁ U₂: Typeˣ} (P: seqˣ_Params T₁ U₁ U₂):
    seqˣ_Params T₁ (optionˣ U₁) (optionˣ U₂) := {|
  seqˣ_step := seqˣ_step P ∘ snd;
  seqˣ_may_exit '(e₁,u₁) := unitˣ_to_bool e₁ || seqˣ_may_exit P u₁;
  seqˣ_merge := failT_post (seqˣ_merge P);
|}.

Lemma seqˣ_interp_fail {E F T₁ U₁ U₂} (h: E ~> failA (aflow F)) P f k:
  aflow_eq
    (aflow_interp_fail h (@seqˣ E T₁ U₁ U₂ P f k))
    (seqˣ (U₁ := optionˣ U₁) (U₂ := optionˣ U₂)
          (seqˣ_Params_failT P)
          (aflow_interp_fail h f)
          (aflow_interp_fail h (R := _) ∘ k)).
Proof.
  cbn. apply aflowEqSeq; reflexivity || now intros [[]].
Qed.

(* TODO: This is not complete. We use unitˣ as the exception type because
   failTˣ doesn't carry any. *)
Definition seqˣ_Params_failT_catch {T₁ U₁ U₂: Typeˣ} (P: seqˣ_Params T₁ U₁ U₂):
    seqˣ_Params (T₁ * unitˣ) (optionˣ U₁) U₂ := {|
  (* Send the error value directly *)
  seqˣ_step '(e₁, u₁) := (seqˣ_step P u₁, e₁);
  (* Cannot propagate failure since we catch everything *)
  seqˣ_may_exit '(e₁,u₁) := seqˣ_may_exit P u₁;
  (* No new error to merge since U₂ is unchanged *)
  seqˣ_merge b ee₁ u₁ := seqˣ_merge P b (snd ee₁) u₁;
|}.

(* --- *)

(* Top-level interface for language definitions *)
(* TODO: Either rename them or map the DSL onto this, i.e.
     denote: DSL -> itree ...
     | Sequence t k => seqᵒ (denote t) CSR_Continue (denote k) *)
(* BEGIN TODO MOVE *)
Definition cseq {E T R} (t: itree E T) (k: T -> itree E R) :=
  seqᵒ (seqᵒ_Params_init _ _) t k.
Definition aseq {E} {T₁ T₂: Typeˣ} (f: aflow E T₁) (k: T₁ -> aflow E T₂) :=
  seqˣ (seqˣ_Params_init _ _) f k.

(* Extra lemma: commutes with bind. This is useful when the definitions of the
   concrete and abstract computations don't perfectly line up because one can
   move pure computations around a cseq. Note that the bind/map involved in
   these lemmas should only ever be used to represent computations internal to
   the interpreter that do not involve real sequencing in the language. *)
Definition cseq_bind {E T R X}
    (t: itree E T) (k: T -> itree E R) (k': R -> itree E X):
  ITree.bind (cseq t k) k'
  ≈ cseq t (fun t => ITree.bind (k t) k').
Proof.
  unfold cseq, seqᵒ. now rewrite bind_bind.
Qed.

Definition cseq_map {E T R X} (t: itree E T) (k: T -> itree E R) (f: R -> X):
  ITree.map f (cseq t k)
  ≈ cseq t (fun t => ITree.map f (k t)).
Proof.
  apply cseq_bind.
Qed.
(* END TODO MOVE *)

(* ========================================================================= *)

(* Predicate modeling the soundness of the monadic effects captured by a seqᵒ
   and a seqˣ while interpreting. Importantly it is, you guessed it, preserved
   by interpretation.*)
Record seq_sound_effects {T₁ U₁ U₂: Type} {T₁ˣ U₁ˣ U₂ˣ: Typeˣ}
  (P: seqᵒ_Params T₁ U₁ U₂) (Pˣ: seqˣ_Params T₁ˣ U₁ˣ U₂ˣ)
  {GC_U₂: U₂ˣ <#> U₂} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}
:= {
  seq_SE_dist: forall u₁ b u₁ˣ u₂ˣ,
    galois_in u₁ u₁ˣ ->
    match P u₁ with
    | CSR_Continue t₁ => galois_in t₁ (seqˣ_step Pˣ u₁ˣ)
    | CSR_Fail err => seqˣ_may_exit Pˣ u₁ˣ = true /\
        galois_in err (seqˣ_merge Pˣ (orb (seqˣ_may_exit Pˣ u₁ˣ) b) u₁ˣ u₂ˣ)
    end;
  seq_SE_merge: forall b u₁ˣ u₂ˣ,
    u₂ˣ ⊆ seqˣ_merge Pˣ b u₁ˣ u₂ˣ;
}.

Lemma seq_sound_effects_init {T₁ U₂: Type} {T₁ˣ U₂ˣ: Typeˣ}
  {GC_U₂: U₂ˣ <#> U₂} {GC_T₁: T₁ˣ <#> T₁}:
  seq_sound_effects (seqᵒ_Params_init T₁ U₂)
                    (seqˣ_Params_init T₁ˣ U₂ˣ).
Proof.
  constructor.
  - auto.
  - intros; cbn. apply lle_refl.
Qed.

Lemma seq_sound_effects_stateT {T₁ U₁ U₂ S: Type} {T₁ˣ U₁ˣ U₂ˣ Sˣ: Typeˣ}
    (P: seqᵒ_Params T₁ U₁ U₂) (Pˣ: seqˣ_Params T₁ˣ U₁ˣ U₂ˣ)
    {GC_U₂: U₂ˣ <#> U₂} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁} {GC_S: Sˣ<#>S}:
  seq_sound_effects P Pˣ ->
  seq_sound_effects (seqᵒ_Params_stateT (S := S) P)
                    (seqˣ_Params_stateT (S := Sˣ) Pˣ).
Proof.
  intros HSE. constructor.
  (* TODO: Old names in this proof *)
  - intros [su1 u1] b [st2 t2] [sr2 r2] [Hins Hin]; cbn.
    pose proof (Hdist := seq_SE_dist _ _ HSE u1 b t2 r2 Hin).
    destruct (P u1); cbn.
    * split; auto.
    * split; [|split]; try now apply Hdist.
      assert (seqˣ_may_exit Pˣ t2 = true) as -> by apply Hdist; cbn.
      eapply galois_lble. apply join_l. apply Hins.
  - intros b [] []. cbn. rewrite Bool.andb_true_iff; split.
    * destruct b; (try apply join_r); apply lle_refl.
    * eapply seq_SE_merge; eauto.
Qed.

Lemma seq_sound_effects_failT {T₁ U₁ U₂: Type} {T₁ˣ U₁ˣ U₂ˣ: Typeˣ}
    (P: seqᵒ_Params T₁ U₁ U₂) (Pˣ: seqˣ_Params T₁ˣ U₁ˣ U₂ˣ)
    {GC_U₂: U₂ˣ <#> U₂} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}:
  seq_sound_effects P Pˣ ->
  seq_sound_effects (seqᵒ_Params_failT P) (seqˣ_Params_failT Pˣ).
Proof.
  intros HSE. constructor.
  (* TODO: Old names in this proof *)
  - intros ou1 b [err_t t2] [err_r r2] Hin.
    (* The case where first concrete half fails is pretty easy *)
    destruct ou1 as [u1|]; [|destruct err_t; cbn in *; discriminate || auto].
    (* Otherwise, we have to worry about not being too agressive when
       optimizing the sequence to not take the intermediate state *)
    cbn. pose proof (Hdist := seq_SE_dist _ _ HSE u1).
    destruct (P u1); cbn in *.
    { now apply Hdist. }
    destruct err_t.
    { split; auto; cbn. specialize (Hdist b t2 r2 Hin).
      destruct Hdist as [-> Hdist]. now rewrite Bool.orb_true_l in Hdist. }
    { now apply Hdist. }
  - intros b [] []; cbn. destruct u0, u; eapply seq_SE_merge; eauto.
Qed.

Lemma seq_sound_effects_failT_catch {T₁ U₁ U₂: Type} {T₁ˣ U₁ˣ U₂ˣ: Typeˣ}
    (P: seqᵒ_Params T₁ U₁ U₂) (Pˣ: seqˣ_Params T₁ˣ U₁ˣ U₂ˣ)
    {GC_U₂: U₂ˣ <#> U₂} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}:
  seq_sound_effects P Pˣ ->
  seq_sound_effects (seqᵒ_Params_failT_catch P) (seqˣ_Params_failT_catch Pˣ).
Proof.
  intros HSE. constructor.
  - intros [u₁|] b [eu₁ˣ u₁ˣ] u₂ˣ Hu₁; cbn.
    * cbn in Hu₁. pose proof (Hdist := seq_SE_dist _ _ HSE u₁).
      destruct (P u₁); cbn in *; eauto.
    * inversion Hu₁; cbn in *; now subst.
  - intros b [eu₁ˣ u₁ˣ] u₂ˣ. cbn. apply (seq_SE_merge _ _ HSE).
Qed.

(* Proof that sound_seq_effects projects to soundness after unfolding. This is
   the traditional abstract interpretation theorem which proves that the
   abstract sequence "algorithm" computes a correct approximation of a concrete
   sequence. It is used as part of `Core.Soundness.sound_unfold` in the very
   last step of Figure 6 (unfolding). *)
Lemma sound_seq
    {E} {T₁ U₁ U₂: Type}
    {Eˣ} {T₁ˣ U₁ˣ U₂ˣ: Typeˣ}
    {GC_U₂: U₂ˣ <#> U₂} {GC_U₁: U₁ˣ <#> U₁} {GC_T₁: T₁ˣ <#> T₁}
    (* Concrete data *)
    (P: seqᵒ_Params T₁ U₁ U₂) (f: itree E U₁) (k: T₁ -> itree E U₂)
    (* Abstract data *)
    (Pˣ: seqˣ_Params T₁ˣ U₁ˣ U₂ˣ) (fˣ: aflow Eˣ _) (kˣ: T₁ˣ -> aflow Eˣ U₂ˣ)
    (* Theorem statement *)
    (Hsound_f: sound f fˣ)
    (Hsound_k: forall t tˣ, galois_in t tˣ -> sound (k t) (kˣ tˣ))
    (HSE: seq_sound_effects P Pˣ)
    p pˣ
    (Hp: p ≈ seqᵒ P f k)
    (Hpˣ: aflow_eq pˣ (@seqˣ Eˣ T₁ˣ U₁ˣ U₂ˣ Pˣ fˣ kˣ)):
  sound p pˣ.
Proof.
  intros u₂ u₂ˣ Hu₂ Hu₂ˣ.
  rewrite Hp  in Hu₂;  clear p  Hp;  cbn in Hu₂.
  rewrite Hpˣ in Hu₂ˣ; clear pˣ Hpˣ; cbn in Hu₂ˣ.

  (* Expose the sequence *)
  inversion_Leaf_bind Hu₂ u₁ Hu₁.
  unfold seqˣ, aflow_seq in Hu₂ˣ.
  simp unfold in Hu₂ˣ.
  inversion_Leaf_bind Hu₂ˣ u₁ˣ Hu₁ˣ.
  inversion_Leaf_bind Hu₂ˣ tmp Htmp.
  simp unfold in Hu₂ˣ.
  inversion_Leaf_ret Hu₂ˣ.
  rename tmp into u₂ˣ, Htmp into Hu₂ˣ.

  (* Now check whether the concrete sequence failed *)
  pose proof (Hdist := seq_SE_dist _ _ HSE u₁ (seqˣ_may_exit Pˣ u₁ˣ) u₁ˣ u₂ˣ).
  specialize (Hsound_f _ _ Hu₁ Hu₁ˣ).
  destruct (P u₁) as [t₁|err₁]; cbn in *.
  * eapply galois_lble. eapply seq_SE_merge; eauto. eapply Hsound_k; eauto.
  * inversion_Leaf_ret Hu₂. rewrite Bool.orb_diag in Hdist.
    apply Hdist, Hsound_f.
Qed.
