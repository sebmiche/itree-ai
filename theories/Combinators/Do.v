(* ** Abstract do combinator

This control flow combinator represents a do/while loop and implements the
naive post-fixpoint approximation strategy with widening at every iteration.

         Concrete version: doᵒ               Abstract version: doˣ

                                              init: T
          init: T  ┌──────┐                       │
             │     │      │                    ⊔/widen ────────┐
           ┌─────────┐    │                     T ▼            │
           │  body   │    │                  ┌─────────┐       │
           └─────────┘    │ TRUE:            │  body   │       ▲
          U₁ │            │ T                └─────────┘       T
           step           │                    U₁ │            │
             ▼   OK: U₂   │                     step           │
         CSR ? ── dist ── ? Value                 ▼ Value*T*U₃ │
             │            │                       ├────────────┘
      ERROR: │            │ FALSE:             ⊔/widen
          U₃ ▼            ▼ U₃                 U₃ ▼

TODO [CombinatorDo]: Decidable conditions, meet in the loop body.
*)

From Coq Require Import Lists.List Morphisms.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad Structures.Functor.
From ITree Require Import
  ITree ITreeFacts Props.Leaf
  Interp.InterpFacts Events.State Events.StateFacts Events.FailFacts.
From ITreeAI.Lattices Require Import
  Lattice LatticeOption.
From ITreeAI Require Import
  BooleanDomain AflowMonad AbstractMonad Util Events
  FailA.

Import Basics.Monads.
Import MonadNotation.
Import LeafNotations.
Local Open Scope monad_scope.
Local Open Scope bool_scope.

(* TODO: Unify the Concrete*Result types across combinators? *)
From ITreeAI Require Import Combinators.Seq.
Record doᵒ_Params {T U₁ U₂ U₃ Value} := {
  doᵒ_step: seqᵒ_Params U₂ U₁ U₃;
  doᵒ_dist: U₂ -> Value * T * U₃;
}.
Arguments doᵒ_Params: clear implicits.

Definition doᵒ_Params_init {T U₂ U₃ Value} (dist : U₂ -> Value * T * U₃):
    doᵒ_Params T U₂ U₂ U₃ Value := {|
  doᵒ_step := seqᵒ_Params_init _ _;
  doᵒ_dist := dist;
|}.

Definition doᵒ_Params_init_unit Value:
    doᵒ_Params unit Value Value unit Value := 
  doᵒ_Params_init (fun v => (v,tt,tt)).

Definition doᵒ_Params_stateT {T U₁ U₂ U₃ Value S}
    (P: doᵒ_Params T U₁ U₂ U₃ Value):
    doᵒ_Params (S * T) (S * U₁) (S * U₂) (S * U₃) Value := {|
  doᵒ_step := seqᵒ_Params_stateT (doᵒ_step P);
  doᵒ_dist '(s, u₂) :=
    let '(v, t, u₃) := doᵒ_dist P u₂ in (v, (s, t), (s, u₃));
|}.

Definition doᵒ_Params_failT {T U₁ U₂ U₃ Value}
    (P: doᵒ_Params T U₁ U₂ U₃ Value):
    doᵒ_Params T (option U₁) U₂ (option U₃) Value := {|
  doᵒ_step := seqᵒ_Params_failT (doᵒ_step P);
  doᵒ_dist u₂ :=
    let '(v, t, u₃) := doᵒ_dist P u₂ in (v, t, Some u₃);
|}.

(* T:  Loop accumulator, pure value as seen by source program.
   U₁: Loop's return status. step tells us whether it failed or not.
   U₂: A subset of U₁ describing success values. dist lets us extract a useful
       triplet (condition, true-value, false-value) from it.
   U₃: Final return type, basically U₁ but only includes failure or
       false-condition values. *)
Definition doᵒ_body {E T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    (P: doᵒ_Params T U₁ U₂ U₃ Value) (body: T -> itree E U₁) (t: T):
    itree E (T + U₃) :=
  u₁ <- body t;;
  ret match doᵒ_step P u₁ with
      | CSR_Continue u₂ =>
          let '(v, continue, finish) := doᵒ_dist P u₂ in
          if BooleanDomain_isfalse v then inr finish else inl continue
      | CSR_Fail err => inr err
      end.

Definition doᵒ {E T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    (P: doᵒ_Params T U₁ U₂ U₃ Value) (body: T -> itree E U₁) (init: T):
    itree E U₃ :=
  ITree.iter (doᵒ_body P body) init.

#[global] Instance doᵒ_eutt {E T U₁ U₂ U₃ Value} {BD: BooleanDomain Value}:
  Proper (eq ==> (eq ==> eutt eq) ==> eq ==> eutt eq)
         (@doᵒ E T U₁ U₂ U₃ _ BD).
Proof.
  intros P ? <- t t' Ht ? ? <-.
  apply eutt_iter; intro; cbn.
  apply eutt_eq_bind'; now auto.
Qed.

Lemma doᵒ_interp {E F T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    (h: E ~> itree F) P body init:
  interp h (@doᵒ E T U₁ U₂ U₃ Value _ P body init)
  ≈ doᵒ P (interp h (T := _) ∘ body) init.
Proof.
  unfold doᵒ; cbn.
  rewrite interp_iter'_eutt; [reflexivity|].
  intros initial; cbn.
  rewrite interp_bind; apply eutt_eq_bind.
  intros; now rewrite ? interp_ret.
Qed.

Lemma doᵒ_interp_state {E F S T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    (h: E ~> stateT S (itree F)) P body init s:
  interp_state h (@doᵒ E T U₁ U₂ U₃ Value _ P body init) s
  ≈ doᵒ (doᵒ_Params_stateT P)
        (fun '(s, x) => interp_state h (body x) s)
        (s, init).
Proof.
  unfold doᵒ. rewrite eutt_eq_interp_state_iter.
  apply eutt_iter; intros []; cbn.
  unfold ITree.map. rewrite interp_state_bind, ! bind_bind.
  apply eutt_eq_bind; intros []; cbn;
    rewrite interp_state_ret, ! bind_ret_l; cbn.
  destruct (doᵒ_step P u) as [u₂|?]; [|reflexivity].
  destruct (doᵒ_dist P u₂) as [[? ?] ?]; cbn.
  now case BooleanDomain_isfalse.
Qed.

Lemma doᵒ_interp_fail {E F T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    (h: E ~> failT (itree F)) P body init:
  interp_fail h (@doᵒ E T U₁ U₂ U₃ Value _ P body init)
  ≈ doᵒ (doᵒ_Params_failT P)
        (interp_fail h (T := _) ∘ body)
        init.
Proof.
  unfold doᵒ. rewrite eutt_eq_interp_fail_iter.
  apply eutt_iter; intros j; cbn.
  rewrite interp_fail_bind, ! bind_bind.
  apply eutt_eq_bind; intros []; cbn.
  - rewrite interp_fail_ret; cbn; rewrite bind_ret_l; cbn.
    destruct (doᵒ_step P u) as [u₂|?]; [|reflexivity].
    destruct (doᵒ_dist P u₂) as [[? ?] ?]; cbn.
    now case BooleanDomain_isfalse.
  - now rewrite bind_ret_l.
Qed.

(* ========================================================================= *)

(* The only parameter is the step function. This definition is provided for API
   consistency so all combinators have their params. *)
Definition doˣ_Params (T U₁ U₃ Value: Typeˣ) :=
  U₁ -> Value * T * U₃.

Definition doˣ {E} {T U₁ U₃ Value: Typeˣ} `{BooleanDomain Value}
    (step: doˣ_Params T U₁ U₃ Value)
    (body: T -> aflow E U₁) init: aflow E U₃ :=
  aflow_fixpoint body step init.

Definition doˣ_Params_init {T U₁ U₃ Value: Typeˣ} (dist : U₁ -> (Value * T * U₃)):
    doˣ_Params T U₁ U₃ Value := dist.

Definition doˣ_Params_init_unit (Value: Typeˣ):
    doˣ_Params unitˣ Value unitˣ Value :=
  fun value => (value, ttˣ, ttˣ).

Lemma doˣ_aflow_eq {E} {T U₁ U₃ Value: Typeˣ} `{BooleanDomain Value}
    step body body' init:
  pointwise aflow_eq body body' ->
  aflow_eq (@doˣ E T U₁ U₃ Value _ step body init)
           (@doˣ E T U₁ U₃ Value _ step body' init).
Proof.
  intros. now apply aflowEqFixpoint.
Qed.

Lemma doˣ_interp {E F T U₁ U₃} {Value: Typeˣ} `{BooleanDomain Value}
    (h: E ~> aflow F) step body init:
  aflow_eq
    (aflow_interp h (@doˣ E T U₁ U₃ Value _ step body init))
    (doˣ step (aflow_interp h (R := _) ∘ body) init).
Proof.
  reflexivity.
Qed.

Definition doˣ_Params_stateT {T U₁ U₃ Value S}
    (step: U₁ -> Value * T * U₃):
    (S * U₁) -> Value * (S * T) * (S * U₃) :=
  fun '(s, u₁) => let '(value, t, u₃) := step u₁ in (value, (s, t), (s, u₃)).

Lemma doˣ_interp_state {E F T U₁ U₃} {Value S: Typeˣ} `{BooleanDomain Value}
    (h: E ~> stateT S (aflow F)) P body init s:
  aflow_eq
    (aflow_interp_state h (@doˣ E T U₁ U₃ Value _ P body init) s)
    (doˣ (doˣ_Params_stateT P)
         (fun '(s, x) => aflow_interp_state h (body x) s)
         (s, init)).
Proof.
  cbn; apply aflowEqFixpoint; now intros [].
Qed.

Definition doˣ_Params_failT {T U₁ U₃ Value}
    (step: U₁ -> Value * T * U₃):
    optionˣ U₁ -> Value * T * optionˣ U₃ :=
  fun '(err, u₁) => let '(value, t, u₃) := step u₁ in (value, t, (err, u₃)).

Lemma doˣ_interp_fail {E F T U₁ U₃} {Value: Typeˣ} `{BooleanDomain Value}
    (h: E ~> failA (aflow F)) P body init:
  aflow_eq
    (aflow_interp_fail h (@doˣ E T U₁ U₃ Value _ P body init))
    (doˣ (doˣ_Params_failT P)
         (aflow_interp_fail h (R := _) ∘ body)
         init).
Proof.
  cbn; apply aflowEqFixpoint.
  - reflexivity.
  - now intros [].
  - now intros [[] ?].
Qed.

(* ========================================================================= *)

Definition do_sound_effects {T U₁ U₂ U₃ Value} {Tˣ U₁ˣ U₃ˣ Valueˣ: Typeˣ}
    {GC_T: Tˣ <#> T} {GC_U₁: U₁ˣ <#> U₁} {GC_U₃: U₃ˣ <#> U₃}
    {GC_Value: Valueˣ <#> Value}
    (P: doᵒ_Params T U₁ U₂ U₃ Value) (stepˣ: U₁ˣ -> Valueˣ * Tˣ * U₃ˣ) :=
  forall u₁ u₁ˣ,
    galois_in u₁ u₁ˣ ->
    let '(valueˣ, tˣ, u₃ˣ) := stepˣ u₁ˣ in
    match doᵒ_step P u₁ with
    | CSR_Continue u₂ =>
        (* TODO: this doesn't account for decidable conditions *)
        let '(value, continue, finish) := doᵒ_dist P u₂ in
        galois_in value valueˣ /\
        galois_in continue tˣ /\
        galois_in finish u₃ˣ
    | CSR_Fail err => galois_in err u₃ˣ
    end.

Lemma do_sound_effects_init {T U₂ U₃ Value} {Tˣ U₁ˣ U₃ˣ Valueˣ: Typeˣ}
  {GC_T: Tˣ <#> T} {GC_U₂ : U₁ˣ <#> U₂} {GC_U₃: U₃ˣ <#> U₃} {GC_Value: Valueˣ <#> Value}
  (dist : U₂ -> (Value * T * U₃)) (distˣ : U₁ˣ -> (Valueˣ * Tˣ * U₃ˣ))
  (Hdist_sound : forall x y, galois_in x y -> galois_in (dist x) (distˣ y))
  :
  do_sound_effects (doᵒ_Params_init dist)
                   (doˣ_Params_init distˣ).
Proof.
  intros x y ?; cbn; specialize (Hdist_sound x y); unfold doˣ_Params_init in *;
    destruct (distˣ y) as [[value t] u₃], (dist x) as [[valueˣ tˣ] u₃ˣ].
  cbn in *; intuition.
Qed.

Lemma do_sound_effects_init_unit {Value} {Valueˣ: Typeˣ}
  {GC_Value: Valueˣ <#> Value}:
  do_sound_effects (doᵒ_Params_init_unit Value)
                   (doˣ_Params_init_unit Valueˣ).
Proof.
  intros u₁ u₁ˣ Hu₁; cbn; auto.
Qed.

Lemma do_sound_effects_stateT {T U₁ U₂ U₃ Value S} {Tˣ U₁ˣ U₃ˣ Valueˣ Sˣ: Typeˣ}
    {GC_T: Tˣ <#> T} {GC_U₁: U₁ˣ <#> U₁} {GC_U₃: U₃ˣ <#> U₃}
    {GC_Value: Valueˣ <#> Value} {GC_S: Sˣ <#> S}
    (P: doᵒ_Params T U₁ U₂ U₃ Value) (Pˣ: doˣ_Params Tˣ U₁ˣ U₃ˣ Valueˣ):
  do_sound_effects P Pˣ ->
  do_sound_effects (doᵒ_Params_stateT (S := S) P)
                   (doˣ_Params_stateT (S := Sˣ) Pˣ).
Proof.
  intros Hstep. intros [s u₁] [sˣ u₁ˣ] [Hs Hu₁]; cbn.
  specialize (Hstep _ _ Hu₁). destruct (Pˣ u₁ˣ) as [[valueˣ tˣ] u₃ˣ].
  destruct (doᵒ_step P u₁) as [u₂|?]; [|auto].
  destruct (doᵒ_dist P u₂) as [[? ?] ?]; cbn.
  intuition now apply Hstep.
Qed.

Lemma do_sound_effects_failT {T U₁ U₂ U₃ Value} {Tˣ U₁ˣ U₃ˣ Valueˣ: Typeˣ}
    {GC_T: Tˣ <#> T} {GC_U₁: U₁ˣ <#> U₁} {GC_U₃: U₃ˣ <#> U₃}
    {GC_Value: Valueˣ <#> Value}
    (P: doᵒ_Params T U₁ U₂ U₃ Value) (Pˣ: U₁ˣ -> Valueˣ * Tˣ * U₃ˣ):
  do_sound_effects P Pˣ ->
  do_sound_effects (doᵒ_Params_failT P)
                   (doˣ_Params_failT Pˣ).
Proof.
  intros Hstep [] [] Hin'; cbn in *; auto;
  [specialize (Hstep _ _ Hin')|]; destruct (Pˣ t) as [[]]; [|auto].
  destruct (doᵒ_step P u) as [u₂|?]; [|auto].
  destruct (doᵒ_dist P u₂) as [[? ?] ?]; cbn; auto.
Qed.

Lemma sound_do
    {E T U₁ U₂ U₃ Value} `{BooleanDomain Value}
    {Eˣ} {Tˣ U₁ˣ U₃ˣ Valueˣ: Typeˣ} `{BooleanDomain Valueˣ}
    {EvE: E =# Eˣ} {GC_U₁: U₁ˣ <#> U₁} {GC_U₃: U₃ˣ <#> U₃} {GC_T: Tˣ <#> T}
    {GC_Value: Valueˣ <#> Value}
    (* Concrete data *)
    (body: T -> itree E U₁) (P: doᵒ_Params T U₁ U₂ U₃ Value)
    (init: T)
    (* Abstract data *)
    (Pˣ: doˣ_Params Tˣ U₁ˣ U₃ˣ Valueˣ) (bodyˣ: Tˣ -> aflow Eˣ U₁ˣ) (initˣ: Tˣ)
    (* Theorem statement *)
    (Hinit: galois_in init initˣ)
    (Hsound_body: forall t tˣ, galois_in t tˣ -> sound (body t) (bodyˣ tˣ))
    (HSE: do_sound_effects P Pˣ)
    p pˣ
    (Hp: p ≈ doᵒ P body init)
    (Hpˣ: aflow_eq pˣ (doˣ Pˣ bodyˣ initˣ)):
  sound p pˣ.
Proof.
  intros u₃ u₃ˣ Hu₃ Hu₃ˣ.
  rewrite Hp in Hu₃; clear p Hp; cbn in Hu₃.
  rewrite Hpˣ in Hu₃ˣ; clear pˣ Hpˣ; unfold doˣ in Hu₃ˣ.
  (* Expose the input r2_in and the output r2 of the last abstract iteration *)
  apply unfold_aflow_fixpoint_pfp in Hu₃ˣ.
  destruct Hu₃ˣ as (tˣ_pfp & u₁_pfp & Hpfp & Htˣ_pfp & Hu₃ˣ).
  destruct (Pˣ u₁_pfp) as [[value_pfp t_pfp] TMP] eqn:Hstepˣ; cbn;
    cbn in Hu₃ˣ; subst TMP.

  (* We prove that all states that come in/out of the concrete loop are covered
     by the input/output of the post-fixpoint iteration, by induction on the
     number of concrete iterations. *)
  unfold doᵒ in Hu₃.
  set (Inv (t: T) := galois_in t tˣ_pfp).
  set (Q (u₃: U₃) := galois_in u₃ u₃ˣ).
  apply (Leaf_iter_inv (doᵒ_body P body) init Inv Q); auto.

  (* Show that if we run the concrete loop on an input t that is abstracted by
     the fixpoint input tˣ_pfp, we get an output u_iter that is abstracted by
     uˣ. We do that by comparing the concrete iteration t --> u_iter with an
     abstract iteration tˣ_pfp --> uˣ, which exists because tˣ_pfp is a pfp. *)
  - intros t u_iter Ht Hu_iter.

    unfold doᵒ_body in Hu_iter; cbn in Hu_iter.
    inversion_Leaf_bind Hu_iter u_body Hu_body.
    inversion_Leaf_ret Hu_iter.

    inversion_Leaf_bind Hpfp uˣ_body Huˣ_body.
    inversion_Leaf_ret Hpfp.
    case (tˣ_pfp ⊔ snd (fst (Pˣ uˣ_body)) ⊆? tˣ_pfp) eqn:Hcheck;
      inversion Hpfp; subst.

    assert (Hpfp': snd (fst (Pˣ uˣ_body)) ⊆ tˣ_pfp).
    { eapply lle_trans. apply join_r. apply Hcheck. }

    specialize (Hsound_body _ _ Ht _ _ Hu_body Huˣ_body).
    specialize (HSE _ _ Hsound_body); rewrite Hstepˣ in HSE.
    destruct (doᵒ_step P u_body) as [u₂_body|err].
    * destruct (doᵒ_dist P u₂_body) as [[v continue] finish].
      case (BooleanDomain_isfalse v); subst Inv; cbn.
      + apply HSE.
      + eapply galois_lble. apply Hpfp'. rewrite Hstepˣ; cbn. apply HSE.
    * apply HSE.

  - subst Inv; cbn.
    eapply galois_lble; [| apply Hinit].
    apply Htˣ_pfp.
Qed.
