(* ** If combinator

This combinator implements a simple two-branch path guarded by a test (in the
form of a boolean-like value).

Interestingly, it is not implemented as a primitive combinator like Seq, Do,
or TailMRec. Instead it's defined as a specialization of TailMRec (which is
essentially a CFG with multiple entry points) with two non-recursive blocks and
an entry point decided by the condition.

This style of definining control flow structures allows us to reuse primitive
control flow structures without having to reprove all the lemmas about their
parameters being preserved by interpretation. All we need to do is to specify
what makes an if (`if_shape_effects`), namely two non-recursive blocks, and
prove that *that* is preserved (which is way easier).

Of course, analyzing an if directly would provide more precision than analyzing
a full CFG. The `unfold` function in `Core.AflowMonad` accounts for this. The
`TailMRec` that we generate here is annotated as being of the `TailMRec_If`
specialization, and the `unfold` function unfolds it as a simpler algorithm. We
piggy-back off the syntactic structure of `TailMRec`, but we still carry out
our own analysis. `sound_if` at the end of this file proves the correctness of
this specialized analysis (which is implicit in `sound p pˣ` as `pˣ` uses the
`TailMRec_If` specialization and `sound` is a relation between `p` and
`unfold pˣ`). *)

From Coq Require Import Morphisms Program.Equality.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad Structures.Functor.
From ITree Require Import
  ITree ITreeFacts Props.Leaf
  Interp.InterpFacts Events.State Events.StateFacts Events.FailFacts.
From ITreeAI Require Import
  BooleanDomain AflowMonad AbstractMonad Util Events FailA.
From ITreeAI.Lattices Require Import Lattice LatticeOption LatticeVec.
From ITreeAI.Combinators Require Import TailMRec.

Import Basics.Monads.
Import VecNotations.
Import LeafNotations.
Import FunctorNotation.
Import MonadNotation.
Local Open Scope type_scope. (* ≈ is eutt *)
Local Open Scope monad_scope.

Definition tailmrecᵒ_Params_init_if U: tailmrecᵒ_Params 2 unit U U :=
  Seq.CSR_Continue ∘ inr.

Definition ifᵒ {E R Value} {BD: BooleanDomain Value}
    (cond: Value) (thenᵒ elseᵒ: itree E R) :=
  tailmrecᵒ (tailmrecᵒ_Params_init_if R)
            [fun _ => thenᵒ, [fun _ => elseᵒ, vec_nil]]
            (if BooleanDomain_istrue cond then Fin.F1 else Fin.FS Fin.F1)
            tt.

Definition tailmrecˣ_Params_init_if U:
    tailmrecˣ_Params 2 (*T:=*)unitˣ (*Tb:=*)unitˣ U (*Ub:=*)U := {|
  (* TODO: Decidable conditions *)
  (* TODO: If bot at entry doesn't give bot at exit, this version of the
           combinator will have garbage precision. 3rd block? *)
  tailmrecˣ_scatter t := vec_initf 2 (fun _ => t);
  tailmrecˣ_step u := (TailBranch_mk_ret, bot, u);
|}.

Definition ifˣ {E Value} {R: Typeˣ} {BD: BooleanDomain Value}
    (cond: Value) (thenˣ elseˣ: aflow E R): aflow E R :=
  tailmrecˣ (tailmrecˣ_Params_init_if R)
            [fun _ => thenˣ, [fun _ => elseˣ, vec_nil]]
            ttˣ.

(* ========================================================================= *)

Record if_shape_effects {Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U} {GC_Ub: Ubˣ <#> Ub}
    {P: tailmrecᵒ_Params 2 Tb Ub U} {entry: Fin.t 2}
    {Pˣ: tailmrecˣ_Params 2 Tˣ Tbˣ Uˣ Ubˣ} := {
  (* Base tailmrec shape *)
  if_SE_tailmrec: tailmrec_sound_effects P entry Pˣ;
  (* No branching *)
  if_SE_returns:
    forall ub,
        (exists u, P ub = Seq.CSR_Continue (inr u))
     \/ (exists err, P ub = Seq.CSR_Fail err)
}.
Arguments if_shape_effects: clear implicits.
Arguments if_shape_effects {Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ} {_ _ _ _}.

Lemma if_shape_effects_init {U} {Uˣ: Typeˣ} {GC_U: Uˣ <#> U} entry:
  if_shape_effects (tailmrecᵒ_Params_init_if U) entry
                   (tailmrecˣ_Params_init_if Uˣ).
Proof.
  constructor.
  - constructor.
    * intros; cbn. rewrite vec_nth_initf; auto.
    * intros; cbn; auto.
  - intros. unfold tailmrecᵒ_Params_init_if. left; now eexists.
Qed.

Lemma if_shape_effects_stateT {Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ} {S} {Sˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U} {GC_Ub: Ubˣ <#> Ub}
    {GC_S: Sˣ <#> S}
    {P: tailmrecᵒ_Params 2 Tb Ub U} {entry: Fin.t 2}
    {Pˣ: tailmrecˣ_Params 2 Tˣ Tbˣ Uˣ Ubˣ}:
  if_shape_effects P entry Pˣ ->
  if_shape_effects (tailmrecᵒ_Params_stateT (S := S) P) entry
                   (tailmrecˣ_Params_stateT (S := Sˣ) Pˣ).
Proof.
  intros Hshape; constructor.
  - now apply tailmrec_sound_effects_stateT, if_SE_tailmrec.
  - intros [s ub]. destruct (if_SE_returns Hshape ub) as [[u Hu]|[err Herr]].
    * left. exists (s, u). now cbn; rewrite Hu.
    * right. exists (s, err). now cbn; rewrite Herr.
Qed.

Lemma if_shape_effects_failT {Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U} {GC_Ub: Ubˣ <#> Ub}
    {P: tailmrecᵒ_Params 2 Tb Ub U} {entry: Fin.t 2}
    {Pˣ: tailmrecˣ_Params 2 Tˣ Tbˣ Uˣ Ubˣ}:
  if_shape_effects P entry Pˣ ->
  if_shape_effects (tailmrecᵒ_Params_failT P) entry
                   (tailmrecˣ_Params_failT Pˣ).
Proof.
  intros Hshape; constructor.
  - now apply tailmrec_sound_effects_failT, if_SE_tailmrec.
  - intros [ub|].
    * destruct (if_SE_returns Hshape ub) as [[u Hu]|[err Herr]].
      + left. exists (Some u). now cbn; rewrite Hu.
      + right. exists (Some err). now cbn; rewrite Herr.
    * right. exists None. now cbn.
Qed.

Lemma sound_if
    {E Eˣ: Type -> Type} {Tb Ub U: Type} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U}
    {GC_Ub: Ubˣ <#> Ub}
    (* Concrete data *)
    (P: tailmrecᵒ_Params 2 Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) 2) (entry: Fin.t 2) (init: Tb)
    (* Abstract data *)
    (Pˣ: tailmrecˣ_Params 2 Tˣ Tbˣ Uˣ Ubˣ)
    (blocksˣ: vec (Tbˣ -> aflow Eˣ Ubˣ) 2) (initˣ: Tˣ)
    (* Theorem statement *)
    (Hinit: galois_in init initˣ)
    (Hsound_blocks:
      vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
                galois_in tb tbˣ -> sound (b tb) (bˣ tbˣ))
             blocks blocksˣ)
    (Hshape: if_shape_effects P entry Pˣ)
    p pˣ
    (Hp: p ≈ tailmrecᵒ P blocks entry init)
    (Hpˣ: aflow_eq pˣ (aflow_tailmrec TailMRec_If (tailmrecˣ_scatter Pˣ)
                                      (tailmrecˣ_step Pˣ) blocksˣ initˣ)):
  sound p pˣ.
Proof.
  pose proof (HSE := if_SE_tailmrec Hshape).

  intros u uˣ Hu Huˣ.
  rewrite Hp in Hu; clear p Hp.
  rewrite Hpˣ in Huˣ; clear pˣ Hpˣ.

  unfold tailmrecᵒ in Hu.
  rewrite unfold_iter in Hu.
  inversion_Leaf_bind Hu uTMP HuTMP.
  inversion_Leaf_bind HuTMP ub Hub.

  unfold aflow_tailmrec in Huˣ; simp unfold in Huˣ.
  rewrite unfold_unfold_mapM in Huˣ.
  inversion_Leaf_bind Huˣ outsˣ Houtsˣ.
  simp unfold in Huˣ.
  inversion_Leaf_ret Huˣ.

  eapply step_sound_ret.
  6: apply Houtsˣ.
  4: apply Hub.
  2: apply HSE.
  { clear -Hsound_blocks. intros i tb tbˣ Hgalois ub ubˣ Hub Hubˣ.
    apply (Hsound_blocks i _ _ Hgalois ub ubˣ Hub).
    now rewrite vec_nth_map in Hubˣ. }
  apply tailmrec_SE_scatter with (P := P); auto.

  destruct (if_SE_returns Hshape ub) as [[uTMP' HPub]|[uTMP' HPub]];
    rewrite HPub in *;
    inversion_Leaf_ret HuTMP;
    inversion_Leaf_ret Hu; [right|left]; easy.
Qed.
