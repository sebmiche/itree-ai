(* ** Abstract mutual-tail-recursion combinator

This structure describes a set of mutually tail-recursive subprograms running
after scattering the initial state until a final join. The iteration strategy
is current the naive fixpoint algorithm.

TODO [TailMRec]: Diagrams for tailmrecᵒ and tailmrecˣ. ('o_o)

This is typically used to implement CFGs (with straight-line or structured
blocks equally). *)

From Coq Require Import Lists.List Lia Bool Program.Equality ZArith.ZArith.
From Equations Require Import Equations.
From ExtLib Require Import Structures.Monad.
From ITree Require Import
  ITree ITreeFacts Events.State Events.StateFacts Events.FailFacts
  Props.Finite Props.HasPost Props.Leaf.
From ITreeAI.Lattices Require Import
  Lattice LatticeOption LatticeVec LatticePprod.
From ITreeAI Require Import
  Lattice LatticeOption FailA
  AflowMonad AbstractMonad Events Util Util.List Util.MonadicIterators.

Import Basics.Monads.
Import ListNotations.
Import MonadNotation.
Import LeafNotations.
Import PprodNotations.
Local Open Scope monad_scope.
Local Open Scope bool_scope.

(* Concrete version *)

(* TODO: Unify concrete result types *)
From ITreeAI.Combinators Require Import Seq.
(* Just the step function *)
Definition tailmrecᵒ_Params (N: nat) (Tb Ub U: Type) :=
  Ub -> ConcreteSeqResult (Fin.t N * Tb + U) U.

Definition tailmrecᵒ_Params_stateT {N Tb Ub U S}
    (step: tailmrecᵒ_Params N Tb Ub U):
    tailmrecᵒ_Params N (S * Tb) (S * Ub) (S * U) :=
  fun '(s, ub) =>
    match step ub with
    | CSR_Continue (inl (l, tb)) => CSR_Continue (inl (l, (s, tb)))
    | CSR_Continue (inr u) => CSR_Continue (inr (s, u))
    | CSR_Fail u => CSR_Fail (s, u)
    end.

Definition tailmrecᵒ_Params_failT {N Tb Ub U}
    (step: tailmrecᵒ_Params N Tb Ub U):
    tailmrecᵒ_Params N Tb (option Ub) (option U) :=
  fun o_u =>
    match o_u with
    | Some u => match step u with
                | CSR_Continue (inl l_tb) => CSR_Continue (inl l_tb)
                | CSR_Continue (inr u) => CSR_Continue (inr (Some u))
                | CSR_Fail u => CSR_Fail (Some u)
                end
    | None => CSR_Fail None
    end.

Definition tailmrecᵒ {E N Tb Ub U}
    (P: tailmrecᵒ_Params N Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) N) (entry: Fin.t N) (i: Tb):
    itree E U :=
  ITree.iter (fun '(l, input) =>
               u <- vec_nth blocks l input;;
               ret match P u with
                   | CSR_Continue x => x
                   | CSR_Fail o => inr o
                   end)
             (entry, i).

Definition tailmrecᵒ_eutt {E N Tb Ub U}
    (P: tailmrecᵒ_Params N Tb Ub U) blocks₁ blocks₂ entry i:
  vec_eq (pointwise (eutt eq)) blocks₁ blocks₂ ->
  tailmrecᵒ (E := E) P blocks₁ entry i ≈ tailmrecᵒ P blocks₂ entry i.
Proof.
  intros Hblocks; unfold tailmrecᵒ.
  apply eutt_iter. intros [l tb].
  now rewrite (Hblocks l tb).
Qed.

Lemma tailmrecᵒ_interp {E F N Tb Ub U} (h: E ~> itree F)
    (P: tailmrecᵒ_Params N Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) N) entry i:
  interp h (tailmrecᵒ P blocks entry i)
  ≈ tailmrecᵒ P (vec_map (fun block x => interp h (block x)) blocks)
              entry i.
Proof.
  apply interp_iter'_eutt. intros [l input]. rewrite vec_nth_map; cbn.
  rewrite interp_bind. apply eutt_eq_bind; intros.
  now rewrite interp_ret.
Qed.

Lemma tailmrecᵒ_interp_state {E F N Tb Ub U S}
    (h: E ~> stateT S (itree F)) (P: tailmrecᵒ_Params N Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) N) entry i s:
  interp_state h (tailmrecᵒ P blocks entry i) s
  ≈ tailmrecᵒ
      (tailmrecᵒ_Params_stateT P)
      (vec_map (fun block '(s, i) => interp_state h (block i) s) blocks)
      entry (s, i).
Proof.
  unfold tailmrecᵒ.

  (* Loop accumulators are related by RI, ie. equal when reassociated. *)
  rewrite eutt_eq_interp_state_iter.
  eapply eutt_iter' with
    (RI := fun '(s1,(l1,i1)) '(l2,(s2,i2)) => s1 = s2 /\ l1 = l2 /\ i1 = i2);
  auto; clear i s entry.

  intros [s [l i]] [? [? ?]] (<- & <- & <-); cbn.
  rewrite vec_nth_map, interp_state_bind, bind_bind.
  apply eutt_eq_bind; intros []; cbn.
  rewrite interp_state_ret, bind_ret_l; cbn. apply eutt_Ret.
  destruct (P u) as [[[]|]|]; now constructor.
Qed.

Lemma tailmrecᵒ_interp_fail {E F N Tb Ub U}
    (h: E ~> failT (itree F)) (P: tailmrecᵒ_Params N Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) N) entry i:
  interp_fail h (tailmrecᵒ P blocks entry i)
  ≈ tailmrecᵒ
      (tailmrecᵒ_Params_failT P)
      (vec_map (fun block i => (interp_fail h (block i))) blocks)
      entry i.
Proof.
  unfold tailmrecᵒ.
  rewrite eutt_eq_interp_fail_iter.
  apply eutt_iter; hnf.
  clear entry i; intros [l i]; cbn.
  rewrite interp_fail_bind, bind_bind, vec_nth_map. apply eutt_eq_bind.
  intros []; cbn.
  - rewrite interp_fail_ret; cbn; rewrite bind_ret_l.
    destruct (P u) as [[[]|]|]; now apply eutt_Ret.
  - now rewrite bind_ret_l.
Qed.

(* ========================================================================= *)

(* Abstract version. Since the combinator is just aflow_tailmrec itself with
   no wrapping, most of the entries here are aliases for aflow lemmas. *)

Record tailmrecˣ_Params {N} {T Tb U Ub: Typeˣ} := {
  tailmrecˣ_scatter: T -> vec Tb N;
  tailmrecˣ_step: Ub -> TailBranch N * Tb * U;
}.
Arguments tailmrecˣ_Params: clear implicits.

Definition tailmrecˣ {E N T Tb U Ub}
    (P: tailmrecˣ_Params N T Tb U Ub) :=
  aflow_tailmrec (E := E) TailMRec_None
                 (tailmrecˣ_scatter P) (tailmrecˣ_step P).

(* TODO: Heterogeneous equality through a sound relation? *)
Lemma tailmrecˣ_aflow_eq {E N T Tb U Ub}
    (P: tailmrecˣ_Params N T Tb U Ub)
    (blocks blocks': vec (Tb -> aflow E Ub) N) i:
  vec_eq (pointwise aflow_eq) blocks blocks' ->
  aflow_eq
    (tailmrecˣ P blocks i)
    (tailmrecˣ P blocks' i).
Proof.
  intros; now apply Proper_aflow_tailmrec.
Qed.

Lemma tailmrecˣ_interp {E F N T Tb U Ub}
    (h: E ~> aflow F) (P: tailmrecˣ_Params N T Tb U Ub)
    (blocks: vec (Tb -> aflow E Ub) N) i:
  aflow_eq
    (aflow_interp h (tailmrecˣ P blocks i))
    (tailmrecˣ P
               (vec_map (fun block => aflow_interp h (R := _) ∘ block) blocks)
               i).
Proof. reflexivity. Qed.

Definition tailmrecˣ_Params_stateT {N T Tb U Ub} {S: Typeˣ}
    (P: tailmrecˣ_Params N T Tb U Ub):
    tailmrecˣ_Params N (S * T) (S * Tb) (S * U) (S * Ub) := {|
  tailmrecˣ_scatter '((s, t): S * T) :=
    vec_map (fun tb => (s, tb)) (tailmrecˣ_scatter P t);
  tailmrecˣ_step '((s, ub): S * Ub) :=
    let '(branch, tb, u) := tailmrecˣ_step P ub in
    (branch, (s, tb), (s, u));
|}.

Lemma tailmrecˣ_interp_state {E F N T Tb U Ub} {S: Typeˣ}
    (h: E ~> stateT S (aflow F)) (P: tailmrecˣ_Params N T Tb U Ub)
    (blocks: vec (Tb -> aflow E Ub) N) i s:
  aflow_eq
    (aflow_interp_state h (tailmrecˣ P blocks i) s)
    (tailmrecˣ
      (tailmrecˣ_Params_stateT P)
      (vec_map (fun block '(s, i) => aflow_interp_state h (block i) s)
       blocks)
      (s, i)).
Proof.
  unfold tailmrecˣ, aflow_tailmrec; simp aflow_interp_state.
  constructor. reflexivity. intros []; reflexivity.
Qed.

Definition tailmrecˣ_Params_failT {N T Tb U Ub}
    (P: tailmrecˣ_Params N T Tb U Ub):
    tailmrecˣ_Params N T Tb (optionˣ U) (optionˣ Ub) := {|
  tailmrecˣ_scatter := tailmrecˣ_scatter P;
  tailmrecˣ_step '((err, ub): optionˣ Ub) :=
    let '(branch, tb, u) := tailmrecˣ_step P ub in
    (TB_add_fail_path branch err, tb, (err, u));
|}.

Lemma tailmrecˣ_interp_fail {E F N} {T Tb U Ub: Typeˣ}
    (h: E ~> failA (aflow F)) (P: tailmrecˣ_Params N T Tb U Ub)
    (blocks: vec (Tb -> aflow E Ub) N) i:
  aflow_eq
    (aflow_interp_fail h (tailmrecˣ P blocks i))
    (tailmrecˣ
      (tailmrecˣ_Params_failT P)
      (vec_map (fun block => aflow_interp_fail h (R := _) ∘ block) blocks)
      i).
Proof.
  unfold tailmrecˣ, aflow_tailmrec; simp aflow_interp_fail; cbn.
  constructor. reflexivity. intros []; cbn; now rewrite failT_merge_bot_r.
Qed.

(* ========================================================================= *)

Record tailmrec_sound_effects {N Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U} {GC_Ub: Ubˣ <#> Ub}
    (P: tailmrecᵒ_Params N Tb Ub U) (entry: Fin.t N)
    (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ) := {
  (* Scatter covers the initial block *)
  tailmrec_SE_scatter: forall (tb: Tb) (tˣ: Tˣ),
    galois_in tb tˣ ->
    galois_in tb (vec_nth (tailmrecˣ_scatter Pˣ tˣ) entry);
  (* Compatibility of the Ub/Ubˣ encoding with step *)
  tailmrec_SE_step: forall (ub: Ub) (ubˣ: Ubˣ),
    galois_in ub ubˣ ->
    let '(branchˣ, tbˣ, uˣ) := tailmrecˣ_step Pˣ ubˣ in
    match P ub with
    | CSR_Continue (inl (l, tb)) =>
        TB_may_branch_to branchˣ l = true /\ galois_in tb tbˣ
    | CSR_Continue (inr u) =>
        TB_may_return branchˣ = true /\ galois_in u uˣ
    | CSR_Fail u =>
        TB_may_return branchˣ = true /\ galois_in u uˣ
    end;
}.

Definition tailmrec_sound_effects_stateT {N Tb Ub U S} {Tˣ Tbˣ Ubˣ Uˣ Sˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U}
    {GC_Ub: Ubˣ <#> Ub} {GC_S: Sˣ <#> S}
    (P: tailmrecᵒ_Params N Tb Ub U) entry
    (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ):
  tailmrec_sound_effects P entry Pˣ ->
  tailmrec_sound_effects (tailmrecᵒ_Params_stateT (S := S) P) entry
                         (tailmrecˣ_Params_stateT (S := Sˣ) Pˣ).
Proof.
  intros HSE; constructor.
  - intros [] [] []; cbn. rewrite vec_nth_map. split; auto.
    eapply tailmrec_SE_scatter; eauto.
  - intros [s ub] [sˣ ubˣ] [Hs Hub]; cbn.
    pose proof (HH := tailmrec_SE_step _ _ _ HSE _ _ Hub).
    destruct (tailmrecˣ_step Pˣ ubˣ) as [[branchˣ tbˣ] uˣ]; cbn.
    destruct (P ub) as [[[? ?]|?]|?]; intuition auto.
Qed.

Definition tailmrec_sound_effects_failT {N Tb Ub U} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}
    {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U}
    {GC_Ub: Ubˣ <#> Ub}
    (P: tailmrecᵒ_Params N Tb Ub U) entry
    (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ):
  tailmrec_sound_effects P entry Pˣ ->
  tailmrec_sound_effects (tailmrecᵒ_Params_failT P) entry
                         (tailmrecˣ_Params_failT Pˣ).
Proof.
  intros HSE; constructor.
  - cbn. eapply tailmrec_SE_scatter; eauto.
  - intros ub [errˣ ubˣ] Hin; cbn in *; auto.
    destruct (tailmrecˣ_step Pˣ ubˣ) as [[branchˣ tbˣ] uˣ] eqn:Hstepˣ; cbn.
    destruct ub; cbn.
    * pose proof (HH := tailmrec_SE_step _ _ _ HSE _ _ Hin).
      rewrite Hstepˣ in HH; cbn in HH.
      destruct (P u) as [[[? ?]|?]|?]; intuition auto using TB_add_fail_path_1.
    * subst. split; [apply TB_add_fail_path_2|]; auto.
Qed.

Section soundness_theorem.
Context {N: nat} {E Eˣ: Type -> Type} {Tb Ub U: Type} {Tˣ Tbˣ Ubˣ Uˣ: Typeˣ}.
Context {GC_T: Tˣ <#> Tb} {GC_Tb: Tbˣ <#> Tb} {GC_U: Uˣ <#> U}
        {GC_Ub: Ubˣ <#> Ub}.

(* Like sound, but with abstract blocks already unfolded. *)
Definition blocks_sound
    (blocks: vec (Tb -> itree E Ub) N)
    (ublocksˣ: vec (Tbˣ -> itree Eˣ Ubˣ) N) :=
  vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
            galois_in tb tbˣ ->
            forall ub ubˣ, ub ∈ b tb -> ubˣ ∈ bˣ tbˣ -> galois_in ub ubˣ)
          blocks ublocksˣ.

(* Step 1: The parallel execution of abstract blocks captures the execution of
   any concrete block. *)
Lemma parallel_blocks_sound {tb: Tb} {tbsˣ: vec Tbˣ N} {l: Fin.t N}
  {blocks ublocksˣ} (Hsound_blocks: blocks_sound blocks ublocksˣ):
  forall ub ubsˣ,
    galois_in tb (vec_nth tbsˣ l) ->
    ub ∈ vec_nth blocks l tb ->
    ubsˣ ∈ vec_sequenceM (vec_map2 (fun b tb => b tb) ublocksˣ tbsˣ) ->
    galois_in ub (vec_nth ubsˣ l).
Proof.
  cbn; intros ub ubsˣ Hinit Hub Hubsˣ.
  unfold vec_map2 in Hubsˣ.
  (* Expose the particular block being run *)
  eassert (Hubˣ: vec_nth ubsˣ l ∈ vec_nth (_: vec (itree Eˣ _) N) l).
  { apply Leaf_sequenceM. apply Hubsˣ. }
  repeat rewrite ? vec_nth_map, ? vec_nth_combine in Hubˣ.
  (* Use blocks' individual soundness *)
  eapply Hsound_blocks; eauto.
Qed.

(* Step 2a: If the concrete block performs a branch, the global TailMRec step
   function will capture it and generate a suitable input to the next abstract
   iteration. *)
Lemma step_sound_loop
    {l next_l: Fin.t N} {tb next_tb: Tb} {tbsˣ: vec Tbˣ N} {ub: Ub}
    {ubsˣ: vec Ubˣ N}
    {P: tailmrecᵒ_Params N Tb Ub U} {entry}
    {Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ}
    {blocks ublocksˣ} (Hblocks: blocks_sound blocks ublocksˣ)
    (HSE: tailmrec_sound_effects P entry Pˣ):
  galois_in tb (vec_nth tbsˣ l) ->
  ub ∈ vec_nth blocks l tb ->
  P ub = CSR_Continue (inl (next_l, next_tb)) ->
  ubsˣ ∈ aflow_runN ublocksˣ tbsˣ ->
  galois_in next_tb (vec_nth (aflow_mrec_step (tailmrecˣ_step Pˣ) ubsˣ) next_l).
Proof.
  intros Hin_tb Hub Hlooping Hubsˣ.

  (* Relate outputs of blocks' executions *)
  assert (Hin_ub := parallel_blocks_sound Hblocks _ _ Hin_tb Hub Hubsˣ).
  pose proof (Hstep := tailmrec_SE_step _ _ _ HSE _ _ Hin_ub).
  destruct (tailmrecˣ_step Pˣ (vec_nth ubsˣ l)) as [[ubˣ_br tbˣ_next] uˣ]
    eqn:Hstepˣ.
  rewrite Hlooping in Hstep; cbn in Hstep.

  unfold aflow_mrec_step. rewrite vec_nth_initf; cbn.
  assert (Hublˣ := Leaf_sequenceM _ _ Hubsˣ l).
  rewrite vec_nth_map2 in Hublˣ; unfold id in Hublˣ.

  apply (vec_foldr_iter_after (galois_in next_tb) l).
  (* Prove that we capture the concrete output when folding at step #i *)
  - intros next_input; cbn. rewrite Hstepˣ; cbn.
    rewrite (proj1 Hstep). apply (galois_lble (join_r _ _)), Hstep.
  (* Prove that the ensuing joins preserve this property *)
  - intros j next_input Hnext_input.
    destruct (tailmrecˣ_step Pˣ (vec_nth ubsˣ j)) as [[]].
    case TB_may_branch_to.
    * apply (galois_lble (join_l _ _) Hnext_input).
    * auto.
Qed.

(* Step 2b: If the concrete block returns, the reduce function will account for
   it and join the return state into the combinator's final output. *)
Lemma step_sound_ret
    {l: Fin.t N} {tb: Tb} {tbsˣ: vec Tbˣ N} {ub: Ub} {u: U} {ubsˣ: vec Ubˣ N}
    {P: tailmrecᵒ_Params N Tb Ub U} {entry}
    {Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ}
    {blocks ublocksˣ} (Hblocks: blocks_sound blocks ublocksˣ)
    (HSE: tailmrec_sound_effects P entry Pˣ):
  galois_in tb (vec_nth tbsˣ l) ->
  ub ∈ vec_nth blocks l tb ->
  (P ub = CSR_Fail u \/ P ub = CSR_Continue (inr u)) ->
  ubsˣ ∈ aflow_runN ublocksˣ tbsˣ ->
  galois_in u (aflow_mrec_reduce (tailmrecˣ_step Pˣ) ubsˣ).
Proof.
  hnf; intros * Hin_tb Hub Hexiting Hubsˣ.
  (* Relate outputs of blocks' executions *)
  assert (Hin_ub := parallel_blocks_sound Hblocks _ _ Hin_tb Hub Hubsˣ).
  pose proof (Hstep := tailmrec_SE_step _ _ _ HSE _ _ Hin_ub).
  destruct (tailmrecˣ_step Pˣ (vec_nth ubsˣ l)) as [[ubˣ_br tbˣ_next] uˣ]
    eqn:Hstepˣ.
  (* Deduce that the abstract block accounted for returning *)
  assert (Hreturning: TB_may_return ubˣ_br = true /\ galois_in u uˣ).
  { destruct (P ub) as [[[]|]|]; inversion Hexiting; inversion H;
      subst; apply Hstep. }
  (* Now show that aflow_mrec_reduce will collect it. *)
  eapply galois_lble; [|apply Hreturning].
  apply (aflow_mrec_reduce_sound _ ubsˣ l _ _ _ Hstepˣ), Hreturning.
Qed.

Lemma sound_tailmrec
    (* Concrete data *)
    (P: tailmrecᵒ_Params N Tb Ub U)
    (blocks: vec (Tb -> itree E Ub) N) (entry: Fin.t N) (init: Tb)
    (* Abstract data *)
    (Pˣ: tailmrecˣ_Params N Tˣ Tbˣ Uˣ Ubˣ)
    (blocksˣ: vec (Tbˣ -> aflow Eˣ Ubˣ) N) (initˣ: Tˣ)
    (* Theorem statement *)
    (Hinit: galois_in init initˣ)
    (Hsound_blocks:
      vec_eq (fun b bˣ => forall (tb: Tb) (tbˣ: Tbˣ),
                galois_in tb tbˣ -> sound (b tb) (bˣ tbˣ))
             blocks blocksˣ)
    (HSE: tailmrec_sound_effects P entry Pˣ)
    p pˣ
    (Hp: p ≈ tailmrecᵒ P blocks entry init)
    (Hpˣ: aflow_eq pˣ (tailmrecˣ Pˣ blocksˣ initˣ)):
  sound p pˣ.
Proof.
  intros u uˣ Hu Huˣ.
  rewrite Hp in Hu; clear p Hp; unfold tailmrecᵒ in Hu.
  rewrite Hpˣ in Huˣ; clear pˣ Hpˣ; unfold tailmrecˣ in Huˣ.

  (* Expose the reduction and last iteration on the abstract side *)
  apply unfold_aflow_tailmrec_pfp in Huˣ.
  destruct Huˣ as (pfp_in & pfp_out & Hpfp & -> & Hmap); cbn in pfp_in, pfp_out.
  (* Name the set of unfolded blocks and prove soundness *)
  match type of Hpfp with
  | context[aflow_runN ?COMP] => set (ublocksˣ := COMP)
  end.
  assert (Hblocks: blocks_sound blocks ublocksˣ).
  { intros i tb tbˣ Hgalois ub ubˣ Hub Hubˣ.
    apply (Hsound_blocks i _ _ Hgalois ub ubˣ Hub).
    unfold ublocksˣ in Hubˣ. now rewrite ! vec_nth_map in Hubˣ. }

  (* We prove that all states that come in/out of the concrete loop are covered
     by the input/output of the post-fixpoint iteration, by induction on the
     number of concrete iterations. *)
  set (Inv := fun '((l, tb): Fin.t N * Tb) => galois_in tb (vec_nth pfp_in l)).
  set (Q := fun ub =>
    galois_in ub (aflow_mrec_reduce (tailmrecˣ_step Pˣ) pfp_out)).
  match type of Hu with
  | context[ITree.iter ?BODY ?INIT] => apply (Leaf_iter_inv BODY INIT Inv Q)
  end; auto; clear u Hu.

  (* Prove that abstract blocks + transitions capture concrete semantics *)
  - intros [l tb] next Hentry Hnext.
    inversion_Leaf_bind Hnext ub Hub.
    inversion_Leaf_ret Hnext.

    inversion_Leaf_bind Hpfp tmp_out Hpfp_out; cbn in tmp_out.
    Opaque lble join widen.
    inversion_Leaf_ret Hpfp.
    Transparent lble join widen.
    case (pfp_in ⊔ aflow_mrec_step _ tmp_out ⊆? pfp_in) eqn:Hcheck;
    inversion Hpfp; subst tmp_out; clear Hpfp.

    destruct (P ub) as [next_spec|u] eqn:H_P_ub; cbn.
    * destruct next_spec as [[next_l next_tb]|u]; cbn.
      + (* Scuffed *)
        pose proof (HH :=
          step_sound_loop Hblocks HSE Hentry Hub H_P_ub Hpfp_out).
        rewrite vec_lble_extensionality in Hcheck.
        specialize (Hcheck next_l); cbn in Hcheck.
        rewrite vec_nth_map2 in Hcheck.
        eapply galois_lble; [|apply HH].
        eapply lle_trans. apply join_r. apply Hcheck.
      + subst Q; cbn. eapply step_sound_ret; eauto.
    * subst Q; cbn. eapply step_sound_ret; eauto.

  (* Prove that the initial state if properly covered by scatter *)
  - subst Inv; cbn.
    rewrite vec_lle_extensionality in Hmap.
    eapply galois_lble; [apply Hmap|].
    apply (tailmrec_SE_scatter _ _ _ HSE _ _ Hinit).
Qed.
End soundness_theorem.

(* ========================================================================= *)

(* Specific parameters for a standard CFG. *)
Definition tailmrecᵒ_Params_init_cfg N Tb U:
    tailmrecᵒ_Params N Tb (*Ub:=*)(Fin.t N * Tb + U) U :=
  CSR_Continue.

Definition tailmrecˣ_Params_init_cfg N T U (entry: Fin.t N):
    tailmrecˣ_Params N T (*Tb:=*)T U (*Ub:=*)(TailBranch N * T * U) := {|
  tailmrecˣ_scatter t :=
    vec_initf N (fun l => if Fin.eqb l entry then t else bot);
  tailmrecˣ_step := id;
|}.

Local Lemma triplet_lle_iff {α β γ: Typeˣ} (a a': α) (b b': β) (c c': γ):
  (a, b, c) ⊆ (a', b', c') <-> a ⊆ a' /\ b ⊆ b' /\ c ⊆ c'.
Proof. cbn. now rewrite ! andb_true_iff. Qed.

Local Lemma triplet_meet {α β γ: Typeˣ} (a a': α) (b b': β) (c c': γ):
  (a, b, c) ⊓ (a', b', c') = (a ⊓ a', b ⊓ b', c ⊓ c').
Proof. reflexivity. Qed.

(* TODO: This is redundant with tailmrec_SE_step; merge them *)
#[refine]
Instance GaloisConnection_TB_CSR {N Tb U} {Tˣ Uˣ: Typeˣ}
    {GC_Tb: Tˣ <#> Tb} {GC_U: Uˣ <#> U}:
    TailBranch N * Tˣ * Uˣ <#> (Fin.t N * Tb + U) := {
  galois_in cr '(branchˣ, tbˣ, uˣ) :=
    match cr with
    | inl (l, tb) =>
        TB_may_branch_to branchˣ l = true /\ galois_in tb tbˣ
    | inr u =>
        TB_may_return branchˣ = true /\ galois_in u uˣ
    end;
}.
Proof.
  - intros [[branch₁ˣ tb₁ˣ] u₁ˣ] [[branch₂ˣ tb₂ˣ] u₂ˣ] cr Hsubset.
    rewrite triplet_lle_iff in Hsubset; destruct Hsubset as [Hbr [Htb Hu]].
    destruct cr as [[l tb]|u]; intros [Hbranch Hin];
    eauto using TB_may_branch_to_lble, TB_may_return_lble, @galois_lble.
  - intros [[branch₁ˣ tb₁ˣ] u₁ˣ] [[branch₂ˣ tb₂ˣ] u₂ˣ] cr.
    rewrite triplet_meet.
    destruct cr as [[l tb]|u]; intros [] [];
    auto using TB_may_branch_to_meet, TB_may_return_meet, @galois_meet.
  - intros [[l tb]|u]; cbn; auto using @galois_top.
Defined.

Lemma Fin_eqb_refl {N} {i: Fin.t N}: Fin.eqb i i = true.
Proof. now rewrite Fin.eqb_eq. Qed.

Lemma tailmrec_sound_effects_init_cfg {N Tb U} {Tˣ Uˣ: Typeˣ}
  (entry: Fin.t N) {GC_Tb: Tˣ <#> Tb} {GC_U: Uˣ <#> U}:
  tailmrec_sound_effects (tailmrecᵒ_Params_init_cfg N Tb U) entry
                         (tailmrecˣ_Params_init_cfg N Tˣ Uˣ entry).
Proof.
  constructor.
  - intros; cbn. now rewrite vec_nth_initf, Fin_eqb_refl.
  - intros ub [[branchˣ tbˣ] uˣ]; cbn. auto.
Qed.
