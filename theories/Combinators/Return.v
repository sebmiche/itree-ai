(* ** Pure computation

This base case of a control flow "combinator" can be used for pure computations
when building up programs or more complex combinators. Its role is to provide a
notation for pure computations that is syntactically stable by interpretation.

This is usually not needed to define languages (a normal monadic ret is enough
for that) but it is required when including pure computations in complex flow
combinators (which *do* need to be syntactically stable by interpretation).

Notice how this combinator is the only one to explicitly re-wrap a source
T-value into an interpreter U-value, i.e. the opposite of the usual dist
function. *)

From Equations Require Import Equations.
From ITree Require Import
  ITree ITreeFacts Eq.Eqit Props.Leaf
  Interp.InterpFacts Events.State Events.StateFacts Events.FailFacts.
From ITreeAI.Lattices Require Import
  Lattice LatticeOption.
From ITreeAI Require Import
  AflowMonad AbstractMonad Util Events FailA.

Import Basics.Monads.
Import LeafNotations.
Local Open Scope monad_scope.

Definition returnᵒ {E T U} (wrap: T -> U) (t: T): itree E U :=
  ret (wrap t).

Definition returnᵒ_wrap_stateT {T U S}:
    (T -> U) -> (S * T -> S * U) :=
  fun wrap '(s, t) => (s, wrap t).

Definition returnᵒ_wrap_failT {T U}:
    (T -> U) -> (T -> option U) :=
  fun wrap t => Some (wrap t).

Lemma returnᵒ_interp {E F T U} (h: E ~> itree F) wrap t:
  interp h (@returnᵒ E T U wrap t)
  ≈ returnᵒ wrap t.
Proof. cbn. now rewrite interp_ret. Qed.

Lemma returnᵒ_interp_state {E F T U S} (h: E ~> stateT S (itree F)) wrap t s:
  interp_state h (@returnᵒ E T U wrap t) s
  ≈ returnᵒ (returnᵒ_wrap_stateT wrap) (s, t).
Proof. cbn. now rewrite interp_state_ret. Qed.

Lemma returnᵒ_interp_fail {E F T U} (h: E ~> failT (itree F)) wrap t:
  interp_fail h (@returnᵒ E T U wrap t)
  ≈ returnᵒ (returnᵒ_wrap_failT wrap) t.
Proof. cbn. now rewrite interp_fail_ret. Qed.

(* ========================================================================= *)

(* Only the wrapˣ function. *)
Definition returnˣ_Params (T U: Typeˣ) := T -> U.

Definition returnˣ {E T U} (P: returnˣ_Params T U) (t: T) :=
  aflow_Ret (E := E) (P t).

Definition returnˣ_Params_init T: returnˣ_Params T (* U := *) T := id.

Lemma returnˣ_interp {E F T U} (h: E ~> aflow F) (P: returnˣ_Params T U) t:
  aflow_eq (aflow_interp h (returnˣ P t))
           (returnˣ P t).
Proof. now simp aflow_interp. Qed.

Definition returnˣ_Params_stateT {T U S: Typeˣ} (P: returnˣ_Params T U):
    returnˣ_Params (S * T) (S * U) :=
  fun '(s, t) => (s, P t).

Lemma returnˣ_interp_state {E F T U} {S: Typeˣ} (h: E ~> stateT S (aflow F))
    P t s:
  aflow_eq (aflow_interp_state h (@returnˣ E T U P t) s)
           (returnˣ (returnˣ_Params_stateT P) (s, t)).
Proof. now simp aflow_interp_state. Qed.

Definition returnˣ_Params_failT {T U} (P: returnˣ_Params T U):
    returnˣ_Params T (optionˣ U) :=
  optionˣ_some ∘ P.

Lemma returnˣ_interp_fail {E F T U} (h: E ~> failA (aflow F)) P t:
  aflow_eq (aflow_interp_fail h (@returnˣ E T U P t))
           (returnˣ (returnˣ_Params_failT P) t).
Proof. now simp aflow_interp_fail. Qed.

(* ========================================================================= *)

Definition return_sound_effects {T U: Type} {Tˣ Uˣ: Typeˣ}
    `{Tˣ <#> T} `{Uˣ <#> U} (wrap: T -> U) (wrapˣ: Tˣ -> Uˣ) :=
  forall t tˣ, galois_in t tˣ -> galois_in (wrap t) (wrapˣ tˣ).

Lemma return_sound_effects_stateT {T U} {Tˣ Uˣ: Typeˣ} `{Tˣ <#> T} `{Uˣ <#> U}
    (wrap: T -> U) (wrapˣ: Tˣ -> Uˣ):
  return_sound_effects wrap wrapˣ ->
  return_sound_effects (returnᵒ_wrap_stateT wrap)
                       (returnˣ_Params_stateT wrapˣ).
Proof. intros HSE [t u] [tˣ uˣ] [Ht Hu]; cbn; auto. Qed.

Lemma return_sound_effects_failT {T U} {Tˣ Uˣ: Typeˣ} `{Tˣ <#> T} `{Uˣ <#> U}
    (wrap: T -> U) (wrapˣ: Tˣ -> Uˣ):
  return_sound_effects wrap wrapˣ ->
  return_sound_effects (returnᵒ_wrap_failT wrap)
                       (returnˣ_Params_failT wrapˣ).
Proof. auto. Qed.

Lemma sound_return
    {E} {T U: Type} {Eˣ} {Tˣ Uˣ: Typeˣ} {GC_T: Tˣ <#> T} {GC_U: Uˣ <#> U}
    (* Concrete data *)
    (wrap: T -> U) (t: T)
    (* Abstract data *)
    (Pˣ: returnˣ_Params Tˣ Uˣ) (tˣ: Tˣ)
    (* Theorem statement *)
    (Ht: galois_in t tˣ)
    (HSE: return_sound_effects wrap Pˣ)
    p (Hp: p ≈ @returnᵒ E T U wrap t)
    pˣ (Hpˣ: aflow_eq pˣ (@returnˣ Eˣ Tˣ Uˣ Pˣ tˣ)):
  sound p pˣ.
Proof.
  intros u uˣ; rewrite Hp, Hpˣ; clear p Hp pˣ Hpˣ; intros Hu Huˣ.
  inversion_Leaf_ret Hu.
  unfold returnˣ in Huˣ; simp unfold in Huˣ; inversion_Leaf_ret Huˣ.
  now apply HSE.
Qed.
