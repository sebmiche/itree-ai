FROM ubuntu:22.04 AS layered
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /root

RUN apt -y update && apt -y upgrade && apt -y install \
	opam libgmp-dev

ENV OPAMROOTISOK=1
ENV OPAMCOLOR=always
ENV OPAMCONFIRMLEVEL=yes

# Copy just the deps file so we can change the source code later
COPY coq-itree-ai.opam .

RUN opam init --disable-sandboxing && \
    opam repo add coq-released https://coq.inria.fr/opam/released && \
    opam repo add coq-extra-dev https://coq.inria.fr/opam/extra-dev && \
    opam install --deps-only .

RUN echo "export OPAMROOTISOK=1 OPAMCOLOR=always" >> .bashrc && \
    echo "test -r /root/.opam/opam-init/init.sh && . /root/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true" >> .bashrc

# Copy source code now
COPY extract extract/
COPY theories theories/
COPY _CoqConfig _CoqProject dune-project LICENSE README.md .

# Squash the final image
FROM scratch
COPY --from=layered / /
WORKDIR /root
CMD ["/bin/bash"]
