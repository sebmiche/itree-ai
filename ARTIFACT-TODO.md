* Make an icfp24 branch at some point so we can remove identifying info
* The README file
  - Anonymity: do not add link to repo?
* Upload to Zenodo
* Stuff on HotCRP?
  - Submit along with revised version of paper if possible
  - Specify if the included paper has already been revised in the comments
* Make the tarball with the source files
* Insert Zenodo link in README for future repo visitors

Feasable route:

1. Push Dockerfile and extended README, doc, etc. everything with all identifying information to the repo.
2. Get a DOI from the Zenodo submissions page and add the link in the README.
3. Create the `icfp24` branch and remove identifying info there. Add artifact-specific instructions to the README if there are any.
4. Rebuild the Docker image from this `icfp24` branch and make the tarball also from the `icfp24` branch.
5. Push the Docker image to Zenodo.

DOI : 10.5281/zenodo.11470739
